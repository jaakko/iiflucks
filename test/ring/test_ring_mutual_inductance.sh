#!/bin/sh

echo "Create volume-current density file for 2 cm diameter ring from 100 um wire."

ringwire 0.01 0 6.28318530718 0.0001 2514 1e-6 0 0 1 0 0 0 0 > ring_1cm_6_2831_0cm01_1em6.pdv

echo "File 'ring_1cm_6_2831_0cm01_1em6.pdv' created."

echo "Calculate mutual inductance between two 2 cm rings with 1 cm separation along axis."

indmtrx ring_1cm_6_2831_0cm01_1em6.pdv ring_1cm_6_2831_0cm01_1em6.pdv 0 0 0.01 1 0 0 0 6 1e-6 1e-6

echo "Calculate again to get right histogram min and max values."
indmtrx ring_1cm_6_2831_0cm01_1em6.pdv ring_1cm_6_2831_0cm01_1em6.pdv 0 0 0.01 1 0 0 0 6 1e-6 1e-6

echo "Grover1973: k^2 = ( (1-a/A)^2 + (d/A)^2 ) / ( (1+a/A)^2 + (d/A)^2 ), where a and A are radii and d separation in cm."
echo "k^2=0.2 gives f = 0.004941 from table and M = f * sqrt(A*a) [uH]."

echo "Plot histogram files hx.png, hy.png, hz.png, hdst.png, hvol1.png, hvol2.png, hprob.png, hJ1.png and hJ2.png"

xhist.p
yhist.p
zhist.p
dsthist.p
vol1hist.p
vol2hist.p
probhist.p
J1hist.p
J2hist.p

