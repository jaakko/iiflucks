#!/bin/sh

echo "Create volume-current density file for 2 cm diameter ring from 100 um wire."

ringwire 0.01 0 6.28318530718 0.0001 2514 1e-6 0 0 1 0 0 0 0 > ring_1cm_6_2831_0cm01_1em6.pdv

echo "File 'ring_1cm_6_2831_0cm01_1em6.pdv' created."

echo "Calculate field map on ring central z-axis."

mapfield.pl -1 ring_1cm_6_2831_0cm01_1em6.pdv -a 1,0,0,0 -t 0,0,0 -p 0,0,-0.02 -d 0,0,0.001 -s 41 > ring_field_map.txt

echo "Plot 'ring_field_map.txt' to file 'ring_field_map.png'"

Bzplot.p

