#!/bin/sh

echo "Create volume-current density file for 2 cm diameter ring from 100 um wire."

ringwire 0.01 0 6.28318530718 0.0001 2514 1e-6 0 0 1 0 0 0 0 > ring_1cm_6_2831_0cm01_1em6.pdv

echo "File 'ring_1cm_6_2831_0cm01_1em6.pdv' created."

echo "Calculate self inductance of 2 cm wire ring. Number of elements is important parameter."

indmtrx ring_1cm_6_2831_0cm01_1em6.pdv ring_1cm_6_2831_0cm01_1em6.pdv 0 0 0 1 0 0 0 6 1e-6 1e-6

echo "Dengler2013: Correction u0/4pi * 0.5 * 0.0628318530718 m = +3.14e-9 H with 2514 elements"
echo "Anderson1989: L = 0.01257 * R * ( ln(8*R/a) - 1.75 ) = 0.070741 uH, R radius and a wire radius in cm"

echo "Plot histogram files hx.png, hy.png, hz.png, hdst.png, hvol1.png, hvol2.png, hprob.png, hJ1.png and hJ2.png"

xhist.p
yhist.p
zhist.p
dsthist.p
vol1hist.p
vol2hist.p
probhist.p
J1hist.p
J2hist.p

