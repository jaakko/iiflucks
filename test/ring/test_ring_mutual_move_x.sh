#!/bin/sh

echo "Create volume-current density file for 2 cm diameter ring from 100 um wire."

ringwire 0.01 0 6.28318530718 0.0001 2514 1e-6 0 0 1 0 0 0 0 > ring_1cm_6_2831_0cm01_1em6.pdv

echo "File 'ring_1cm_6_2831_0cm01_1em6.pdv' created."

echo "Calculate mutual inductance between two 2 cm rings with 1 cm separation along axis."
echo "Move first coil along x-axis in 20 steps by 10 cm."
echo "Write to file 'ring_1cm_move_x.txt'"

mapindmtrx.pl -1 ring_1cm_6_2831_0cm01_1em6.pdv -2 ring_1cm_6_2831_0cm01_1em6.pdv -p 0,0,0.01 -t 0.005,0,0 -a 1,0,0 -r 0,0 -s 20 -N 6 -c 1e-6,1e-6 > ring_1cm_move_x.txt

echo "Plot to file 'ring_1cm_move_x.png'"

M12xplot.p

