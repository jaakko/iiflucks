#!/bin/sh

echo "Create volume-current density file for 2 cm diameter ring from 100 um wire."

ringwire 0.01 0 6.28318530718 0.0001 2514 1e-6 0 0 1 0 0 0 0 > ring_1cm_6_2831_0cm01_1em6.pdv

echo "File 'ring_1cm_6_2831_0cm01_1em6.pdv' created."

echo "Calculate field at 1 cm on z-axis from ring center."

probeB ring_1cm_6_2831_0cm01_1em6.pdv 1 0 0 0 0 0 0 0 0 0.01

echo "Bz = u0 * I / 2 * a^2 / ( z^2 + a^2 )^3/2 = 2.221e-11 T with a = z = 1 cm"

