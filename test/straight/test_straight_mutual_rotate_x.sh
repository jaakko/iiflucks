#!/bin/sh
GEOMFILE='straight_10cm_0mm1_1em6.pdv'
WRITEFILE='straight_10cm_1cm_rota_x.txt'
PLOTFILE='straight_10cm_1cm_rota_x.png'

echo "Create volume-current density file for 10 cm long 100 um diameter wire."

straightwire 0.1 0.0001 4000 1e-6 1 0 0 0 0 0 0 > $GEOMFILE

echo "File '${GEOMFILE}' created."

echo "Calculate mutual inductance between two 10 cm wires with 1 cm separation."

echo "Rotate first wire around x-axis in 41 steps by 360 degree."
echo "Write to file ${WRITEFILE}"

mapindmtrx.pl -1 straight_10cm_0mm1_1em6.pdv -2 straight_10cm_0mm1_1em6.pdv -p 0.01,0,0.0 -t 0,0,0 -a 1,0,0 -r 0,0.1570796327 -s 41 -N 6 -c 1e-6,1e-6 > $WRITEFILE 

echo "Plot to file '${PLOTFILE}'"

M12rotaxplot_straight.p 


