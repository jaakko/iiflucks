#!/bin/sh

echo "Create volume-current density file for 10 cm long 100 um diameter wire."

straightwire 0.1 0.0001 4000 1e-6 1 0 0 0 0 0 0 > straight_10cm_0mm1_1em6.pdv

echo "File 'straight_10cm_0mm1_1em6.pdv' created."

echo "Calculate mutual inductance between two 10 wires with 1 cm separation."

indmtrx straight_10cm_0mm1_1em6.pdv straight_10cm_0mm1_1em6.pdv 0.01 0 0 1 0 0 0 6 1e-6 1e-6

echo "Calculate again to get right histogram min and max values."

indmtrx straight_10cm_0mm1_1em6.pdv straight_10cm_0mm1_1em6.pdv 0.01 0 0 1 0 0 0 6 1e-6 1e-6

echo "Grover1973: M = 0.002 l ( ln( l/d + sqrt(1 + (l/d)^2 ) ) - sqrt(1 + (d/l)^2) + d/l ), where l is length and d separation in cm."
echo "M = 0.042 uH with l = 10 cm and d = 1 cm."

echo "Plot histogram files hx.png, hy.png, hz.png, hdst.png, hvol1.png, hvol2.png, hprob.png, hJ1.png and hJ2.png"

xhist.p
yhist.p
zhist.p
dsthist.p
vol1hist.p
vol2hist.p
probhist.p
J1hist.p
J2hist.p

