#!/bin/sh

echo "Create volume-current density file for 10 cm long 100 um diameter wire."

straightwire 0.1 0.0001 4000 1e-6 1 0 0 0 0 0 0 > straight_10cm_0mm1_1em6.pdv

echo "File 'straight_10cm_0mm1_1em6.pdv' created."

echo "Calculate self inductance of 10 cm wire."

indmtrx straight_10cm_0mm1_1em6.pdv straight_10cm_0mm1_1em6.pdv 0 0 0 1 0 0 0 6 1e-6 1e-6

echo "Calculate again to get right histogram min and max values."

indmtrx straight_10cm_0mm1_1em6.pdv straight_10cm_0mm1_1em6.pdv 0 0 0 1 0 0 0 6 1e-6 1e-6

echo "Dengler2013: Correction u0/4pi * 0.5 * 0.1 m = +5 nH."
echo "Grover1973: L = 0.002 l ( ln( 2 l / r ) - 3/4 ), where l is length and r wire radius in cm."
echo "L = 0.15088 uH with l = 10 cm and r = 0.005 cm."

echo "Plot histogram files hx.png, hy.png, hz.png, hdst.png, hvol1.png, hvol2.png, hprob.png, hJ1.png and hJ2.png"

xhist.p
yhist.p
zhist.p
dsthist.p
vol1hist.p
vol2hist.p
probhist.p
J1hist.p
J2hist.p

