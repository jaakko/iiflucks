#!/bin/sh

echo "Create volume-current density file for 3 m high, 1.27591 m wide, 1.8688 m separation 'Helmholtz' coil with single layer of 2 mm wire. Corner radius is 0.2159 m and coil length 3.8 cm with 18 turns. This gives about 3 uT in center."

helmholtzwire 3 1.27591 1.8688 0.2159 0.038 0.002 18 410 0.34639 > helmholtz_3m_1m27591_1m8688_0m2159_0m038_2mm_18.pdv

echo "Create volume-current density file for 2 x 3.2 cm diameter almost planar figure '8' gradiometer coil from 100 um wire with 4 cm separation between centers."

gradient8wire 0.016 0.04 0.001 0.0001 2 8315 1e-6 > gradient1cm6_4cm_0cm1_0cm01_N2_I1em6.pdv 

echo "Calculate mutual inductance between Helmholtz coil and gradient '8' in center of it."
echo "Rotate ring around x-axis in 20 steps by 360 degree."
echo "Write to file 'Hgradient1cm6_rota_x.txt'"

mapindmtrx.pl -1 gradient1cm6_4cm_0cm1_0cm01_N2_I1em6.pdv -2 helmholtz_3m_1m27591_1m8688_0m2159_0m038_2mm_18.pdv -p 0,0,0 -t 0,0,0 -a 1,0,0 -r 0,0.314159265359 -s 20 -N 6 -c 1e-6,0.34639 > Hgradient1cm6_rota_x.txt

echo "Plot to file 'Hgradient1cm6_rota_x.png'"

M12rotaxplot_Hgradient.p

