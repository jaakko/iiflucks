#!/bin/sh

echo "Create volume-current density file for 3 m high, 1.27591 m wide, 1.8688 m separation 'Helmholtz' coil with single layer of 2 mm wire. Corner radius is 0.2159 m and coil length 3.8 cm with 18 turns. This gives about 3 uT in center."

helmholtzwire 3 1.27591 1.8688 0.2159 0.038 0.002 18 410 0.34639 > helmholtz_3m_1m27591_1m8688_0m2159_0m038_2mm_18.pdv

echo "Create volume-current density file for 3.2 cm diameter ring from 100 um wire."

ringwire 0.016 6.28318530718 0.0001 4021 1e-6 > ring_1cm6_6_2831_0cm01_1em6.pdv 

echo "Calculate mutual inductance between Helmholtz coil and ring in center of it."
echo "Rotate ring around z-axis in 20 steps by 360 degree."
echo "Write to file 'Hring1cm6_rota_z.txt'"

mapindmtrx.pl -1 ring_1cm6_6_2831_0cm01_1em6.pdv -2 helmholtz_3m_1m27591_1m8688_0m2159_0m038_2mm_18.pdv -p 0,0,0 -t 0,0,0 -a 0,0,1 -r 0,0.314159265359 -s 20 -N 6 -c 1e-6,0.34639 > Hring1cm6_rota_z.txt

echo "Plot to file 'Hring1cm6_rota_z.png'"

M12rotazplot_Hring.p

