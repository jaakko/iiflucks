#!/bin/sh

echo "Create volume-current density file for 3 m high, 1.27591 m wide, 1.8688 m separation 'Helmholtz' coil with single layer of 2 mm wire. Corner radius is 0.2159 m and coil length 3.8 cm with 18 turns. This gives about 3 uT in center."

helmholtzwire 3 1.27591 1.8688 0.2159 0.038 0.002 18 410 0.34639 > helmholtz_3m_1m27591_1m8688_0m2159_0m038_2mm_18.pdv

echo "Calculate field map on Helmholtz central z-axis."

mapfield.pl -1 helmholtz_3m_1m27591_1m8688_0m2159_0m038_2mm_18.pdv -a 1,0,0,0 -t 0,0,0 -p 0,0,-2 -d 0,0,0.01 -s 401 > helmholtz_field_map_z.txt

echo "Plot 'helmholtz_field_map_z.txt' to file 'helmholtz_field_map_z.png'"

Bzplot_helmholtz_z.p


