#!/bin/sh

echo "Create volume-current density file for 3.2 cm diameter almost planar helix coil from 100 um wire."

helixwire 0.016 0.001 0.0001 1 4021 1e-6 0 0 1 0 0 0 0 > helix_1cm6_1mm_0mm1_N1_I1em6.pdv

echo "File 'helix_1cm6_1mm_0mm1_N1_I1em6.pdv' created."

echo "Calculate field map on helix central z-axis."

mapfield.pl -1 helix_1cm6_1mm_0mm1_N1_I1em6.pdv -a 1,0,0,0 -t 0,0,0 -p 0,0,-0.02 -d 0,0,0.001 -s 41 > helix_field_map.txt

echo "Plot 'helix_field_map.txt' to file 'helix_field_map.png'"

Bzplot_helix.p

