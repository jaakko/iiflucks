#!/bin/sh

echo "Create volume-current density file for 20 cm diameter ring from 1 mm wire."

ringwire 0.1 0 6.28318530718 0.001 2514 1e-3 0 0 1 0 0 0 0 > ring_10cm_6_2831_0cm1_1em3.pdv

echo "File 'ring_10cm_6_2831_0cm1_1em6.pdv' created."

echo "Create volume-current density file for 3.2 cm diameter almost planar helix coil from 100 um wire."

helixwire 0.016 0.001 0.0001 1 4021 1e-6 0 0 1 0 0 0 0 > helix1cm6_0cm1_0cm01_N1_I1em6.pdv 

echo "Calculate mutual inductance between 20 cm ring and helix in center of it."
echo "Rotate helix coil around z-axis in 20 steps by 360 degree."
echo "Write to file 'helix1cm6_rota_z.txt'"

mapindmtrx.pl -1 helix1cm6_0cm1_0cm01_N1_I1em6.pdv -2 ring_10cm_6_2831_0cm1_1em3.pdv -p 0,0,0 -t 0,0,0 -a 0,0,1 -r 0,0.314159265359 -s 20 -N 6 -c 1e-6,1e-3 > helix1cm6_rota_z.txt

echo "Plot to file 'helix1cm6_rota_z.png'"

M12rotazplot_helix.p

