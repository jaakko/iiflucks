#!/bin/sh

echo "Create volume-current density file for 2 x 3.2 cm diameter axial gradiometer from 100 um wire with 7 cm separation between centers."

axialgradiometer 0.016 0.07 0.001 0.0001 13562 1e-6 0 0 1 0 0 0 -0.035 > axialgradient_1cm6_7cm_1mm_0mm1_I1em6.pdv

echo "File 'axialgradient_1cm6_7cm_1mm_0mm1_I1em6.pdv' created."

echo "Calculate Bz field map of gradiometer on z-axis through point (0, 0, 0)."

mapfield.pl -1 axialgradient_1cm6_7cm_1mm_0mm1_I1em6.pdv -a 1,0,0,0 -t 0,0,0 -p 0,0,-0.08 -d 0,0,0.001 -s 161 > axialgradient_field_map_z.txt

echo "Plot 'axialgradient_field_map_z.txt' to file 'axialgradient_field_map_z.png'"

Bzplot_axialgradient_z.p

