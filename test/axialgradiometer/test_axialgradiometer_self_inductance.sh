#!/bin/sh

echo "Create volume-current density file for 2 x 3.2 cm diameter axial gradiometer from 100 um wire with 3.2 cm separation between centers."

axialgradiometer 0.016 0.032 0.001 0.0001 10522 1e-6 0 0 1 0 0 0 -0.016 > axialgradient_1cm6_3cm2_1mm_0mm1_I1em6.pdv

echo "File 'axialgradient_1cm6_3cm2_1mm_0mm1_I1em6.pdv' created."

echo "Calculate self inductance of axial gradiometer."

indmtrx axialgradient_1cm6_3cm2_1mm_0mm1_I1em6.pdv axialgradient_1cm6_3cm2_1mm_0mm1_I1em6.pdv 0 0 0 0 0 1 0 6 1e-6 1e-6

echo "Anderson1989: One ring Lr = 0.01257 * R * ( ln(8*R/a) - 1.75 ) = 0.1226 uH, R radius and a wire radius in cm."
echo "Grover1973: One wire Ls = 0.002 l ( ln( 2 l / r ) - 3/4 ), where l is length and r wire radius in cm."
echo "Ls = 0.04099 uH with l = 3.2 cm and r = 0.005 cm."
echo "Total L = 2 Lr + 2 Ls + Mrr + Mss + Mrs."
