#!/bin/sh

echo "Create volume-current density file for 20 cm diameter ring from 1 mm wire."

ringwire 0.1 0 6.28318530718 0.001 2514 1e-3 0 0 1 0 0 0 0 > ring_10cm_6_2831_0cm1_1em3.pdv

echo "File 'ring_10cm_6_2831_0cm1_1em6.pdv' created."

echo "Create volume-current density file for 2 x 3.2 cm diameter almost planar figure '8' gradiometer coil from 100 um wire with 4 cm separation between centers."

gradient8wire 0.016 0.04 0.001 0.0001 2 8315 1e-6 > gradient1cm6_4cm_0cm1_0cm01_N2_I1em6.pdv 

echo "Calculate mutual inductance between 20 cm ring and gradiometer in center of it."
echo "Rotate gradiometer coil around x-axis in 20 steps by 360 degree."
echo "Write to file 'gradient1cm6_4cm_rota_x.txt'"

mapindmtrx.pl -1 gradient1cm6_4cm_0cm1_0cm01_N2_I1em6.pdv -2 ring_10cm_6_2831_0cm1_1em3.pdv -p 0,0,0 -t 0,0,0 -a 1,0,0 -r 0,0.314159265359 -s 20 -N 8 -c 1e-6,1e-3 > gradient1cm6_4cm_rota_x.txt

echo "Plot to file 'gradient1cm6_4cm_rota_x.png'"

M12rotaxplot_gradient.p

