#!/bin/sh

echo "Create volume-current density file for 2 x 3.2 cm diameter almost planar figure '8' gradiometer from 100 um wire with 4 cm separation between centers."

gradient8wire 0.016 0.04 0.001 0.0001 2 8315 1e-6 > gradient_1cm6_4cm_1mm_0mm1_N2_I1em6.pdv

echo "File 'gradient_1cm6_4cm_1mm_0mm1_N2_I1em6.pdv' created."

echo "Calculate Bz field map of gradiometer on y-axis through point (0, 0, 0.01)."

mapfield.pl -1 gradient_1cm6_4cm_1mm_0mm1_N2_I1em6.pdv -a 1,0,0,0 -t 0,0,0 -p 0,-0.1,0.01 -d 0,0.001,0 -s 201 > gradient8_field_map_y.txt

echo "Plot 'gradient8_field_map_y.txt' to file 'gradient8_field_map_y.png'"

Bzplot_gradient8_y.p

