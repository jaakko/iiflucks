#!/bin/sh
#
# Field map for octupole magnet for trapping atomic hydrogen.
#

A=Octupole-Turku2021_A
B=Octupole-Turku2021_B
C=Octupole-Turku2021_C
D=Octupole-Turku2021_D
E=Octupole-Turku2021_E
F=Octupole-Turku2021_F
G=Octupole-Turku2021_G
H=Octupole-Turku2021_H

xt=0
yt=0
zt=0
ax=0
ay=0
az=1
theta=0
x0=0
y0=0
z0=0
dx=0.002
dy=0.002
dz=0
steps=50

p=_xy
echo "Calculate field map A"
echo " xt="$xt" yt="$yt" zt="$zt" ax="$ax" ay="$ay" az="$az" theta="$theta
echo " x0="$x0" y0="$y0" z0="$z0" dx="$dx" dy="$dy" dz="$dz" steps="$steps
{ 
  mapB $A.pdv $ax $ay $az $theta $xt $yt $zt $x0 $y0 $z0 $dx $dy $dz $steps > $A$p.txt 
} || { 
  echo "-- calculation of field map A failed" 
}

echo "Calculate field map B"
echo " xt="$xt" yt="$yt" zt="$zt" ax="$ax" ay="$ay" az="$az" theta="$theta
echo " x0="$x0" y0="$y0" z0="$z0" dx="$dx" dy="$dy" dz="$dz" steps="$steps
{ 
  mapB $B.pdv $ax $ay $az $theta $xt $yt $zt $x0 $y0 $z0 $dx $dy $dz $steps > $B$p.txt 
} || { 
  echo "-- calculation of field map B failed" 
}

echo "Calculate field map C"
echo " xt="$xt" yt="$yt" zt="$zt" ax="$ax" ay="$ay" az="$az" theta="$theta
echo " x0="$x0" y0="$y0" z0="$z0" dx="$dx" dy="$dy" dz="$dz" steps="$steps
{ 
  mapB $C.pdv $ax $ay $az $theta $xt $yt $zt $x0 $y0 $z0 $dx $dy $dz $steps > $C$p.txt 
} || { 
  echo "-- calculation of field map C failed" 
}

echo "Calculate field map D"
echo " xt="$xt" yt="$yt" zt="$zt" ax="$ax" ay="$ay" az="$az" theta="$theta
echo " x0="$x0" y0="$y0" z0="$z0" dx="$dx" dy="$dy" dz="$dz" steps="$steps
{ 
  mapB $D.pdv $ax $ay $az $theta $xt $yt $zt $x0 $y0 $z0 $dx $dy $dz $steps > $D$p.txt 
} || { 
  echo "-- calculation of field map D failed" 
}

echo "Calculate field map E"
echo " xt="$xt" yt="$yt" zt="$zt" ax="$ax" ay="$ay" az="$az" theta="$theta
echo " x0="$x0" y0="$y0" z0="$z0" dx="$dx" dy="$dy" dz="$dz" steps="$steps
{ 
  mapB $E.pdv $ax $ay $az $theta $xt $yt $zt $x0 $y0 $z0 $dx $dy $dz $steps > $E$p.txt 
} || { 
  echo "-- calculation of field map E failed" 
}

echo "Calculate field map F"
echo " xt="$xt" yt="$yt" zt="$zt" ax="$ax" ay="$ay" az="$az" theta="$theta
echo " x0="$x0" y0="$y0" z0="$z0" dx="$dx" dy="$dy" dz="$dz" steps="$steps
{ 
  mapB $F.pdv $ax $ay $az $theta $xt $yt $zt $x0 $y0 $z0 $dx $dy $dz $steps > $F$p.txt 
} || { 
  echo "-- calculation of field map F failed" 
}

echo "Calculate field map G"
echo " xt="$xt" yt="$yt" zt="$zt" ax="$ax" ay="$ay" az="$az" theta="$theta
echo " x0="$x0" y0="$y0" z0="$z0" dx="$dx" dy="$dy" dz="$dz" steps="$steps
{ 
  mapB $G.pdv $ax $ay $az $theta $xt $yt $zt $x0 $y0 $z0 $dx $dy $dz $steps > $G$p.txt 
} || { 
  echo "-- calculation of field map G failed" 
}

echo "Calculate field map H"
echo " xt="$xt" yt="$yt" zt="$zt" ax="$ax" ay="$ay" az="$az" theta="$theta
echo " x0="$x0" y0="$y0" z0="$z0" dx="$dx" dy="$dy" dz="$dz" steps="$steps
{ 
  mapB $H.pdv $ax $ay $az $theta $xt $yt $zt $x0 $y0 $z0 $dx $dy $dz $steps > $H$p.txt 
} || { 
  echo "-- calculation of field map H failed" 
}

