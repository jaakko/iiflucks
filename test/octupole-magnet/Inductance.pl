#!/usr/bin/perl -w
#
# Inductance and force matrix for octupole magnet for trapping atomic hydrogen.
#

my $verbose = 1;

my $Nrand = 8;
my $xt = 0;
my $yt = 0;
my $zt = 0;
my $ax = 0;
my $ay = 0;
my $az = 1;
my $theta = 0;
my $I1 = 150;
my $I2 = 150;

my %ind; # inductance matrix hash
my %inderr; # inductance errror matrix hash
my %E; # energy matrix hash
my %Eerr; # energy error matrix hash
my %Flux; # flux matrix hash
my %Fluxerr; # flux error matrix hash
my %Fx; # force matrix hash
my %Fy; # force matrix hash
my %Fz; # force matrix hash
my %Fxerr; # force error matrix hash
my %Fyerr; # force error matrix hash
my %Fzerr; # force error matrix hash

my $fname = "Octupole-Turku2021_";
my @pfix = ("A", "B", "C", "D", "E", "F", "G", "H");

my $cmd = "";
my $res = "";
my $M21 = "";
my $M21err = "";
my $E21 = "";
my $E21err = "";
my $Flux21 = "";
my $Flux21err = "";
my $F12x = "";
my $F12y = "";
my $F12z = "";
my $F12xerr = "";
my $F12yerr = "";
my $F12zerr = "";

foreach my $c ( @pfix )
{
  $I1 =  150 if( $c =~ /[ACEG]/ );
  $I1 = -150 if( $c =~ /[BDFH]/ );
  foreach my $r ( @pfix )
  {
    $I2 =  150 if( $r =~ /[ACEG]/ );
    $I2 = -150 if( $r =~ /[BDFH]/ );

    $cmd = "indmtrx $fname$c.pdv $fname$r.pdv $xt $yt $zt $ax $ay $az $theta $Nrand $I1 $I2";
  
    $res = `$cmd`;
    print( $res ) if( $verbose );

    $M12    = "none";
    $M12err = "none";
    if( $res =~ /-- M21 ([+-]?\d{1,4}\.\d{1,9}([+-]e\d\d)?) \+- ([+-]?\d{1,4}\.\d{1,9}([+-]e\d\d)?) H/ )
    {
      $M12    = $1;
      $M12err = $3;
    } 

    print( "M12 = $M12, M12err = $M12err\n" ) if( $verbose );

    $ind{ $c.$r } = $M12;
    $inderr{ $c.$r } = $M12err;

    $E12    = "none";
    $E12err = "none";
    if( $res =~ /-- E21 ([+-]?\d{1,4}\.\d{1,9}([+-]e\d\d)?) \+- ([+-]?\d{1,4}\.\d{1,9}([+-]e\d\d)?) J/ )
    {
      $E12    = $1;
      $E12err = $3;
    } 

    print( "E12 = $E12, E12err = $E12err\n" ) if( $verbose );

    $E{ $c.$r } = $E12;
    $Eerr{ $c.$r } = $E12err;

    $Flux12    = "none";
    $Flux12err = "none";
    if( $res =~ /-- Flux21 ([+-]?\d{1,4}\.\d{1,9}([+-]e\d\d)?) \+- ([+-]?\d{1,4}\.\d{1,9}([+-]e\d\d)?) Wb/ )
    {
      $Flux12    = $1;
      $Flux12err = $3;
    } 

    print( "Flux12 = $Flux12, Flux12err = $Flux12err\n" ) if( $verbose );

    $Flux{ $c.$r } = $Flux12;
    $Fluxerr{ $c.$r } = $Flux12err;

    $F12x = "none";
    $F12y = "none";
    $F12z = "none";
    if( $res =~ /-- F21 \(([+-]?\d{1,6}\.?\d{1,9}([+-]e\d\d)?) N, ([+-]?\d{1,6}\.?\d{1,9}([+-]e\d\d)?) N, ([+-]?\d{1,4}\.?\d{1,9}([+-]e\d\d)?) N\)/ )
    {
      $F12x = $1;
      $F12y = $3;
      $F12z = $5;
    } 

    print( "F12x = $F12x, F12y = $F12y, F12z = $F12z\n" ) if( $verbose );

    $Fx{ $c.$r } = $F12x;
    $Fy{ $c.$r } = $F12y;
    $Fz{ $c.$r } = $F12z;

    $F12xerr = "none";
    $F12yerr = "none";
    $F12zerr = "none";
    if( $res =~ /-- \+-\(([+-]?\d{1,6}\.?\d{1,9}([+-]e\d\d)?) N, ([+-]?\d{1,6}\.?\d{1,9}([+-]e\d\d)?) N, ([+-]?\d{1,4}\.?\d{1,9}([+-]e\d\d)?) N\)/ )
    {
      $F12xerr = $1;
      $F12yerr = $3;
      $F12zerr = $5;
    } 

    print( "F12xerr = $F12xerr, F12yerr = $F12yerr, F12zerr = $F12zerr\n" ) if( $verbose );

    $Fxerr{ $c.$r } = $F12xerr;
    $Fyerr{ $c.$r } = $F12yerr;
    $Fzerr{ $c.$r } = $F12zerr;

  }
}

print("--- Inductance Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $ind{ $c.$r } ");
  }
  print "\n";
}

print("--- Inductance Maximum Error Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $inderr{ $c.$r } ");
  }
  print "\n";
}

print("--- Energy Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $E{ $c.$r } ");
  }
  print "\n";
}

print("--- Energy Maximum Error Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Eerr{ $c.$r } ");
  }
  print "\n";
}

print("--- Flux Linkage Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Flux{ $c.$r } ");
  }
  print "\n";
}

print("--- Flux Linkage Error Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Fluxerr{ $c.$r } ");
  }
  print "\n";
}

print("--- Fx Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Fx{ $c.$r } ");
  }
  print "\n";
}

print("--- Fy Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Fy{ $c.$r } ");
  }
  print "\n";
}

print("--- Fz Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Fz{ $c.$r } ");
  }
  print "\n";
}

print("--- Fx Error Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Fxerr{ $c.$r } ");
  }
  print "\n";
}

print("--- Fy Error Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Fyerr{ $c.$r } ");
  }
  print "\n";
}

print("--- Fz Error Matrix ----\n");
foreach my $c ( @pfix )
{
  foreach my $r ( @pfix )
  {
    print(" $Fzerr{ $c.$r } ");
  }
  print "\n";
}

