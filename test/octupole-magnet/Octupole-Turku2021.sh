#!/bin/sh
#
# Octupole magnet for trapping atomic hydrogen.
#

# magnet current[A]
I=150

# number of elements
M=67000

# rotation angle around z-axis
TH=0

echo "-- create first racetrack coil"
{ 
  racetrackcylinder 1.13e-2 5.9e-2 0.36 0 1.2e-2 19 1.8e-2 18 5e-4 $M $I 0 0 1 $TH 0 0 0 > Octupole-Turku2021_A.pdv
} || { 
  echo "-- creation of first racetrack coil failed" 
}

I=-150
TH=0.785398163
racetrackcylinder 1.13e-2 5.9e-2 0.36 0 1.2e-2 19 1.8e-2 18 5e-4 $M $I 0 0 1 $TH 0 0 0 > Octupole-Turku2021_B.pdv

I=150
TH=1.570796327
racetrackcylinder 1.13e-2 5.9e-2 0.36 0 1.2e-2 19 1.8e-2 18 5e-4 $M $I 0 0 1 $TH 0 0 0 > Octupole-Turku2021_C.pdv

I=-150
TH=2.35619449
racetrackcylinder 1.13e-2 5.9e-2 0.36 0 1.2e-2 19 1.8e-2 18 5e-4 $M $I 0 0 1 $TH 0 0 0 > Octupole-Turku2021_D.pdv

I=150
TH=3.141592654
racetrackcylinder 1.13e-2 5.9e-2 0.36 0 1.2e-2 19 1.8e-2 18 5e-4 $M $I 0 0 1 $TH 0 0 0 > Octupole-Turku2021_E.pdv

I=-150
TH=3.926990817
racetrackcylinder 1.13e-2 5.9e-2 0.36 0 1.2e-2 19 1.8e-2 18 5e-4 $M $I 0 0 1 $TH 0 0 0 > Octupole-Turku2021_F.pdv

I=150
TH=4.71238898
racetrackcylinder 1.13e-2 5.9e-2 0.36 0 1.2e-2 19 1.8e-2 18 5e-4 $M $I 0 0 1 $TH 0 0 0 > Octupole-Turku2021_G.pdv

I=-150
TH=5.497787144
racetrackcylinder 1.13e-2 5.9e-2 0.36 0 1.2e-2 19 1.8e-2 18 5e-4 $M $I 0 0 1 $TH 0 0 0 > Octupole-Turku2021_H.pdv


