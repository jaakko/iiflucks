#!/usr/bin/gnuplot
# Gnuplot script to plot already binned histogram.
# Execute: gnuplot Jrndhist.p
#

set title "Jrnd histogram"

set style fill solid 0.5
set format x "%7.1e"
set tics font "Times,10"

set term png
set output "hJrnd.png"

plot 'hJrnd.txt' using ( ($1+$2)/2 ):3 with boxes title 'count'

