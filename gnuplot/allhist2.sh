#!/bin/sh
#
# Plot all 2D histograms from text files if found.
#

echo "Plot xy histogram"
{ 
  xyhist.p 
} || { 
  echo "-- xy histogram plotting failed, check file hxy.txt and path to xyhist.p" 
}

echo "Plot xz histogram"
{ 
  xzhist.p 
} || { 
  echo "-- xz histogram plotting failed, check file hxz.txt and path to xzhist.p" 
}

echo "Plot yz histogram"
{ 
  yzhist.p 
} || { 
  echo "-- yz histogram plotting failed, check file hyz.txt and path to yzhist.p" 
}

