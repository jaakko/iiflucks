#!/bin/sh
#
# Plot all 1D histograms from text files if found.
#

if [ -r hx.txt ]
then
  echo "Plot x histogram"
  xhist.p
else
  echo "-- x histogram plotting failed, check file hx.txt and path to xhist.p" 
fi

if [ -r hy.txt ]
then
  echo "Plot y histogram"
  yhist.p
else
  echo "-- y histogram plotting failed, check file hy.txt and path to yhist.p" 
fi

if [ -r hz.txt ]
then
  echo "Plot z histogram"
  zhist.p
else
  echo "-- z histogram plotting failed, check file hz.txt and path to zhist.p" 
fi

if [ -r hdst.txt ]
then
  echo "Plot element distance histogram"
  dsthist.p
else
  echo "-- element distance histogram plotting failed, check file hdts.txt and path to dsthist.p" 
fi

if [ -r hvol1.txt ]
then
  echo "Plot vol1 histogram"
  vol1hist.p
else
  echo "-- element 1 volume histogram plotting failed, check file hvol1.txt and path to vol1hist.p" 
fi

if [ -r hvol2.txt ]
then
  echo "Plot vol2 histogram"
  vol2hist.p
else
  echo "-- element 2 volume histogram plotting failed, check file hvol2.txt and path to vol2hist.p" 
fi

if [ -r harea2.txt ]
then
  echo "Plot area2 histogram"
  area2hist.p
else
  echo "-- element 2 area histogram plotting failed, check file harea2.txt and path to area2hist.p" 
fi

if [ -r hprob1.txt ]
then
  echo "Plot probability1 histogram"
  prob1hist.p
else
  echo "-- probability1 histogram plotting failed, check file hprob1.txt and path to prob1hist.p"
fi

if [ -r hprob2.txt ]
then
  echo "Plot probability2 histogram"
  prob2hist.p
else
  echo "-- probability2 histogram plotting failed, check file hprob2.txt and path to prob2hist.p"
fi

if [ -r hJ1.txt ]
then
  echo "Plot J1 histogram"
  J1hist.p
else
  echo "-- J1 histogram plotting failed, check file hJ1.txt and path to J1hist.p"
fi

if [ -r hJ1x.txt ]
then
  echo "Plot J1x histogram"
  J1xhist.p
else
  echo "-- J1x histogram plotting failed, check file hJ1x.txt and path to J1xhist.p"
fi

if [ -r hJ1y.txt ]
then
  echo "Plot J1y histogram"
  J1yhist.p
else
  echo "-- J1y histogram plotting failed, check file hJ1y.txt and path to J1yhist.p"
fi

if [ -r hJ1z.txt ]
then
  echo "Plot J1z histogram"
  J1zhist.p
else
  echo "-- J1z histogram plotting failed, check file hJ1z.txt and path to J1zhist.p"
fi

if [ -r hJ2.txt ]
then
  echo "Plot J2 histogram"
  J2hist.p
else
  echo "-- J2 histogram plotting failed, check file hJ2.txt and path to J2hist.p"
fi

if [ -r hJ2x.txt ]
then
  echo "Plot J2x histogram"
  J2xhist.p
else
  echo "-- J2x histogram plotting failed, check file hJ2x.txt and path to J2xhist.p"
fi

if [ -r hJ2y.txt ]
then
  echo "Plot J2y histogram"
  J2yhist.p
else
  echo "-- J2y histogram plotting failed, check file hJ2y.txt and path to J2yhist.p"
fi

if [ -r hJ2z.txt ]
then
  echo "Plot J2z histogram"
  J2zhist.p
else
  echo "-- J2z histogram plotting failed, check file hJ2z.txt and path to J2zhist.p"
fi

if [ -r hJrnd.txt ]
then
  echo "Plot Jrnd histogram"
  Jrndhist.p
else
  echo "-- Jrnd histogram plotting failed, check file hJrnd.txt and path to Jrndhist.p" 
fi

if [ -r hdM12.txt ]
then
  echo "Plot dM12 histogram"
  dM12hist.p
else
  echo "-- dM12 histogram plotting failed, check file hdM12.txt and path to dM12hist.p"
fi

if [ -r hjm2.txt ]
then
  echo "Plot jm2 histogram"
  jm2hist.p
else
  echo "-- jm2 histogram plotting failed, check file hjm2.txt and path to jm2hist.p" 
fi

if [ -r hjm2x.txt ]
then
  echo "Plot jm2x histogram"
  jm2xhist.p
else
  echo "-- jm2x plotting failed, check hjm2x.txt and jm2xhist.p" 
fi

if [ -r hjm2y.txt ]
then
  echo "Plot jm2y histogram"
  jm2yhist.p
else
  echo "-- jm2y plotting failed, check hjm2y.txt and jm2yhist.p" 
fi

if [ -r hjm2z.txt ]
then
  echo "Plot jm2z histogram"
  jm2zhist.p
else
  echo "-- jm2z plotting failed, check hjm2z.txt and jm2zhist.p" 
fi

if [ -r hFx.txt ]
then
  echo "Plot Fx histogram"
  Fxhist.p
else
  echo "-- Fx histogram plotting failed, check file hFx.txt and path to Fxhist.p" 
fi

if [ -r hFy.txt ]
then
  echo "Plot Fy histogram"
  Fyhist.p
else
  echo "-- Fy histogram plotting failed, check file hFy.txt and path to Fyhist.p" 
fi

if [ -r hFz.txt ]
then
  echo "Plot Fz histogram"
  Fzhist.p
else
  echo "-- Fz histogram plotting failed, check file hFz.txt and path to Fzhist.p" 
fi

if [ -r hF.txt ]
then
  echo "Plot F histogram"
  Fhist.p
else
  echo "-- F histogram plotting failed, check file hF.txt and path to Fhist.p" 
fi

if [ -r hBx.txt ]
then
  echo "Plot Bx histogram"
  Bxhist.p
else
  echo "-- Bx histogram plotting failed, check file hBx.txt and path to Bxhist.p"
fi

if [ -r hBy.txt ]
then
  echo "Plot By histogram"
  Byhist.p
else
  echo "-- By histogram plotting failed, check file hBy.txt and path to Byhist.p"
fi

if [ -r hBz.txt ]
then
  echo "Plot Bz histogram"
  Bzhist.p
else
  echo "-- Bz histogram plotting failed, check file hBz.txt and path to Bzhist.p"
fi

if [ -r hB.txt ]
then
  echo "Plot B histogram"
  Bhist.p
else
  echo "-- B histogram plotting failed, check file hB.txt and path to Bhist.p"
fi

