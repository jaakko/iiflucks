#!/usr/bin/gnuplot
# Gnuplot script M12rotayplot_gradient.p to plot mutual inductance vs rotation 
# angle around y-axis 
# Execute:  gnuplot M12rotayplot_gradient.p
#
set title "Mutual inductance vs rotation angle around y-axis"
set xlabel "theta[rad]"
set ylabel "M[H]"

set format y "%.4e"

set term png
set output "gradient1cm6_4cm_rota_y.png"

plot 'gradient1cm6_4cm_rota_y.txt' using 4:9:10 with yerrorbars, '' using 4:9 with lines
 
