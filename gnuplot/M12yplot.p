#!/usr/bin/gnuplot
# Gnuplot script M12yplot.p to plot mutual inductance vs y-position 
# Execute:  gnuplot M12yplot.p
#
set title "Mutual inductance vs y-position"
set xlabel "y[m]"
set ylabel "L[H]"

set format y "%.4e"

set term png
set output "ring_1cm_move_y.png"

plot 'ring_1cm_move_y.txt' using 2:9 with lines 
