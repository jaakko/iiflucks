#!/usr/bin/gnuplot
# Gnuplot script Fluxrotaxplot_Hring.p to plot flux linkage vs rotation 
# angle around x-axis 
# Execute:  gnuplot Fluxrotaxplot_Hring.p
#
set title "Flux linkage vs rotation angle around x-axis"
set xlabel "theta[rad]"
set ylabel "Flux[Wb]"

set format y "%.4e"

set term png
set output "Hring1cm6_rota_x_F.png"

plot 'Hring1cm6_rota_x.txt' using 4:7:8 with yerrorbars, '' using 4:7 with lines
 
