#!/bin/sh
#
# Delete histogram plots and histogram ranges for new calculation. 
#

echo "Delete png-files"
/usr/bin/rm *.png

echo "Delete pdf-files"
/usr/bin/rm *.pdf

echo "Delete svg-files"
/usr/bin/rm *.svg

echo "Copy .indmtrx.yaml to .indmtrx.old"
/usr/bin/cp .indmtrx.yaml .indmtrx.old

echo "Delete .indmtrx.yaml"
/usr/bin/rm .indmtrx.yaml

