#!/usr/bin/gnuplot
# Gnuplot script M12rotaxplot_Hring.p to plot mutual inductance vs rotation 
# angle around x-axis 
# Execute:  gnuplot M12rotaxplot_Hring.p
#
set title "Mutual inductance vs rotation angle around x-axis"
set xlabel "theta[rad]"
set ylabel "M[H]"

set format y "%.4e"

set term png
set output "Hring1cm6_rota_x.png"

plot 'Hring1cm6_rota_x.txt' using 4:9:10 with yerrorbars, '' using 4:9 with lines
 
