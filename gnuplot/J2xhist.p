#!/usr/bin/gnuplot
# Gnuplot script to plot already binned histogram.
# Execute: gnuplot J2xhist.p
#

set title "J2x histogram"

set style fill solid 0.5
set format x "%7.1e"
set tics font "Times,10"

set term png
set output "hJ2x.png"

plot 'hJ2x.txt' using ( ($1+$2)/2 ):3 with boxes title 'count'

