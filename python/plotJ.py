#!/usr/bin/env python

import argparse
import re
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

p = argparse.ArgumentParser(
      prog='plotJ.py',
      description='Plot current density vectors of small differential volumes.',
      epilog='')

p.add_argument('length', type=float, help='length about size / J') 
p.add_argument('filename', help='position-current_density-dv file') 
p.add_argument('length2', nargs='?', type=float, help='second length about size / J') 
p.add_argument('filename2', nargs='?', help='optional second position-current_density-dv file')
p.add_argument('-s', '--show', help='show plot on screen, default is to only write picture files', action='store_true') 
p.add_argument('-e', '--equal', help='equal aspect ratio, boxed', action='store_true') 

args = p.parse_args()

fname = args.filename
fname2 = args.filename2

plt.style.use('_mpl-gallery')

# pdv-file: xc, yc, zc, jx, jy, jz, dv
pdv = pd.read_csv( fname , sep='\s+', header=None )

m = np.asarray( pdv )

x = m[ :, 0 ]
y = m[ :, 1 ]
z = m[ :, 2 ]
Jx = m[ :, 3 ]
Jy = m[ :, 4 ]
Jz = m[ :, 5 ]

if fname2:
    # pdv-file: xc, yc, zc, jx, jy, jz, dv
    pdv2 = pd.read_csv( fname2 , sep='\s+', header=None )

    m2 = np.asarray( pdv2 )

    x2 = m2[ :, 0 ]
    y2 = m2[ :, 1 ]
    z2 = m2[ :, 2 ]
    J2x = m2[ :, 3 ]
    J2y = m2[ :, 4 ]
    J2z = m2[ :, 5 ]

# change figure size as needed
fig = plt.figure( figsize=(4, 4), layout='constrained' )
ax = fig.add_subplot( projection='3d' )

ax.quiver(x, y, z, Jx, Jy, Jz, length=args.length, color='tab:blue')

if fname2:
    ax.quiver(x2, y2, z2, J2x, J2y, J2z, length=args.length2, color='tab:orange')

ax.set_xlabel('x[m]')
ax.set_ylabel('y[m]')
ax.set_zlabel('z[m]')
ax.set_title(r'$\vec{J}$ position')

if args.equal:
    ax.set_aspect('equal', 'box')

r = re.compile(r'[/.]')

s = r.split( fname )
if s:
    figname = s[ -2 ]

if fname2:
    s2 = r.split( fname2 )
    if s2:
        figname2 = s2[ -2 ]
        figname = figname + '_' + figname2

if s:
    fpng = figname + '_J.png'
    plt.savefig( fpng )
    fpdf = figname + '_J.pdf'
    plt.savefig( fpdf )
    fsvg = figname + '_J.svg'
    plt.savefig( fsvg )
else:
    print('File name has non-standard characters, no figures saved')

if args.show:
    plt.show()

