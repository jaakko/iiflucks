#!/usr/bin/env python

import argparse
import re
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

p = argparse.ArgumentParser(
      prog='plothist.py',
      description='Plot histograms.',
      epilog='')

p.add_argument('filename', nargs='+', help='histogram file name')
p.add_argument('-y', '--ylabel', type=ascii, default='Samples', help='y-axis label', action='store')
p.add_argument('-s', '--show', help='show plot on screen, default is to only write picture files', action='store_true') 
p.add_argument('-l', '--log', help='logarithmic bins', action='store_true') 

args = p.parse_args()

fnames = args.filename

ylabel = args.ylabel
ylabel = ylabel.replace( "'", "" )

r = re.compile(r'[/.]')

plt.style.use('_mpl-gallery')

# GSL 1D histogram data with x bin edges and number of samples in each bin
# xl xu samples
for f in fnames:
  histo = pd.read_csv(f, sep='\s+', header=None)
  h = np.asarray( histo )

  w = abs( h[ 0, 1 ] - h[ 0, 0 ] )
  xmin = h[ :, 0 ].min()
  xmax = h[ :, 1 ].max()

  if not args.log:
# midpoint between xl and xu for plotting
    x = ( h[ :, 0 ] + h[ :, 1 ] ) / 2
  else:
    w = 1

  xlabels = ['-1e-1', '-1e-2', '-1e-3', '-1e-4', '-1e-5', '-1e-6', '-1e-7', '-1e-8', '+1e-8', '+1e-7', '+1e-6', '+1e-5', '+1e-4', '+1e-3', '+1e-2', '+1e-1']

# number of samples
  y = h[ :, 2 ]

  # change figure size as needed
  fig, ax = plt.subplots( figsize=(4, 4), layout='constrained' )

  if not args.log:
    ax.bar(x, y, width=w)
  else:
    y = np.append( y, [ 0 ] )
    ax.bar(xlabels, y, width=w, align='edge')
    plt.xticks( fontsize = 10, rotation = 90 )

  title = ""
  xlabel = ""
  s = r.split( f )
  if s:
    figname = s[ -2 ]
    title = figname
    xlabel = title[1:]

  if xlabel == 'x' or xlabel == 'y' or xlabel == 'z' or xlabel == 'dst':
    xlabel = xlabel + "[m]"

  if xlabel == 'Fx' or xlabel == 'Fy' or xlabel == 'Fz' or xlabel == 'F':
    xlabel = xlabel + "[N]"

  if xlabel == 'J1' or xlabel == 'J2' or xlabel == 'Jrnd':
    xlabel = xlabel + "[A/m^2]"

  if xlabel == 'J1x' or xlabel == 'J1y' or xlabel == 'J1z':
    xlabel = xlabel + "[A/m^2]"

  if xlabel == 'J2x' or xlabel == 'J2y' or xlabel == 'J2z':
    xlabel = xlabel + "[A/m^2]"

  if xlabel == 'vol1' or xlabel == 'vol2':
    xlabel = xlabel + "[m^3]"

  ax.set_xlabel( xlabel )
  ax.set_ylabel( ylabel )
  ax.set_title( title )

  if s:
    fpng = figname + '.png'
    plt.savefig( fpng )
    fpdf = figname + '.pdf'
    plt.savefig( fpdf )
    fsvg = figname + '.svg'
    plt.savefig( fsvg )
  else:
    print('File name has non-standard characters, no figures saved')

  if args.show:
    plt.show()

