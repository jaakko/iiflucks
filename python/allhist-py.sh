#!/bin/sh
#
# Plot all 1D and 2D histograms from text files if found.
# The python scripts use matplotlib for plotting, numpy for arrays and pandas
# to read the GSL histogram data file.

echo "Plot histograms"
{ 
  plothist.py hx.txt hy.txt hz.txt hdst.txt hvol1.txt hvol2.txt hprob1.txt hprob2.txt hJ1.txt hJ1x.txt hJ1y.txt hJ1z.txt hJ2.txt hJ2x.txt hJ2y.txt hJ2z.txt hFx.txt hFy.txt hFz.txt hF.txt
} || { 
  echo "-- histogram plotting failed, check files hx.txt, ... and path to plothist.py" 
}

echo "Plot logarithmic bin histograms"
{
  plothist.py --log hdM12.txt
} || {
  echo "-- histogram plotting failed, check files hdM12.txt, ... and path to plothist.py"
}

echo "Plot 2D histograms"
{ 
  plothist2d.py hxy.txt hxz.txt hyz.txt
  plothist2d.py hFxy1.txt hFxy2.txt
  plothist2d.py hFyz1.txt hFyz2.txt
  plothist2d.py hFyz1.txt hFyz2.txt
  plothist2d.py hJ1x_xy.txt hJ1y_xy.txt hJ1z_xy.txt
  plothist2d.py hJ1x_xz.txt hJ1y_xz.txt hJ1z_xz.txt
  plothist2d.py hJ1x_yz.txt hJ1y_yz.txt hJ1z_yz.txt
  plothist2d.py hJ2x_xy.txt hJ2y_xy.txt hJ2z_xy.txt
  plothist2d.py hJ2x_xz.txt hJ2y_xz.txt hJ2z_xz.txt
  plothist2d.py hJ2x_yz.txt hJ2y_yz.txt hJ2z_yz.txt
} || { 
  echo "-- 2D histogram plotting failed, check files hFxy1.txt, ... and path to plothist2d.py" 
}

