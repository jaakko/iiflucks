#!/usr/bin/env python

import argparse
import re
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

p = argparse.ArgumentParser(
      prog='plothist2d.py',
      description='Plot 2D histograms.',
      epilog='')

p.add_argument('filename', nargs='+', help='histogram file name')
p.add_argument('-s', '--show', help='show plot on screen, default is to only write picture files', action='store_true') 

args = p.parse_args()

fnames = args.filename

r = re.compile(r'[/.]')

plt.style.use('_mpl-gallery')

# GSL 2D histogram data with x and y bin edges, and number of samples in each bin
# xl xu yl yu samples
for f in fnames:
  histo = pd.read_csv(f, sep='\s+', header=None)
  h = np.asarray( histo )

  # midpoint between xl and xu for plotting 
  xmin = h[ :, 0 ].min()
  xmax = h[ :, 1 ].max()
  x = ( h[:,0] + h[:,1] ) / 2

  # midpoint between yl and yu for plotting 
  ymin = h[ :, 2 ].min()
  ymax = h[ :, 3 ].max()
  y = ( h[:, 2] + h[:, 3] ) / 2

  # number of samples
  z = h[:, 4]

  # indices for non-zero samples 
  indx = np.flatnonzero( z )

  # plot only bins where number of samples is not zero
  xh = np.take_along_axis( x, indx, axis=None)
  yh = np.take_along_axis( y, indx, axis=None)
  zh = np.take_along_axis( z, indx, axis=None)

  # marker size can be adjusted here
  sizes = np.ones( zh.size )
  sizes *= 1

  # change figure size as needed
  fig, ax = plt.subplots( figsize=(4, 4), layout='constrained' )

  # color map 'cmap' can be changed here
  pc = ax.scatter(xh, yh, s=sizes, c=zh, vmin=zh.min(), vmax=zh.max(), cmap='viridis')
  ax.set( xlim=( xmin, xmax ), ylim=(ymin, ymax ) )

  title = ""
  xlabel = ""
  s = r.split( f )
  if s:
    figname = s[ -2 ]
    title = figname
    if title[ 1 ] == 'F':
      xlabel = title[ 2 ]
      ylabel = title[ 3 ]
      title = title + "[N]"
    elif title[ 1 ] == 'J':
      xlabel = title[ 5 ]
      ylabel = title[ 6 ]
      title = title + "[A/m^2]"
    else:
      xlabel = title[ 1 ]
      ylabel = title[ 2 ]

  if xlabel == 'x' or xlabel == 'y' or xlabel == 'z':
    xlabel = xlabel + "[m]"

  if ylabel == 'x' or ylabel == 'y' or ylabel == 'z':
    ylabel = ylabel + "[m]"

  ax.set_xlabel( xlabel )
  ax.set_ylabel( ylabel )
  ax.set_title( title )
  fig.colorbar( pc )

  if s:
    fpng = figname + '.png'
    plt.savefig( fpng )
    fpdf = figname + '.pdf'
    plt.savefig( fpdf )
    fsvg = figname + '.svg'
    plt.savefig( fsvg )
  else:
    print('File name has non-standard characters, no figures saved')

  if args.show:
    plt.show()

