#!/usr/bin/env python

import argparse
import re
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def list_of_ints( arg ):
    return list( map( int, arg.split(',') ) )

p = argparse.ArgumentParser(
      prog='plotB.py',
      description='Plot calculated field map from mapB.',
      epilog='')

p.add_argument('filename', nargs='+', help='x, y, z, Bx, By, Bz, B file and more maps to add to first field map') 
p.add_argument('-x', '--xaxis', help='plot field on x-axis', action='store_true') 
p.add_argument('-y', '--yaxis', help='plot field on y-axis', action='store_true') 
p.add_argument('-z', '--zaxis', help='plot field on z-axis', action='store_true') 
p.add_argument('-r', '--raxis', help='plot field on r-axis', action='store_true')
p.add_argument('--xy', help='plot field on xy-plane', action='store_true') 
p.add_argument('--xz', help='plot field on xz-plane', action='store_true') 
p.add_argument('--yz', help='plot field on yz-plane', action='store_true') 
p.add_argument('--phir', help='plot field on phi-r-plane', action='store_true')
p.add_argument('--Bx', help='plot Bx', action='store_true') 
p.add_argument('--By', help='plot By', action='store_true') 
p.add_argument('--Bz', help='plot Bz', action='store_true') 
p.add_argument('--B', help='plot B', action='store_true') 
p.add_argument('-c', '--colors', help='color filled contour map', action='store_true') 
p.add_argument('--colormap', type=ascii, default='viridis', help='color map for filled colors', action='store') 
p.add_argument('--levels', type=int, default=7, help='contour map levels, default is 7', action='store') 
p.add_argument('--shape', type=list_of_ints, help='shape of 2D plot array', action='store')
p.add_argument('-d', '--diametric', help='plot on cylindrical radius', action='store_true')
p.add_argument('-s', '--show', help='show plot on screen, default is to only write picture files', action='store_true') 
p.add_argument('-e', '--equal', help='equal aspect ratio, boxed', action='store_true') 

args = p.parse_args()

fnames = args.filename

r = re.compile(r'[/.]')

s = r.split( fnames[ 0 ] )
if s:
    figname = s[ -2 ]
    title = figname

cmap = args.colormap
cmap = cmap.replace( "'", "" )

size1 = 0
size2 = 0
if args.shape:
    size1 = args.shape[0]
    size2 = args.shape[1]

plt.style.use('_mpl-gallery')

# create new array B with zero values
fmap = pd.read_csv( fnames[ 0 ], header=None, comment='#' )
m = np.asarray( fmap )
Bx = m[ :, 3 ]
By = m[ :, 3 ]
Bz = m[ :, 3 ]
B = m[ :, 3 ]
Bx = 0
By = 0
Bz = 0
B = 0

# field map file: x, y, z, Bx, By, Bz, B 
for f in fnames:
    fmap = pd.read_csv( f, header=None, comment='#' )

    m = np.asarray( fmap )

    if args.Bx:
        B += m[ :, 3 ]
    elif args.By:
        B += m[ :, 4 ]
    elif args.Bz:
        B += m[ :, 5 ]
    elif args.B:
        Bx += m[ :, 3 ]
        By += m[ :, 4 ]
        Bz += m[ :, 5 ]
        B = np.sqrt( Bx * Bx + By * By + Bz * Bz )

    if args.xaxis or args.raxis:
        x = m[ :, 0 ]
    elif args.yaxis:
        x = m[ :, 1 ]
    elif args.zaxis:
        x = m[ :, 2 ]
    elif args.xy:
        x = np.array( m[ :, 0 ] )
        y = np.array( m[ :, 1 ] )
        if args.shape:
            if size1 * size2 != x.size:
                print('Shape ', size1,'x' , size2, ' is not ', x.size)
                exit()
            else:
                X = x.reshape( size1, size2 )
                Y = y.reshape( size1, size2 )
                z = np.array( B )
                Z = z.reshape( size1, size2 )
        else:
            size = int( np.sqrt( x.size ) )
            X = x.reshape( size, size )
            Y = y.reshape( size, size )
            z = np.array( B )
            Z = z.reshape( size, size )
    elif args.xz:
        x = np.array( m[ :, 0 ] )
        y = np.array( m[ :, 2 ] )
        if args.shape:
            if size1 * size2 != x.size:
                print('Shape ', size1,'x' , size2, ' is not ', x.size)
                exit()
            else:
                X = x.reshape( size1, size2 )
                Y = y.reshape( size1, size2 )
                z = np.array( B )
                Z = z.reshape( size1, size2 )
        else:
            size = int( np.sqrt( x.size ) )
            X = x.reshape( size, size )
            Y = y.reshape( size, size )
            z = np.array( B )
            Z = z.reshape( size, size )
    elif args.yz:
        x = np.array( m[ :, 1 ] )
        y = np.array( m[ :, 2 ] )
        if args.shape:
            if size1 * size2 != x.size:
                print('Shape ', size1,'x' , size2, ' is not ', x.size)
                exit()
            else:
                X = x.reshape( size1, size2 )
                Y = y.reshape( size1, size2 )
                z = np.array( B )
                Z = z.reshape( size1, size2 )
        else:
            size = int( np.sqrt( x.size ) )
            X = x.reshape( size, size )
            Y = y.reshape( size, size )
            z = np.array( B )
            Z = z.reshape( size, size )
    elif args.phir:
        x = np.array( m[ :, 1 ] )
        y = np.array( m[ :, 0 ] )
        if args.shape:
            if size1 * size2 != x.size:
                print('Shape ', size1,'x' , size2, ' is not ', x.size)
                exit()
            else:
                X = x.reshape( size1, size2 )
                Y = y.reshape( size1, size2 )
                z = np.array( B )
                Z = z.reshape( size1, size2 )
        else:
            size1 = int( np.sqrt( x.size ) )
            size2 = size1 + 1
            X = x.reshape( size1, size2 )
            Y = y.reshape( size1, size2 )
            z = np.array( B )
            Z = z.reshape( size1, size2 )

# change figure size as needed
fig = plt.figure( figsize=(4, 4), layout='constrained' )
ax = fig.add_subplot( )

if args.diametric:
    for i in range( size1 ):
        ax.plot( Y[i, :], Z[i, :], label="{:.3f}".format( X[i, 0] ) )
    ax.legend()
elif args.xy or args.xz or args.yz or args.phir:
    levels = np.linspace( np.min( B ), np.max( B ), args.levels )
    if args.colors:
        cs = ax.contourf(X, Y, Z, levels=levels, cmap=cmap )
        cb = fig.colorbar( cs, shrink=0.8, format='%1.4f' )
    else:
        cs = ax.contour(X, Y, Z, levels=levels)
        ax.clabel( cs, levels[1::2], inline=True, fmt='%1.2f', fontsize=10 )
        cb = fig.colorbar( cs, shrink=0.8, format='%1.4f' )
else:  
    ax.plot(x, B)

pf = ""

if args.xaxis:
    ax.set_xlabel('x[m]')
elif args.yaxis:
    ax.set_xlabel('y[m]')
elif args.zaxis:
    ax.set_xlabel('z[m]')
elif args.raxis or args.diametric:
    ax.set_xlabel('r[m]')
elif args.xy:
    ax.set_xlabel('x[m]')
    ax.set_ylabel('y[m]')
elif args.xz:
    ax.set_xlabel('x[m]')
    ax.set_ylabel('z[m]')
elif args.yz:
    ax.set_xlabel('y[m]')
    ax.set_ylabel('z[m]')
elif args.phir:
    ax.set_xlabel('phi[rad]')
    ax.set_ylabel('r[m]')

if args.Bx:
    pf += 'Bx'
    if args.xaxis or args.yaxis or args.zaxis or args.diametric:
        ax.set_ylabel('Bx[T]')
    else:
        title += ' Bx[T]' 
elif args.By:
    pf += 'By'
    if args.xaxis or args.yaxis or args.zaxis or args.diametric:
        ax.set_ylabel('By[T]')
    else:
        title += ' By[T]' 
elif args.Bz:
    pf += 'Bz'
    if args.xaxis or args.yaxis or args.zaxis or args.diametric:
        ax.set_ylabel('Bz[T]')
    else:
        title += ' Bz[T]' 
elif args.B:
    pf += 'B'
    if args.xaxis or args.yaxis or args.zaxis or args.diametric:
        ax.set_ylabel('B[T]')
    else:
        title += ' B[T]' 

ax.set_title( title )

if args.equal:
    ax.set_aspect('equal', 'box')

if s:
    fpng = figname + pf + '.png'
    plt.savefig( fpng )
    fpdf = figname + pf + '.pdf'
    plt.savefig( fpdf )
    fsvg = figname + pf + '.svg'
    plt.savefig( fsvg )
else:
    print('File name has non-standard characters, no figures saved')

if args.show:
    plt.show()

