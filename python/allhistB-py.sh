#!/bin/sh
#
# Plot all 1D and 2D histograms from text files if found.
# The python scripts use matplotlib for plotting, numpy for arrays and pandas
# to read the GSL histogram data file.

echo "Plot histograms"
{ 
  plothist.py hx.txt hy.txt hz.txt hdst.txt hvol1.txt hvol2.txt hprob1.txt hprob2.txt hJ1.txt hJ2.txt hBx.txt hBy.txt hBz.txt hB.txt
} || { 
  echo "-- histogram plotting failed, check files hx.txt, ... and path to plothist.py" 
}

echo "Plot 2D histograms"
{ 
  plothist2d.py hxy.txt hxz.txt hyz.txt
} || { 
  echo "-- 2D histogram plotting failed, check files hFxy1.txt, ... and path to plothist2d.py" 
}

