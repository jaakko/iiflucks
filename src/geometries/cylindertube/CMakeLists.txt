add_executable (cylindertube src/cylindertube.cpp)

target_link_libraries (cylindertube gmsh)

install (TARGETS cylindertube DESTINATION bin)
