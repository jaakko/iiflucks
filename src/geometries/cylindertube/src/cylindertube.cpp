//
// Create gmsh mesh file for cylindrical tube volume. 
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Fri Apr 12 15:17:24 CDT 2019
// Edit: Sun Sep 15 19:43:35 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <string.h>
#include <gmsh.h>
#include <gsl/gsl_const_mksa.h>

void printusage()
{
  std::cout << "Usage: cylindertube file irad wall height lc Nlayers [Ir Iphi Iz] [v]" << std::endl;
}

void printversion()
{
  std::cout << "cylindertube v. 20190915, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create 3D mesh for cylindrical tube with Gmsh library. The written mesh file has name 'file.msh' and current density file 'file_J.pos'. The current density vector is defined for each mesh node coordinate." << std::endl;
  std::cout << std::endl;
  std::cout << "The mesh has physical groups Top, Bottom, Inner, Outer and Solid." << std::endl; 
  std::cout << std::endl;
  std::cout << "- irad inner radius [m]" << std::endl;
  std::cout << "- wall thickness [m]" << std::endl;
  std::cout << "- height [m]" << std::endl;
  std::cout << "- lc characteristic length for meshing [m]" << std::endl;
  std::cout << "- nlayers number of radial mesh layers" << std::endl;
  std::cout << "- Ir current in outward radial direction [A]" << std::endl;
  std::cout << "- Iphi current in circumference [A]" << std::endl;
  std::cout << "- Iz current in axial direction [A]" << std::endl;
}

int main(int argc, char **argv)
{
  bool debug = false;
  bool verbose = false;
  double irad= 0; // inner radius [m]
  double wall = 0; // wall thickness [m]
  double height = 0; // height [m]
  double lc = 0.1;
  int nlayers = 1; // number of radial mesh layers

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }

  if( argc < 7 )
  {
    printusage();
    return 0;
  }

  if( argc == 8 ) verbose = true;

  irad = atof( argv[ 2 ] );
  wall = atof( argv[ 3 ] );
  height = atof( argv[ 4 ] );
  lc = atof( argv[ 5 ] );
  nlayers = atoi( argv[ 6 ] );

  if( verbose ) 
    std::cout << "-- irad=" << irad << ", wall=" << wall << ", height="
	      << height << ", lc=" << lc << ", nlayers=" << nlayers 
	      << std::endl;

  double Ir = 0, Iphi = 0, Iz = 0;
  double Jr = 0, Jphi = 0, J0z = 0;
  if( argc == 10 || argc == 11 )
  {
    Ir = atof( argv[ 7 ] );
    Iphi = atof( argv[ 8 ] );
    Iz = atof( argv[ 9 ] );    

    Jr = Ir / (2 * M_PI *irad *height); // radial current density [A/m^2]
    Jphi = Iphi/ ( wall * height ); // cylindrical current density [A/m^2]
    J0z = Iz / ( M_PI * ( (irad + wall) * (irad + wall) - irad * irad ) );
  }

  if( argc == 11 ) verbose = true;

  if( verbose ) std::cout << "-- Ir=" << Ir << ", Iphi=" << Iphi << ", Iz="
	      << Iz << std::endl;

  gmsh::initialize();

  if( verbose ) gmsh::option::setNumber("General.Terminal", 1);

  gmsh::model::add("CylinderTube");

  gmsh::model::geo::addPoint(irad, 0, 0, lc, 1);
  gmsh::model::geo::addPoint(irad, 0, height, lc, 2);

  gmsh::model::geo::addLine(1, 2, 1);

  std::vector<std::pair<int, int> > ov;
  // 1=line with tag 1, translate by (wall, 0, 0), output vector and 
  // 1=number of layers
  gmsh::model::geo::extrude({{1, 1}}, wall, 0, 0, ov, {nlayers});

  // 2=surface with tag 5, point on rotation axis, direction of rotation axis,
  // rotation angle, output vector and 6=number of layers
  gmsh::model::geo::revolve({{2, 5}}, 0,0,0, 0,0,1, M_PI/2, ov, {6});
  gmsh::model::geo::revolve({{2, 27}}, 0,0,0, 0,0,1, M_PI/2, ov, {6});
  gmsh::model::geo::revolve({{2, 49}}, 0,0,0, 0,0,1, M_PI/2, ov, {6});
  gmsh::model::geo::revolve({{2, 71}}, 0,0,0, 0,0,1, M_PI/2, ov, {6});

  // 2=surfaces, entity tags, new physical group tag 
  gmsh::model::addPhysicalGroup(2, {40, 62, 84, 18}, 1);
  gmsh::model::setPhysicalName(2, 1, "Top");

  gmsh::model::addPhysicalGroup(2, {48, 70, 92, 26}, 2);
  gmsh::model::setPhysicalName(2, 2, "Bottom");

  gmsh::model::addPhysicalGroup(2, {14, 36, 58, 80}, 3);
  gmsh::model::setPhysicalName(2, 3, "Inner");

  gmsh::model::addPhysicalGroup(2, {22, 44, 66, 88}, 4);
  gmsh::model::setPhysicalName(2, 4, "Outer");

  gmsh::model::addPhysicalGroup(3, {1, 2, 3, 4}, 1);
  gmsh::model::setPhysicalName(3, 1, "Solid");

  gmsh::model::geo::synchronize();

  gmsh::model::mesh::generate(3);

  std::string meshFile = argv[ 1 ];
  meshFile = meshFile + ".msh";
  gmsh::write( meshFile );

  if( fabs(Ir) > 0 || fabs(Iphi) > 0 || fabs(Iz) > 0 )
  {
    int CurDens = gmsh::view::add("Current density J[A/m^2]");

    // get all node coordinates
    std::vector<std::size_t> nodeTags;
    std::vector<double> nodeCoords, nodeParams;
    gmsh::model::mesh::getNodes(nodeTags, nodeCoords, nodeParams, -1, -1);

    // hash to get correct node tag fast
    if( verbose ) std::cout << "-- create node hash" << std::endl;
    unsigned int max_node_tag = nodeTags.size() + 1;
    unsigned int nodeHash[ max_node_tag ];
    for( unsigned int i = 0; i < max_node_tag; i++ )
    {
      if( nodeTags[ i ] >= 1 && nodeTags[ i ] <= max_node_tag )
      {
        nodeHash[ nodeTags[ i ] ] = i;
	if( verbose ) std::cout << nodeTags[ i ] << " -> " << i << std::endl;
      }
    }

    std::vector<double> vDataNode; 
    std::vector<double> data;
    double xn = 0, yn = 0, zn = 0, len = 0; // node coordinates 
    double xr = 0, yr = 0; // radial unit vector
    double xphi = 0, yphi = 0; // cylindrical unit vector
    double Jx = 0, Jy = 0, Jz = 0;

    if( verbose ) std::cout << "-- " << nodeTags.size() << " node tags\n";

    int nd = 0;
    for( unsigned int j = 1; j < max_node_tag ; j++)
    {
      if( debug ) std::cout << "-- " << j;

      xn = nodeCoords[ 3 * nodeHash[ j ] ];
      yn = nodeCoords[ 3 * nodeHash[ j ] + 1 ];
      zn = nodeCoords[ 3 * nodeHash[ j ] + 2 ];

      if( verbose ) std::cout << " (" << xn << "," << yn << "," << zn << ")" << std::endl;

      len = sqrt( xn*xn + yn*yn );

      vDataNode.erase( vDataNode.begin(), vDataNode.end() );
      vDataNode.push_back(xn);
      vDataNode.push_back(yn);
      vDataNode.push_back(zn);

      xr = xn/len;
      yr = yn/len;
      xphi = -yn/len;
      yphi = xn/len;

      Jx = Jr * xr + Jphi * xphi;
      Jy = Jr * yr + Jphi * yphi;
      Jz = J0z;
      if( len < irad || len > irad+wall )
      {
        Jx = 0; Jy = 0; Jz = 0;
      }

      vDataNode.push_back( Jx );
      vDataNode.push_back( Jy );
      vDataNode.push_back( Jz );

      data.insert(data.end(), vDataNode.begin(), vDataNode.end());
      nd++;
   }

   gmsh::view::addListData(CurDens, "VP", nd, data);

   std::string dataFile = argv[ 1 ];
   dataFile = dataFile + "_J.pos";
   gmsh::view::write(CurDens, dataFile ); 

  }

  gmsh::finalize();

  return 0;
}
