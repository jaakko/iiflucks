//
// Create gmsh mesh file for cylinder volume. 
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Wed Apr 24 19:34:57 CDT 2019
// Edit: Sun Sep 15 20:30:04 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <string.h>
#include <gmsh.h>

void printusage()
{
  std::cout << "Usage: cylindervolume file.msh dim rad height lc Nlayers [v]" << std::endl;
}

void printversion()
{
  std::cout << "cylindervolume v. 20190915, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create 1D, 2D or 3D mesh for cylinder with Gmsh library." << std::endl;
  std::cout << std::endl;
  std::cout << "The mesh has physical groups Top, Bottom, Outer and Solid." << std::endl;
  std::cout << std::endl;
  std::cout << "- dim dimension 1, 2 or 3" << std::endl;
  std::cout << "- rad radius [m]" << std::endl;
  std::cout << "- height [m]" << std::endl;
  std::cout << "- lc characteristic length for mesh generation [m]" << std::endl;
  std::cout << "- Nlayers number of mesh layers" << std::endl;

}

int main(int argc, char **argv)
{
  bool verbose = false;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }

  if( argc < 7 )
  {
    printusage();
    return 0;
  }

  if( argc == 8 ) verbose = true;

  int dim = atoi( argv[ 2 ] ); // 1D, 2D or 3D mesh
  double r = atof( argv[ 3 ] ); // radius [m]
  double h = atof( argv[ 4 ] ); // height [m]
  double lc = atof( argv[ 5 ] ); // characteristic length 
  int nlayers = atoi( argv[ 6 ] ); // number of mesh layers

  if( dim < 1 || dim > 3 )
  {
    std::cout << "Mesh can be only 1D, 2D or 3D" << std::endl;
    return 0;
  }

  if( verbose ) std::cout << "-- rad=" << r << ", h=" << h 
	       << ", lc=" << lc << ", nlayers=" << nlayers 
	       << std::endl;

  gmsh::initialize();

  if( verbose ) gmsh::option::setNumber("General.Terminal", 1);

  gmsh::model::add("CylinderVolume");

  gmsh::model::geo::addPoint(0, 0, 0, lc, 1);

  gmsh::model::geo::addPoint(-r, 0, 0, lc, 2);
  gmsh::model::geo::addPoint(0, r, 0, lc, 3);
  gmsh::model::geo::addPoint(r, 0, 0, lc, 4);
  gmsh::model::geo::addPoint(0, -r, 0, lc, 5);

  gmsh::model::geo::addCircleArc(2, 1, 3, 1);
  gmsh::model::geo::addCircleArc(3, 1, 4, 2);
  gmsh::model::geo::addCircleArc(4, 1, 5, 3);
  gmsh::model::geo::addCircleArc(5, 1, 2, 4);

  gmsh::model::geo::addCurveLoop({1, 2, 3, 4}, 5);

  gmsh::model::geo::addPlaneSurface({5}, 6);

  std::vector<std::pair<int, int> > ov;
  // 2=surface with tag 6, translate by (0, 0, h), output vector and 
  // nlayers=number of layers
  gmsh::model::geo::extrude({{2, 6}}, 0, 0, h, ov, {nlayers});

  // 2=surfaces, entity tags, new physical group tag 
  if( dim == 2)
  {
    gmsh::model::addPhysicalGroup(2, {28}, 1);
    gmsh::model::setPhysicalName(2, 1, "Top");

    gmsh::model::addPhysicalGroup(2, {6}, 2);
    gmsh::model::setPhysicalName(2, 2, "Bottom");

    gmsh::model::addPhysicalGroup(2, {15, 19, 23, 27}, 3);
    gmsh::model::setPhysicalName(2, 3, "Outer");
  }
  else if( dim == 3 )
  {
    gmsh::model::addPhysicalGroup(3, {1}, 1);
    gmsh::model::setPhysicalName(3, 1, "Solid");
  }

  gmsh::model::geo::synchronize();

  gmsh::model::mesh::generate( dim );

  gmsh::write( argv[ 1 ] );

  gmsh::finalize();

  return 0;
}
