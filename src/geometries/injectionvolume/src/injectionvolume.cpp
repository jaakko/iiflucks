//
// Create gmsh mesh file for injection volume. 
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Sat May  4 18:46:38 CDT 2019
// Edit: Sun Sep 15 21:35:06 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <string.h>
#include <gmsh.h>

void printusage()
{
  std::cout << "Usage: injectionvolume file.msh dim rcyl hcyl rtube htube lhorizon lc [v]" << std::endl;
}

void printversion()
{
  std::cout << "injectionvolume v. 20190915, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create 1D, 2D or 3D mesh for cylider with 90 degree elbow on bottom connected to horizontal tube." << std::endl;
  std::cout << std::endl;
  std::cout << "- dimension 1, 2 or 3" << std::endl;
  std::cout << "- rcyl top cylinder radius [m]" << std::endl;
  std::cout << "- hcyl top cylinder height [m]" << std::endl;
  std::cout << "- rtube bottom tube radius [m]" << std::endl;
  std::cout << "- htube bottom tube height [m]" << std::endl;
  std::cout << "- lhorizon horizontal tube length [m]" << std::endl;
  std::cout << "- lc characteristic length for mesh generation [m]" << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }

  if( argc < 9 )
  {
    printusage();
    return 0;
  }

  if( argc == 8 ) verbose = true;

  int dim = atoi( argv[ 2 ] ); // 1D, 2D or 3D mesh
  double rcyl = atof( argv[ 3 ] ); // cylinder radius [m]
  double hcyl = atof( argv[ 4 ] ); // cylinder height [m]
  double rtube = atof( argv[ 5 ] ); // tube radius [m]
  double htube = atof( argv[ 6 ] ); // tube height [m]
  double lhorizon = atof( argv[ 7 ] ); // horizontal tube length [m]
  double lc = atof( argv[ 8 ] ); // characteristic length 

  if( dim < 1 || dim > 3 )
  {
    std::cout << "Mesh can be only 1D, 2D or 3D" << std::endl;
    return 0;
  }

  if( verbose ) std::cout << "-- cylinder rad=" << rcyl << ", h=" << hcyl 
       << ", tube rad=" << rtube << ", h=" << htube << ", horizontal="
       << lhorizon << std::endl;

  gmsh::initialize();

  if( verbose ) gmsh::option::setNumber("General.Terminal", 1);

  gmsh::model::add("InjectionVolume");

// top cylinder
  gmsh::model::geo::addPoint(0, 0, 0, lc, 1);

  gmsh::model::geo::addPoint(-rcyl, 0, 0, lc, 2);
  gmsh::model::geo::addPoint(0, rcyl, 0, lc, 3);
  gmsh::model::geo::addPoint(rcyl, 0, 0, lc, 4);
  gmsh::model::geo::addPoint(0, -rcyl, 0, lc, 5);

  gmsh::model::geo::addCircleArc(2, 1, 3, 1);
  gmsh::model::geo::addCircleArc(3, 1, 4, 2);
  gmsh::model::geo::addCircleArc(4, 1, 5, 3);
  gmsh::model::geo::addCircleArc(5, 1, 2, 4);

  gmsh::model::geo::addCurveLoop({1, 2, 3, 4}, 5);

  gmsh::model::geo::addPlaneSurface({5}, 6);

  std::vector<std::pair<int, int> > ov;
  // 2=surface with tag 6, translate by (0, 0, h), output vector and 
  // nlayers=number of layers
  gmsh::model::geo::extrude({{2, 6}}, 0, 0, hcyl, ov);

// bottom tube
  gmsh::model::geo::addPoint(-rtube, 0, 0, lc, 19);
  gmsh::model::geo::addPoint(0, rtube, 0, lc, 20);
  gmsh::model::geo::addPoint(rtube, 0, 0, lc, 21);
  gmsh::model::geo::addPoint(0, -rtube, 0, lc, 22);

  gmsh::model::geo::addCircleArc(19, 1, 20, 30);
  gmsh::model::geo::addCircleArc(20, 1, 21, 31);
  gmsh::model::geo::addCircleArc(21, 1, 22, 32);
  gmsh::model::geo::addCircleArc(22, 1, 19, 33);

  gmsh::model::geo::addCurveLoop({30, 31, 32, 33}, 34);
  gmsh::model::geo::addPlaneSurface({34}, 35);

  gmsh::model::geo::extrude({{2, 35}}, 0, 0, -htube, ov);

  gmsh::model::geo::revolve({{2, 57}}, rtube,0,-htube, 0,1,0, -M_PI/2, ov);

  gmsh::model::geo::extrude({{2, 77}}, lhorizon, 0, 0, ov);

  // 2=surfaces, entity tags, new physical group tag 
  if( dim == 2)
  {
    gmsh::model::addPhysicalGroup(2, {28}, 1);
    gmsh::model::setPhysicalName(2, 1, "Top");

    gmsh::model::addPhysicalGroup(2, {6}, 2);
    gmsh::model::setPhysicalName(2, 2, "Bottom");

    gmsh::model::addPhysicalGroup(2, {15, 19, 23, 27}, 3);
    gmsh::model::setPhysicalName(2, 3, "Cylinder");

    gmsh::model::addPhysicalGroup(2, {44, 48, 52, 56}, 4);
    gmsh::model::setPhysicalName(2, 4, "Bottom Tube");

//    gmsh::model::addPhysicalGroup(2, {35}, 5);
//    gmsh::model::setPhysicalName(2, 5, "Tube Top");

//    gmsh::model::addPhysicalGroup(2, {57}, 6);
//    gmsh::model::setPhysicalName(2, 6, "Tube Bottom");

    gmsh::model::addPhysicalGroup(2, {66, 69, 72, 76}, 7);
    gmsh::model::setPhysicalName(2, 7, "Elbow");

//    gmsh::model::addPhysicalGroup(2, {77}, 8);
//    gmsh::model::setPhysicalName(2, 8, "Horizontal Tube Inlet");

    gmsh::model::addPhysicalGroup(2, {86, 90, 94, 98}, 9);
    gmsh::model::setPhysicalName(2, 9, "Horizontal Tube");

    gmsh::model::addPhysicalGroup(2, {99}, 10);
    gmsh::model::setPhysicalName(2, 10, "Horizontal Tube Outlet");
    
  }
  else if( dim == 3 )
  {
    gmsh::model::addPhysicalGroup(3, {1, 2, 3, 4}, 1);
    gmsh::model::setPhysicalName(3, 1, "Helium");
  }

  gmsh::model::geo::synchronize();

  gmsh::model::mesh::generate( dim );

  gmsh::write( argv[ 1 ] );

  gmsh::finalize();

  return 0;
}
