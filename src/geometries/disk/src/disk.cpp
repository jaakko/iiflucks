//
// Create gmsh mesh file for disk volume. 
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
// 
// Fri Apr 12 21:14:02 CDT 2019
// Edit: Sun Sep 15 20:56:39 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <string.h>
#include <gmsh.h>

void printusage()
{
  std::cout << "Usage: disk file.msh rad thickness lc Nlayers [v]" << std::endl;
}

void printversion()
{
  std::cout << "disk v. 20190915, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create 3D mesh for disk with Gmsh library." << std::endl; 
  std::cout << std::endl;
  std::cout << "The mesh has physical groups Top, Bottom, Outer and Solid." << std::endl;
  std::cout << std::endl;
  std::cout << "- rad radius [m]" << std::endl;
  std::cout << "- thickness [m]" << std::endl;
  std::cout << "- lc characteristic length for mesh generation [m]" << std::endl;
  std::cout << "- Nlayes number of mesh layers" << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  double rad= 0; // radius [cm]
  double thck = 0; // thickness [cm]
  double lc = 0.1;
  int nlayers = 1; // number of radial mesh layers

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }

  if( argc < 6 )
  {
    printusage();
    return 0;
  }

  if( argc == 7 ) verbose = true;

  rad = atof( argv[ 2 ] );
  thck = atof( argv[ 3 ] );
  lc = atof( argv[ 4 ] );
  nlayers = atoi( argv[ 5 ] );

  if( verbose ) std::cout << "-- rad=" << rad << ", thck=" << thck 
	       << ", lc=" << lc << ", nlayers=" << nlayers 
	       << std::endl;

  gmsh::initialize();

  if( verbose ) gmsh::option::setNumber("General.Terminal", 1);

  gmsh::model::add("Disk");

  gmsh::model::geo::addPoint(0, 0, 0, lc, 1);
  gmsh::model::geo::addPoint(0, 0, thck, lc, 2);

  gmsh::model::geo::addLine(1, 2, 1);

  std::vector<std::pair<int, int> > ov;
  // 1=line with tag 1, translate by (rad, 0, 0), output vector and 
  // nlayers=number of layers
  gmsh::model::geo::extrude({{1, 1}}, rad, 0, 0, ov, {nlayers});

  // 2=surface with tag 5, point on rotation axis, direction of rotation axis,
  // rotation angle, output vector and 6=number of layers
  gmsh::model::geo::revolve({{2, 5}}, 0,0,0, 0,0,1, M_PI/2, ov, {6}, {}, true);
  gmsh::model::geo::revolve({{2, 22}}, 0,0,0, 0,0,1, M_PI/2, ov, {6}, {}, true);
  gmsh::model::geo::revolve({{2, 39}}, 0,0,0, 0,0,1, M_PI/2, ov, {6}, {}, true);
  gmsh::model::geo::revolve({{2, 56}}, 0,0,0, 0,0,1, M_PI/2, ov, {6}, {}, true);

  // 2=surfaces, entity tags, new physical group tag 
  gmsh::model::addPhysicalGroup(2, {14, 31, 48, 65}, 1);
  gmsh::model::setPhysicalName(2, 1, "Top");

  gmsh::model::addPhysicalGroup(2, {38, 72, 55, 21}, 2);
  gmsh::model::setPhysicalName(2, 2, "Bottom");

  gmsh::model::addPhysicalGroup(2, {18, 35, 52, 69}, 4);
  gmsh::model::setPhysicalName(2, 4, "Outer");

  gmsh::model::addPhysicalGroup(3, {1, 2, 3, 4}, 1);
  gmsh::model::setPhysicalName(3, 1, "Solid");

  gmsh::model::geo::synchronize();

  gmsh::model::mesh::generate(3);

  gmsh::write( argv[ 1 ] );

  gmsh::finalize();

  return 0;
}
