//
// Read position-current_density-volume file (coil) and calculate induced flux 
// from static magnetic field.
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Wed May  8 20:45:40 CDT 2019
// Edit: Thu May 23 21:35:27 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <string.h>
#include <gsl/gsl_const_mksa.h>


void printusage()
{
  std::cout << "Usage: fluxfield file.pdv alpha beta gamma xt yt zt I1 Bx By Bz [v]" << std::endl;
}

void printversion()
{
  std::cout << "fluxfield v. 20190523, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Read position-current_density-volume file (coil) and calculate induced flux from static magnetic field." << std::endl;
  std::cout << std::endl;
  std::cout << "The volume is rotated counter clockwise around z-axis by angle alpha followed by counter clockwise rotation around y-axis by angle beta. Finally it is rotated counter clockwise by angle gamma around z-axis." << std::endl;
  std::cout << std::endl;
  std::cout << "After rotation the volume is translated by vector (xt, yt, zt)." << std::endl;
  std::cout << std::endl;
  std::cout << "Model for fractional energy is" << std::endl;
  std::cout << std::endl;
  std::cout << "dW = 0.5 * (Jx * Ax + Jy * Ay + Jz * Az) * dv" << std::endl;
  std::cout << std::endl;
  std::cout << "Here J = (Jx, Jy, Jz) is current density vector in volume dv. Vector potential A = (Ax, Ay, Az) is calculated from A = -0.5 r x B. I1 is current flowing in volume." << std::endl;
  std::cout << std::endl;
  std::cout << "Flux linkage is calculate from energy" << std::endl;
  std::cout << std::endl;
  std::cout << "Phi = 2 * W / I1" << std::endl;
  std::cout << std::endl;
  std::cout << "Translations can be used to test invariance of results with (xt, yt, zt)." << std::endl;
}

int main(int argc, char **argv)
{

  const int maxpoints = 15000; // maximum number of points that can be read
  bool verbose = false;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }

  if( argc < 12 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ],  "--test", 6 ) == 0)
  {
    if( argc == 3 ) verbose = true;
//    testFunctions( verbose );
    return 0;
  }

  if( argc == 13 ) verbose = true;

  double alpha = atof( argv[ 2 ] );// Euler rotation angles
  double beta = atof( argv[ 3 ] );
  double gamma = atof( argv[ 4 ] );
  double xt = atof( argv[ 5 ] ); // translation vector after rotation
  double yt = atof( argv[ 6 ] );
  double zt = atof( argv[ 7 ] );
  double I1 = atof( argv[ 8 ] ); // current in volume [A]
  double Bx = atof( argv[ 9 ] ); // magnetic field
  double By = atof( argv[ 10 ] );
  double Bz = atof( argv[ 11 ] );

  // position coordinates for small volume dv
  double xc, yc, zc, dv;

  // vector field with same position coordinates
  double Jx, Jy, Jz;

  std::cout << "-- rotate volume with Euler angles (" << alpha << "," << beta << "," << gamma << ")" << std::endl;

  std::cout << "-- translate volume with vector (" << xt << "," << yt << "," << zt << ")" << std::endl;

  std::ifstream infile( argv[ 1 ] );
  if( !infile.good() )
  {
    std::cout << "Could not open " << argv[ 1 ] << "\n";
  }

  double xr = 0, yr = 0, zr = 0;
  double Jxr = 0, Jyr = 0, Jzr =0;
  double Ax = 0, Ay = 0, Az = 0;
  double Phi = 0;
  double W = 0, dW = 0;
  int np = 0;
  double V = 0;
  while( np < maxpoints )
  {
    infile >> xc >> yc >> zc;
    infile >> Jx >> Jy >> Jz;
    infile >> dv;

    if( !infile.good() ) break;

    if( verbose )
    {
      std::cout << "(" << xc << "," << yc << "," << zc << ") ";
      std::cout << "J = (" << Jx << "," << Jy << "," << Jz << ") ";
      std::cout << dv << std::endl;
    }

    xr = ( cos(gamma)*cos(beta)*cos(alpha) - sin(gamma)*sin(alpha) ) * xc              + ( cos(gamma)*cos(beta)*sin(alpha) + sin(gamma)*cos(alpha) ) * yc              - cos(gamma)*sin(beta) * zc;

    yr = (-sin(gamma)*cos(beta)*cos(alpha) - cos(gamma)*sin(alpha) ) * xc
       + (-sin(gamma)*cos(beta)*sin(alpha) + cos(gamma)*cos(alpha) ) * yc
       + sin(gamma)*sin(beta) * zc;

    zr = sin(beta)*cos(alpha) * xc + sin(beta)*sin(alpha) * yc
       + cos(beta) * zc;

    xr += xt;
    yr += yt;
    zr += zt;

    Jxr = ( cos(gamma)*cos(beta)*cos(alpha) - sin(gamma)*sin(alpha) ) * Jx 
	+ ( cos(gamma)*cos(beta)*sin(alpha) + sin(gamma)*cos(alpha) ) * Jy              - cos(gamma)*sin(beta) * Jz;

    Jyr = (-sin(gamma)*cos(beta)*cos(alpha) - cos(gamma)*sin(alpha) ) * Jx 
	+ (-sin(gamma)*cos(beta)*sin(alpha) + cos(gamma)*cos(alpha) ) * Jy
        + sin(gamma)*sin(beta) * Jz;

    Jzr = sin(beta)*cos(alpha) * Jx + sin(beta)*sin(alpha) * Jy
         + cos(beta) * Jz;

    // A(r) = -0.5 r x B
    Ax = -(yr * Bz - zr * By )/2;
    Ay = -(zr * Bx - xr * Bz )/2;
    Az = -(xr * By - yr * Bx )/2;

    dW = (Jxr * Ax + Jyr * Ay + Jzr * Az) * dv;
    W += dW;

    V += dv;
    np++;
  }

  infile.close();

  W *= 0.5;

  Phi = 2 * W / I1;

  std::cout << "-- E " << W << " J and Flux " << Phi << std::endl;

  if( verbose )  std::cout << "-- " << np << " points read with volume " << V << std::endl;

  return 0;
}
