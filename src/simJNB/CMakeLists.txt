add_executable (simJNB src/simJNB.cpp)
find_package (GSL REQUIRED)

target_link_libraries(simJNB iiflucks GSL::gsl yaml-cpp)

install (TARGETS simJNB DESTINATION bin)
