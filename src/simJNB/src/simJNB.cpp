//
// Simulate random Johnson-Nyquist noise currents in conducting volume and 
// calculate resulting magnetic field at given point.
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Tue Jun 18 13:16:15 CDT 2019
// Edit: Tue Jun 18 15:21:15 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <string.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: simJNB file.pdv ax ay az theta xt yt zt x2 y2 z2 sigma T Nsim [v]" << std::endl;
}

void printversion()
{
  std::cout << "simJNB v. 20190618, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Read position-current_density-volume file, generate new random current distribution and calculate magnetic field at position r2 = (x2, y2, z2)." << std::endl;
  std::cout << std::endl;
  std::cout << "The volume is rotated around axis (ax, ay, az) by angle theta. After rotation the volume is translated by vector (xt, yt, zt)." << std::endl; 
  std::cout << std::endl;
  std::cout << "Model for fractional field is" << std::endl;
  std::cout << std::endl;
  std::cout << "dB(r2) = u0/4*pi * J(r1) x (r2 - r1) * dv/|r2 - r1|^3" << std::endl;
  std::cout << std::endl;
  std::cout << "Here J = (Jx, Jy, Jz) is current density vector in volume dv at position r1." << std::endl;
  std::cout << std::endl;
  std::cout << "The simulation is repeated Nsim times using conductivity sigma [S/m] and temperature T [K]. Length of current vector in dv is taken from random Gaussian distribution with standard deviation Jrnd = sqrt( 4 * sigma * kB * T / dv )." << std::endl;
  std::cout << std::endl;
}

int main(int argc, char **argv)
{
  const int maxpoints = 15000;

  bool verbose = false;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test-quat", 11 ) == 0)
  {
    if( argc == 3 ) verbose = true;

    iiflucks::testQuaternion( verbose );

    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test-vect", 11 ) == 0)
  {
    if( argc == 3 ) verbose = true;

    iiflucks::testVector( verbose );

    return 0;
  }

  if( argc < 15 )
  {
    printusage();
    return 0;
  }

  if( argc == 16 ) verbose = true;

  double ax = atof( argv[ 2 ] );// axis of rotation 
  double ay = atof( argv[ 3 ] );
  double az = atof( argv[ 4 ] );
  double theta = atof( argv[ 5 ] );
  double xt = atof( argv[ 6 ] ); // translation vector after rotation
  double yt = atof( argv[ 7 ] );
  double zt = atof( argv[ 8 ] );
  double x2 = atof( argv[ 9 ] ); // position where field is calculated 
  double y2 = atof( argv[ 10 ] );
  double z2 = atof( argv[ 11 ] );
  double sigma = atof( argv[ 12 ] ); // electrical conductivity
  double T = atof( argv[ 13 ] ); // temperature [K]
  int Nsim = atoi( argv[ 14 ] ); // number of different current distributions

  std::vector<double> a; // axis of rotation 
  std::vector<double> r; // r = r2 - r1  
  std::vector<double> J; // current density vector 
  std::vector<double> v; // vector used in rotations 
  std::vector<double> Jr21; // J(r1) x ( r2 - r1 ) 

  double anorm = 0; // length of rotation axis vector

  // position coordinates for small volume dv
  double xc[ maxpoints ], yc[ maxpoints ], zc[ maxpoints ], dv[ maxpoints ];

  // vector field for current density 
  double Jx, Jy, Jz;

  std::ifstream infile( argv[ 1 ] );
  if( !infile.good() )
  {
    std::cout << "Could not open " << argv[ 1 ] << "\n";
  }

  anorm = sqrt( ax*ax + ay*ay + az*az );
  if( anorm > 0 && theta != 0 )
  {
    std::cout << "-- rotate volume around axis (" << ax << "," << ay << "," << az << ") by angle " << theta << std::endl;

    a.push_back( ax / anorm );
    a.push_back( ay / anorm );
    a.push_back( az / anorm );
  }

  std::cout << "-- translate volume with vector (" << xt << "," << yt << "," << zt << ")" << std::endl;

  double Bx = 0, By = 0, Bz = 0, B = 0, dBx = 0, dBy = 0, dBz = 0;
  double r21 = 0;
  int np = 0;
  double V = 0;
  while( np < maxpoints )
  {
    infile >> xc[ np ] >> yc[ np ] >> zc[ np ];
    infile >> Jx >> Jy >> Jz;
    infile >> dv[ np ];

    if( !infile.good() ) break;

    if( verbose )
    {
      std::cout << "(" << xc[ np ] << "," << yc[ np ] << "," << zc[ np ] << ") ";
      std::cout << dv[ np ] << std::endl;
    }

    if( anorm > 0 && theta != 0 )
    {
      v.erase( v.begin(), v.end() );
      v.push_back( xc[ np ] );
      v.push_back( yc[ np ] );
      v.push_back( zc[ np ] );

      iiflucks::QuaternionRotation(a, theta, v);

      xc[ np ] = v[ 0 ];
      yc[ np ] = v[ 1 ];
      zc[ np ] = v[ 2 ];

    }

    xc[ np ] += xt;
    yc[ np ] += yt;
    zc[ np ] += zt;

    V += dv[ np ];
    np++;
  }

  if( np >= maxpoints && infile.good() )
  {
    std::cout << "Maximum number of points " << maxpoints << std::endl;
    return -1;
  }

  std::cout << "-- " << np << " points read with volume " << V << std::endl;

  infile.close();

  const gsl_rng_type * U;
  gsl_rng * rnd;

  U = gsl_rng_default;
  rnd = gsl_rng_alloc (U);

  double xrnd = 0, yrnd = 0, zrnd = 0;
  double Jrms = 0, Jrnd = 0;

  for( int i = 0; i < Nsim; i++ )
  {

    Bx = 0; By = 0; Bz = 0;

    for( int j = 0; j < np; j++ )
    {
      gsl_ran_dir_3d(rnd, &xrnd, &yrnd, &zrnd);
      Jrms = sqrt( 4 * sigma * GSL_CONST_MKSA_BOLTZMANN * T / dv[ j ]);
      Jrnd = gsl_ran_gaussian(rnd, Jrms);

      Jx = Jrnd * xrnd;
      Jy = Jrnd * yrnd;
      Jz = Jrnd * zrnd;
    
      J.erase( J.begin(), J.end() );
      J.push_back( Jx );
      J.push_back( Jy );
      J.push_back( Jz );

      r.erase( r.begin(), r.end() );
      r.push_back( x2 - xc[ j ] );
      r.push_back( y2 - yc[ j ] );
      r.push_back( z2 - zc[ j ] );

      r21 = iiflucks::vectorLength( r );

      iiflucks::crossProduct( J, r, Jr21 );

      dBx = Jr21[ 0 ] * dv[ j ] / ( r21 * r21 * r21 );
      dBy = Jr21[ 1 ] * dv[ j ] / ( r21 * r21 * r21 );
      dBz = Jr21[ 2 ] * dv[ j ] / ( r21 * r21 * r21 );

      Bx += dBx;
      By += dBy;
      Bz += dBz;
    }

    Bx *= 1e-7;
    By *= 1e-7;
    Bz *= 1e-7;

    B = sqrt( Bx * Bx + By * By + Bz * Bz );
    std::cout << Bx << " " << By << " " << Bz << " " << B << std::endl;

  }

  return 0;
}
