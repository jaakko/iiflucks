//
// Read position-surface_current-area file and calculate induced flux to coil. 
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// The file1.pdv has list of coordinates, current densities and volume of
// small element in this position.
//
// x y z Jx Jy Jz dv
// ...
//
// The file2.pda has list of coordinates, surface current densities jm = M x n
// and area of element in this position.
//
// x y z jmx jmy jmz da
// ...
//
// The first volume is rotated around axis (ax, ay, az) by angle theta.
// After rotation the first volume is translated by vector (xt, yt, zt).
//
// Model for coupled energy and flux is
//
// dFlux12 = 1e-7 * sqrt(ur1*ur2) * dv * da * J1 . jm2 / (r12 * prob *I1)
//
// Here prob = prob1 * prob2 with prob1 = dv/V and prob2 = da/A.
//
// 10^nrand random samples are taken from volume V and surface A. Average 
// of dFlux12 gives estimate of total flux coupling. The error is estimated
// from variance of these samples.
//
// I1 is current flowing in volume V. This was used to generate the file1.pdv
// from known geometry like helix. Otherwise it needs to be estimated from 
// the data.
//
// The minimum and maximum values, and number of bins for histograms are 
// read from file '.fluxind' if available. The file is written with new 
// values before program exits. The histograms are:
//
// - x, y, z position of randomly taken volume or surface sample
// - element volume
// - element area
// - distance between elements
// - probability to select these two elements
// - volume current density J1
// - surface current jm2
//
// Random thermal noise currents can be simulated by giving electrical
// conductivity sigma [S/m], temperature T [K] and number of different
// random direction surface current density Jrnd distributions in file 2.
// The jm in file2.pda is ignored in this case. The length of current
// vector in da is taken from random Gaussian distribution with
// standard deviation Jrnd = sqrt( 4 * sigma * kB * T / dv ). Here 
// dv = skin_depth * da and skin depth is given by
// sqrt( 2 / ur * u0 * 2 * pi * f * sigma ). 
//
// Mon Apr 15 19:19:19 CDT 2019
// Edit: Fri Sep 13 14:43:48 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_histogram2d.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: fluxind file1.pdv file2.pda xt yt zt ax ay az theta I1 nrand [sigma T f Nsim] [v]" << std::endl;
}

void printversion()
{
  std::cout << "fluxind v. 20190913, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Read position-surface_current-area file and calculate induced flux to coil." << std::endl; 
  std::cout << std::endl;
  std::cout << "The file1.pdv has list of coordinates, current densities and volume of small element in this position." << std::endl;
  std::cout << std::endl;
  std::cout << "x y z Jx Jy Jz dv" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "The file2.pda has list of coordinates, surface current densities jm = M x n and area of element in this position." << std::endl;
  std::cout << std::endl;
  std::cout << "x y z jmx jmy jmz da" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "The first volume is rotated around axis (ax, ay, az) by angle theta. After rotation the first volume is translated by vector (xt, yt, zt)." << std::endl;
  std::cout << std::endl;
  std::cout << "Model for coupled energy and flux is" << std::endl;
  std::cout << std::endl;
  std::cout << "dFlux12 = 1e-7 * sqrt(ur1*ur2) * dv * da * J1 . jm2 / (r12 * prob *I1)" << std::endl;
  std::cout << std::endl;
  std::cout << "Here prob = prob1 * prob2 with prob1 = dv/V and prob2 = da/A." << std::endl;
  std::cout << std::endl;
  std::cout << "10^nrand random samples are taken from volume V and surface A. Average of dFlux12 gives estimate of total flux coupling. The error is estimated from variance of these samples." << std::endl;
  std::cout << std::endl;
  std::cout << "I1 is current flowing in volume V. This was used to generate the file1.pdv from known geometry like helix. Otherwise it needs to be estimated from the data." << std::endl;
  std::cout << std::endl;
  std::cout << "The minimum and maximum values, and number of bins for histograms are read from file '.fluxind' if available. The file is written with new values before program exits. The histograms are:" << std::endl;
  std::cout << std::endl;
  std::cout << "- x, y, z position of randomly taken volume or surface sample" << std::endl;
  std::cout << "- element volume" << std::endl;
  std::cout << "- element area" << std::endl;
  std::cout << "- distance between elements" << std::endl;
  std::cout << "- probability to select these two elements" << std::endl;
  std::cout << "- volume current density J1" << std::endl;
  std::cout << "- surface current jm2" << std::endl;
  std::cout << std::endl;
  std::cout << "File '.fluxind' can be edited for histogram minimum and maximum values. It has following lines" << std::endl;
  std::cout << std::endl;
  std::cout << "min_prob max_prob min_vol max_vol min_area max_area min_dist max_dist" << std::endl;
  std::cout << "mix_x max_x min_y max_y min_z max_z" << std::endl;
  std::cout << "min_J max_J min_jm max_jm" << std::endl;
  std::cout << "min_jmx max_jmx min_jmy max_jmy min_jmz max_jmz" << std::endl;
  std::cout << "min_Jrnd max_Jrnd" << std::endl;
  std::cout << "number of bins" << std::endl;
  std::cout << std::endl;
  std::cout << "Random thermal noise currents can be simulated by giving electrical conductivity sigma [S/m], temperature T [K] and number of different random direction surface current density Jrnd distributions in file 2. The jm in file2.pda is ignored in this case. The length of current vector in da is taken from random Gaussian distribution with standard deviation Jrnd = sqrt( 4 * sigma * kB * T / dv ). Here dv = skin_depth * da and skin depth is given by sqrt( 2 / ur * u0 * 2 * pi * f * sigma )." << std::endl; 

}

int main(int argc, char **argv)
{

  const int maxpoints = 15000; // maximum number of points that can be read
  bool verbose = false;

  double ur1 = 1, ur2 = 1; // relative permeability

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test-quat", 11 ) == 0)
  {
    if( argc == 3 ) verbose = true;
    iiflucks::testQuaternion( verbose );
    return 0;
  }

  if( argc < 12 )
  {
    printusage();
    return 0;
  }

  if( argc == 13 ) verbose = true;

  double xt = atof( argv[ 3 ] ); // translation vector
  double yt = atof( argv[ 4 ] );
  double zt = atof( argv[ 5 ] );
  double ax = atof( argv[ 6 ] );// Euler rotation angles
  double ay = atof( argv[ 7 ] );
  double az = atof( argv[ 8 ] );
  double theta = atof( argv[ 9 ] );
  double I1 = atof( argv[ 10 ] ); // current in first volume [A]
  double nr = atof( argv[ 11 ] ); // number of random samples 10^nr

  int nrand = pow( 10, nr ); 

  double sigma = 0; // conductivity [S/m]
  double T = 0; // temperature [K]
  double f = 0; // frequency [Hz]
  double deltas = 0; // skin depth [m]
  int nsim = 1; // number of simulated random current distributions

  if( argc == 16 || argc == 17 )
  {
    sigma = atof( argv[ 12 ] );
    T = atof( argv[ 13 ] );
    f = atof( argv[ 14 ] );

    deltas = sqrt( 2 / ( ur2 * GSL_CONST_MKSA_VACUUM_PERMEABILITY * 2 * M_PI * f * sigma ) );

    std::cout << "-- skin depth " << deltas << std::endl;
    nsim = atoi( argv[ 15 ] );

    if( argc == 17 ) verbose = true;
  }

  // position coordinates for small volume dv
  double x1c[ maxpoints ], y1c[ maxpoints ], z1c[ maxpoints ], dv[ maxpoints ];
  // vector field with same position coordinates
  double J1x[ maxpoints ], J1y[ maxpoints ], J1z[ maxpoints ];

  // position coordinates for small area da
  double x2c[ maxpoints ], y2c[ maxpoints ], z2c[ maxpoints ], da[ maxpoints ];
  // surface current or plane vector u with same position coordinates
  double jm2x[ maxpoints ], jm2y[ maxpoints ], jm2z[ maxpoints ];
  // plane vector v with same position coordinates
  double vx[ maxpoints ], vy[ maxpoints ], vz[ maxpoints ];

  std::ifstream infile1( argv[ 1 ] );
  if( !infile1.good() )
  {
    std::cout << "Could not open " << argv[ 1 ] << "\n";
  }

  int np1 = 0;
  double V = 0;
  while( np1 < maxpoints )
  {
    infile1 >> x1c[ np1 ] >> y1c[ np1 ] >> z1c[ np1 ];
    infile1 >> J1x[ np1 ] >> J1y[ np1 ] >> J1z[ np1 ];
    infile1 >> dv[ np1 ];

    if( !infile1.good() ) break;

    if( verbose )
    {
      std::cout << "(" << x1c[ np1 ] << "," << y1c[ np1 ] << "," << z1c[ np1 ] << ") ";
      std::cout << "J = (" << J1x[ np1 ] << "," << J1y[ np1 ] << "," << J1z[ np1 ] << ") ";
      std::cout << dv[ np1 ] << std::endl;
    }

    V += dv[ np1 ];
    np1++;
  }

  infile1.close();

  if( np1 >= maxpoints && infile1.good() )
  {
    std::cout << "Maximum number of points " << maxpoints << std::endl;
    return -1;
  }

  std::cout << "-- " << np1 << " points read with volume " << V << std::endl;

  std::ifstream infile2( argv[ 2 ] );
  if( !infile2.good() )
  {
    std::cout << "Could not open " << argv[ 2 ] << "\n";
  }

  int np2 = 0;
  double A = 0;
  while( np2 < maxpoints )
  {
    infile2 >> x2c[ np2 ] >> y2c[ np2 ] >> z2c[ np2 ];
    infile2 >> jm2x[ np2 ] >> jm2y[ np2 ] >> jm2z[ np2 ];
    if( nsim > 1 ) infile2 >> vx[ np2 ] >> vy[ np2 ] >> vz[ np2 ];
    infile2 >> da[ np2 ];

    if( !infile2.good() ) break;

    if( verbose )
    {
      std::cout << "(" << x2c[ np2 ] << "," << y2c[ np2 ] << "," << z2c[ np2 ] << ") ";
      if( nsim > 1 )
      {
        std::cout << "u = (" << jm2x[ np2 ] << "," << jm2y[ np2 ] << "," << jm2z[ np2 ] << ") ";
	std::cout << "v = (" << vx[ np2 ] << "," << vy[ np2 ] << "," << vz[ np2 ] << ") ";
      }
      else std::cout << "jm = (" << jm2x[ np2 ] << "," << jm2y[ np2 ] << "," << jm2z[ np2 ] << ") ";

      std::cout << da[ np2 ] << std::endl;
    }

    A += da[ np2 ];
    np2++;
  }

  infile2.close();

  if( np2 >= maxpoints && infile2.good() )
  {
    std::cout << "Maximum number of points " << maxpoints << std::endl;
    return -1;
  }

  std::cout << "-- " << np2 << " points read with area " << A << std::endl;

  std::vector<double> a, v; // axis of rotation and vector to rotate

  double anorm = sqrt( ax*ax + ay*ay + az*az );

  if( anorm > 0 && theta != 0 )
  {
    std::cout << "-- rotate first volume around axis (" << ax << "," << ay << "," << az << ") by angle " << theta << std::endl;

    a.push_back( ax / anorm );
    a.push_back( ay / anorm );
    a.push_back( az / anorm );

    for( int i = 0; i < np1; i++)
    {
      v.erase( v.begin(), v.end() );
      v.push_back( x1c[ i ] );
      v.push_back( y1c[ i ] );
      v.push_back( z1c[ i ] );

      iiflucks::QuaternionRotation(a, theta, v);

      x1c[ i ] = v[ 0 ];
      y1c[ i ] = v[ 1 ];
      z1c[ i ] = v[ 2 ];

      v.erase( v.begin(), v.end() );
      v.push_back( J1x[ i ] );
      v.push_back( J1y[ i ] );
      v.push_back( J1z[ i ] );

      iiflucks::QuaternionRotation(a, theta, v);

      J1x[ i ] = v[ 0 ];
      J1y[ i ] = v[ 1 ];
      J1z[ i ] = v[ 2 ];
    }
  }

  std::cout << "-- translate volume with (" << xt << "," << yt << "," << zt << ")" << std::endl;

  for( int i = 0; i < np1; i++)
  {
    x1c[ i ] += xt;
    y1c[ i ] += yt;
    z1c[ i ] += zt;
  }

  int nbin = 400;
  double mnprob = 1e10, mxprob = -1e10;
  double mnvol1 = 1e10, mxvol1 = -1e10;
  double mnarea2 = 1e10, mxarea2 = -1e10;
  double mndst = 1e10, mxdst = -1e10;
  double minJ1 = 1e20, maxJ1 = -1e20;
  double minjm2 = 1e20, maxjm2 = -1e20;
  double minjm2x = 1e20, maxjm2x = -1e20;
  double minjm2y = 1e20, maxjm2y = -1e20;
  double minjm2z = 1e20, maxjm2z = -1e20;
  double minJrnd = 1e20, maxJrnd = -1e20;
  double minx = 1e20, maxx = -1e20;
  double miny = 1e20, maxy = -1e20;
  double minz = 1e20, maxz = -1e20;

  bool confread = false;
  std::ifstream confile( ".fluxind" );
  if( confile.good() )
  {
    confile >>  mnprob >>  mxprob;
    confile >> mnvol1 >> mxvol1 >> mnarea2 >> mxarea2;
    confile >> mndst >> mxdst;
    confile >> minx >> maxx >> miny >> maxy;
    confile >> minz >> maxz;
    confile >> minJ1 >> maxJ1 >> minjm2 >> maxjm2;
    confile >> minjm2x >> maxjm2x >> minjm2y >> maxjm2y >> minjm2z >> maxjm2z;
    confile >> minJrnd >> maxJrnd;
    confile >> nbin;

    if( minx == maxx )
    {
      if( minx != 0 )
      {
      	minx *= 0.9;
        maxx *= 1.1;
      }
      else
      {
        minx = -0.1;
	maxx = +0.1;
      }
    }

    if( miny == maxy )
    {
      if( miny != 0 )
      {
        miny *= 0.9;
        maxy *= 1.1;
      }
      else
      {
        miny = -0.1;
	maxy = +0.1;
      }
    }
    if( minz == maxz )
    {
      if( minz != 0 )
      {
        minz *= 0.9;
        maxz *= 1.1;
      }
      else
      {
        minz = -0.1;
	maxz = +0.1;
      }
    }
    if( mnprob == mxprob )
    {
      mnprob *= 0.9;
      mxprob *= 1.1;
    }
    if( mnvol1 == mxvol1 )
    {
      mnvol1 *= 0.9;
      mxvol1 *= 1.1;
    }
    if( mnarea2 == mxarea2 )
    {
      mnarea2 *= 0.9;
      mxarea2 *= 1.1;
    }
    if( minJ1 == maxJ1 )
    {
      minJ1 *= 0.9;
      maxJ1 *= 1.1;
    }
    if( minjm2 == maxjm2 )
    {
      minjm2 *= 0.9;
      maxjm2 *= 1.1;
    }
    if( minjm2x == maxjm2x )
    {
      minjm2x *= 0.9;
      maxjm2x *= 1.1;
    }
    if( minjm2y == maxjm2y )
    {
      minjm2y *= 0.9;
      maxjm2y *= 1.1;
    }
    if( minjm2z == maxjm2z )
    {
      minjm2z *= 0.9;
      maxjm2z *= 1.1;
    }

    if( minJrnd == 1e20 && maxJrnd == -1e20 )
    {
      minJrnd = -10;
      maxJrnd = +10;
    }
    else if( minJrnd == maxJrnd )
    {
      minJrnd *= 0.9;
      maxJrnd *= 1.1;
    }

    if( verbose )
    {
      std::cout << "-- probability histogram [" << mnprob << "," << mxprob << "]" << std::endl;
      std::cout << "-- volume1 histogram [" << mnvol1 << "," << mxvol1 << "]" << std::endl;
      std::cout << "-- area2 histogram [" << mnarea2 << "," << mxarea2 << "]" << std::endl;
      std::cout << "-- distance histogram [" << mndst << "," << mxdst << "]" << std::endl;
      std::cout << "-- x histogram [" << minx << "," << maxx << "]" << std::endl;
      std::cout << "-- y histogram [" << miny << "," << maxy << "]" << std::endl;
      std::cout << "-- z histogram [" << minz << "," << maxz << "]" << std::endl;
      std::cout << "-- J1 histogram [" << minJ1 << "," << maxJ1 << "]" << std::endl;
      std::cout << "-- jm2 histogram [" << minjm2 << "," << maxjm2 << "]" << std::endl;
      std::cout << "-- jm2x histogram [" << minjm2x << "," << maxjm2x << "]" << std::endl;
      std::cout << "-- jm2y histogram [" << minjm2y << "," << maxjm2y << "]" << std::endl;
      std::cout << "-- jm2z histogram [" << minjm2z << "," << maxjm2z << "]" << std::endl;
      std::cout << "-- Jrnd histogram [" << minJrnd << "," << maxJrnd << "]" << std::endl;
    }

    confile.close();
    confread = true;
  }

  gsl_histogram * hprob = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hprob, mnprob, mxprob);
  else gsl_histogram_set_ranges_uniform ( hprob, 0, 1);

  gsl_histogram * hvol1 = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hvol1, mnvol1, mxvol1 );
  else gsl_histogram_set_ranges_uniform ( hvol1, 0, 1);

  gsl_histogram * harea2 = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( harea2, mnarea2, mxarea2 );
  else gsl_histogram_set_ranges_uniform ( harea2, 0, 1);

  gsl_histogram * hdst = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hdst, mndst, mxdst );
  else gsl_histogram_set_ranges_uniform ( hdst, 0, 1);

  gsl_histogram * hx = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hx, minx, maxx);
  else gsl_histogram_set_ranges_uniform ( hx, -1, +1);

  gsl_histogram * hy = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hy, miny, maxy);
  else gsl_histogram_set_ranges_uniform ( hy, -1, +1);

  gsl_histogram * hz = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hz, minz, maxz);
  else gsl_histogram_set_ranges_uniform ( hz, -1, +1);

  gsl_histogram2d * hxy = gsl_histogram2d_alloc( nbin, nbin );
  if( confread )gsl_histogram2d_set_ranges_uniform ( hxy, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hxy, -1, +1, -1, +1);

  gsl_histogram2d * hxz = gsl_histogram2d_alloc( nbin, nbin );
  if( confread ) gsl_histogram2d_set_ranges_uniform ( hxz, minx, maxx, minz, maxz);
  else gsl_histogram2d_set_ranges_uniform ( hxz, -1, +1, -1, +1);

  gsl_histogram2d * hyz = gsl_histogram2d_alloc( nbin, nbin );
  if( confread ) gsl_histogram2d_set_ranges_uniform ( hyz, miny, maxy, minz, maxz);
  else gsl_histogram2d_set_ranges_uniform ( hyz, -1, +1, -1, +1);

  gsl_histogram * hJ1 = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hJ1, minJ1, maxJ1);
  else gsl_histogram_set_ranges_uniform ( hJ1, 0, 1);

  gsl_histogram * hjm2 = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hjm2, minjm2, maxjm2);
  else gsl_histogram_set_ranges_uniform ( hjm2, 0, 1);

  gsl_histogram * hjm2x = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hjm2x, minjm2x, maxjm2x);
  else gsl_histogram_set_ranges_uniform ( hjm2x, 0, 1);

  gsl_histogram * hjm2y = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hjm2y, minjm2y, maxjm2y);
  else gsl_histogram_set_ranges_uniform ( hjm2y, 0, 1);

  gsl_histogram * hjm2z = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hjm2z, minjm2z, maxjm2z);
  else gsl_histogram_set_ranges_uniform ( hjm2z, 0, 1);

  gsl_histogram * hJrnd = gsl_histogram_alloc( nbin );
  if( confread ) gsl_histogram_set_ranges_uniform ( hJrnd, minJrnd, maxJrnd); 
  else gsl_histogram_set_ranges_uniform ( hJrnd, 0, 1);

  const gsl_rng_type * U;
  gsl_rng * r;

  U = gsl_rng_default;
  r = gsl_rng_alloc (U);

  double dFlux = 0, Flux = 0;

  int n1 = 0, n2 = 0;
  double prob1 = 0, prob2 = 0, prob = 0;
  double rij = 0;
  double J1 = 0;
  double jm2 = 0;
  double dM12 = 0, M12 = 0, Msqsum = 0;
  double var = 0, M12errmax = 0;
  double E21 = 0, E21errmax = 0;
  double Flux21 = 0, Flux21errmax = 0;
  double vtot = 0;
  double atot = 0;
  int nzero = 0;

  double xrnd = 0, yrnd = 0;
  double Jrms = 0, Jrnd = 0;
  double ux = 0, uy = 0, uz = 0, len = 0;

  if( nsim > 1 ) std::cout << "E21 E21errmax             Flux21  Flux21errmax" << std::endl;	 

  for( int j = 0; j < nsim; j++ )
  {
    vtot = 0;
    atot = 0;

    M12 = 0;
    dM12 = 0;
    Msqsum = 0;
    nzero = 0;

    n1 = 0, n2 = 0;
    J1 = 0, jm2 = 0;
    rij = 0;
    var = 0;
    prob1 = 0, prob2 = 0, prob = 0;

    // thermal noise current simulation
    if( nsim > 1 )
    {
      if( verbose ) std::cout << "-- new random thermal currents" << std::endl;
      for( int k = 0; k < np2; k++ )
      {
        // random direction vector of unit length
        gsl_ran_dir_2d(r, &xrnd, &yrnd);
        Jrms = sqrt( 4 * sigma * GSL_CONST_MKSA_BOLTZMANN * T / ( da[ k ] * deltas ) );
        Jrnd = gsl_ran_gaussian(r, Jrms);
        ux = jm2x[ k ];
	uy = jm2y[ k ];
	uz = jm2z[ k ];
	
	len = sqrt( (vx[k]+ux)*(vx[k]+ux) + (vy[k]+uy)*(vy[k]+uy) + (vz[k]+uz)*(vz[k]+uz) );

	jm2x[ k ] = Jrnd * ( xrnd * ux + yrnd * vx[ k ] ) / len;
	jm2y[ k ] = Jrnd * ( xrnd * uy + yrnd * vy[ k ] ) / len;
        jm2z[ k ] = Jrnd * ( xrnd * uz + yrnd * vz[ k ] ) / len;

        if( verbose ) std::cout << Jrms << ": " << jm2x[ k ] << " " << jm2y[ k ] << " " << jm2z[ k ] << std::endl;

	if( Jrnd > maxJrnd ) maxJrnd = Jrnd;
	if( Jrnd < minJrnd ) minJrnd = Jrnd;
	gsl_histogram_increment( hJrnd, Jrnd );
      }
    }

    for( int i = 0; i < nrand; i++ )
    {
      n1 = (int)( gsl_ran_flat (r, 0, np1) );
      n2 = (int)( gsl_ran_flat (r, 0, np2) );

      if( verbose ) std::cout << "-- " << i << " : " << n1 << " " << n2;

      // probability to select n1 and n2 segments
      prob1 = dv[ n1 ] / V;
      prob2 = da[ n2 ] / A;

      // probability to select n1 and n2 segments
      prob = prob1 * prob2;

      if( verbose ) std::cout << " " << prob1 << " " << prob2;

      // calculate distance between point 1 and 2
      rij = sqrt( (x1c[n1]-x2c[n2])*(x1c[n1]-x2c[n2])
                + (y1c[n1]-y2c[n2])*(y1c[n1]-y2c[n2])
                + (z1c[n1]-z2c[n2])*(z1c[n1]-z2c[n2]) ) ;

      J1 = sqrt( J1x[n1]*J1x[n1] + J1y[n1]*J1y[n1] + J1z[n1]*J1z[n1] );
      jm2 = sqrt( jm2x[n2]*jm2x[n2] + jm2y[n2]*jm2y[n2] + jm2z[n2]*jm2z[n2] );

      if( verbose ) std::cout <<  " " << rij << " m" << std::endl;

      if( rij > 0 )
      {
        dM12 = dv[ n1 ] * da[ n2 ]
             * ( J1x[n1] * jm2x[n2] + J1y[n1] * jm2y[n2] + J1z[n1] * jm2z[n2] )
             /( rij * prob );
        vtot += dv[ n1 ];
        atot += da[ n2 ];
      }
      else nzero++;

      M12 += dM12;
      Msqsum += dM12 * dM12;

      if( prob > mxprob ) mxprob = prob;
      if( prob < mnprob ) mnprob = prob;
      gsl_histogram_increment( hprob, prob);

      if( dv[ n1 ] > mxvol1 ) mxvol1 = dv[ n1 ];
      if( dv[ n1 ] < mnvol1 ) mnvol1 = dv[ n1 ];
      gsl_histogram_increment( hvol1, dv[ n1 ] );

      if( da[ n2 ] > mxarea2 ) mxarea2 = da[ n2 ];
      if( da[ n2 ] < mnarea2 ) mnarea2 = da[ n2 ];
      gsl_histogram_increment( harea2, da[ n2 ] );

      if( rij > mxdst ) mxdst = rij;
      if( rij < mndst ) mndst = rij;
      gsl_histogram_increment( hdst, rij );

      if( J1 > maxJ1 ) maxJ1 = J1;
      if( J1 < minJ1 ) minJ1 = J1;
      gsl_histogram_increment( hJ1, J1 );

      if( jm2 > maxjm2 ) maxjm2 = jm2;
      if( jm2 < minjm2 ) minjm2 = jm2;
      gsl_histogram_increment( hjm2, jm2 );

      if( jm2x[ n2 ] > maxjm2x ) maxjm2x = jm2x[ n2 ];
      if( jm2x[ n2 ] < minjm2x ) minjm2x = jm2x[ n2 ];
      gsl_histogram_increment( hjm2x, jm2x[ n2 ] );

      if( jm2y[ n2 ] > maxjm2y ) maxjm2y = jm2y[ n2 ];
      if( jm2y[ n2 ] < minjm2y ) minjm2y = jm2y[ n2 ];
      gsl_histogram_increment( hjm2y, jm2y[ n2 ] );

      if( jm2z[ n2 ] > maxjm2z ) maxjm2z = jm2z[ n2 ];
      if( jm2z[ n2 ] < minjm2z ) minjm2z = jm2z[ n2 ];
      gsl_histogram_increment( hjm2z, jm2z[ n2 ] );

      if( x1c[ n1 ] > maxx ) maxx = x1c[ n1 ];
      if( x2c[ n2 ] > maxx ) maxx = x2c[ n2 ];
      if( x1c[ n1 ] < minx ) minx = x1c[ n1 ];
      if( x2c[ n2 ] < minx ) minx = x2c[ n2 ];
      gsl_histogram_increment( hx, x1c[ n1 ] );
      gsl_histogram_increment( hx, x2c[ n2 ] );

      if( y1c[ n1 ] > maxy ) maxy = y1c[ n1 ];
      if( y2c[ n2 ] > maxy ) maxy = y2c[ n2 ];
      if( y1c[ n1 ] < miny ) miny = y1c[ n1 ];
      if( y2c[ n2 ] < miny ) miny = y2c[ n2 ];
      gsl_histogram_increment( hy, y1c[ n1 ] );
      gsl_histogram_increment( hy, y2c[ n2 ] );

      if( z1c[ n1 ] > maxz ) maxz = z1c[ n1 ];
      if( z2c[ n2 ] > maxz ) maxz = z2c[ n2 ];
      if( z1c[ n1 ] < minz ) minz = z1c[ n1 ];
      if( z2c[ n2 ] < minz ) minz = z2c[ n2 ];
      gsl_histogram_increment( hz, z1c[ n1 ] );
      gsl_histogram_increment( hz, z2c[ n2 ] );

      gsl_histogram2d_increment( hxy, x1c[ n1 ], y1c[ n1 ] );
      gsl_histogram2d_increment( hxy, x2c[ n2 ], y2c[ n2 ] );

      gsl_histogram2d_increment( hxz, x1c[ n1 ], z1c[ n1 ] );
      gsl_histogram2d_increment( hxz, x2c[ n2 ], z2c[ n2 ] );

      gsl_histogram2d_increment( hyz, y1c[ n1 ], z1c[ n1 ] );
      gsl_histogram2d_increment( hyz, y2c[ n2 ], z2c[ n2 ] );

    }

    nrand -= nzero; // correct for zero distance samples
    if( verbose ) std::cout << "-- " << nrand << " random samples" << std::endl;

    var = sqrt( Msqsum/nrand - (M12/nrand)*(M12/nrand) );

    M12 /= nrand;
    if( nsim > 1 ) M12 *= deltas;
    M12errmax = 3 * var / sqrt( (double)nrand );
    if( nsim > 1 ) M12errmax *= deltas;

    E21 = 0.5 * 1e-7 * sqrt( ur1 * ur2 ) * M12;
    E21errmax = 0.5 * 1e-7 * sqrt( ur1 * ur2 ) * M12errmax;
    if( nsim < 2 ) std::cout << "-- E21 " << E21 << " +- " << E21errmax << " J" << std::endl;

    Flux21 = 1e-7 * sqrt( ur1 * ur2 ) * M12 / I1;
    Flux21errmax = 1e-7 * sqrt( ur1 * ur2 ) * M12errmax / I1;
    if( nsim < 2 ) std::cout << "-- Flux21 " << Flux21 << " +- " << Flux21errmax << " Wb" << std::endl;

    if( nsim > 1 ) std::cout << E21 << " " << E21errmax << " " << Flux21 << " " << Flux21errmax << std::endl;

    vtot /= (double)nrand;
    atot /= (double)nrand;

    if( verbose ) std::cout << "-- average volume " << vtot << " m^3 and area " 
	  << atot << " m^2" << std::endl;
  }

  if( minjm2x == maxjm2x )
  {
    if( minjm2x != 0 )
    {
      minjm2x *= 0.9;
      maxjm2x *= 1.1;
    }
    else
    {
      minjm2x = -0.1;
      maxjm2x = +0.1;
    }
  }

  if( minjm2y == maxjm2y )
  {
    if( minjm2y != 0 )
    {
      minjm2y *= 0.9;
      maxjm2y *= 1.1;
    }
    else
    {
      minjm2y = -0.1;
      maxjm2y = +0.1;
    }
  }

  if( minjm2z == maxjm2z )
  {
    if( minjm2z != 0 )
    {
      minjm2z *= 0.9;
      maxjm2z *= 1.1;
    }
    else
    {
      minjm2z = -0.1;
      maxjm2z = +0.1;
    }
  }

  std::ofstream outfile( ".fluxind" );
  if( !outfile.good() )
  {
    std::cout << "Could not write '.fluxind' file" << std::endl;
  }
  else
  {
    outfile << mnprob << " " << mxprob << " ";
    outfile << mnvol1 << " " << mxvol1 << " " << mnarea2 << " " << mxarea2 << " ";
    outfile << mndst << " " << mxdst << std::endl;
    outfile << minx << " " << maxx << " " << miny << " " << maxy << " ";
    outfile << minz << " " << maxz << std::endl;
    outfile << minJ1 << " " << maxJ1 << " " << minjm2 << " " << maxjm2 << std::endl;
    outfile << minjm2x << " " << maxjm2x << " " << minjm2y << " " << maxjm2y << " " << minjm2z << " " << maxjm2z << std::endl;
    outfile << minJrnd << " " << maxJrnd << std::endl;
    outfile << nbin << std::endl;
    outfile.close();
  }

  FILE *outhprob;
  outhprob = fopen( "hprob.txt" , "w");
  if( outhprob == NULL )
  {
    std::cout << "Could not write 'hprob.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhprob, hprob, "%g", "%g");
    fclose( outhprob );
  }
  gsl_histogram_free( hprob );

  FILE *outhvol1;
  outhvol1 = fopen( "hvol1.txt" , "w");
  if( outhvol1 == NULL )
  {
    std::cout << "Could not write 'hvol1.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhvol1, hvol1, "%g", "%g");
    fclose( outhvol1 );
  }
  gsl_histogram_free( hvol1 );

  FILE *outharea2;
  outharea2 = fopen( "harea2.txt" , "w");
  if( outharea2 == NULL )
  {
    std::cout << "Could not write 'harea2.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outharea2, harea2, "%g", "%g");
    fclose( outharea2 );
  }
  gsl_histogram_free( harea2 );

  FILE *outhdst;
  outhdst = fopen( "hdst.txt" , "w");
  if( outhdst == NULL )
  {
    std::cout << "Could not write 'hdst.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhdst, hdst, "%g", "%g");
    fclose( outhdst );
  }
  gsl_histogram_free( hdst );

  FILE *outhx;
  outhx = fopen( "hx.txt" , "w");
  if( outhx == NULL )
  {
    std::cout << "Could not write 'hx.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhx, hx, "%g", "%g");
    fclose( outhx );
  }
  gsl_histogram_free( hx );

  FILE *outhy;
  outhy = fopen( "hy.txt" , "w");
  if( outhy == NULL )
  {
    std::cout << "Could not write 'hy.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhy, hy, "%g", "%g");
    fclose( outhy );
  }
  gsl_histogram_free( hy );

  FILE *outhz;
  outhz = fopen( "hz.txt" , "w");
  if( outhz == NULL )
  {
    std::cout << "Could not write 'hz.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhz, hz, "%g", "%g");
    fclose( outhz );
  }
  gsl_histogram_free( hz );

  FILE *outJ1;
  outJ1 = fopen( "hJ1.txt" , "w");
  if( outJ1 == NULL )
  {
    std::cout << "Could not write 'hJ1.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outJ1, hJ1, "%g", "%g");
    fclose( outJ1 );
  }
  gsl_histogram_free( hJ1 );

  FILE *outjm2;
  outjm2 = fopen( "hjm2.txt" , "w");
  if( outjm2 == NULL )
  {
    std::cout << "Could not write 'hjm2.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outjm2, hjm2, "%g", "%g");
    fclose( outjm2 );
  }
  gsl_histogram_free( hjm2 );

  FILE *outjm2x;
  outjm2x = fopen( "hjm2x.txt" , "w");
  if( outjm2x == NULL )
  {
    std::cout << "Could not write 'hjm2x.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outjm2x, hjm2x, "%g", "%g");
    fclose( outjm2x );
  }
  gsl_histogram_free( hjm2x );

  FILE *outjm2y;
  outjm2y = fopen( "hjm2y.txt" , "w");
  if( outjm2y == NULL )
  {
    std::cout << "Could not write 'hjm2y.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outjm2y, hjm2y, "%g", "%g");
    fclose( outjm2y );
  }
  gsl_histogram_free( hjm2y );

  FILE *outjm2z;
  outjm2z = fopen( "hjm2z.txt" , "w");
  if( outjm2z == NULL )
  {
    std::cout << "Could not write 'hjm2z.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outjm2z, hjm2z, "%g", "%g");
    fclose( outjm2z );
  }
  gsl_histogram_free( hjm2z );

  FILE *outJrnd;
  outJrnd = fopen( "hJrnd.txt" , "w");
  if( outJrnd == NULL )
  {
    std::cout << "Could not write 'hJrnd.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outJrnd, hJrnd, "%g", "%g");
    fclose( outJrnd );
  }
  gsl_histogram_free( hJrnd );

  FILE *outhxy;
  outhxy = fopen( "hxy.txt" , "w");
  if( outhxy == NULL )
  { 
    std::cout << "Could not write 'hxy.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram2d_fprintf( outhxy, hxy, "%g", "%g");
    fclose( outhxy );
  }
  gsl_histogram2d_free( hxy );

  FILE *outhxz;
  outhxz = fopen( "hxz.txt" , "w");
  if( outhxz == NULL )
  {
    std::cout << "Could not write 'hxz.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram2d_fprintf( outhxz, hxz, "%g", "%g");
    fclose( outhxz );
  }
  gsl_histogram2d_free( hxz );

  FILE *outhyz;
  outhyz = fopen( "hyz.txt" , "w");
  if( outhyz == NULL )
  {
    std::cout << "Could not write 'hyz.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram2d_fprintf( outhyz, hyz, "%g", "%g");
    fclose( outhyz );
  }
  gsl_histogram2d_free( hyz );

  gsl_rng_free( r );

  return 0;
}
