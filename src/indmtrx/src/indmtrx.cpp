//
// Calculate inductance between two spatial current distributions
// using Neumann formula with Monte Carlo integration. Note that Neumann 
// formula has singularity for self-inductance.
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// The file1.yaml and file2.yaml have parametic definition of coil integration
// or file name for list of coordinates, current densities and volume of small
// element in this position.
//
// x y z Jx Jy Jz dv
// ...
//
// The yaml-files define rotation around axis (ax, ay, az) by angle theta. 
// After rotation the volume is translated by vector (xt, yt, zt).
//
// Model for small fractional inductance is
//
// dM12 = 1e-7 * sqrt( ur1 * ur2 ) * dv1 * dv2 * J1 . J2 /( r12 * prob * I1 * I2)
//
// Here prob = prob1 * prob2 with prob1 = dv1/V1 and prob2 = dv2/V2.
//
// Fractional force between two volume elements is
//
// dF12 = 1e-7 * sqrt( ur1 * ur2 ) * dv1 * dv2 * J2 x ( J1 x ( r2 - r1 ) ) / ( r12^3 * prob )
//
// 10^nrand random samples are taken from volumes V1 and V2. Average of
// dM12 gives estimate of total mutual inductance between the volumes.
// Error is estimated from variance of these samples.
//
// I1 is current flowing in volume V1 and I2 current in V2. These were used
// to generate file1.pdv and file2.pdv. Otherwise they need to be estimated
// from the data.
//
// Minimum and maximum values, and number of bins for histograms are read
// from file '.indmtrx.yaml' if available. This file is written with new
// values before program exits. The histograms are:
//
// - x, y, z position of randomly taken volume sample
// - element volume dv1 and dv2
// - distance between elements
// - probability to select these two elements
// - volume current density J1 and J2
//
// Random thermal noise currents can be simulated by giving electrical
// conductivity sigma [S/m], temperature T [K] and number of different
// random direction current density Jrnd distributions in volume 2.
// The J2 in file2.pdv is ignored in this case. The length of current
// vector in dv2 is taken from random Gaussian distribution with
// standard deviation Jrnd = sqrt( 4 * sigma * kB * T / dv2 ).
//
// Fri Apr 19 10:43:36 CDT 2019
// Edit: Wed Jul  3 02:28:49 PM CDT 2024
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_histogram2d.h>
#include <yaml-cpp/yaml.h>
#include "iiflucks.h"
#include "constants.h"
#include "Histograms.h"
#include "MonteCarlo.h"

void printusage()
{
  std::cout << "Usage: indmtrx file1.yaml file2.yaml Nrand [v|d]" << std::endl;
}

void printversion()
{
  std::cout << "indmtrx v. 20240703, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Calculate inductance between two spatial current distributions using Neumann formula with Monte Carlo integration. Note that Neumann formula has singularity for self-inductance." << std::endl;
  std::cout << std::endl;
  std::cout << "The file1.yaml and file2.yaml have parametric definition of coil integration or file name for list of coordinates, current densities and volume of small element in this position." << std::endl;
  std::cout << std::endl;
  std::cout << "x y z Jx Jy Jz dv" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "The yaml-files define rotation around axis (ax, ay, az) by angle theta. After rotation the volume is translated by vector (xt, yt, zt)." << std::endl;
  std::cout << std::endl;
  std::cout << "Model for small fractional inductance is" << std::endl;
  std::cout << std::endl;
  std::cout << "dM12 = 1e-7 * sqrt( ur1 * ur2 ) * dv1 * dv2 * J1 . J2 /( r12 * prob * I1 * I2)" << std::endl;
  std::cout << std::endl;
  std::cout << "Here prob = prob1 * prob2 with prob1 = dv1/V1 and prob2 = dv2/V2." << std::endl;
  std::cout << std::endl;
  std::cout << "Fractional force between two volume elements is" << std::endl;
  std::cout << std::endl;
  std::cout << "dF12 = 1e-7 * sqrt( ur1 * ur2 ) * dv1 * dv2 * J2 x ( J1 x ( r2 - r1 ) ) / ( r12^3 * prob )" << std::endl;
  std::cout << std::endl;
  std::cout << "10^nrand random samples are taken from volumes V1 and V2. Average of dM12 gives estimate of total mutual inductance between the volumes. Error is estimated from variance of these samples." << std::endl;
  std::cout << std::endl;
  std::cout << "I1 is current flowing in volume V1 and I2 current in V2. These were used to generate file1.pdv and file2.pdv. Otherwise they need to be estimated from the data." << std::endl;
  std::cout << std::endl;
  std::cout << "Minimum and maximum values, and number of bins for histograms are read from file '.indmtrx.yaml' if available. This file is written with new values before program exits." << std::endl;
  std::cout << std::endl;
  std::cout << "File '.indmtrx.yaml' can be edited for histogram maximum and minimum values but not for number of bins. This file is overwritten with new histogram ranges and good values need to be kept in a separate file." << std::endl;
  std::cout << std::endl;
//  std::cout << "Random thermal noise currents can be simulated by giving electrical conductivity sigma [S/m], temperature T [K] and number of different random direction current density Jrnd distributions in volume 2. The J2 in file2.pdv is ignored in this case. The length of current vector in dv2 is taken from random Gaussian distribution with standard deviation Jrnd = sqrt( 4 * sigma * kB * T / dv2 )." << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  bool debug = false;
  bool test = false;

  double theta = 0; // rotation angle around axis

  double nr = 0; // number of random samples nrand = 10^nr
  double I1 = 0; // current in first volume
  double I2 = 0; // current in second volume

//  double sigma = 0; // electrical conductivity [S/m]
//  double T = 0; // temperature [K]
//  int nsim = 1; // number of different current distributions

  std::vector<double> q, q2, qc, qh; // test quartenions
  std::vector<double> a, v; // axis of rotation and vector to rotate
  std::vector<double> axis, tr; // axis of rotation and translation 

  double dt, t1, t2, du, u1, u2; // coil parametric curves
  double dr, r1, r2, drb, r1b, r2b; // radial integration on cross section
  double lc1, lc2; // integration length scale
  std::vector<double> t1v, t2v, u1v, u2v;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file10", 13 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlHistogramRanges( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file1", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlHelix( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file2", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlRing( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file3", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlLine( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file4", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlRingCylinder( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file5", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlRacetrackCylinder( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file6", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlRacetrackPlanar( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file7", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlSolenoid( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file8", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlLineSphere( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-file9", 12 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testYamlLoopSphere( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-ind1", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testMonteCarlo( 1, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-ind2", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testMonteCarlo( 2, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-ind3", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testMonteCarlo( 3, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-ind4", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testMonteCarlo( 4, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-ind5", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testMonteCarlo( 5, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-ind6", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testMonteCarlo( 6, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-ind7", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testMonteCarlo( 7, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-ind8", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testMonteCarlo( 8, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-quat", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
    iiflucks::testQuaternion( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-calc", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    // conjugate quaternion 
    q.push_back( 1 );
    q.push_back( 2 );
    q.push_back( 3 );
    q.push_back( 4 );

    if( verbose ) std::cout << "q = {" << q[0] << "," << q[1] << "," << q[2] << "," << q[3] << "} conjugate ";

    iiflucks::ConjugateQuaternion(q, qc);

    std::cout << "{" << qc[0] << "," << qc[1] << "," << qc[2] << "," << qc[3] << "}";

    if( verbose ) std::cout << std::endl; else std::cout << " ";

    // Hamilton product q and q2 
    q2.push_back( 5 );
    q2.push_back( 6 );
    q2.push_back( 7 );
    q2.push_back( 8 );

    if( verbose ) std::cout << "q2 = {" << q[0] << "," << q[1] << "," << q[2] << "," << q[3] << "} Hamilton product q q2 = ";

    iiflucks::HamiltonProduct(q, q2, qh);

    std::cout << "{" << qh[0] << "," << qh[1] << "," << qh[2] << "," << qh[3] << "}";

    if( verbose ) std::cout << std::endl; else std::cout << " ";

    // rotate r around axis a by angle theta
    v.push_back( 1 );
    v.push_back( 1 );
    v.push_back( 1 );

    a.push_back( 1/sqrt(2) );
    a.push_back( 1/sqrt(2) );
    a.push_back( 0 );

//    theta = M_PI_2;
    theta = M_PI;

    if( verbose ) std::cout << "rotate v = (" << v[0] << "," << v[1] << "," << v[2] << ") around axis (" << a[0] << "," << a[1] << "," << a[2] << ") by angle " << theta << " =";

    iiflucks::QuaternionRotation(a, theta, v);

    std::cout << "(" << v[0] << "," << v[1] << "," << v[2] << ")";

    std::cout << std::endl;

    return 0;
  }
  else
  {
    if( argc != 4 && argc != 5 )
    {
      printusage();
      return 0;
    }

    if( argc == 5 ) verbose = true;
    if( argc == 5 && strncmp( argv[ 4 ], "d", 1 ) == 0 ) debug = true;

    nr = atof( argv[ 3 ] );

//    if( argc == 16 || argc == 17 )
//    {
//      sigma = atof( argv[ 13 ] );
//      T = atof( argv[ 14 ] );
//      nsim = atoi( argv[ 15 ] );
//      if( argc == 17 ) verbose = true;
//    }
  }

  int nzerom[ constants::matrixsize ][ constants::matrixsize  ]; // nzero matrix
  double Length1m[ constants::matrixsize ][ constants::matrixsize  ]; // length1 matrix
  double Length2m[ constants::matrixsize ][ constants::matrixsize  ]; // length2 matrix
  double V1m[ constants::matrixsize ][ constants::matrixsize  ]; // V1 matrix
  double V2m[ constants::matrixsize ][ constants::matrixsize ]; // V2 matrix
  double E12m[ constants::matrixsize ][ constants::matrixsize ]; // E12 matrix
  double E12errm[ constants::matrixsize ][ constants::matrixsize ]; // E12 error matrix
  double Flux12m[ constants::matrixsize ][ constants::matrixsize ]; // Flux12 matrix
  double Flux12errm[ constants::matrixsize ][ constants::matrixsize ]; // Flux12 error matrix
  double M12m[ constants::matrixsize ][ constants::matrixsize ]; // M12 matrix
  double M12errm[ constants::matrixsize ][ constants::matrixsize ]; // M12 error matrix
  double Fx12m[ constants::matrixsize ][ constants::matrixsize ]; // Fx12 matrix
  double Fx12errm[ constants::matrixsize ][ constants::matrixsize ]; // Fx12 error matrix
  double Fy12m[ constants::matrixsize ][ constants::matrixsize ]; // Fy12 matrix
  double Fy12errm[ constants::matrixsize ][ constants::matrixsize ]; // Fy12 error matrix
  double Fz12m[ constants::matrixsize ][ constants::matrixsize ]; // Fz12 matrix
  double Fz12errm[ constants::matrixsize ][ constants::matrixsize ]; // Fz12 error matrix

  Histograms *histograms = new Histograms("Inductance calculation");

  YAML::Node hist_ranges;

  bool ranges = true;
  try
  {
    hist_ranges = YAML::LoadFile( ".indmtrx.yaml" );
  }

  catch(...)
  {
    std::cerr << "Parsing '.indmtrx.yaml' failed, finding histogram ranges from data." << std::endl;
    ranges = false;
  }

  if( ranges ) histograms->ParseRangesYaml( hist_ranges );
  histograms->SetRanges( false );

  MonteCarlo *calc = new MonteCarlo("Mutual inductance", nr, 0, 0, histograms);
//  calc->TestCalc();
  if( debug ) calc->Debug();

  std::string fname1 = argv[ 1 ];
  std::string fname2 = argv[ 2 ];

  std::vector<YAML::Node> coils1 = YAML::LoadAllFromFile( fname1 );
  std::vector<YAML::Node> coils2 = YAML::LoadAllFromFile( fname2 );

  Helix * helix1 = nullptr;
  Helix * helix2 = nullptr;
  Ring * ring1 = nullptr;
  Ring * ring2 = nullptr;
  RingCylinder * ringcyl1 = nullptr;
  RingCylinder * ringcyl2 = nullptr;
  Line * line1 = nullptr;
  Line * line2 = nullptr;
  LineSphere * lsphere1 = nullptr;
  LineSphere * lsphere2 = nullptr;
  RacetrackCylinder * rtcyl1 = nullptr;
  RacetrackCylinder * rtcyl2 = nullptr;
  RacetrackPlanar * rtplanar1 = nullptr;
  RacetrackPlanar * rtplanar2 = nullptr;
  Solenoid * solenoid1 = nullptr;
  Solenoid * solenoid2 = nullptr;

  int nzero = 0;
  double length1, length2;
  int row = 0, nrow = 0, col = 0;
  bool file1 = false, file2 = false;
  std::string coil_pair;
  for(std::vector<YAML::Node>::const_iterator coil1 = coils1.begin(); coil1 != coils1.end(); ++coil1)
  {
    length1 = 0, length2 = 0;
    if( iiflucks::isHelixYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,helix1" << std::endl;
      helix1 = iiflucks::parseHelix( *coil1, dt, t1, t2, dr, r1, r2, I1, false );
      length1 = helix1->Length(t1, t2);
      coil_pair = "helix_";
    }
    else if( iiflucks::isRingYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,ring1" << std::endl;
      ring1 = iiflucks::parseRing( *coil1, dt, t1, t2, dr, r1, r2, I1, false );
      length1 = ring1->Length(t1, t2);
      coil_pair = "ring_";
    }
    else if( iiflucks::isRingCylinderYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,ringcylinder1" << std::endl;
      ringcyl1 = iiflucks::parseRingCylinder( *coil1, dt, t1, t2, dr, r1, r2, I1, false );
      length1 = ring1->Length(t1, t2);
      coil_pair = "ringcyl_";
    }
    else if( iiflucks::isLineYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,line1" << std::endl;
      line1 = iiflucks::parseLine( *coil1, dt, t1, t2, dr, r1, r2, I1, false );
      length1 = line1->Length(t1, t2);
      coil_pair = "line_";
    }
    else if( iiflucks::isLineSphereYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,linesphere1" << std::endl;
      lsphere1 = iiflucks::parseLineSphere( *coil1, dt, t1, t2, dr, r1, r2, I1, false );
      length1 = lsphere1->Length(t1, t2);
      coil_pair = "lsphere_";
    }
    else if( iiflucks::isRacetrackCylinderYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,racetrack_cylinder1" << std::endl;
      rtcyl1 = iiflucks::parseRacetrackCylinder( *coil1, lc1, t1v, t2v, dr, r1, r2, I1, false );
      length1 = rtcyl1->CalcLength( t1v, t2v, false );
      coil_pair = "rtcyl_";
    }
    else if( iiflucks::isRacetrackPlanarYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,racetrack_planar1" << std::endl;
      rtplanar1 = iiflucks::parseRacetrackPlanar( *coil1, lc1, t1v, t2v, dr, r1, r2, I1, false );
      length1 = rtplanar1->CalcLength( t1v, t2v, false );
      coil_pair = "rtplanar_";
    }
    else if( iiflucks::isSolenoidYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,solenoid" << std::endl;
      solenoid1 = iiflucks::parseSolenoid( *coil1, lc1, t1v, t2v, dr, r1, r2, I1, false );
      length1 = solenoid1->CalcLength( t1v, t2v, false );
      coil_pair = "solenoid_";
    }
    else if( iiflucks::isCurrentYaml( *coil1 ) )
    {
      if( verbose ) std::cout << "parse,pdv-file1" << std::endl;
      file1 = iiflucks::parseCurrent( *coil1, true, I1, theta, axis, tr, fname1 );
      coil_pair = "pdv_";
    }

    calc->SetI1( I1 );

    for(std::vector<YAML::Node>::const_iterator coil2 = coils2.begin(); coil2 != coils2.end(); ++coil2)
    {
      if( iiflucks::isHelixYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,helix2" << std::endl;
        helix2 = iiflucks::parseHelix( *coil2, du, u1, u2, drb, r1b, r2b, I2, false );
        length2 = helix2->Length(u1, u2);
        coil_pair += "helix";
      }
      else if( iiflucks::isRingYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,ring2" << std::endl;
        ring2 = iiflucks::parseRing( *coil2, du, u1, u2, drb, r1b, r2b, I2, false );
        length2 = ring2->Length(u1, u2);
        coil_pair += "ring";
      }
      else if( iiflucks::isRingCylinderYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,ringcylinder2" << std::endl;
        ringcyl2 = iiflucks::parseRingCylinder( *coil2, du, u1, u2, drb, r1b, r2b, I2, false );
        length2 = ringcyl2->Length(u1, u2);
        coil_pair += "ringcyl";
      }
      else if( iiflucks::isLineYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,line2" << std::endl;
        line2 = iiflucks::parseLine( *coil2, du, u1, u2, drb, r1b, r2b, I2, false );
        length2 = line2->Length(u1, u2);
        coil_pair += "line";
      }
      else if( iiflucks::isLineSphereYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,linesphere2" << std::endl;
        lsphere2 = iiflucks::parseLineSphere( *coil2, du, u1, u2, drb, r1b, r2b, I2, false );
	length2 = lsphere2->Length(u1, u2);
        coil_pair += "lsphere";
      }
      else if( iiflucks::isRacetrackCylinderYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,racetrack_cylinder2" << std::endl;
        rtcyl2 = iiflucks::parseRacetrackCylinder( *coil2, lc2, u1v, u2v, drb, r1b, r2b, I2, false );
        length2 = rtcyl2->CalcLength( u1v, u2v, false );
        coil_pair += "rtcyl";
      }
      else if( iiflucks::isRacetrackPlanarYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,racetrack_planar2" << std::endl;
        rtplanar2 = iiflucks::parseRacetrackPlanar( *coil2, lc2, u1v, u2v, drb, r1b, r2b, I2, false );
        length2 = rtplanar2->CalcLength( u1v, u2v, false );
        coil_pair += "rtplanar";
      }
      else if( iiflucks::isSolenoidYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,solenoid" << std::endl;
        solenoid2 = iiflucks::parseSolenoid( *coil2, lc2, u1v, u2v, drb, r1b, r2b, I2, false );
        length2 = solenoid2->CalcLength( u1v, u2v, false );
        coil_pair += "solenoid";
      }
      else if( iiflucks::isCurrentYaml( *coil2 ) )
      {
        if( verbose ) std::cout << "parse,pdv-file2" << std::endl;
        file2 = iiflucks::parseCurrent( *coil2, true, I2, theta, axis, tr, fname2 );
        coil_pair += "pdv";
      }

      calc->SetI2( I2 );

//      std::cout << "row = " << row << " col = " << col << std::endl;

      if( coil_pair == "helix_helix" ) nzero = calc->Run(helix1, helix2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "helix_line" ) nzero = calc->Run(helix1, line2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "helix_lsphere" ) nzero = calc->Run(helix1, lsphere2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "helix_rtcyl" ) nzero = calc->Run(helix1, rtcyl2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "helix_rtplanar" ) nzero = calc->Run(helix1, rtplanar2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "helix_ringcyl" ) nzero = calc->Run(helix1, ringcyl2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "helix_ring" ) nzero = calc->Run(helix1, ring2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "helix_solenoid" ) nzero = calc->Run(helix1, solenoid2, dt, t1, t2, lc2, u1v, u2v);

      if( coil_pair == "line_helix" ) nzero = calc->Run(line1, helix2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "line_line" ) nzero = calc->Run(line1, line2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "line_lsphere" ) nzero = calc->Run(line1, lsphere2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "line_rtcyl" ) nzero = calc->Run(line1, rtcyl2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "line_rtplanar" ) nzero = calc->Run(line1, rtplanar2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "line_ringcyl" ) nzero = calc->Run(line1, ringcyl2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "line_ring" ) nzero = calc->Run(line1, ring2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "line_solenoid" ) nzero = calc->Run(line1, solenoid2, dt, t1, t2, lc2, u1v, u2v);

      if( coil_pair == "lsphere_helix" ) nzero = calc->Run(lsphere1, helix2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "lsphere_line" ) nzero = calc->Run(lsphere1, line2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "lsphere_lsphere" ) nzero = calc->Run(lsphere1, lsphere2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "lsphere_rtcyl" ) nzero = calc->Run(lsphere1, rtcyl2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "lsphere_rtplanar" ) nzero = calc->Run(lsphere1, rtplanar2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "lsphere_ringcyl" ) nzero = calc->Run(lsphere1, ringcyl2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "lsphere_ring" ) nzero = calc->Run(lsphere1, ring2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "lsphere_solenoid" ) nzero = calc->Run(lsphere1, solenoid2, dt, t1, t2, lc2, u1v, u2v);

      if( coil_pair == "rtcyl_helix" ) nzero = calc->Run(rtcyl1, helix2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtcyl_line" ) nzero = calc->Run(rtcyl1, line2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtcyl_lsphere" ) nzero = calc->Run(rtcyl1, lsphere2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtcyl_rtcyl" ) nzero = calc->Run(rtcyl1, rtcyl2, lc1, t1v, t2v, lc2, u1v, u2v);
      if( coil_pair == "rtcyl_rtplanar" ) nzero = calc->Run(rtcyl1, rtplanar2, lc1, t1v, t2v, lc2, u1v, u2v);
      if( coil_pair == "rtcyl_ringcyl" ) nzero = calc->Run(rtcyl1, ringcyl2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtcyl_ring" ) nzero = calc->Run(rtcyl1, ring2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtcyl_solenoid" ) nzero = calc->Run(rtcyl1, solenoid2, lc1, t1v, t2v, lc2, u1v, u2v);

      if( coil_pair == "rtplanar_helix" ) nzero = calc->Run(rtplanar1, helix2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtplanar_line" ) nzero = calc->Run(rtplanar1, line2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtplanar_lsphere" ) nzero = calc->Run(rtplanar1, lsphere2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtplanar_rtcyl" ) nzero = calc->Run(rtplanar1, rtcyl2, lc1, t1v, t2v, lc2, u1v, u2v);
      if( coil_pair == "rtplanar_rtplanar" ) nzero = calc->Run(rtplanar1, rtplanar2, lc1, t1v, t2v, lc2, u1v, u2v);
      if( coil_pair == "rtplanar_ringcyl" ) nzero = calc->Run(rtplanar1, ringcyl2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtplanar_ring" ) nzero = calc->Run(rtplanar1, ring2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "rtplanar_solenoid" ) nzero = calc->Run(rtplanar1, solenoid2, lc1, t1v, t2v, lc2, u1v, u2v);

      if( coil_pair == "ringcyl_helix" ) nzero = calc->Run(ringcyl1, helix2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ringcyl_line" ) nzero = calc->Run(ringcyl1, line2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ringcyl_linesphere" ) nzero = calc->Run(ringcyl1, lsphere2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ringcyl_rtcyl" ) nzero = calc->Run(ringcyl1, rtcyl2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "ringcyl_rtplanar" ) nzero = calc->Run(ringcyl1, rtplanar2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "ringcyl_ringcyl" ) nzero = calc->Run(ringcyl1, ringcyl2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ringcyl_ring" ) nzero = calc->Run(ringcyl1, ring2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ringcyl_solenoid" ) nzero = calc->Run(ringcyl1, solenoid2, dt, t1, t2, lc2, u1v, u2v);

      if( coil_pair == "ring_helix" ) nzero = calc->Run(ring1, helix2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ring_line" ) nzero = calc->Run(ring1, line2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ring_linesphere" ) nzero = calc->Run(ring1, lsphere2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ring_rtcyl" ) nzero = calc->Run(ring1, rtcyl2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "ring_rtplanar" ) nzero = calc->Run(ring1, rtplanar2, dt, t1, t2, lc2, u1v, u2v);
      if( coil_pair == "ring_ringcyl" ) nzero = calc->Run(ring1, ringcyl2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ring_ring" ) nzero = calc->Run(ring1, ring2, dt, t1, t2, du, u1, u2);
      if( coil_pair == "ring_solenoid" ) nzero = calc->Run(ring1, solenoid2, dt, t1, t2, lc2, u1v, u2v);

      if( coil_pair == "solenoid_helix" ) nzero = calc->Run(solenoid1, helix2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "solenoid_line" ) nzero = calc->Run(solenoid1, line2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "solenoid_lsphere" ) nzero = calc->Run(solenoid1, lsphere2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "solenoid_rtcyl" ) nzero = calc->Run(solenoid1, rtcyl2, lc1, t1v, t2v, lc2, u1v, u2v);
      if( coil_pair == "solenoid_rtplanar" ) nzero = calc->Run(solenoid1, rtplanar2, lc1, t1v, t2v, lc2, u1v, u2v);
      if( coil_pair == "solenoid_ringcyl" ) nzero = calc->Run(solenoid1, ringcyl2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "solenoid_ring" ) nzero = calc->Run(solenoid1, ring2, lc1, t1v, t2v, du, u1, u2);
      if( coil_pair == "solenoid_solenoid" ) nzero = calc->Run(solenoid1, solenoid2, lc1, t1v, t2v, lc2, u1v, u2v);

      if( coil_pair == "pdv_helix" ) nzero = calc->Run_pdv(fname1, helix2, du, u1, u2, theta, axis, tr);
      if( coil_pair == "pdv_line" ) nzero = calc->Run_pdv(fname1, line2, du, u1, u2, theta, axis, tr);
      if( coil_pair == "pdv_lsphere" ) nzero = calc->Run_pdv(fname1, lsphere2, du, u1, u2, theta, axis, tr);
      if( coil_pair == "pdv_rtcyl" ) nzero = calc->Run_pdv(fname1, rtcyl2, lc2, u1v, u2v, theta, axis, tr);
      if( coil_pair == "pdv_rtplanar" ) nzero = calc->Run_pdv(fname1, rtplanar2, lc2, u1v, u2v, theta, axis, tr);
      if( coil_pair == "pdv_ringcyl" ) nzero = calc->Run_pdv(fname1, ringcyl2, du, u1, u2, theta, axis, tr);
      if( coil_pair == "pdv_ring" ) nzero = calc->Run_pdv(fname1, ring2, du, u1, u2, theta, axis, tr);
      if( coil_pair == "pdv_solenoid" ) nzero = calc->Run_pdv(fname1, solenoid2, lc2, u1v, u2v, theta, axis, tr);

      if( coil_pair == "pdv_pdv" ) nzero = calc->Run(fname1, fname2, theta, axis, tr);

      if( nzero < 0 )
      {
        std::cerr << "Calculation failed with nzero = " << nzero << std::endl;
	return -1;
      }

      if( verbose )
      {
        std::cout << argv[ 1 ] << " " << argv[ 2 ] << std::endl; 
        std::cout << "zero distance " << nzero << " times" << std::endl;
        std::cout << "length1 = " << length1 << " m, length2 = " << length2 << " m" << std::endl;
        std::cout << "V1 = " << calc->V1 << " m^3, V2 = " << calc->V2 << " m^3" << std::endl;
        std::cout << "E12 = " << calc->E12  << " +- " << calc->E12err << " J" << std::endl;
        std::cout << "Flux12 = " << calc->Flux12 << " +- " << calc->Flux12err << " Wb" << std::endl;
        std::cout << "M12 = " << calc->M12 << " +- " << calc->M12err << " H" << std::endl;
        std::cout << "F12 = (" << calc->F12[ 0 ] << " N, " << calc->F12[ 1 ] << " N, " << calc->F12[ 2 ] << " N)"	<< std::endl;
        std::cout << "+-(" << calc->F12err[ 0 ] << " N, " << calc->F12err[ 1 ] << " N, " << calc->F12err[ 2 ] << " N)"	<< std::endl;
      }
      else
      {
        std::cout << length1 << " " << length2 << " ";
        std::cout << calc->V1 << " " << calc->V2 << " ";
        std::cout << calc->M12 << " " << calc->M12err << " ";
        std::cout << calc->F12[ 0 ] << " " << calc->F12err[ 0 ] << " ";
        std::cout << calc->F12[ 1 ] << " " << calc->F12err[ 1 ] << " ";
        std::cout << calc->F12[ 2 ] << " " << calc->F12err[ 2 ];
        std::cout << std::endl;
      }

      nzerom[ row ][ col ] = calc->nzero;
      Length1m[ row ][ col ] = length1;
      Length2m[ row ][ col ] = length2;
      V1m[ row ][ col ] = calc->V1;
      V2m[ row ][ col ] = calc->V2;
      E12m[ row ][ col ] = calc->E12;
      E12errm[ row ][ col ] = calc->E12err;
      Flux12m[ row ][ col ] = calc->Flux12;
      Flux12errm[ row ][ col ] = calc->Flux12err;
      M12m[ row ][ col ] = calc->M12;
      M12errm[ row ][ col ] = calc->M12err;
      Fx12m[ row ][ col ] = calc->F12[ 0 ];
      Fx12errm[ row ][ col ] = calc->F12err[ 0 ];
      Fy12m[ row ][ col ] = calc->F12[ 1 ];
      Fy12errm[ row ][ col ] = calc->F12err[ 1 ];
      Fz12m[ row ][ col ] = calc->F12[ 2 ];
      Fz12errm[ row ][ col ] = calc->F12err[ 2 ];

      row++;

      if( row >= constants::matrixsize )
      {
        std::cerr << "maximum matrix row count " << constants::matrixsize << std::endl;
        return -1;
      }

    }
    nrow = row;
    row = 0;
    col++;

    if( col >= constants::matrixsize )
    {
      std::cerr << "maximum matrix column count " << constants::matrixsize << std::endl;
      return -1;
    }
  }

//  std::string name1 = fname1.substr(0, fname1.find_last_of("/"));
//  std::string name2 = fname2.substr(0, fname2.find_last_of("/"));
//  name1 = name1.substr(0, name1.find_last_of("."));
//  name2 = name2.substr(0, name2.find_last_of("."));
//  std::string prefix = name1 + "_" + name2;

  if( nrow > 1 || col > 1 )
  {
    if( verbose ) std::cout << "----- zero distance -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << nzerom[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- length1 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Length1m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- length2 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Length2m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- V1 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << V1m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- V2 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << V2m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- E12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << E12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- E12 error -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << E12errm[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Flux12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Flux12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Flux12 error -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Flux12errm[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- M12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << M12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- M12 error -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << M12errm[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fx12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fx12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fx12 error -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fx12errm[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fy12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fy12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fy12 error -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fy12errm[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fz12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fz12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fz12 error -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fz12errm[ i ][ j ];
      std::cout << std::endl;
    }
  }

  histograms->SaveRangesYaml(".indmtrx.yaml", false);

  histograms->ScaleValues();
  std::string prefix = "";
  histograms->SaveHistograms( prefix );

  return 0;
}
