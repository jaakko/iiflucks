//
// Read position-current_density-volume file (coil) and calculate magnetic
// field.
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Fri Jun  7 19:34:51 CDT 2019
// Edit: Sat Jun 15 20:18:26 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: probeB file.pdv ax ay az theta xt yt zt x2 y2 z2 [v]" << std::endl;
}

void printversion()
{
  std::cout << "probeB v. 20190615, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Read position-current_density-volume file (coil) and calculate magnetic field at position r2 = (x2, y2, z2)." << std::endl;
  std::cout << std::endl;
  std::cout << "The volume is rotated around axis (ax, ay, az) by angle theta. After rotation the volume is translated by vector (xt, yt, zt)." << std::endl; 
  std::cout << std::endl;
  std::cout << "Model for fractional field is" << std::endl;
  std::cout << std::endl;
  std::cout << "dB(r2) = u0/4*pi * J(r1) x (r2 - r1) * dv/|r2 - r1|^3" << std::endl;
  std::cout << std::endl;
  std::cout << "Here J = (Jx, Jy, Jz) is current density vector in volume dv at position r1." << std::endl;
}

int main(int argc, char **argv)
{

  bool verbose = false;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test-quat", 11 ) == 0)
  {
    if( argc == 3 ) verbose = true;

    iiflucks::testQuaternion( verbose );

    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test-vect", 11 ) == 0)
  {
    if( argc == 3 ) verbose = true;

    iiflucks::testVector( verbose );

    return 0;
  }

  if( argc < 12 )
  {
    printusage();
    return 0;
  }

  if( argc == 13 ) verbose = true;

  double ax = atof( argv[ 2 ] );// axis of rotation 
  double ay = atof( argv[ 3 ] );
  double az = atof( argv[ 4 ] );
  double theta = atof( argv[ 5 ] );
  double xt = atof( argv[ 6 ] ); // translation vector after rotation
  double yt = atof( argv[ 7 ] );
  double zt = atof( argv[ 8 ] );
  double x2 = atof( argv[ 9 ] ); // position where field is calculated 
  double y2 = atof( argv[ 10 ] );
  double z2 = atof( argv[ 11 ] );

  std::vector<double> a; // axis of rotation 
  std::vector<double> r; // r = r2 - r1  
  std::vector<double> J; // current density vector 
  std::vector<double> v; // vector used in rotations 
  std::vector<double> Jr21; // J(r1) x ( r2 - r1 ) 

  double anorm = 0; // length of rotation axis vector

  // position coordinates for small volume dv
  double xc, yc, zc, dv;

  // vector field with same position coordinates
  double Jx, Jy, Jz;

  std::ifstream infile( argv[ 1 ] );
  if( !infile.good() )
  {
    std::cout << "Could not open " << argv[ 1 ] << "\n";
  }

  anorm = sqrt( ax*ax + ay*ay + az*az );
  if( anorm > 0 && theta != 0 )
  {
    std::cout << "-- rotate volume around axis (" << ax << "," << ay << "," << az << ") by angle " << theta << std::endl;

    a.push_back( ax / anorm );
    a.push_back( ay / anorm );
    a.push_back( az / anorm );
  }

  std::cout << "-- translate volume with vector (" << xt << "," << yt << "," << zt << ")" << std::endl;

  double Bx = 0, By = 0, Bz = 0, dBx = 0, dBy = 0, dBz = 0;
  double r21 = 0;
  int np = 0;
  double V = 0;
  while( true )
  {
    infile >> xc >> yc >> zc;
    infile >> Jx >> Jy >> Jz;
    infile >> dv;

    if( !infile.good() ) break;

    if( verbose )
    {
      std::cout << "(" << xc << "," << yc << "," << zc << ") ";
      std::cout << "J = (" << Jx << "," << Jy << "," << Jz << ") ";
      std::cout << dv << std::endl;
    }

    if( anorm > 0 && theta != 0 )
    {
      v.erase( v.begin(), v.end() );
      v.push_back( xc );
      v.push_back( yc );
      v.push_back( zc );

      iiflucks::QuaternionRotation(a, theta, v);

      xc = v[ 0 ];
      yc = v[ 1 ];
      zc = v[ 2 ];

      v.erase( v.begin(), v.end() );
      v.push_back( Jx );
      v.push_back( Jy );
      v.push_back( Jz );

      iiflucks::QuaternionRotation(a, theta, v);

      Jx = v[ 0 ];
      Jy = v[ 1 ];
      Jz = v[ 2 ];
    }

    xc += xt;
    yc += yt;
    zc += zt;

    J.erase( J.begin(), J.end() );
    J.push_back( Jx );
    J.push_back( Jy );
    J.push_back( Jz );

    r.erase( r.begin(), r.end() );
    r.push_back( x2 - xc );
    r.push_back( y2 - yc );
    r.push_back( z2 - zc );

    r21 = iiflucks::vectorLength( r );

    iiflucks::crossProduct( J, r, Jr21 );

    dBx = Jr21[ 0 ] * dv / ( r21 * r21 * r21 );
    dBy = Jr21[ 1 ] * dv / ( r21 * r21 * r21 );
    dBz = Jr21[ 2 ] * dv / ( r21 * r21 * r21 );

    Bx += dBx;
    By += dBy;
    Bz += dBz;

    V += dv;
    np++;
  }

  infile.close();

  Bx *= 1e-7;
  By *= 1e-7;
  Bz *= 1e-7;

  if( verbose ) std::cout << "-- field (";
  std::cout << Bx << "," << By << "," << Bz;
  if( verbose ) std::cout << ")";
  std::cout << std::endl;

  if( verbose )  std::cout << "-- " << np << " points read with volume " << V << std::endl;

  return 0;
}
