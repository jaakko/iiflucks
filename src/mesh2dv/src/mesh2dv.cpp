//
// Read gmsh mesh file and print centroid of each tetrahedron element
// followed by tetrahedron volume. 
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// The file.msh has Gmsh 3D mesh to model current carrying volume. In addition
// file_J.pos is needed to calculate current density vector in each volume
// element. Verbosed printing of file processing can be seen with option 'v'.
// Details on how the mesh file describes the geometry can be found from
// Gmsh Reference Manual in section File formats. The section Gmsh API
// documents programming interface to the library libgmsh.
//
// Node tags and coordinates from mesh are read in with function call 
// 
// gmsh::model::mesh::getNodes() 
// 
// 3D elements are read in with 
//
// gmsh::model::mesh::getElements()
//
// The current density J data is read in with
//
// gmsh::view::getListData()
//
// List (vector) of elements is used to get node coordinates (vertices)
// of each tetrahedron. Centroid (xc, yc, zc) and volume dv of each 
// tetrahedron is calculated from these coordinates. Current density vector
// (Jx, Jy, Jz) is taken as average from values on each tetrahedron node. 
// This data is printed as list
// 
// xc, yc, zc, Jx, Jy, Jz, dv
// ...
//
// for each volume element.
//
// With option 'zeroJ' file_J.pos is not used and zero is written to each
// current density vector (Jx, Jy, Jz).
//
// With option '--test v' some test results are printed.
//
// Fri Apr 12 14:11:32 CDT 2019
// Edit: Sat Jun 15 18:56:31 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <cstdlib>
#include <string.h>
#include <gmsh.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: mesh2dv file scale [zeroJ] [v]" << std::endl;
}

void printversion()
{
  std::cout << "mesh2dv v. 20190615, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Read gmsh mesh file and print centroid of each tetrahedron element followed by tetrahedron volume." << std::endl;
  std::cout << std::endl; 
  std::cout << "The file.msh has Gmsh 3D mesh to model current carrying volume. In addition file_J.pos is needed to calculate current density vector in each volume element. Verbosed printing of file processing can be seen with option 'v'." << std::endl;
  std::cout << std::endl; 
  std::cout << "Node tags and coordinates from mesh are read in with function call" << std::endl; 
  std::cout << std::endl;
  std::cout << "gmsh::model::mesh::getNodes()" << std::endl;
  std::cout << std::endl; 
  std::cout << "3D elements are read in with" << std::endl; 
  std::cout << std::endl;
  std::cout << "gmsh::model::mesh::getElements()" << std::endl;
  std::cout << std::endl;
  std::cout << "The current density J data is read in with" << std::endl;
  std::cout << std::endl;
  std::cout << "gmsh::view::getListData()" << std::endl;
  std::cout << std::endl;
  std::cout << "List (vector) of elements is used to get node coordinates (vertices) of each tetrahedron. Centroid (xc, yc, zc) and volume dv of each tetrahedron is calculated from these coordinates. Current density vector (Jx, Jy, Jz) is taken as average from values on each tetrahedron node.  This data is printed as list" << std::endl;
  std::cout << std::endl;
  std::cout << "xc, yc, zc, Jx, Jy, Jz, dv" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "for each volume element." << std::endl;
  std::cout << std::endl;
  std::cout << "The 'scale' is used for linear scaling of dimensions." << std::endl;
  std::cout << std::endl;
  std::cout << "With option 'zeroJ' file_J.pos is not used and zero is written to each current density vector (Jx, Jy, Jz)." << std::endl;
  std::cout << std::endl;
}

void testFunctions(bool verbose)
{
  std::vector<double> P0, P1, P2, P3, Pc;

  // test tetrahedron volume calculation
  P0.push_back(0); P0.push_back(0); P0.push_back(0); 
  P1.push_back(0); P1.push_back(0); P1.push_back(1); 
  P2.push_back(1); P2.push_back(0); P2.push_back(1); 
  P3.push_back(0); P3.push_back(1); P3.push_back(1); 

  if( verbose )
  {
    std::cout << "-- tetrahedron ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- volume ";
  }

  std::cout << iiflucks::tetrahedronVolume(P0, P1, P2, P3);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  iiflucks::tetrahedronCentroid(P0, P1, P2, P3, Pc);

  if( verbose ) std::cout << "-- centroid ";
  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";
  if( verbose ) std::cout << std::endl; else std::cout << " ";

  std::vector<double> P4, P5;
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());
  Pc.erase (Pc.begin(), Pc.end());

  // test prism volume calculation
  P0.push_back(1); P0.push_back(0); P0.push_back(0); 
  P1.push_back(1); P1.push_back(1); P1.push_back(0); 
  P2.push_back(0); P2.push_back(0); P2.push_back(0); 
  P3.push_back(1); P3.push_back(0); P3.push_back(1); 
  P4.push_back(1); P4.push_back(1); P4.push_back(2); 
  P5.push_back(0); P5.push_back(0); P5.push_back(2); 

  if( verbose )
  {
    std::cout << "-- prism ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << "(" << P4[0] << "," << P4[1] << "," << P4[2] << ") ";
    std::cout << "(" << P5[0] << "," << P5[1] << "," << P5[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- volume approximate ";
  }

  std::cout << iiflucks::prismVolume(P0, P1, P2, P3, P4, P5);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  iiflucks::prismCentroid(P0, P1, P2, P3, P4, P5, Pc);

  if( verbose ) std::cout << "-- centroid ";
  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";
  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());
  P4.erase (P4.begin(), P4.end());
  Pc.erase (Pc.begin(), Pc.end());

  // test pyramid volume calculation
  P0.push_back(+1); P0.push_back(+1); P0.push_back(0); 
  P1.push_back(-1); P1.push_back(+1); P1.push_back(0); 
  P2.push_back(-1); P2.push_back(-1); P2.push_back(0); 
  P3.push_back(+1); P3.push_back(-1); P3.push_back(0); 
  P4.push_back(0); P4.push_back(0); P4.push_back(1);

  if( verbose )
  {
    std::cout << "-- pyramid ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << "(" << P4[0] << "," << P4[1] << "," << P4[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- volume ";
  }

  std::cout << iiflucks::pyramidVolume(P0, P1, P2, P3, P4);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  iiflucks::pyramidCentroid(P0, P1, P2, P3, P4, Pc);

  if( verbose ) std::cout << "-- centroid ";
  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";
  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  Pc.erase (Pc.begin(), Pc.end());

  // test triangle centroid calculation 
  P0.push_back(0); P0.push_back(0); P0.push_back(2); 
  P1.push_back(1); P1.push_back(0); P1.push_back(0); 
  P2.push_back(0); P2.push_back(1); P2.push_back(0); 

  if( verbose )
  {
    std::cout << "-- triangle ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- centroid ";
  }
  
  iiflucks::triangleCentroid(P0, P1, P2, Pc);
  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";
  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());

  // test vector length calculation 
  P0.push_back(1); P0.push_back(2); P0.push_back(3); 

  if( verbose )
  {
    std::cout << "-- vector ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- length ";
  }
  
  std::cout << iiflucks::vectorLength( P0 );

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());

  // test vector product calculation 
  P0.push_back(1); P0.push_back(1); P0.push_back(1); 
  P1.push_back(1); P1.push_back(-1); P1.push_back(-1); 

  if( verbose )
  {
    std::cout << "-- vector product ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") x ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") = ";
  }

  iiflucks::crossProduct(P0, P1, P2);

  std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());

  // test quadrangle area calculation
  P0.push_back(0); P0.push_back(0); P0.push_back(0); 
  P1.push_back(3); P1.push_back(0); P1.push_back(0); 
  P2.push_back(4); P2.push_back(2); P2.push_back(0); 
  P3.push_back(-2); P3.push_back(2); P3.push_back(0); 

  if( verbose )
  {
    std::cout << "-- quadrangle ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- area ";
  }

  std::cout << iiflucks::quadrangleArea(P0, P1, P2, P3);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());

  // test triple product calculation
  P0.push_back(0); P0.push_back(2); P0.push_back(0); 
  P1.push_back(1); P1.push_back(0); P1.push_back(0); 
  P2.push_back(0); P2.push_back(0); P2.push_back(5); 

  if( verbose )
  {
    std::cout << "-- triple product ";
    std::cout << "[(" << P0[0] << "," << P0[1] << "," << P0[2] << "), ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << "), ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")] = ";
  }

  std::cout << iiflucks::tripleProduct(P0, P1, P2);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());

  // test P0 distance from plane spanned by two vectors P2 and P3
  // P1 is on the plane
  P0.push_back(0); P0.push_back(1); P0.push_back(0); 
  P1.push_back(0); P1.push_back(0); P1.push_back(1); 
  P2.push_back(0); P2.push_back(0); P2.push_back(1); 
  P3.push_back(1); P3.push_back(1); P3.push_back(0); 

  if( verbose )
  {
    std::cout << "-- plane vectors ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << "point on plane ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "distance to ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") = ";
  }

  std::cout << iiflucks::distancePointPlane(P0, P1, P2, P3);

  std::cout << std::endl;

}

int main(int argc, char **argv)
{
  bool debug = false;
  bool verbose = false;
  bool zeroJ = false;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ],  "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ],  "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test-geom", 11 ) == 0)
  {
    if( argc == 3) verbose = true;
    iiflucks::testGeometry( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test-vect", 11 ) == 0)
  {
    if( argc == 3) verbose = true;
    iiflucks::testVector( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test", 6 ) == 0)
  {
    if( argc == 3) verbose = true;
    testFunctions( verbose );
    return 0;
  }

  double scale = atof( argv[ 2 ] );

  if( argc > 4 ) 
  {
    if( strncmp( argv[ 3 ], "zeroJ", 5 ) == 0 ) zeroJ = true; 
    verbose = true;
  }
  else if( argc == 4 )
  {
    if( strncmp( argv[ 3 ], "v", 1 ) == 0 ) verbose = true;
    else if( strncmp( argv[ 3 ], "zeroJ", 5 ) == 0 ) zeroJ = true; 
  }

  gmsh::initialize();

  if( verbose ) gmsh::option::setNumber("General.Terminal", 1);

  std::string meshFile = argv[ 1 ];
  meshFile = meshFile + ".msh";
  gmsh::open( meshFile );

  std::string dataFile = argv[ 1 ];
  dataFile = dataFile + "_J.pos";
  if( !zeroJ ) gmsh::merge( dataFile );

  // get all node coordinates
  std::vector<std::size_t> nodeTags;
  std::vector<double> nodeCoords, nodeParams;
  gmsh::model::mesh::getNodes(nodeTags, nodeCoords, nodeParams, -1, -1);

  // hash to get correct node tag fast
  if( verbose ) std::cout << "-- create node hash" << std::endl;
  unsigned int max_node_tag = nodeTags.size()+1;
  unsigned int nodeHash[ max_node_tag ];
  for( unsigned int i = 0; i < max_node_tag; i++)
  {
    if( nodeTags[ i ] >= 1 && nodeTags[ i ] <= max_node_tag )
    {
      nodeHash[ nodeTags[ i ] ] = i;
      if( verbose ) std::cout << nodeTags[ i ] << " -> " << i << std::endl;
    }
  }

  // get all 3D elements
  std::vector<int> elemTypes;
  std::vector<std::vector<std::size_t> > elemTags, elemNodeTags;
  gmsh::model::mesh::getElements(elemTypes, elemTags, elemNodeTags, -1, -1);

  if( debug )
  {
    std::cout << "-- " << elemTags.size() << " element tags" << std::endl;
    std::cout << "-- " << elemNodeTags.size() << " element node tags" << std::endl;
    std::cout << "-- " << elemTypes.size() << " element types" << std::endl;

  }

  if( verbose )
  {
    std::cout << "element type   number of element tags" << std::endl;
    for( unsigned int i = 0; i < elemTags.size(); i++)
      std::cout << elemTypes[i] << "              " 
	        << elemTags[ i ].size() << std::endl; 

    std::cout <<  "element type   number of element node tags" << std::endl;
    for( unsigned int i = 0; i < elemNodeTags.size(); i++)
      std::cout << elemTypes[i] << "              " 
                << elemNodeTags[ i ].size() << std::endl; 

//    std::cout << "-- mesh element tags: ";
//    for( unsigned int i = 0; i < 4; i++)
//      std::cout << " " << elemTags[1][i];
//    std::cout << std::endl;

//    std::cout << "-- sample of mesh element type 4 (tetrahedra) tags: " << std::endl;
//    for( unsigned int i = 0; i < 4; i++)
//      if( elemTypes[ i ] == 4 )
//      {
//        for( unsigned int j = 0; j < 10; j++)
//      		std::cout << " " << elemTags[i][j];
//        std::cout << std::endl;
//      }

//    std::cout << "-- sample of mesh element type 4 (tetrahedra) node tags: " 
//	      << std::endl;
//    for( unsigned int i = 0; i < 4; i++)
//      if( elemTypes[ i ] == 4 )
//      {
//        for( unsigned int j = 0; j < 10; j++)
//      		std::cout << " " << elemNodeTags[i][j];
//        std::cout << std::endl;
//      }
  }

  std::vector<int> viewTags;
  std::vector<std::string> dataTypes;
  std::vector<int> numElements;
  std::vector<std::vector<double> > data;

  if( !zeroJ )
  {
    gmsh::view::getTags( viewTags );

    if( viewTags.size() == 1)
    {
      gmsh::view::getListData(viewTags[0], dataTypes, numElements, data);
      if( verbose )
      {
        std::cout << "-- " << numElements[0] << " data elements" << std::endl;
        for( unsigned int i = 0; i < dataTypes.size(); i++)
          std::cout << dataTypes[ i ] << " ";
        for( unsigned int i = 0; i < numElements.size(); i++)
          std::cout << numElements[ i ] << " ";
        std::cout << std::endl;
      }
    }
  }

  // volume weight center coordinates
  double xc = 0, yc = 0, zc =0;

  // minimum and maximum coordinates for bounding box
  double xmin = 1e20, xmax = -1e20;
  double ymin = 1e20, ymax = -1e20;
  double zmin = 1e20, zmax = -1e20;

//  std::cout << "-- sample of mesh element type 4 (tetrahedra) node coordinates (bug here): " << std::endl;

  int row4 = -1; 
  for( unsigned int i = 0; i < elemTags.size(); i++) 
    if( elemTypes[ i ] == 4 ) row4 = i;

  std::vector<double> P0, P1, P2, P3, P4, P5, Pc;
  double Jx = 0, Jy = 0, Jz = 0;
  double dV=-1; // element volume
  double V = 0; // total volume

  unsigned int elemNodeTag = 0;
  unsigned int elemNodeTagHash = 0;
  double nodex = 0, nodey = 0, nodez = 0;

  int ntetrahed = 0;
  if( row4 < 0 ) 
  {
    std::cout << "-- no tetrahedra found" << std::endl;
  }
  else
  {
    if( verbose ) std::cout << "-- " << elemNodeTags[ row4 ].size() 
	    << " element node tags\n";

    for( unsigned int j = 0; j < elemNodeTags[ row4 ].size() ; j+=4)
    {
      Jx = 0; Jy = 0; Jz = 0;

      elemNodeTag = elemNodeTags[ row4 ][ j ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug )
        std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P0.erase (P0.begin(), P0.end());
      P0.push_back( nodex/scale );
      P0.push_back( nodey/scale );
      P0.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      if( !zeroJ )
      {
        if( fabs( P0[0] - data[0][ 6 * elemNodeTagHash ] ) > 1e-6 || fabs( P0[1] - data[0][ 6 * elemNodeTagHash + 1 ] ) > 1e-6 || fabs( P0[2] - data[0][ 6 * elemNodeTagHash + 2 ] ) > 1e-6 )
        {
          std::cout << "-- " << ntetrahed << " P0 does not matches vector data position" << std::endl;
          std::cout << "P0(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
          std::cout << " data(" << data[0][ 6 * elemNodeTagHash ] << ","
  	            << data[0][ 6 * elemNodeTagHash + 1 ] << "," 
                    << data[0][ 6 * elemNodeTagHash + 2 ] << ")" << std::endl;
        }
        else
        {
          Jx = data[ 0 ][ 6 * elemNodeTagHash + 3 ];
          Jy = data[ 0 ][ 6 * elemNodeTagHash + 4 ];
          Jz = data[ 0 ][ 6 * elemNodeTagHash + 5 ];
          if( debug ) std::cout << "-- " << ntetrahed << " J = (" << Jx << ","
                      << Jy << "," << Jz << ")" << std::endl;
        }
      }

      elemNodeTag = elemNodeTags[ row4 ][ j + 1 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash;

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P1.erase (P1.begin(), P1.end());
      P1.push_back( nodex/scale );
      P1.push_back( nodey/scale );
      P1.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      if( !zeroJ )
      {
        if( fabs( P1[0] - data[0][ 6 * elemNodeTagHash ] ) > 1e-6 || fabs( P1[1] - data[0][ 6 * elemNodeTagHash + 1 ] ) > 1e-6 || fabs( P1[2] - data[0][ 6 * elemNodeTagHash + 2 ] ) > 1e-6 )
        {
          std::cout << "-- " << ntetrahed << " P1 does not matches vector data position" << std::endl;
          std::cout << "P1(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
          std::cout << " data(" << data[0][ 6 * elemNodeTagHash ] << ","
                    << data[0][ 6 * elemNodeTagHash + 1 ] << "," 
                    << data[0][ 6 * elemNodeTagHash + 2 ] << ")" << std::endl;
        }
        else
        {
          Jx += data[ 0 ][ 6 * elemNodeTagHash + 3 ];
          Jy += data[ 0 ][ 6 * elemNodeTagHash + 4 ];
          Jz += data[ 0 ][ 6 * elemNodeTagHash + 5 ];
          if( debug ) std::cout << "-- " << ntetrahed << " J = (" << Jx << ","
                                << Jy << "," << Jz << ")" << std::endl;
        }
      }

      elemNodeTag = elemNodeTags[ row4 ][ j + 2 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash;

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P2.erase (P2.begin(), P2.end());
      P2.push_back( nodex/scale );
      P2.push_back( nodey/scale );
      P2.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      if( !zeroJ )
      {
        if( fabs( P2[0] - data[0][ 6 * elemNodeTagHash ] ) > 1e-6 || fabs( P2[1] - data[0][ 6 * elemNodeTagHash + 1 ] ) > 1e-6 || fabs( P2[2] - data[0][ 6 * elemNodeTagHash + 2 ] ) > 1e-6 )
        {
          std::cout << "-- " << ntetrahed << " P2 does not matches vector data position" << std::endl;
          std::cout << "P2(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
          std::cout << " data(" << data[0][ 6 * elemNodeTagHash ] << ","
                    << data[0][ 6 * elemNodeTagHash + 1 ] << "," 
                    << data[0][ 6 * elemNodeTagHash + 2 ] << ")" << std::endl;
        }
        else
        {
          Jx += data[ 0 ][ 6 * elemNodeTagHash + 3 ];
          Jy += data[ 0 ][ 6 * elemNodeTagHash + 4 ];
          Jz += data[ 0 ][ 6 * elemNodeTagHash + 5 ];
          if( debug ) std::cout << "-- " << ntetrahed << " J = (" << Jx << ","
                                << Jy << "," << Jz << ")" << std::endl;
        }
      }

      elemNodeTag = elemNodeTags[ row4 ][ j + 3 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash;

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P3.erase (P3.begin(), P3.end());
      P3.push_back( nodex/scale );
      P3.push_back( nodey/scale );
      P3.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      if( !zeroJ )
      {
        if( fabs( P3[0] - data[0][ 6 * elemNodeTagHash ] ) > 1e-6 || fabs( P3[1] - data[0][ 6 * elemNodeTagHash + 1 ] ) > 1e-6 || fabs( P3[2] - data[0][ 6 * elemNodeTagHash + 2 ] ) > 1e-6 )
        {
          std::cout << "-- " << ntetrahed << " P3 does not matches vector data position" << std::endl;
          std::cout << "P3(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
          std::cout << " data(" << data[0][ 6 * elemNodeTagHash ] << ","
                    << data[0][ 6 * elemNodeTagHash + 1 ] << "," 
                    << data[0][ 6 * elemNodeTagHash + 2 ] << ")" << std::endl;
        }
        else
        {
          Jx += data[ 0 ][ 6 * elemNodeTagHash + 3 ];
          Jy += data[ 0 ][ 6 * elemNodeTagHash + 4 ];
          Jz += data[ 0 ][ 6 * elemNodeTagHash + 5 ];
          if( debug ) std::cout << "-- " << ntetrahed << " J = (" << Jx << ","
                                << Jy << "," << Jz << ")" << std::endl;
        }
      }

      Jx /= 4;
      Jy /= 4;
      Jz /= 4;

      Pc.erase (Pc.begin(), Pc.end());
      iiflucks::tetrahedronCentroid(P0, P1, P2, P3, Pc);
      dV = iiflucks::tetrahedronVolume(P0, P1, P2, P3);
      std::cout << Pc[0] << " " << Pc[1] << " " << Pc[2] << " ";
      std::cout << Jx << " " << Jy << " " << Jz << " " << dV << std::endl;

      xc += dV * Pc[0];
      yc += dV * Pc[1];
      zc += dV * Pc[2];

      V += dV;

      ntetrahed++;

    }
  }

  int row6 = -1; 
  for( unsigned int i = 0; i < elemTags.size(); i++) 
    if( elemTypes[ i ] == 6 ) row6 = i;

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());
  P4.erase (P4.begin(), P4.end());
  P5.erase (P5.begin(), P5.end());
  Pc.erase (Pc.begin(), Pc.end());

  int nprism = 0;
  if( row6 < 0 ) 
  {
    if( verbose ) std::cout << "-- no prism found" << std::endl;
  }
  else
  {
    if( verbose ) std::cout << "-- " << elemNodeTags[ row6 ].size() 
	    << " element node tags\n";

    for( unsigned int j = 0; j < elemNodeTags[ row6 ].size(); j+=6)
    {
      elemNodeTag = elemNodeTags[ row6 ][ j ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P0.erase (P0.begin(), P0.end());
      P0.push_back( nodex/scale );
      P0.push_back( nodey/scale );
      P0.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row6 ][ j + 1 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P1.erase (P1.begin(), P1.end());
      P1.push_back( nodex/scale );
      P1.push_back( nodey/scale );
      P1.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row6 ][ j + 2 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P2.erase (P2.begin(), P2.end());
      P2.push_back( nodex/scale );
      P2.push_back( nodey/scale );
      P2.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row6 ][ j + 3 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P3.erase (P3.begin(), P3.end());
      P3.push_back( nodex/scale );
      P3.push_back( nodey/scale );
      P3.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row6 ][ j + 4 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P4.erase (P4.begin(), P4.end());
      P4.push_back( nodex/scale );
      P4.push_back( nodey/scale );
      P4.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row6 ][ j + 5 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P5.erase (P5.begin(), P5.end());
      P5.push_back( nodex/scale );
      P5.push_back( nodey/scale );
      P5.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      Pc.erase (Pc.begin(), Pc.end());
      iiflucks::prismCentroid(P0, P1, P2, P3, P4, P5, Pc);
      dV = iiflucks::prismVolume(P0, P1, P2, P3, P4, P5);

      std::cout << Pc[0] << " " << Pc[1] << " " << Pc[2] << " ";
      std::cout << Jx << " " << Jy << " " << Jz << " " << dV << std::endl;

      xc += dV * Pc[0];
      yc += dV * Pc[1];
      zc += dV * Pc[2];

      V += dV;
      nprism++;
    }
  }

  int row7 = -1; 
  for( unsigned int i = 0; i < elemTags.size(); i++) 
    if( elemTypes[ i ] == 7 ) row7 = i;

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());
  P4.erase (P4.begin(), P4.end());
  Pc.erase (Pc.begin(), Pc.end());

  int npyramid = 0;
  if( row7 < 0 ) 
  {
    if( verbose ) std::cout << "-- no pyramid found" << std::endl;
  }
  else
  {
    if( verbose ) std::cout << "-- " << elemNodeTags[ row7 ].size() 
	    << " element node tags\n";

    for( unsigned int j = 0; j < elemNodeTags[ row7 ].size(); j+=5)
    {
      elemNodeTag = elemNodeTags[ row7 ][ j ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P0.erase (P0.begin(), P0.end());
      P0.push_back( nodex/scale );
      P0.push_back( nodey/scale );
      P0.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row7 ][ j + 1 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P1.erase (P1.begin(), P1.end());
      P1.push_back( nodex/scale );
      P1.push_back( nodey/scale );
      P1.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row7 ][ j + 2 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P2.erase (P2.begin(), P2.end());
      P2.push_back( nodex/scale );
      P2.push_back( nodey/scale );
      P2.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row7 ][ j + 3 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P3.erase (P3.begin(), P3.end());
      P3.push_back( nodex/scale );
      P3.push_back( nodey/scale );
      P3.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row7 ][ j + 4 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;
      
      P4.erase (P4.begin(), P4.end());
      P4.push_back( nodex/scale );
      P4.push_back( nodey/scale );
      P4.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      Pc.erase (Pc.begin(), Pc.end());
      iiflucks::pyramidCentroid(P0, P1, P2, P3, P4, Pc);
      dV = iiflucks::pyramidVolume(P0, P1, P2, P3, P4);
      std::cout << Pc[0] << " " << Pc[1] << " " << Pc[2] << " ";
      std::cout << Jx << " " << Jy << " " << Jz << " " << dV << std::endl;

      xc += dV * Pc[0];
      yc += dV * Pc[1];
      zc += dV * Pc[2];

      V += dV;
      npyramid++;
    }
  }

  xc /= V;
  yc /= V;
  zc /= V;

  xmin /= scale; xmax /= scale;
  ymin /= scale; ymax /= scale;
  zmin /= scale; zmax /= scale;

  if( verbose )
  {
    std::cout << std::endl;
    std::cout << ntetrahed << " tetrahedra, " << nprism << " prism and " 
	      << npyramid << " pyramid" << std::endl;
    std::cout << "Total volume " << V << std::endl;
    std::cout << "Volume center (" << xc << "," << yc << "," << zc << ")" << std::endl;
    std::cout << "Bounding box x[" << xmin << "," << xmax << "] ";
    std::cout << "y[" << ymin << "," << ymax << "] ";
    std::cout << "z[" << zmin << "," << zmax << "]" << std::endl;
  }

  gmsh::finalize();

  return 0;
}
