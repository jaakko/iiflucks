cmake_minimum_required (VERSION 3.0)
project (iiflucs VERSION 0.1 LANGUAGES CXX)

option (USE_GMSH_LIBS "Use Gmsh libraries" ON)
add_compile_options(-Wno-psabi)

include (GNUInstallDirs)
include_directories ("${PROJECT_SOURCE_DIR}/../include")

add_subdirectory (iiflucks)

if (USE_GMSH_LIBS)
  message("-- Use Gmsh libraries")
  find_library (GMSH_LIB gmsh PATH_SUFFIXES lib)
  find_path (GMSH_INC "gmsh.h" PATH_SUFFIXES include gmsh include/gmsh)
  if (GMSH_LIB AND GMSH_INC)
    message("-- Gmsh library and headers found")
    add_subdirectory (geometries)
    add_subdirectory (mesh2da)
    add_subdirectory (mesh2dv)
  else ()
    message(FATAL_ERROR "-- Gmsh library needed, set USE_GMSH_LIBS=OFF if not available")
  endif()
endif ()

add_subdirectory (fluxind)
add_subdirectory (fluxfield)
add_subdirectory (indmtrx)
add_subdirectory (mapB)
add_subdirectory (probeB)
add_subdirectory (simJNB)
add_subdirectory (volumes)

# install Gnuplot scripts
install (DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../gnuplot/
    DESTINATION bin
    FILE_PERMISSIONS
       OWNER_EXECUTE OWNER_WRITE OWNER_READ
       GROUP_EXECUTE GROUP_READ
       WORLD_EXECUTE WORLD_READ
    FILES_MATCHING
    PATTERN "*.p"
    PATTERN "*.sh")

# install Octave scripts
install (DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../octave/
    DESTINATION bin
    FILE_PERMISSIONS
       OWNER_EXECUTE OWNER_WRITE OWNER_READ
       GROUP_EXECUTE GROUP_READ
       WORLD_EXECUTE WORLD_READ
    FILES_MATCHING
    PATTERN "*.m")

# install Perl scripts
install (DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../perl/
    DESTINATION bin
    FILE_PERMISSIONS
       OWNER_EXECUTE OWNER_WRITE OWNER_READ
       GROUP_EXECUTE GROUP_READ
       WORLD_EXECUTE WORLD_READ
    FILES_MATCHING
    PATTERN "*.pl")

# install Python scripts
install (DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../python/
    DESTINATION bin
    FILE_PERMISSIONS
       OWNER_EXECUTE OWNER_WRITE OWNER_READ
       GROUP_EXECUTE GROUP_READ
       WORLD_EXECUTE WORLD_READ
    FILES_MATCHING
    PATTERN "*.py"
    PATTERN "*.sh")

# build documentation (from V. Rudakova)
option(BUILD_DOC "Build documentation" ON)

find_package(Doxygen)
if (DOXYGEN_FOUND)
   set(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/../doc/Doxyfile.in)
   set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

   configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
   message("Doxygen build started")

   add_custom_target( doc_doxygen ALL
       COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
       WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
       COMMENT "Generating API document with Doxygen"
       VERBATIM )
else (DOXYGEN_FOUND)
   message("Doxygen need to be installed to generate doxygen documentation")
endif (DOXYGEN_FOUND)

include (CTest)

if (USE_GMSH_LIBS)

# check that application runs
  add_test (mesh2da_runs mesh2da)

# run internal test calculations
  add_test (mesh2da_calc mesh2da --test)
  set_tests_properties (mesh2da_calc PROPERTIES PASS_REGULAR_EXPRESSION "0.86602[0-9]* \\(0.5773[0-9]*,0.5773[0-9]*,0.5773[0-9]*\\) \\(0.33333[3]*,0.33333[3]*,0.33333[3]*\\) 3.7416[0-9]* 32 0.22572[0-9]* 1 yes yes \\(0,2,-2\\) 1 9")

  add_test (mesh2da_geometry mesh2da --test-geom)
  set_tests_properties (mesh2da_geometry PROPERTIES PASS_REGULAR_EXPRESSION "1 yes yes 1 0 0.70710[0-9]* \\(1,1,0\\) \\(0.66666[6-7]*,0.66666[6-7]*,0.66666[6-7]*\\) 9 0.16666[6-7]* \\(0.25,0.25,0.75\\) 0.5 \\(0.66666[6-7]*,0.33333[3]*,0.83333[3]*\\) 1.3333[3]* \\(0,0,0.25\\)")

  add_test (mesh2da_vector mesh2da --test-vect)
  set_tests_properties (mesh2da_vector PROPERTIES PASS_REGULAR_EXPRESSION "3.7416[0-9]* 32 0.22572[0-9]* \\(-3,6,-3\\) -10 \\(0,2,2\\) \\(0,0,0\\) \\(2,4,6\\) \\(2,0,0\\) \\(2,5,10\\) \\(0,0.6,0.8\\) \\(1,1,1\\) \\(1,1,1\\) \\(1.41421,0.785398,1\\) \\(1.73205,0.955317,0.785398\\)")

# check that application runs
  add_test (mesh2dv_runs mesh2dv)

# run internal test calculations
  add_test (mesh2dv_calc mesh2dv --test)
  set_tests_properties (mesh2dv_calc PROPERTIES PASS_REGULAR_EXPRESSION "0.16666[6-7]* \\(0.25,0.25,0.75\\) 0.5 \\(0.66666[6-7]*,0.33333[3]*,0.83333[3]*\\) 1.3333[3]* \\(0,0,0.25\\) \\(0.33333[3]*,0.33333[3]*,0.66666[6-7]*\\) 3.74166[0-9]* \\(0,2,-2\\) 9 -10 0.70710[0-9]*")

  add_test (mesh2dv_geometry mesh2dv --test-geom)
  set_tests_properties (mesh2dv_geometry PROPERTIES PASS_REGULAR_EXPRESSION "1 yes yes 1 0 0.70710[0-9]* \\(1,1,0\\) \\(0.66666[6-7]*,0.66666[6-7]*,0.66666[6-7]*\\) 9 0.16666[6-7]* \\(0.25,0.25,0.75\\) 0.5 \\(0.66666[6-7]*,0.33333[3]*,0.83333[3]*\\) 1.3333[3]* \\(0,0,0.25\\)")

  add_test (mesh2dv_vector mesh2dv --test-vect)
  set_tests_properties (mesh2dv_vector PROPERTIES PASS_REGULAR_EXPRESSION "3.7416[0-9]* 32 0.22572[0-9]* \\(-3,6,-3\\) -10 \\(0,2,2\\) \\(0,0,0\\) \\(2,4,6\\) \\(2,0,0\\) \\(2,5,10\\) \\(0,0.6,0.8\\) \\(1,1,1\\) \\(1,1,1\\) \\(1.41421,0.785398,1\\) \\(1.73205,0.955317,0.785398\\)")

endif()

# check that application runs
add_test (fluxind_runs fluxind)

# run internal test quaternion calculation
add_test (fluxind_quaternion fluxind --test-quat)
set_tests_properties (fluxind_quaternion PROPERTIES PASS_REGULAR_EXPRESSION "\\{1,-2,-3,-4\\} \\{-60,12,30,24\\} \\(1,1,-1\\)")

# check that application runs
add_test (axialgradiometer_runs axialgradiometer)

# create test volume elements and calculate wire length
add_test (axialgradiometer_length axialgradiometer --test)
set_tests_properties (axialgradiometer_length PROPERTIES PASS_REGULAR_EXPRESSION "0.13366[0-9]*")

# check that application runs
add_test (gradient8wire_runs gradient8wire)

# create test volume elements and calculate wire length and volume
add_test (gradient8wire_length_volume gradient8wire --test)
set_tests_properties (gradient8wire_length_volume PROPERTIES PASS_REGULAR_EXPRESSION "0.20787[0-9]* 1.6326[0-9]*e-09 -1.4295[0-9]*e-10 -1.1540[0-9]*e-20 1.1131[0-9]*e-06")

# check that application runs
add_test (helixwire_runs helixwire)

# create test volume elements and calculate wire length
add_test (helixwire_length helixwire --test-length)
set_tests_properties (helixwire_length PROPERTIES PASS_REGULAR_EXPRESSION "0.62863[0-9]*")

add_test (helixwire_volume helixwire --test-volume)
set_tests_properties (helixwire_volume PROPERTIES PASS_REGULAR_EXPRESSION "4.9373[0-9]*e-09")

add_test (helixwire_Jxl helixwire --test-Jxl)
set_tests_properties (helixwire_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[0-9].[0-9]*e-[2-9][0-9]")

add_test (helixwire_Jyl helixwire --test-Jyl)
set_tests_properties (helixwire_Jyl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[0-9].[0-9]*e-[2-9][0-9]")

add_test (helixwire_Jzl helixwire --test-Jzl)
set_tests_properties (helixwire_Jzl PROPERTIES PASS_REGULAR_EXPRESSION "2e-05")

# check that application runs
add_test (helmholtzwire_runs helmholtzwire)

# run internal test calculations
add_test (helmholtzwire_length_volume helmholtzwire --test)
set_tests_properties (helmholtzwire_length_volume PROPERTIES PASS_REGULAR_EXPRESSION "33.977[0-9]* 0.00010674[0-9]*")

# check that application runs
add_test (linespherewire_runs linespherewire)

# run internal test calculations
add_test (linespherewire_length linespherewire --test-length)
set_tests_properties (linespherewire_length PROPERTIES PASS_REGULAR_EXPRESSION "0.031415[0-9]*")

# run internal test calculations
add_test (linespherewire_volume linespherewire --test-volume)
set_tests_properties (linespherewire_volume PROPERTIES PASS_REGULAR_EXPRESSION "2.4674[0-9]*e-10")

# run internal test calculations
add_test (linespherewire_Jxl linespherewire --test-Jxl)
set_tests_properties (linespherewire_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

# run internal test calculations
add_test (linespherewire_Jyl linespherewire --test-Jyl)
set_tests_properties (linespherewire_Jyl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

# run internal test calculations
add_test (linespherewire_Jzl linespherewire --test-Jzl)
set_tests_properties (linespherewire_Jzl PROPERTIES PASS_REGULAR_EXPRESSION "2.8[0-9]*e-05")

# check that application runs
add_test (loopspherewire_runs loopspherewire)

# run internal test calculations
add_test (loopspherewire_length loopspherewire --test-length)
set_tests_properties (loopspherewire_length PROPERTIES PASS_REGULAR_EXPRESSION "0.0187[0-9]*")

# run internal test calculations
add_test (loopspherewire_volume loopspherewire --test-volume)
set_tests_properties (loopspherewire_volume PROPERTIES PASS_REGULAR_EXPRESSION "1.469[0-9]*e-10")

# run internal test calculations
add_test (loopspherewire_Jxl loopspherewire --test-Jxl)
set_tests_properties (loopspherewire_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "-7.6[0-9]*e-06")

# run internal test calculations
add_test (loopspherewire_Jyl loopspherewire --test-Jyl)
set_tests_properties (loopspherewire_Jyl PROPERTIES PASS_REGULAR_EXPRESSION "7.6[0-9]*e-06")

# run internal test calculations
add_test (loopspherewire_Jzl loopspherewire --test-Jzl)
set_tests_properties (loopspherewire_Jzl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

# check that application runs
add_test (racetrackplanar_runs racetrackplanar)

# run internal test calculations
add_test (racetrackplanar_length racetrackplanar --test-length)
set_tests_properties (racetrackplanar_length PROPERTIES PASS_REGULAR_EXPRESSION "1.24779[0-9]*")

add_test (racetrackplanar_volume racetrackplanar --test-volume)
set_tests_properties (racetrackplanar_volume PROPERTIES PASS_REGULAR_EXPRESSION "9.8001[0-9]*e-09")

add_test (racetrackplanar_Jxl racetrackplanar --test-Jxl)
set_tests_properties (racetrackplanar_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "0")
#set_tests_properties (racetrackplanar_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

add_test (racetrackplanar_Jyl racetrackplanar --test-Jyl)
set_tests_properties (racetrackplanar_Jyl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

add_test (racetrackplanar_Jzl racetrackplanar --test-Jzl)
set_tests_properties (racetrackplanar_Jzl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[1-9][0-9]")

# check that application runs
add_test (racetrackcylinder_runs racetrackcylinder)

# run internal test calculations
add_test (racetrackcylinder_length racetrackcylinder --test-length)
set_tests_properties (racetrackcylinder_length PROPERTIES PASS_REGULAR_EXPRESSION "0.573935[0-9]*")

add_test (racetrackcylinder_volume racetrackcylinder --test-volume)
set_tests_properties (racetrackcylinder_volume PROPERTIES PASS_REGULAR_EXPRESSION "4.50767[0-9]*e-09")

add_test (racetrackcylinder_Jxl racetrackcylinder --test-Jxl)
set_tests_properties (racetrackcylinder_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

add_test (racetrackcylinder_Jyl racetrackcylinder --test-Jyl)
set_tests_properties (racetrackcylinder_Jyl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

add_test (racetrackcylinder_Jzl racetrackcylinder --test-Jzl)
set_tests_properties (racetrackcylinder_Jzl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

# check that application runs
add_test (ringwire_runs ringwire)

# straight line parametric curve
add_test (ringwire_curves1 ringwire --test-parametric-curves1)
set_tests_properties (ringwire_curves1 PROPERTIES PASS_REGULAR_EXPRESSION "\\(0,0,0.003\\) \\(0,0,1\\) 0.004 1")

# ring parametric curve
add_test (ringwire_curves2 ringwire --test-parametric-curves2)
set_tests_properties (ringwire_curves2 PROPERTIES PASS_REGULAR_EXPRESSION "\\(-0.00951057[0-9]*,0.00309017[0-9]*,0\\) \\(-0.30901[0-9]*,-0.95105[0-9]*,0\\) 0.00942478[0-9]* 0.01[0]*")

# helix parametric curve
add_test (ringwire_curves3 ringwire --test-parametric-curves3)
set_tests_properties (ringwire_curves3 PROPERTIES PASS_REGULAR_EXPRESSION "\\([+-]?[1-9].[0-9]*e-[1-9][0-9],0.01[0]*,0.003125[0]*\\) \\(-0.999209[0-9]*,[+-]?[1-9].[0-9]*e-[1-9][0-9],0.039757[0-9]*\\) 0.031440[0-9]* 0.0100079[0-9]*")

# ring on cylinder parametric curve
add_test (ringwire_curves4 ringwire --test-parametric-curves4)
#set_tests_properties (ringwire_curves4 PROPERTIES PASS_REGULAR_EXPRESSION "\\(0.0388529,-0.00951057,0.00309017\\) \\(-0.0754269,-0.308137,-0.948347\\) 0.00944769 0.0100286")

# line on sphere parametric curve
add_test (ringwire_curves5 ringwire --test-parametric-curves5)
set_tests_properties (ringwire_curves5 PROPERTIES PASS_REGULAR_EXPRESSION "\\(0.01264[0-9]*,0.006324[0-9]*,0.01414[0-9]*\\) \\(-0.6324[0-9]*,-0.3162[0-9]*,0.7071[0-9]*\\) 0.031415[0-9]* 0.02")

# loop on sphere parametric curve
add_test (ringwire_curves6 ringwire --test-parametric-curves6)
set_tests_properties (ringwire_curves6 PROPERTIES PASS_REGULAR_EXPRESSION "0.0054[0-9]*,0.0054[0-9]*,0.018[0-9]*[\r\n]-0.70[0-9]*,0.70[0-9]*,[-]?0[\r\n]0.018[0-9]*,0.003[0-9]*")

add_test (ringwire_length ringwire --test-length)
set_tests_properties (ringwire_length PROPERTIES PASS_REGULAR_EXPRESSION "0.062831[0-9]*")

add_test (ringwire_volume ringwire --test-volume)
set_tests_properties (ringwire_volume PROPERTIES PASS_REGULAR_EXPRESSION "4.9348*e-10")

add_test (ringwire_Jxl ringwire --test-Jxl)
set_tests_properties (ringwire_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

add_test (ringwire_Jyl ringwire --test-Jyl)
set_tests_properties (ringwire_Jyl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

add_test (ringwire_Jzl ringwire --test-Jzl)
set_tests_properties (ringwire_Jzl PROPERTIES PASS_REGULAR_EXPRESSION "0")

# check that application runs
add_test (ringcylinderwire_runs ringcylinderwire)

# run internal test calculations
add_test (ringcylinderwire_length ringcylinderwire --test-length)
set_tests_properties (ringcylinderwire_length PROPERTIES PASS_REGULAR_EXPRESSION "0.063084[0-9]*")

add_test (ringcylinderwire_volume ringcylinderwire --test-volume)
set_tests_properties (ringcylinderwire_volume PROPERTIES PASS_REGULAR_EXPRESSION "4.95465*e-10")

add_test (ringcylinderwire_Jxl ringcylinderwire --test-Jxl)
set_tests_properties (ringcylinderwire_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

add_test (ringcylinderwire_Jyl ringcylinderwire --test-Jyl)
set_tests_properties (ringcylinderwire_Jyl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

add_test (ringcylinderwire_Jzl ringcylinderwire --test-Jzl)
set_tests_properties (ringcylinderwire_Jzl PROPERTIES PASS_REGULAR_EXPRESSION "[+-]?[1-9].[0-9]*e-[2-9][0-9]")

# check that application runs
add_test (straightwire_runs straightwire)

# run internal test calculations
add_test (straightwire_length straightwire --test-length)
set_tests_properties (straightwire_length PROPERTIES PASS_REGULAR_EXPRESSION "0.095[0]*")

add_test (straightwire_volume straightwire --test-volume)
set_tests_properties (straightwire_volume PROPERTIES PASS_REGULAR_EXPRESSION "7.46128*e-10")

add_test (straightwire_Jxl straightwire --test-Jxl)
set_tests_properties (straightwire_Jxl PROPERTIES PASS_REGULAR_EXPRESSION "0")

add_test (straightwire_Jyl straightwire --test-Jyl)
set_tests_properties (straightwire_Jyl PROPERTIES PASS_REGULAR_EXPRESSION "0")

add_test (straightwire_Jzl straightwire --test-Jzl)
set_tests_properties (straightwire_Jzl PROPERTIES PASS_REGULAR_EXPRESSION "9.5e-05")

# check that application runs
add_test (simJNB_runs simJNB)

# run internal test quaternion calculation
add_test (simJNB_quaternion simJNB --test-quat)
set_tests_properties (simJNB_quaternion PROPERTIES PASS_REGULAR_EXPRESSION "\\{1,-2,-3,-4\\} \\{-60,12,30,24\\} \\(1,1,-1\\)")

# run internal test vector calculation
add_test (simJNB_vector simJNB --test-vect)
set_tests_properties (simJNB_vector PROPERTIES PASS_REGULAR_EXPRESSION "3.7416[0-9]* 32 0.22572[0-9]* \\(-3,6,-3\\) -10")

# check that application runs
add_test (probeB_runs probeB)

# run internal test quaternion calculation
add_test (probeB_quaternion probeB --test-quat)
set_tests_properties (probeB_quaternion PROPERTIES PASS_REGULAR_EXPRESSION "\\{1,-2,-3,-4\\} \\{-60,12,30,24\\} \\(1,1,-1\\)")

# run internal test vector calculation
add_test (probeB_vector probeB --test-vect)
set_tests_properties (probeB_vector PROPERTIES PASS_REGULAR_EXPRESSION "3.7416[0-9]* 32 0.22572[0-9]* \\(-3,6,-3\\) -10 \\(0,2,2\\) \\(0,0,0\\) \\(2,4,6\\) \\(2,0,0\\) \\(2,5,10\\) \\(0,0.6,0.8\\) \\(1,1,1\\) \\(1,1,1\\) \\(1.41421,0.785398,1\\) \\(1.73205,0.955317,0.785398\\)")

# check that application runs
add_test (mapB_runs mapB)

# run internal test quaternion calculation
add_test (mapB_quaternion mapB --test-quat)
set_tests_properties (mapB_quaternion PROPERTIES PASS_REGULAR_EXPRESSION "\\{1,-2,-3,-4\\} \\{-60,12,30,24\\} \\(1,1,-1\\)")

# run internal test vector calculation
add_test (mapB_vector mapB --test-vect)
set_tests_properties (mapB_vector PROPERTIES PASS_REGULAR_EXPRESSION "3.7416[0-9]* 32 0.22572[0-9]* \\(-3,6,-3\\) -10 \\(0,2,2\\) \\(0,0,0\\) \\(2,4,6\\) \\(2,0,0\\) \\(2,5,10\\) \\(0,0.6,0.8\\) \\(1,1,1\\) \\(1,1,1\\) \\(1.41421,0.785398,1\\) \\(1.73205,0.955317,0.785398\\)")

# circle of diameter 2 cm with 100 um wire and 1 mA current
add_test (mapB_map1 mapB --test-map1)
set_tests_properties (mapB_map1 PROPERTIES PASS_REGULAR_EXPRESSION "4.9348[0-9]*e-10[\r\n]0,0,-0.01,[+-]?[1-9].[0-9]*e-[2-9][0-9],[+-]?[1-9].[0-9]*e-[2-9][0-9],2.2214[0-9]*e-08,2.2214[0-9]*e-08[\r\n]0,0,0,0,0,6.2831[0-9]*e-08,6.2831[0-9]*e-08[\r\n]0,0,0.01,[+-]?[1-9].[0-9]*e-[2-9][0-9],[+-]?[1-9].[0-9]*e-[2-9][0-9],2.2214[0-9]*e-08,2.2214[0-9]*e-08[\r\n]0,0,0,0,0,6.283[0-9]*e-08,0,0")

# helix 1 cm diameter, 1 cm long and 10 turns with 1 mA current
add_test (mapB_map2 mapB --test-map2)
set_tests_properties (mapB_map2 PROPERTIES PASS_REGULAR_EXPRESSION "0,0,0.005,[+-]?[1-9].[0-9]*e-[1-9][0-9],[+-]?[1-9].[0-9]*e-0[8-9],8.8[0-9]*e-07,8.8[0-9]*e-07[\r\n]0,0,0.005,[+-]?[1-9].[0-9]*e-[2-9][0-9],[+-]?[1-9].[0-9]*e-0[8-9],8.8[0-9]*e-07,[+-]?[1-9].[0-9]*e-[2-9][0-9],[+-]?[1-9].[0-9]*e-[2-9][0-9],[+-]?[1-9].[0-9]*e-[2-9][0-9],43[\r\n]-0.02,0,-1.570[8][0-9]*,1.0[0-9]*e-08")

# line 10 cm long with 1 mA current
add_test (mapB_map3 mapB --test-map3)
set_tests_properties (mapB_map3 PROPERTIES PASS_REGULAR_EXPRESSION "0.01,0,0.05,0,1.9[0-9]*e-08,0,1.9[0-9]*e-08[\r\n]0.01,0,0.05,[-]?0,1.9[0-9]*e-08,0[\r\n]-0.01,-1.570[8]?[0-9]*,0.05,1.9[0-9]*e-08,-3.1415[9]?[0-9]*,0,1.9[0-9]*e-08,[1-9].[0-9]*e-[1-9][0-9],[1-9].[0-9]*e-[1-9][0-9],0[\r\n]0,-1.570[8]?[0-9]*,0.05,0,0,0,0,0,0,0[\r\n]0.01,-1.570[8]?[0-9]*,0.05,1.9[0-9]*e-08,[1-9].[0-9]*e-[1-9][0-9],0,1.9[0-9]*e-08,[1-9].[0-9]*e-[1-9][0-9],[1-9].[0-9]*e-[1-9][0-9],0[\r\n]-0.01,0,0.05,1.9[0-9]*e-08,-1.570[8][0-9]*,0,1.9[0-9]*e-08,[1-9].[0-9]*e-[1-9][0-9],1.570[8][0-9]?,0[\r\n]0,0,0.05,0,0,0,0,0,0,0[\r\n]0.01,0,0.05,1.9[0-9]*e-08,1.570[8][0-9]*,0,1.9[0-9]*e-08,[+-]?[1-9].[0-9]*e-[1-9][0-9],1.570[8]?[0-9]*,0")

# 1 cm ring on 4 cm radius surface with 1 mA current
add_test (mapB_map4 mapB --test-map4)
set_tests_properties (mapB_map4 PROPERTIES PASS_REGULAR_EXPRESSION "0.04,0,0,1.25[0-9]*e-07,[+-]?[1-9].[0-9]*e-[1-9][0-9],[+-]?[1-9].[0-9]*e-[1-9][0-9],1.25[0-9]*e-07[\r\n]0.04,0,0,1.25[0-9]*e-07")

# one turn planar RT 2 cm long, 1 cm end, 5 mm corners with 1 mA current
add_test (mapB_map5 mapB --test-map5)
set_tests_properties (mapB_map5 PROPERTIES PASS_REGULAR_EXPRESSION "0,0,0,4.[23][0-9]*e-08,0,0,4.[23][0-9]*e-08[\r\n]0,0,0,4.85[0-9]*e-08,0,0,[+-]?[1-9].[0-9]*e-[2-9][0-9]")

# one turn cylindrical RT 4 cm long on 4 cm radius surface with 5 mm end radius and 1 mA current
add_test (mapB_map6 mapB --test-map6)
set_tests_properties (mapB_map6 PROPERTIES PASS_REGULAR_EXPRESSION "0.04,0,0,[6-9].[0-9]*e-08,[+-]?[1-9].[0-9]*e-[1-9][0-9],[+-]?[1-9].[0-9]*e-[1-9][0-9],[6-9].[0-9]*e-08[\r\n]0.04,0,0,8.1[0-9]*e-08")

# line on 2 cm radius sphere t=[-pi/4,pi/4], azimuth 0.46365, 1 mA current
add_test (mapB_map8 mapB --test-map8)
set_tests_properties (mapB_map8 PROPERTIES PASS_REGULAR_EXPRESSION "0,0,0,3.512[0-9]*e-09,-7.024[0-9]*e-09,0,7.85[0-9]*e-09[\r\n]0,0,0,3.512[0-9]*e-09,-7.024[0-9]*e-09,0,[+-]?[1-9].[0-9]*e-[2-9][0-9],[+-]?[1-9].[0-9]*e-[2-9][0-9],0,21[\r\n]0,0,0,3.512[0-9]*e-09,-7.024[0-9]*e-09,0,7.85[0-9]*e-09,[+-]?[1-9].[0-9]*e-[2-9][0-9],[+-]?[1-9].[0-9]*e-[2-9][0-9],0")

# half loop on 2 cm radius sphere, altitude pi/4, azimuth pi/8, 1 mA current
add_test (mapB_map9 mapB --test-map9)
set_tests_properties (mapB_map9 PROPERTIES PASS_REGULAR_EXPRESSION "0,0,0,-2.[0-9]*e-09,-2.[0-9]*e-09,1.[0-9]*e-09,3.[0-9]*e-09[\r\n]0,0,0,-1.[0-9]*e-09,-1.[0-9]*e-09,8.[0-9]*e-10")

# check that application runs
add_test (indmtrx_runs indmtrx)

# run internal test quaternion calculation
add_test (indmtrx_calc indmtrx --test-calc)
set_tests_properties (indmtrx_calc PROPERTIES PASS_REGULAR_EXPRESSION "\\{1,-2,-3,-4\\} \\{-60,12,30,24\\} \\(1,1,-1\\)")

add_test (indmtrx_quat indmtrx --test-quat)
set_tests_properties (indmtrx_quat PROPERTIES PASS_REGULAR_EXPRESSION "\\{1,-2,-3,-4\\} \\{-60,12,30,24\\} \\(1,1,-1\\)")

# create Helix object from yaml-node
add_test (indmtrx_file1 indmtrx --test-file1)
set_tests_properties (indmtrx_file1 PROPERTIES PASS_REGULAR_EXPRESSION "dt = 0.001[\r\n]t1 = 0[\r\n]t2 = 62.8319[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 0.001 A[\r\n]Reference designator = L1[\r\n]A = 7.85398e-09 m\\^2[\r\n]J = 127324 A/m\\^2[\r\n]theta = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)[\r\n]R = 0.005 m[\r\n]L = 0.01 m[\r\n]N = 10 turns")

# create Ring object from yaml-node
add_test (indmtrx_file2 indmtrx --test-file2)
set_tests_properties (indmtrx_file2 PROPERTIES PASS_REGULAR_EXPRESSION "dt = 0.001[\r\n]t1 = 0[\r\n]t2 = 6.28319[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 0.001 A[\r\n]Reference designator = L1[\r\n]A = 7.85398e-09 m\\^2[\r\n]J = 127324 A/m\\^2[\r\n]theta = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)[\r\n]R = 0.005 m")

# create Line object from yaml-node
add_test (indmtrx_file3 indmtrx --test-file3)
set_tests_properties (indmtrx_file3 PROPERTIES PASS_REGULAR_EXPRESSION "dt = 0.001[\r\n]t1 = 0[\r\n]t2 = 6.28319[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 0.001 A[\r\n]Reference designator = L1[\r\n]A = 7.85398e-09 m\\^2[\r\n]J = 127324 A/m\\^2[\r\n]theta = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)")

# create RingCylinder object from yaml-node
add_test (indmtrx_file4 indmtrx --test-file4)
set_tests_properties (indmtrx_file4 PROPERTIES PASS_REGULAR_EXPRESSION "dt = 0.001[\r\n]t1 = 0[\r\n]t2 = 6.28319[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 0.001 A[\r\n]Reference designator = L1[\r\n]A = 7.85398e-09 m\\^2[\r\n]J = 127324 A/m\\^2[\r\n]theta = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)[\r\n]R = 0.005 m[\r\n]R2 = 0.04 m")

# create RacetrackCylinder object from yaml-node
add_test (indmtrx_file5 indmtrx --test-file5)
set_tests_properties (indmtrx_file5 PROPERTIES PASS_REGULAR_EXPRESSION "lc = 1e-05[\r\n]t1\\[0\\] = 0[\r\n]t1\\[1\\] = 0[\r\n]t1\\[2\\] = 0[\r\n]t1\\[3\\] = 0[\r\n]t2\\[0\\] = 0.36[\r\n]t2\\[1\\] = 3.14159[\r\n]t2\\[2\\] = 0.36[\r\n]t2\\[3\\] = 3.14159[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 150 A[\r\n]Reference designator = L1[\r\n]A = 1.9635e-07 m\\^2")
#[\r\n]J = 7.63944e+08 A\\/m\\^2")
#[\r\n]theta = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)[\r\n]R = 0.0113 m[\r\n]R2 = 0.059 m[\r\n]h = 0.36 m[\r\n]w = 0.012 m[\r\n]th = 0.018 m[\r\n]N = 19 turns[\r\n]O = 18 turns")

# create RacetrackPlanar object from yaml-node
add_test (indmtrx_file6 indmtrx --test-file6)
set_tests_properties (indmtrx_file6 PROPERTIES PASS_REGULAR_EXPRESSION "t2\\[7\\] = 1.5708[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 0.001 A[\r\n]Reference designator = L1[\r\n]A = 7.85398e-09 m\\^2[\r\n]J = 127324 A/m\\^2[\r\n]theta = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)[\r\n]R = 0.005 m[\r\n]h = 0.3 m[\r\n]e = 0 m[\r\n]w = 0.01 m[\r\n]th = 0.018 m[\r\n]turns on layer = 20[\r\n]layers = 20")

# create Solenoid object from yaml-node
add_test (indmtrx_file7 indmtrx --test-file7)
set_tests_properties (indmtrx_file7 PROPERTIES PASS_REGULAR_EXPRESSION "lc = 1e-05[\r\n]t1\\[0\\] = 0[\r\n]t2\\[0\\] = 31.4159[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 0.001 A[\r\n]Reference designator = L1[\r\n]A = 7.85398e-09 m\\^2[\r\n]J = 127324 A/m\\^2[\r\n]theta = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)[\r\n]R = 0.005 m[\r\n]L = 0.01 m[\r\n]th = 0.001 m[\r\n]turns on layer = 10[\r\n]layers = 1")

# create LineSphere object from yaml-node
add_test (indmtrx_file8 indmtrx --test-file8)
set_tests_properties (indmtrx_file8 PROPERTIES PASS_REGULAR_EXPRESSION "dt = 1e-05[\r\n]t1 = -0.78539[0-9]*[\r\n]t2 = 0.78539[0-9]*[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 0.001 A[\r\n]Reference designator = L1[\r\n]A = 7.8539[0-9]*e-09 m\\^2[\r\n]J = 127324 A/m\\^2[\r\n]theta = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)[\r\n]R1 = 0.01 m[\r\n]R2 = 0.02 m")

# create LoopSphere object from yaml-node
add_test (indmtrx_file9 indmtrx --test-file9)
set_tests_properties (indmtrx_file9 PROPERTIES PASS_REGULAR_EXPRESSION "dt = 1e-05[\r\n]t1 = -1[\r\n]t2 = 1[\r\n]dr = 0 m[\r\n]r1 = 0 m[\r\n]r2 = 0 m[\r\n]I = 0.001 A[\r\n]Reference designator = L1[\r\n]A = 7.85398e-09 m\\^2[\r\n]J = 127324 A/m\\^2[\r\n]rotation = 0 rad[\r\n]rotation axis = \\(0,0,1\\)[\r\n]translation = \\(0,0,0\\)[\r\n]R2 = 0.02 m[\r\n]theta1 = 0.785398 rad[\r\n]phi1 = 0.392699 rad[\r\n]dtheta = 0.392699 rad[\r\n]dphi = 0.785398 rad")

# parse histogram ranges from yaml-node
add_test (indmtrx_file10 indmtrx --test-file10)
set_tests_properties (indmtrx_file10 PROPERTIES PASS_REGULAR_EXPRESSION "Default ranges[\r\n]nbin: 100  # bin number can not be edited here")

# 1 cm diameter 1 cm long helix 10 turns and 1 mA current
add_test (indmtrx_inductance1 indmtrx --test-ind1)
set_tests_properties (indmtrx_inductance1 PROPERTIES PASS_REGULAR_EXPRESSION "2.46865[0-9]*e-09 2.46865[0-9]*e-09 6.3[0-9]*e-07 [+-]?[1-9].[0-9]*e-09 [+-]?[1-9].[0-9]*e-1[2-4] [+-]?[1-9].[0-9]*e-1[2-4] [+-]?[1-9].[0-9]*e-1[2-4] [+-]?[1-9].[0-9]*e-1[2-4] [+-]?[1-9].[0-9]*e-1[2-4] [+-]?[1-9].[0-9]*e-1[1-3]")

# two rings mutual inductance
add_test (indmtrx_inductance2 indmtrx --test-ind2)
set_tests_properties (indmtrx_inductance2 PROPERTIES PASS_REGULAR_EXPRESSION "4.93542[0-9]*e-10 4.93542[0-9]*e-10 4.9[0-9]*e-09 [+-]?[1-9].[0-9]*e-1[0-2] [+-]?[1-9].[0-9]*e-1[4-6] [+-]?[1-9].[0-9]*e-1[4-6] [+-]?[1-9].[0-9]*e-1[4-6] [+-]?[1-9].[0-9]*e-1[4-6] -7.19[0-9]*e-13 [+-]?[1-9].[0-9]*e-1[5-9]")

# two parallel 10 cm long wires 1 cm apart from each other
add_test (indmtrx_inductance4 indmtrx --test-ind4)
set_tests_properties (indmtrx_inductance4 PROPERTIES PASS_REGULAR_EXPRESSION "7.85398[0-9]*e-10 7.85398[0-9]*e-10 4.1[8-9][0-9]*e-08 [+-]?[1-9].[0-9]*e-11 0 0 -1.8[0-1][0-9]*e-12 [+-]?[1-9].[0-9]*e-15 0 0")

# ring with radius of 5 mm on cylinder with radius 4 cm
add_test (indmtrx_inductance5 indmtrx --test-ind5)
set_tests_properties (indmtrx_inductance5 PROPERTIES PASS_REGULAR_EXPRESSION "2.46983[0-9]*e-10 2.46983[0-9]*e-10 7.2[0-9]*e-08 1.6[0-9]*e-08 [+-]?[1-9].[0-9]*e-14 [+-]?[1-9].[0-9]*e-13 [+-]?[1-9].[0-9]*e-14 [+-]?[1-9].[0-9]*e-12")

# meridian line on 2 cm radius sphere t=[-pi/4,pi/4], azimuth 0.46365, 1 mA
add_test (indmtrx_inductance6 indmtrx --test-ind6)
set_tests_properties (indmtrx_inductance6 PROPERTIES PASS_REGULAR_EXPRESSION "2.46[0-9]*e-10 2.46[0-9]*e-10")

# Four parallel 10 cm long wires along z-axis at (0.01, 0.01),
# (0.01, -0.01), (-0.01, -0.01) and (-0.01, 0.01) each with current
# 100 mA with opposite signs for adjacent wires. Wire diameter 1 mm.
add_test (indmtrx_inductance7 indmtrx --test-ind7)
set_tests_properties (indmtrx_inductance7 PROPERTIES PASS_REGULAR_EXPRESSION " [0-1] 0 0 0[\r\n] 0 [0-1] 0 0[\r\n] 0 0 [0-1] 0[\r\n] 0 0 0 [0-1][\r\n] 2.4[0-9]*e-07 2.9[0-9]*e-08 2.9[0-9]*e-08 2.4[0-9]*e-08[\r\n] 2.9[0-9]*e-08 2.4[0-9]*e-07 2.4[0-9]*e-08 2.9[0-9]*e-08[\r\n] 2.9[0-9]*e-08 2.4[0-9]*e-08 2.4[0-9]*e-07 2.9[0-9]*e-08[\r\n] 2.4[0-9]*e-08 2.9[0-9]*e-08 2.9[0-9]*e-08 2.4[0-9]*e-07[\r\n] 0 -8.2[0-9]*e-09 0 3.7[0-9]*e-09[\r\n] 8.2[0-9]*e-09 0 -3.7[0-9]*e-09 0[\r\n] 0 3.7[0-9]*e-09 0 -8.2[0-9]*e-09[\r\n] -3.7[0-9]*e-09 0 8.2[0-9]*e-09 0[\r\n] 0 0 8.2[0-9]*e-09 -3.7[0-9]*e-09[\r\n] 0 0 -3.7[0-9]*e-09 8.2[0-9]*e-09[\r\n] -8.2[0-9]*e-09 3.7[0-9]*e-09 0 0[\r\n] 3.7[0-9]*e-09 -8.2[0-9]*e-09 0 0[\r\n] 0 0 0 0[\r\n] 0 0 0 0[\r\n] 0 0 0 0[\r\n] 0 0 0 0")

