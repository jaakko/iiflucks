#include "Vectors.h"

namespace iiflucks {

/// _l_ = sqrt( __a__ . __a__ ). 
double vectorLength(const std::vector<double> &a)
{
  double l = -1;

  l = sqrt( a[0]*a[0] + a[1]*a[1] + a[2]*a[2] );

  return l;
}

/// __a__ . __b__ = _ax_ * _bx_ + _ay_ * _by_ + _az_ * _bz_.
double dotProduct(const std::vector<double> &a, const std::vector<double> &b)
{
  double p = -1;

  p =  a[0]*b[0] + a[1]*b[1] + a[2]*b[2];

  return p;
}

/// _cosangle_ = __a__ . __b__ / | __a__ | | __b__ | 
/// and _angle_ = acos( _cosangle_ ). 
double angleVectors(const std::vector<double> &a, const std::vector<double> &b)
{
  double cosangle = 0;
  double angle = -10;

  double l1 = vectorLength( a );
  double l2 = vectorLength( b );

  if( l1 > 0 && l2 > 0 )
  {
    cosangle = dotProduct(a, b) / ( l1 * l2 );  
    angle = acos( cosangle );
  } 

  return angle;

}

/// __c__ = __a__ x __b__.
void crossProduct(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c)
{
  c.erase(c.begin(), c.end());
  c.push_back( a[1] * b[2] - a[2] * b[1] );
  c.push_back( a[2] * b[0] - a[0] * b[2] );
  c.push_back( a[0] * b[1] - a[1] * b[0] );
}

/// [ __a__, __b__, __c__ ] = __a__ . ( __b__ x __c__ ). 
double tripleProduct(const std::vector<double> &a, const std::vector<double> &b, const std::vector<double> &c)
{
  double prod = -1;

  prod = a[0]*b[1]*c[2] + a[1]*b[2]*c[0] + a[2]*b[0]*c[1]
       - a[0]*b[2]*c[1] - a[1]*b[0]*c[2] - a[2]*b[1]*c[0];

  return prod;
}

/// __c__ = __a__ + __b__.
void addVectors(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c)
{
  c.erase(c.begin(), c.end());
  c.push_back( a[0] + b[0] );
  c.push_back( a[1] + b[1] );
  c.push_back( a[2] + b[2] );
}

/// __c__ = __a__ - __b__.
void subtractVectors(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c)
{
  c.erase(c.begin(), c.end());
  c.push_back( a[0] - b[0] );
  c.push_back( a[1] - b[1] );
  c.push_back( a[2] - b[2] );
}

/// __c[i]__ = __a[i]__ + __b[i]__ * __b[i]__.
void squareAddVector(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c)
{
  c.erase(c.begin(), c.end());
  c.push_back( a[0] + b[0] * b[0] );
  c.push_back( a[1] + b[1] * b[1] );
  c.push_back( a[2] + b[2] * b[2] );
}

/// __a__ = (0, 0, 0).
void zeroVector(std::vector<double> &a)
{
  a.erase(a.begin(), a.end());
  a.push_back( 0 );
  a.push_back( 0 );
  a.push_back( 0 );
}

/// __a__ = (x, y, z).
void makeVector(const double x, const double y, const double z, std::vector<double> &a)
{
  a.erase(a.begin(), a.end());
  a.push_back( x );
  a.push_back( y );
  a.push_back( z );
}

/// __a__ = (x, y, z).
void makeVector(const int x, const int y, const int z, std::vector<int> &a)
{
  a.erase(a.begin(), a.end());
  a.push_back( x );
  a.push_back( y );
  a.push_back( z );
}

/// | __a__ | = 1.
void normalizeVector(std::vector<double> &a)
{
  double l = sqrt( a[0]*a[0] + a[1]*a[1] + a[2]*a[2] );

  if( l > 0 )
  {
    a[ 0 ] /= l;
    a[ 1 ] /= l;
    a[ 2 ] /= l;
  }
}

/// scale * __a__.
void scaleVector(const double scale, std::vector<double> &a)
{
  a[ 0 ] *= scale;
  a[ 1 ] *= scale;
  a[ 2 ] *= scale;
}

/// rotateXY( __a__ ).
void rotateXY(const double alpha, std::vector<double> &a)
{
  double x2 = a[ 0 ] * cos( alpha ) - a[ 1 ] * sin( alpha );
  double y2 = a[ 0 ] * sin( alpha ) + a[ 1 ] * cos( alpha );

  a[ 0 ] = x2;
  a[ 1 ] = y2;
}


/// __b__ = Cartesian( __a__ ).
void cylindricalCartesian(const std::vector<double> &a, std::vector<double> &b)
{
  b.erase(b.begin(), b.end());
  b.push_back( a[ 0 ] * cos( a[ 1 ] ) );
  b.push_back( a[ 0 ] * sin( a[ 1 ] ) );
  b.push_back( a[ 2 ] );
}

/// __b__ = Cartesian( __a__ ).
void sphericalCartesian(const std::vector<double> &a, std::vector<double> &b)
{
  b.erase(b.begin(), b.end());
  b.push_back( a[ 0 ] * sin( a[ 1 ] ) * cos( a[ 2 ] ) );
  b.push_back( a[ 0 ] * sin( a[ 1 ] ) * sin( a[ 2 ] ) );
  b.push_back( a[ 0 ] * cos( a[ 1 ] ) );
}

/// __b__ = Cylindrical( __a__ ).
void cartesianCylindrical(const std::vector<double> &a, std::vector<double> &b)
{
  b.erase(b.begin(), b.end());
  b.push_back( sqrt( a[ 0 ] * a[ 0 ] + a[ 1 ] * a[ 1 ] ) );
  b.push_back( atan2( a[ 1 ], a[ 0 ] ) );
  b.push_back( a[ 2 ] );
}

/// __b__ = Spherical( __a__ ).
void cartesianSpherical(const std::vector<double> &a, std::vector<double> &b)
{
  b.erase(b.begin(), b.end());
  b.push_back( sqrt( a[ 0 ] * a[ 0 ] + a[ 1 ] * a[ 1 ] + a[ 2 ] * a[ 2 ] ) );

  if( b[ 0 ] > 0 ) b.push_back( acos( a[ 2 ] / b [ 0 ] ) );
  else b.push_back( 0 );

  b.push_back( atan2( a[ 1 ], a[ 0 ] ) );
}

}
