#include "Geometry.h"

namespace iiflucks {

/// _d_ = (_p1x_ - _p2x_)^2 + (_p1y_ - _p2y_)^2 + (_p1z_ - _p2z_)^2.
int distancePoints(const std::vector<int> &P1, const std::vector<int> &P2)
{
  int dist = -1;

  dist = (P1[0]-P2[0])*(P1[0]-P2[0]) + (P1[1]-P2[1])*(P1[1]-P2[1]) +
    (P1[2]-P2[2])*(P1[2]-P2[2]);

  return dist;
}

/// _d_ = sqrt( (_p1x_ - _p2x_)^2 + (_p1y_ - _p2y_)^2 + (_p1z_ - _p2z_)^2 ). 
double distancePoints(const std::vector<double> &P1, const std::vector<double> &P2)
{
  double dist = -1;

  dist = sqrt( (P1[0]-P2[0])*(P1[0]-P2[0]) + (P1[1]-P2[1])*(P1[1]-P2[1]) + 
    (P1[2]-P2[2])*(P1[2]-P2[2]) ); 

  return dist;
}

/// Calculate dot product with unit vector __k__ = (0, 0, 1).
/// If | __a__ . __k__ | < 0.1 the vector is approximately on xy-plane.
bool onXYPlane(const std::vector<double> &a)
{
  bool onXY = false;
  double l = 0;
  std::vector<double> k;

  k.push_back( 0 );
  k.push_back( 0 );
  k.push_back( 1 );

  l = fabs( iiflucks::dotProduct( a, k ) );

  if( l < 0.1 ) onXY = true;

  return onXY;
}

/// Calculate dot product with unit vector __j__ = (0, 1, 0).
/// If | __a__ . __j__ | < 0.1 the vector is approximately on xz-plane.
bool onXZPlane(const std::vector<double> &a)
{
  bool onXZ = false;
  double l = 0;
  std::vector<double> j;

  j.push_back( 0 );
  j.push_back( 1 );
  j.push_back( 0 );

  l = fabs( iiflucks::dotProduct( a, j ) );

  if( l < 0.1 ) onXZ = true;

  return onXZ;
}

/// Calculate dot product with unit vector __i__ = (1, 0, 0).
/// If | __a__ . __i__ | < 0.1 the vector is approximately on yz-plane.
bool onYZPlane(const std::vector<double> &a)
{
  bool onYZ = false;
  double l = 0;
  std::vector<double> i;

  i.push_back( 1 );
  i.push_back( 0 );
  i.push_back( 0 );

  l = fabs( iiflucks::dotProduct( a, i ) );

  if( l < 0.1 ) onYZ = true;

  return onYZ;
}

/// The plane is defined by normal vector n and point on plane P0.
/// All the points _P_ = (_Px_, _Py_, _Pz_) on the plane have 
/// | _nx_ ( _Px_ - _P0x_ ) +  _ny_ ( _Py_ - _P0y_ ) + _nz_ ( _Pz_ - _P0z_ ) | < _epsilon_.
bool pointOnPlane(const std::vector<double> &n, const std::vector<double> &P0, const std::vector<double> &P)
{
  bool onPlane = false;
  double plane = -1;

  plane = n[0]*( P[0] - P0[0] ) + n[1]*( P[1] - P0[1] ) + n[2]*( P[2] - P0[2] );

  if( fabs( plane ) < 1e-10 ) onPlane = true;

  return onPlane;
}

/// Calculate dot product __n__ . __v__  and if | __n__ . __v__ | < 0.1 
/// the vector is approximately on any plane defined by normal vector __n__.
bool vectorAnyPlane(const std::vector<double> &n, const std::vector<double> &v)
{
  bool onPlane = false;
  double plane = -1;

  plane = n[0]*v[0] + n[1]*v[1] + n[2]*v[2];

  if( fabs( plane ) < 0.1 ) onPlane = true;

  return onPlane;
}

/// _d_ = | __v__ x __P0P1__ | / | __v__ |.  
double distancePointLine(const std::vector<double> &v, const std::vector<double> &P0, const std::vector<double> &P1)
{
  double dist = -1;
  std::vector<double> c01, c;

  c01.push_back( P1[ 0 ] - P0[ 0 ] );
  c01.push_back( P1[ 1 ] - P0[ 1 ] );
  c01.push_back( P1[ 2 ] - P0[ 2 ] );

  iiflucks::crossProduct(v, c01, c);

  dist = iiflucks::vectorLength( c ) / iiflucks::vectorLength( v );

  return dist;
}

/// Plane normal vector __n__ = __a__ x __b__.
/// Distance to plane _d_ = | __n__ . __P0P1__ | / | __n__ |.
double distancePointPlane(const std::vector<double> P0, const std::vector<double> P1, const std::vector<double> &a, const std::vector<double> &b)
{
  double dist = -1;
  std::vector<double> c;

  crossProduct(a, b, c);
  double len = vectorLength( c );

  dist = fabs ( c[0] * (P1[0] - P0[0]) + c[1] * (P1[1] - P0[1]) + c[2] * (P1[2] - P0[2]) ) / len;

  return dist;
}

/// Triangle is defined by three vertices _P0_, _P1_ and _P2_.
/// Normal vector __c__ = __P0P1__ x __P0P2__, 
/// area _A_ = 0.5 * | __c__ | and unit length
/// normal pseudovector __nv__ = __c__ / | __c__ |. 
double triangleAreaNormal(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, std::vector<double> &nv)
{
  double A = -1, len = 0;

  std::vector<double> c01, c02, c, n;

  c01.push_back( P1[ 0 ] - P0[ 0 ] );
  c01.push_back( P1[ 1 ] - P0[ 1 ] );
  c01.push_back( P1[ 2 ] - P0[ 2 ] );

  c02.push_back( P2[ 0 ] - P0[ 0 ] );
  c02.push_back( P2[ 1 ] - P0[ 1 ] );
  c02.push_back( P2[ 2 ] - P0[ 2 ] );

  iiflucks::crossProduct(c01, c02, c);

  len = iiflucks::vectorLength( c );

  A = 0.5 * len;

  nv.push_back( c[ 0 ] / len );
  nv.push_back( c[ 1 ] / len );
  nv.push_back( c[ 2 ] / len );

  return A;
}

/// _Pc_ = ( _P0_ + _P1_ + _P2_ ) / 3. 
void triangleCentroid(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, std::vector<double> &Pc)
{
  Pc.erase( Pc.begin(), Pc.end() );
  Pc.push_back( (P0[0] + P1[0] + P2[0]) / 3 ); 
  Pc.push_back( (P0[1] + P1[1] + P2[1]) / 3 ); 
  Pc.push_back( (P0[2] + P1[2] + P2[2]) / 3 ); 
}

/// _A_ = 0.5 | __P0P2__ x __P1P3__ |.
double quadrangleArea(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3)
{
  double A = -1;
  std::vector<double> c02, c13, c;

  c02.push_back( P2[0] - P0[0] );
  c02.push_back( P2[1] - P0[1] );
  c02.push_back( P2[2] - P0[2] );

  c13.push_back( P3[0] - P1[0] );
  c13.push_back( P3[1] - P1[1] );
  c13.push_back( P3[2] - P1[2] );

  iiflucks::crossProduct( c02, c13, c );

  A = 0.5 * iiflucks::vectorLength( c );

  return A;
}


// Not implemented yet and returns point (0, 0, 0).
void quadrangleCentroid(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3, std::vector<double> &Pc)
{
  double c01x = 0.5 * (P1[0] - P0[0]);
  double c01y = 0.5 * (P1[1] - P0[1]);
  double c01z = 0.5 * (P1[2] - P0[2]);

  Pc[ 0 ] = 0;
  Pc[ 1 ] = 0;
  Pc[ 2 ] = 0;
}

/// Volume is calculated from triple product using vectors 
/// __P0P1__, __P0P2__ and __P0P3__.
/// _V_ = | [ __P0P1__, __P0P2__, __P0P3__ ] / 6 |.
double tetrahedronVolume(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3)
{
  double dV=-1;
  double ax, ay, az, bx, by, bz, cx, cy, cz;

  ax = P1[0] - P0[0];
  ay = P1[1] - P0[1];
  az = P1[2] - P0[2];

  bx = P2[0] - P0[0];
  by = P2[1] - P0[1];
  bz = P2[2] - P0[2];

  cx = P3[0] - P0[0];
  cy = P3[1] - P0[1];
  cz = P3[2] - P0[2];

  dV = ax*by*cz + ay*bz*cx + az*bx*cy - ax*bz*cy - ay*bx*cz - az*by*cx;
  dV /= 6;

  return dV;
}

/// _Pc_ = (_P0_ + _P1_ + _P2_ + _P3_) / 4.
void tetrahedronCentroid(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3, std::vector<double> &Pc)
{
  Pc.erase( Pc.begin(), Pc.end() );
  Pc.push_back( (P0[0] + P1[0] + P2[0] + P3[0])/4 ); 
  Pc.push_back( (P0[1] + P1[1] + P2[1] + P3[1])/4 ); 
  Pc.push_back( (P0[2] + P1[2] + P2[2] + P3[2])/4 ); 
}

/// Use formula for parallelepiped with 
///_V_ ~ 0.5 | [ __P0P3__, __P0P1__, __P0P2__ ] |.
double prismVolume(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3, const std::vector<double> &P4, const std::vector<double> &P5)
{
  double dV=-1;
  std::vector<double> c01, c02, c03;

  c01.push_back( P1[0] - P0[0] );
  c01.push_back( P1[1] - P0[1] );
  c01.push_back( P1[2] - P0[2] );
  double d01 = iiflucks::vectorLength( c01 );

  c02.push_back( P2[0] - P0[0] );
  c02.push_back( P2[1] - P0[1] );
  c02.push_back( P2[2] - P0[2] );
  double d02 = iiflucks::vectorLength( c02 );

  c03.push_back( P3[0] - P0[0] );
  c03.push_back( P3[1] - P0[1] );
  c03.push_back( P3[2] - P0[2] );
  double d03 = iiflucks::vectorLength( c03 );

  dV = 0.5 * fabs( iiflucks::tripleProduct( c03, c01, c02) );

  return dV;
}

/// Midpoint between bottom and top triangle centroids is used as prism 
/// centroid.
void prismCentroid(std::vector<double> &P0, std::vector<double> &P1, std::vector<double> &P2, std::vector<double> &P3, std::vector<double> &P4, std::vector<double> &P5, std::vector<double> &Pc)
{
  std::vector<double> c1, c2;

  iiflucks::triangleCentroid(P0, P1, P2, c1);
  iiflucks::triangleCentroid(P3, P4, P5, c2);

  Pc.push_back( (c1[0] + c2[0]) / 2 ); 
  Pc.push_back( (c1[1] + c2[1]) / 2 ); 
  Pc.push_back( (c1[2] + c2[2]) / 2 ); 

}

/// Pyramid base area A is given by quadrangle with vertices 
/// _P0_, _P1_, _P2_ and _P3_.
/// Distance of apex vertex _P4_ from base gives the pyramid height _h_.
/// _V_ = _h_ * _A_ / 3. 
double pyramidVolume(const std::vector<double> P0, const std::vector<double> P1, const std::vector<double> P2, const std::vector<double> P3, const std::vector<double> P4)
{
  double dV = -1;

  // calculate base area
  double A = iiflucks::quadrangleArea(P0, P1, P2, P3);

  // calculate distance from base plane to apex
  std::vector<double> c01, c03;
  c01.push_back( P1[0] - P0[0] );
  c01.push_back( P1[1] - P0[1] );
  c01.push_back( P1[2] - P0[2] );

  c03.push_back( P3[0] - P0[0] );
  c03.push_back( P3[1] - P0[1] );
  c03.push_back( P3[2] - P0[2] );

  double h = iiflucks::distancePointPlane(P0, P4, c01, c03);

  dV = h * A / 3;

  return dV;  
}

/// Base quadrangle is given by vertices _P0_, _P1_, _P2_ and _P3_. 
/// _P4_ is apex vertex.
void pyramidCentroid(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3, const std::vector<double> &P4, std::vector<double> &Pc)
{
  Pc.erase(Pc.begin(), Pc.end());
  Pc.push_back( 0.375*P0[0] + 0.375*P2[0] + 0.25*P4[0] );
  Pc.push_back( 0.375*P0[1] + 0.375*P2[1] + 0.25*P4[1] );
  Pc.push_back( 0.375*P0[2] + 0.375*P2[2] + 0.25*P4[2] );
}

}

