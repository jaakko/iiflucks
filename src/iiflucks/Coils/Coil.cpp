#include "iiflucks.h"

/// Construct coil object with parameters.
Coil::Coil(double A, double J, double theta, std::vector<double> axis, std::vector<double> tr)
{
  this->A = A;
  this->J = J;
  this->theta = theta;
  this->axis = axis;
  this->tr = tr;
};

Coil::~Coil() { };

