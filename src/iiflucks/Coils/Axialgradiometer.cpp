#include "Axialgradiometer.h"

namespace iiflucks {

/// Bottom ring center is at (0, 0, 0) and current flows in counter clockwise
/// direction. The arc starts at ( _R_ cos( _phi_ ) , _R_ sin( _phi_ ), 0 )
/// and ends at ( _R_ cos( -_phi_ ) , _R_ sin( -_phi_ ), 0 ). 
/// Here sin( _phi_ ) = _w_ / 2 _R_. 
/// The top ring starts at ( _R_ cos( -_phi_ ), _R_ sin( -_phi_ ), _b_)
/// and ends at ( _R_ cos( _phi_ ), _R_ sin( _phi_ ), _b_ ).
/// The current is flowing in clockwise direction on the top ring. 
/// The two vertical wires separated by distance _w_ connecting the top
/// and bottom loops create small parasitic inductance to the gradiometer
/// coil. _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
double axialGradiometer(const double R, const double b, const double w, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const bool test)
{
  double phi0 = asin( w / (2 * R) );
  double phi1 = 2 * M_PI - phi0;
  double xw0 = sqrt( R * R - w * w / 4 );
  double yw0 = w / 2;
  double xw1 = xw0; 
  double yw1 = -yw0;
  double len = 0;
  int NR = R * ( M_PI - phi0 ) * M / ( 2 * R * ( M_PI - phi0 ) + b );
  int NS = b * M / ( 4 * R * ( M_PI - phi0 ) + 2 * b );

  if( verbose ) std::cout << "-- " << NR << " ring elements, " << NS << " straight elements" << std::endl;

  std::vector<double> a, v; // axis of rotation and vector to rotate

  double anorm = sqrt( ax*ax + ay*ay + az*az );

  if( anorm > 0 && theta != 0 )
  {
    a.push_back( ax / anorm );
    a.push_back( ay / anorm );
    a.push_back( az / anorm );
  }

  // bottom ring
  len = std::abs( ringWire(R, phi0, phi1, d, NR, I, ax, ay, az, theta, xt, yt, zt, verbose, test) );

  // start position of vertical wire 1, rotated like the bottom ring
  double xt1 = xw0;
  double yt1 = -yw0;
  double zt1 = 0;

  if( anorm > 0 && theta != 0 )
  {
    v.erase( v.begin(), v.end() );
    v.push_back( xt1 );
    v.push_back( yt1 );
    v.push_back( zt1 );
        
    iiflucks::QuaternionRotation(a, theta, v);

    xt1 = v[ 0 ];
    yt1 = v[ 1 ];
    zt1 = v[ 2 ];
  }

  len += std::abs( straightWire(b, d, NS, I, ax, ay, az, theta, xt + xt1, yt + yt1, zt + zt1, verbose, test) );

  // center position of top ring, rotated like the bottom ring
  xt1 = 0;
  yt1 = 0;
  zt1 = b;

  if( anorm > 0 && theta != 0 )
  {
    v.erase( v.begin(), v.end() );
    v.push_back( xt1 );
    v.push_back( yt1 );
    v.push_back( zt1 );
        
    iiflucks::QuaternionRotation(a, theta, v);

    xt1 = v[ 0 ];
    yt1 = v[ 1 ];
    zt1 = v[ 2 ];
  }

  len += std::abs( ringWire(R, phi1, phi0, d, NR, -I, ax, ay, az, theta, xt + xt1, yt + yt1, zt + zt1, verbose, test) );

  // start position of vertical wire 2, rotated like the bottom ring
  xt1 = xw0;
  yt1 = yw0;
  zt1 = b;

  if( anorm > 0 && theta != 0 )
  {
    v.erase( v.begin(), v.end() );
    v.push_back( xt1 );
    v.push_back( yt1 );
    v.push_back( zt1 );
        
    iiflucks::QuaternionRotation(a, theta, v);

    xt1 = v[ 0 ];
    yt1 = v[ 1 ];
    zt1 = v[ 2 ];
  }

  len += std::abs( straightWire(-b, d, NS, -I, ax, ay, az, theta, xt + xt1, yt + yt1, zt + zt1, verbose, test) );

  return len;
}

}

