#include "iiflucks.h"
#include <iostream>
#include <gsl/gsl_integration.h>

// Bx(t) function.
double helix_Bx(double t, void * params)
{
   double R = ( (double *) params)[ 0 ];
   double L = ( (double *) params)[ 1 ];
   double N = ( (double *) params)[ 2 ];
   double x = ( (double *) params)[ 3 ];
   double y = ( (double *) params)[ 4 ];
   double z = ( (double *) params)[ 5 ];

   double Bx = ( R * cos( t ) * ( z - t * L /( 2 * M_PI * N) ) - L * ( y - R * sin( t ) ) / ( 2 * M_PI * N ) ) / pow( ( x - R * cos( t ) ) * ( x - R * cos( t ) ) + ( y - R * sin( t ) ) * ( y - R * sin( t ) ) + ( z - t * L / ( 2 * M_PI * N ) ) * ( z - t * L /( 2 * M_PI * N ) ), 1.5);

   return Bx;
}

// By(t) function.
double helix_By(double t, void * params)
{
   double R = ( (double *) params)[ 0 ];
   double L = ( (double *) params)[ 1 ];
   double N = ( (double *) params)[ 2 ];
   double x = ( (double *) params)[ 3 ];
   double y = ( (double *) params)[ 4 ];
   double z = ( (double *) params)[ 5 ];

   double By = ( R * sin( t ) * ( z - t * L /( 2 * M_PI * N) ) + L * ( x - R * cos( t ) ) / ( 2 * M_PI * N ) ) / pow( ( x - R * cos( t ) ) * ( x - R * cos( t ) ) + ( y - R * sin( t ) ) * ( y - R * sin( t ) ) + ( z - t * L / ( 2 * M_PI * N ) ) * ( z - t * L /( 2 * M_PI * N ) ), 1.5);

   return By;
}

// Bz(t) function.
double helix_Bz(double t, void * params)
{
   double R = ( (double *) params)[ 0 ];
   double L = ( (double *) params)[ 1 ];
   double N = ( (double *) params)[ 2 ];
   double x = ( (double *) params)[ 3 ];
   double y = ( (double *) params)[ 4 ];
   double z = ( (double *) params)[ 5 ];

   double Bz = ( R * R - R * y * sin( t ) - R * x * cos( t ) ) / pow( ( x - R * cos( t ) ) * ( x - R * cos( t ) ) + ( y - R * sin( t ) ) * ( y - R * sin( t ) ) + ( z - t * L / ( 2 * M_PI * N ) ) * ( z - t * L /( 2 * M_PI * N ) ), 1.5);

   return Bz;
}

/// Numerical integration of magnetic field at position P. Return 0 in success.
  int iiflucks::fieldHelix(const double R, const double L, const double N, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  double params[] = {R, L, N, P[ 0 ], P[ 1 ], P[ 2 ]};

  double Bx, By, Bz, Bxerr, Byerr, Bzerr;

  gsl_function FBx;
  FBx.function = &helix_Bx;
  FBx.params = params;

  int intres = gsl_integration_qng(&FBx, t1, t2, epsabs, 0, &Bx, &Bxerr, &neval);

  if( intres != 0 ) std::cerr << "** Helix Bx integration error. " << intres << std::endl;

  gsl_function FBy;
  FBy.function = &helix_By;
  FBy.params = params;

  intres = gsl_integration_qng(&FBy, t1, t2, epsabs, 0, &By, &Byerr, &neval);

  if( intres != 0 ) std::cerr << "** Helix By integration error. " << intres << std::endl;

  gsl_function FBz;
  FBz.function = &helix_Bz;
  FBz.params = params;

  intres = gsl_integration_qng(&FBz, t1, t2, epsabs, 0, &Bz, &Bzerr, &neval);

  if( intres != 0 ) std::cerr << "** Helix Bz integration error. " << intres << std::endl;

  iiflucks::makeVector( Bx, By, Bz, B );
  iiflucks::scaleVector( 1e-7 * I, B );
  iiflucks::makeVector( Bxerr, Byerr, Bzerr, Berr );
  iiflucks::scaleVector( 1e-7 * I, Berr );

  return intres;
}

/// Calculation of magnetic field at position P. Return 0 in success.
int iiflucks::fieldLine(const double I, const double t1, const double t2, const std::vector<double> P, std::vector<double> &Bv)
{
  double x = P[ 0 ];
  double y = P[ 1 ];
  double z = P[ 2 ];

  double Bx = 0, By = 0, Bz = 0;

  if( std::abs( x ) > 0 || std::abs( y ) > 0 )
  {
    Bx = ( t2 - z ) / sqrt( t2 * t2 + x * x + y * y - 2 * t2 * z + z * z ) ;
    Bx -= ( t1 - z ) / sqrt( t1 * t1 + x * x + y * y - 2 * t1 * z + z * z ) ;
    Bx *= -y / ( x * x + y * y );

    By = ( t2 - z ) / sqrt( t2 * t2 + x * x + y * y - 2 * t2 * z + z * z ) ;
    By -= ( t1 - z ) / sqrt( t1 * t1 + x * x + y * y - 2 * t1 * z + z * z ) ;
    By *= x / ( x * x + y * y );
  }
  else return -1;

  iiflucks::makeVector( Bx, By, Bz, Bv );
  iiflucks::scaleVector( I * 1e-7, Bv );

  return 0;
}

// Bx(t) function.
double ring_Bx(double t, void * params)
{
   double R = ( (double *) params)[ 0 ];
   double x = ( (double *) params)[ 1 ];
   double y = ( (double *) params)[ 2 ];
   double z = ( (double *) params)[ 3 ];

   double Bx = R * R * z * cos( t ) / pow( ( x - R * cos( t ) ) * ( x - R * cos( t ) ) + ( y - R * sin( t ) ) * ( y - R * sin( t ) ) + z * z , 1.5 );

   return Bx;
}

// By(t) function.
double ring_By(double t, void * params)
{
   double R = ( (double *) params)[ 0 ];
   double x = ( (double *) params)[ 1 ];
   double y = ( (double *) params)[ 2 ];
   double z = ( (double *) params)[ 3 ];

   double By = R * R * z * sin( t ) / pow( ( x - R * cos( t ) ) * ( x - R * cos( t ) ) + ( y - R * sin( t ) ) * ( y - R * sin( t ) ) + z * z , 1.5 );

   return By;
}

// Bz(t) function.
double ring_Bz(double t, void * params)
{
   double R = ( (double *) params)[ 0 ];
   double x = ( (double *) params)[ 1 ];
   double y = ( (double *) params)[ 2 ];
   double z = ( (double *) params)[ 3 ];

   double Bz = R * ( R - x * cos( t ) - y * sin( t ) ) / pow( ( x - R * cos( t ) ) * ( x - R * cos( t ) ) + ( y - R * sin( t ) ) * ( y - R * sin( t ) ) + z * z , 1.5 );

   return Bz;
}

/// Numerical integration of magnetic field at position P.
int iiflucks::fieldRing(const double R, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  double params[] = {R, P[ 0 ], P[ 1 ], P[ 2 ]};

  double Bx, By, Bz, Bxerr, Byerr, Bzerr;

  gsl_function FBx;
  FBx.function = &ring_Bx;
  FBx.params = params;

  int intres = gsl_integration_qng(&FBx, t1, t2, epsabs, 0, &Bx, &Bxerr, &neval);

  if( intres != 0 ) std::cerr << "** Ring Bx integration error. " << intres << std::endl;

  gsl_function FBy;
  FBy.function = &ring_By;
  FBy.params = params;

  intres = gsl_integration_qng(&FBy, t1, t2, epsabs, 0, &By, &Byerr, &neval);

  if( intres != 0 ) std::cerr << "** Ring By integration error. " << intres << std::endl;

  gsl_function FBz;
  FBz.function = &ring_Bz;
  FBz.params = params;

  intres = gsl_integration_qng(&FBz, t1, t2, epsabs, 0, &Bz, &Bzerr, &neval);

  if( intres != 0 ) std::cerr << "** Ring Bz integration error. " << intres << std::endl;

  iiflucks::makeVector( Bx, By, Bz, B );
  iiflucks::scaleVector( 1e-7 * I, B );
  iiflucks::makeVector( Bxerr, Byerr, Bzerr, Berr );
  iiflucks::scaleVector( 1e-7 * I, Berr );

  return intres;
}

// Bx(t) function.
double ringcyl_Bx(double t, void * params)
{
   double R  = ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];
   double x  = ( (double *) params)[ 2 ];
   double y  = ( (double *) params)[ 3 ];
   double z  = ( (double *) params)[ 4 ];

   double Bx = ( R * R - R * y * cos( t ) - R * z * sin( t ) );
   Bx /= pow( ( x - sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) * ( x - sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) + ( y - R * cos( t ) ) * ( y - R * cos( t ) ) + ( z - R * sin( t ) ) * ( z - R * sin( t ) ), 1.5 );

   return Bx;
}

// By(t) function.
double ringcyl_By(double t, void * params)
{
   double R  = ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];
   double x  = ( (double *) params)[ 2 ];
   double y  = ( (double *) params)[ 3 ];
   double z  = ( (double *) params)[ 4 ];

   double By = R * cos( t ) * ( x - sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) );
   By -= R * R * cos( t ) * sin( t ) * ( z - R * sin( t ) ) / sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) );

   By /= pow( ( x - sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) * ( x - sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) + ( y - R * cos( t ) ) * ( y - R * cos( t ) ) + ( z - R * sin( t ) ) * ( z - R * sin( t ) ), 1.5 );

   return By;
}

// Bz(t) function.
double ringcyl_Bz(double t, void * params)
{
   double R  = ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];
   double x  = ( (double *) params)[ 2 ];
   double y  = ( (double *) params)[ 3 ];
   double z  = ( (double *) params)[ 4 ];

   double Bz = R * R * cos( t ) * sin( t ) * ( y - R * cos( t ) ) / sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) );
   Bz += R * sin( t ) * ( x - sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) );

   Bz /= pow( ( x - sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) * ( x - sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) + ( y - R * cos( t ) ) * ( y - R * cos( t ) ) + ( z - R * sin( t ) ) * ( z - R * sin( t ) ), 1.5 );

   return Bz;
}

/// Numerical integration of magnetic field at position P.
int iiflucks::fieldRingCylinder(const double R, const double R2, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  double params[] = {R, R2, P[ 0 ], P[ 1 ], P[ 2 ]};

  double Bx, By, Bz, Bxerr, Byerr, Bzerr;

  gsl_function FBx;
  FBx.function = &ringcyl_Bx;
  FBx.params = params;

  int intres = gsl_integration_qng(&FBx, t1, t2, epsabs, 0, &Bx, &Bxerr, &neval);

  if( intres != 0 ) std::cerr << "** Ring on cylinder Bx integration error. " << intres << std::endl;

  gsl_function FBy;
  FBy.function = &ringcyl_By;
  FBy.params = params;

  intres = gsl_integration_qng(&FBy, t1, t2, epsabs, 0, &By, &Byerr, &neval);

  if( intres != 0 ) std::cerr << "** Ring on cylinder By integration error. " << intres << std::endl;

  gsl_function FBz;
  FBz.function = &ringcyl_Bz;
  FBz.params = params;

  intres = gsl_integration_qng(&FBz, t1, t2, epsabs, 0, &Bz, &Bzerr, &neval);

  if( intres != 0 ) std::cerr << "** Ring on cylinder Bz integration error. " << intres << std::endl;

  iiflucks::makeVector( Bx, By, Bz, B );
  iiflucks::scaleVector( 1e-7 * I, B );
  iiflucks::makeVector( Bxerr, Byerr, Bzerr, Berr );
  iiflucks::scaleVector( 1e-7 * I, Berr );

  return intres;
}

// Bx(t) function.
double lsphere_Bx(double t, void * params)
{
   double R1  = ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];
   double x  = ( (double *) params)[ 2 ];
   double y  = ( (double *) params)[ 3 ];
   double z  = ( (double *) params)[ 4 ];
   double r2;

   double Bx = R2 * ( R1 * R2 / sqrt( R1 * R1 + R2 * R2 ) - y * cos( t ) - z * R1 * sin( t ) / sqrt( R1 * R1 + R2 * R2 ) );

   r2 = ( x - R2 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) ) * ( x - R2 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) );
   r2 += ( y - R1 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) ) * ( y - R1 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) );
   r2 += ( z - R2 * sin( t ) ) * ( z - R2 * sin( t ) );

   Bx /= pow( r2, 1.5 );

   return Bx;
}

// By(t) function.
double lsphere_By(double t, void * params)
{
   double R1  = ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];
   double x  = ( (double *) params)[ 2 ];
   double y  = ( (double *) params)[ 3 ];
   double z  = ( (double *) params)[ 4 ];
   double r2;

   double By = R2 * ( -R2 * R2 / sqrt( R1 * R1 + R2 * R2 ) + x * cos( t ) + z * R2 * sin( t ) / sqrt( R1 * R1 + R2 * R2 ) );

   r2 = ( x - R2 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) ) * ( x - R2 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) );
   r2 += ( y - R1 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) ) * ( y - R1 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) );
   r2 += ( z - R2 * sin( t ) ) * ( z - R2 * sin( t ) );

   By /= pow( r2, 1.5 );

   return By;
}

// Bz(t) function.
double lsphere_Bz(double t, void * params)
{
   double R1  = ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];
   double x  = ( (double *) params)[ 2 ];
   double y  = ( (double *) params)[ 3 ];
   double z  = ( (double *) params)[ 4 ];
   double r2;

   double Bz = R2 * ( x * R1 - y * R2 ) * sin( t ) / sqrt( R1 * R1 + R2 * R2 );

   r2 = ( x - R2 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) ) * ( x - R2 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) );
   r2 += ( y - R1 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) ) * ( y - R1 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2  ) );
   r2 += ( z - R2 * sin( t ) ) * ( z - R2 * sin( t ) );

   Bz /= pow( r2, 1.5 );

   return Bz;
}

/// Numerical integration of magnetic field at position P.
int iiflucks::fieldLineSphere(const double R1, const double R2, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  double params[] = {R1, R2, P[ 0 ], P[ 1 ], P[ 2 ]};

  double Bx, By, Bz, Bxerr, Byerr, Bzerr;

  gsl_function FBx;
  FBx.function = &lsphere_Bx;
  FBx.params = params;

  int intres = gsl_integration_qng(&FBx, t1, t2, epsabs, 0, &Bx, &Bxerr, &neval);

  if( intres != 0 ) std::cerr << "** Line on sphere Bx integration error. " << intres << std::endl;

  gsl_function FBy;
  FBy.function = &lsphere_By;
  FBy.params = params;

  intres = gsl_integration_qng(&FBy, t1, t2, epsabs, 0, &By, &Byerr, &neval);

  if( intres != 0 ) std::cerr << "** Line on sphere By integration error. " << intres << std::endl;

  gsl_function FBz;
  FBz.function = &lsphere_Bz;
  FBz.params = params;

  intres = gsl_integration_qng(&FBz, t1, t2, epsabs, 0, &Bz, &Bzerr, &neval);

  if( intres != 0 ) std::cerr << "** Line on sphere By integration error. " << intres << std::endl;

  iiflucks::makeVector( Bx, By, Bz, B );
  iiflucks::scaleVector( 1e-7 * I, B );
  iiflucks::makeVector( Bxerr, Byerr, Bzerr, Berr );
  iiflucks::scaleVector( 1e-7 * I, Berr );

  return intres;
}

// Bx(t) function.
double loopsphere_Bx(double t, void * params)
{
   double R2  = ( (double *) params)[ 0 ];
   double theta1 = ( (double *) params)[ 1 ];
   double phi1 = ( (double *) params)[ 2 ];
   double dtheta = ( (double *) params)[ 3 ];
   double dphi = ( (double *) params)[ 4 ];
   double x2  = ( (double *) params)[ 5 ];
   double y2  = ( (double *) params)[ 6 ];
   double z2  = ( (double *) params)[ 7 ];

   double theta = theta1 + ( 1 - t * t * t * t ) * dtheta;
   double phi = phi1 + 0.5 * ( t + 1 ) * dphi;

   double x1 = R2 * cos( theta ) * cos( phi );
   double y1 = R2 * cos( theta ) * sin( phi );
   double z1 = R2 * sin( theta );
   double r2 = ( x2 - x1 ) * ( x2 - x1 ) + ( y2 - y1 ) * ( y2 - y1 ) + ( z2 - z1 ) * ( z2 - z1 );

   double lx = -0.5 * dphi * R2 * cos( theta ) * sin( phi ) + 4 * dtheta * R2 * t * t * t * sin( theta ) * cos( phi );
   double ly = 0.5 * dphi * R2 * cos( theta ) * cos( phi ) + 4 * dtheta * R2 * t * t * t * sin( theta ) * sin( phi );
   double lz = -4 * dtheta * R2 * t * t * t * cos( theta );

   double Bx = ly * ( z2 - z1 ) - lz * ( y2 - y1 );

   Bx /= pow( r2, 1.5 );

   return Bx;
}

// By(t) function.
double loopsphere_By(double t, void * params)
{
   double R2  = ( (double *) params)[ 0 ];
   double theta1 = ( (double *) params)[ 1 ];
   double phi1 = ( (double *) params)[ 2 ];
   double dtheta = ( (double *) params)[ 3 ];
   double dphi = ( (double *) params)[ 4 ];
   double x2  = ( (double *) params)[ 5 ];
   double y2  = ( (double *) params)[ 6 ];
   double z2  = ( (double *) params)[ 7 ];

   double theta = theta1 + ( 1 - t * t * t * t ) * dtheta;
   double phi = phi1 + 0.5 * ( t + 1 ) * dphi;

   double x1 = R2 * cos( theta ) * cos( phi );
   double y1 = R2 * cos( theta ) * sin( phi );
   double z1 = R2 * sin( theta );
   double r2 = ( x2 - x1 ) * ( x2 - x1 ) + ( y2 - y1 ) * ( y2 - y1 ) + ( z2 - z1 ) * ( z2 - z1 );

   double lx = -0.5 * dphi * R2 * cos( theta ) * sin( phi ) + 4 * dtheta * R2 * t * t * t * sin( theta ) * cos( phi );
   double ly = 0.5 * dphi * R2 * cos( theta ) * cos( phi ) + 4 * dtheta * R2 * t * t * t * sin( theta ) * sin( phi );
   double lz = -4 * dtheta * R2 * t * t * t * cos( theta );

   double By = lz * ( x2 - x1 ) - lx * ( z2 - z1 );

   By /= pow( r2, 1.5 );

   return By;
}

// Bz(t) function.
double loopsphere_Bz(double t, void * params)
{
   double R2  = ( (double *) params)[ 0 ];
   double theta1 = ( (double *) params)[ 1 ];
   double phi1 = ( (double *) params)[ 2 ];
   double dtheta = ( (double *) params)[ 3 ];
   double dphi = ( (double *) params)[ 4 ];
   double x2  = ( (double *) params)[ 5 ];
   double y2  = ( (double *) params)[ 6 ];
   double z2  = ( (double *) params)[ 7 ];

   double theta = theta1 + ( 1 - t * t * t * t ) * dtheta;
   double phi = phi1 + 0.5 * ( t + 1 ) * dphi;

   double x1 = R2 * cos( theta ) * cos( phi );
   double y1 = R2 * cos( theta ) * sin( phi );
   double z1 = R2 * sin( theta );
   double r2 = ( x2 - x1 ) * ( x2 - x1 ) + ( y2 - y1 ) * ( y2 - y1 ) + ( z2 - z1 ) * ( z2 - z1 );

   double lx = -0.5 * dphi * R2 * cos( theta ) * sin( phi ) + 4 * dtheta * R2 * t * t * t * sin( theta ) * cos( phi );
   double ly = 0.5 * dphi * R2 * cos( theta ) * cos( phi ) + 4 * dtheta * R2 * t * t * t * sin( theta ) * sin( phi );
   double lz = -4 * dtheta * R2 * t * t * t * cos( theta );

   double Bz = lx * ( y2 - y1 ) - ly * ( x2 - x1 );

   Bz /= pow( r2, 1.5 );

   return Bz;
}

/// Numerical integration of magnetic field at position P.
int iiflucks::fieldLoopSphere(const double R2, const double theta1, const double phi1, const double dtheta, const double dphi, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  double params[] = {R2, theta1, phi1, dtheta, dphi, P[ 0 ], P[ 1 ], P[ 2 ]};

  double Bx, By, Bz, Bxerr, Byerr, Bzerr;

  gsl_function FBx;
  FBx.function = &loopsphere_Bx;
  FBx.params = params;

  int intres = gsl_integration_qng(&FBx, t1, t2, epsabs, 0, &Bx, &Bxerr, &neval);

  if( intres != 0 ) std::cerr << "** Loop on sphere Bx integration error. " << intres << std::endl;

  gsl_function FBy;
  FBy.function = &loopsphere_By;
  FBy.params = params;

  intres = gsl_integration_qng(&FBy, t1, t2, epsabs, 0, &By, &Byerr, &neval);

  if( intres != 0 ) std::cerr << "** Loop on sphere By integration error. " << intres << std::endl;

  gsl_function FBz;
  FBz.function = &loopsphere_Bz;
  FBz.params = params;

  intres = gsl_integration_qng(&FBz, t1, t2, epsabs, 0, &Bz, &Bzerr, &neval);

  if( intres != 0 ) std::cerr << "** Loop on sphere By integration error. " << intres << std::endl;

  iiflucks::makeVector( Bx, By, Bz, B );
  iiflucks::scaleVector( 1e-7 * I, B );
  iiflucks::makeVector( Bxerr, Byerr, Bzerr, Berr );
  iiflucks::scaleVector( 1e-7 * I, Berr );

  return intres;
}
