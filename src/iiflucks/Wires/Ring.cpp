#include "iiflucks.h"
#include <iostream>

Ring::~Ring() { };

/// Length = ( t2 - t1 ) * R
double Ring::Length(const double t1, const double t2)
{
  return std::abs( R * ( t2 - t1 ) );
}

/// dl = dt * R
double Ring::dl(const double dt, const double t)
{
  return std::abs( R * dt );
}

/// Volume = A * dt * R 
double Ring::dv(double dt, const double t)
{
  return std::abs( A * dt * R );
}

/// Volume = pi * ( r2^2 - r1^2 ) * dt * R 
double Ring::dv(double dt, const double t, const double r1, const double r2)
{
  return std::abs( M_PI * ( r2 * r2 - r1 * r1 ) * dt * R );
}

/// Length element dl = ds * dt:
double Ring::ds(const double t)
{
  return R;
}

/// The ring position vector P components are:
///    Px = R cos(t)
///    Py = R sin(t)
///    Pz = 0 
std::vector<double> Ring::XYZ(const double t)
{
  std::vector<double> P;

  P.erase( P.begin(), P.end() );
  P.push_back( R * cos( t ) );
  P.push_back( R * sin( t ) );
  P.push_back( 0 ); 

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Ring tangential vector T:
///    Tx = -sin(t)
///    Ty =  cos(t)
///    Tz =   0 
std::vector<double> Ring::Jv(const double t)
{
  std::vector<double> T;

  T.erase( T.begin(), T.end() );
  T.push_back( -sin( t ) );
  T.push_back(  cos( t ) );
  T.push_back( 0 ); 

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// Ring or arc center is at (0, 0, 0) and current flows in counter clockwise
/// direction. The arc starts at ( _R_ cos( _t1_ ) , _R_ sin( _t2_ ), 0 )
/// and ends at ( _R_ cos( _t2_ ) , _R_ sin( _t2_ ), 0 ). 
/// Sweep angle _t2_- _t1_ = 2 pi gives complete ring. 
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging.
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double Ring::Elements(const double t1, const double t2, const int M, const int test, const bool verbose)
{

  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( t2 - t1 ) / M ;
  double ds =  R;
  double dl = dt * ds; 
  double dv = A * dt * ds;

  for( int i = 0; i < M; i++ )
  {
    t = t1 + (i + 0.5) * dt;

    len += dl;
    vol += dv;

    P = XYZ( t );
    T = Jv( t );

    if( dt < 0 ) iiflucks::scaleVector( -1.0, T );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dl=" << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

/// Numerical integration of magnetic field at position P.
int Ring::B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  return iiflucks::fieldRing(R, I, t1, t2, P, epsabs, B, Berr, neval);
}
