#include "Straightwire.h"

namespace iiflucks {

/// The straight line position vector P components are:
///    Px = 0 
///    Py = 0 
///    Pz = t
///
/// Tangential vector T:
///    Tx =  0 
///    Ty =  0 
///    Tz =  1
///
/// Length element dl = ds * dt:
///    ds = 1
///
double straightline(const double t, const double t1, const double t2, std::vector<double> &P, std::vector<double> &T, double &ds)
{
  P.erase( P.begin(), P.end() );
  P.push_back( 0 );
  P.push_back( 0 );
  P.push_back( t ); 

  ds = 1;

  T.erase( T.begin(), T.end() );
  T.push_back( 0 );
  T.push_back( 0 );
  T.push_back( 1 ); 

  return std::abs( ds * ( t2 - t1 ) );
}

/// Wire bottom center is at (0, 0, 0) and top end at (0, 0, _L_).  _L_ is wire 
/// length, _d_ wire diameter, _M_ number of volume elements and _I_ current.
/// Current flows from bottom to top.
/// Rotate wire elements around axis ( _ax_ , _ay_ , _az_ ) with angle 
/// _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging.
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum of Jz dv. With any other value the wire length is
/// returned. 
double straightWire(const double L, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test)
{
  double J = I / (M_PI * (d/2) * (d/2) );
  double dz = L / M;
  double A = (d/2) * (d/2) * M_PI;

  std::vector<double> a, P, T; // axis of rotation and vectors to rotate

  double anorm = sqrt( ax*ax + ay*ay + az*az );

  if( anorm > 0 && theta != 0 )
  {
    a.push_back( ax / anorm );
    a.push_back( ay / anorm );
    a.push_back( az / anorm );
  }

  double ds = 0;
  double dl = 0, len = 0, dv = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t, t1, t2;
  for( int i = 0; i < M; i++ )
  {
    t = (i + 0.5) * dz;
    t1 = i  * dz;
    t2 = (i + 1.0) * dz;

    dl = iiflucks::straightline(t, t1, t2, P, T, ds);

    dv = dl * A;

    len += dl;
    vol += dv;

    if( anorm > 0 && theta != 0 )
    {
      iiflucks::QuaternionRotation(a, theta, P);
      iiflucks::QuaternionRotation(a, theta, T);
    }

    P[ 0 ] += xt;
    P[ 1 ] += yt;
    P[ 2 ] += zt; 

    T[ 0 ] *= J;
    T[ 1 ] *= J;
    T[ 2 ] *= J; 

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dz=" << dz << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

}

