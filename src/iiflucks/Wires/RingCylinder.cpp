#include <iostream>
#include <gsl/gsl_integration.h>
#include "iiflucks.h"

RingCylinder::~RingCylinder() { };

// Ring on cylinder surface wire length.
double fringcyl (double t, void * params)
{
   double R =  ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];

   double fringcyl = sqrt( R * R + R * R * R * R * sin(t) * sin(t) * cos(t) * cos(t) /( R2 * R2 - R * R * cos(t) * cos(t) ) );  

   return fringcyl;
}

   
/// Length t = [t1, t2].
double RingCylinder::Length(const double t1, const double t2)
{
  double params[] = { R, R2 };
  double len = 0, lenerr = 0;
  int intres = 0;
  size_t neval = 0;

  gsl_function F;
  F.function = &fringcyl;
  F.params = params;

  if( t1 != t2 )
  {
    intres = gsl_integration_qng(&F, t1, t2, 0, 1e-5, &len, &lenerr, &neval); 
    len = std::abs( len );
    if( intres != 0 ) len = -1;
  }

  return len;
}

/// dl = dt * ds
double RingCylinder::dl(const double dt, const double t)
{
  double dl = 0;

  if( std::abs( R2 ) > std::abs( R ) ) dl = dt * ds( t );

  return std::abs( dl );
}

/// Volume = A * dt * ds 
double RingCylinder::dv(double dt, const double t)
{
  double dv = 0;

  if( std::abs( R2 ) > std::abs( R ) ) dv = A * dt * ds( t );

  return std::abs( dv );
}

/// Volume = pi * ( r2^2 - r1^2 ) * dt * ds 
double RingCylinder::dv(double dt, const double t, const double r1, const double r2)
{
  double dv = 0;

  if( std::abs( R2 ) > std::abs( R ) ) dv = M_PI * ( r2 * r2 - r1 * r1 ) * dt * ds( t );

  return std::abs( dv );
}

/// Length element dl = ds * dt:
///    ds = sqrt( R^2 + R^4 sin^2(t) cos^2(t) / ( R2^2 - R^2 cos^2(t) ) ) 
double RingCylinder::ds(const double t)
{
  double ds = 0;
  
  if( std::abs( R2 ) > std::abs( R ) ) ds = sqrt( R * R + R * R * R * R * sin( t ) * sin( t ) * cos( t ) * cos( t )/ ( R2 * R2 - R * R * cos( t ) * cos( t ) ) );

  return ds;
}

/// The ring on cylinder position vector P components are:
///    Px = sqrt( R2^2 - R^2 cos^2(t) )
///    Py = R cos(t)
///    Pz = R sin(t) 
std::vector<double> RingCylinder::XYZ(const double t)
{
  std::vector<double> P;

  P.erase( P.begin(), P.end() );
  P.push_back( sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) );
  P.push_back( R * cos( t ) );
  P.push_back( R * sin( t ) );

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Tangential vector T:
///    Tx = R^2 cos(t) sin(t) / ( ds * sqrt( R2^2 - R^2 cos^2(t) ) )
///    Ty = -R sin(t) / ds
///    Tz =  R cos(t) / ds
/// Length element dl = ds * dt:
///    ds = sqrt( R^2 + R^4 sin^2(t) cos^2(t) / ( R2^2 - R^2 cos^2(t) ) ) 
std::vector<double> RingCylinder::Jv(const double t)
{
  double ds;
  std::vector<double> T;

  if( std::abs( R2 ) > std::abs( R ) )
  {
    ds = sqrt( R * R + R * R * R * R * sin( t ) * sin( t ) * cos( t ) * cos( t )/ ( R2 * R2 - R * R * cos( t ) * cos( t ) ) );
  }
  else
  {
    ds = 0;
  }

  T.erase( T.begin(), T.end() );
  if( ds > 0 )
  {
    T.push_back( R * R * cos( t ) * sin( t ) / ( ds * sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) );
    T.push_back( -R * sin( t ) / ds );
    T.push_back(  R * cos( t ) / ds ); 
  }
  else
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 0 ); 
  }

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// Ring or arc on cylinder center is at (_R2_, 0, 0) and current flows in 
/// counter clockwise direction around x-axis. The arc starts at 
/// ( sqrt( _R2_^2 -_R_^2 cos^2( _phi0_ ) ), _R_ cos( _phi0_ ), _R_ sin( _phi0_ ) )
/// and ends at ( sqrt( _R2_^2 -_R_^2 cos^2( _phi1_ ) ), _R_ cos( _phi1_ ), _R_ sin( _phi1_ ) ).
/// Sweep angle _t2_- _t1_ = 2 pi gives complete ring. 
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double RingCylinder::Elements(const double t1, const double t2, const int M, const int test, const bool verbose)
{

  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( t2 - t1 ) / M ;
  double dl;
  double dv;

  for( int i = 0; i < M; i++ )
  {
    t = t1 + (i + 0.5) * dt;

    dl = dt * ds( t );
    dv = A * dt * ds( t );

    len += dl;
    vol += dv;

    P = XYZ( t );
    if( ds( t ) > 0 ) T = Jv( t ); else iiflucks::zeroVector( T );

    if( dt < 0 ) iiflucks::scaleVector( -1.0, T );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dl=" << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

/// Numerical integration of magnetic field at position P.
int RingCylinder::B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  return iiflucks::fieldRingCylinder(R, R2, I, t1, t2, P, epsabs, B, Berr, neval);
}
