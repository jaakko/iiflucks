#include "iiflucks.h"
#include <iostream>

Solenoid::~Solenoid() { };

/// Total length for all turns and layers. 
double Solenoid::CalcLength(std::vector<double> t1v, std::vector<double> t2v, bool verbose)
{
  dth = 0;
  if( layers > 1 ) dth = th / ( layers - 1 );

  double length = 0;
  for( int layer = 0; layer < layers; layer++)
  {
    length += 2 * M_PI * turns * Solenoid::ds( 0, 0, 0, layer );
  }

  return length; 
}

/// Length t = [t1, t2] for helix on given layer number.
double Solenoid::Length(const double t1, const double t2, const int section, const int turn, const int layer)
{
  double ds = Solenoid::ds( 0, 0, 0, layer );

  return std::abs( ds * ( t2 - t1 ) );
}

/// dl = dt * ds
double Solenoid::dl(const double dt, const double t, const int section, const int turn, const int layer)
{
  double dl = dt * Solenoid::ds( 0, 0, 0, layer );

  return std::abs( dl );
}

/// Volume = A * dt * ds 
double Solenoid::dv(double dt, const double t, const int section, const int turn, const int layer)
{
  double dv = A * dt * Solenoid::ds( 0, 0, 0, layer );

  return std::abs( dv );
}

/// Volume = pi * ( r2^2 - r1^2 ) * dt * ds 
double Solenoid::dv(double dt, const double t, const double r1, const double r2, const int section, const int turn, const int layer)
{
  double dv = M_PI * ( r2 * r2 - r1 * r1 ) * dt * Solenoid::ds( 0, 0, 0, layer );

  return std::abs( dv );
}

/// Length element dl = ds * dt:
double Solenoid::ds(const double t, const int section, const int turn, const int layer)
{
  double Rl = R + layer * dth;
  double ds = sqrt( Rl * Rl + L * L / ( 4 * M_PI * M_PI * turns * turns ) ); 

  return ds;
}

/// The solenoid position vector P components.
std::vector<double> Solenoid::XYZ(const double t, const int section, const int turn, const int layer)
{
  double Rl = R + layer * dth;

  std::vector<double> P;
  P.push_back( Rl * cos( t ) );
  P.push_back( Rl * sin( t ) );
  P.push_back( t * L / ( 2 * M_PI * turns ) ); 

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Tangential vector T.
std::vector<double> Solenoid::Jv(const double t, const int section, const int turn, const int layer)
{
  double Rl = R + layer * dth;
  double ds = Solenoid::ds( 0, 0, 0, layer );

  std::vector<double> T;
  if( ds > 0 )
  {
    T.push_back( -Rl * sin( t ) / ds );
    T.push_back(  Rl * cos( t ) / ds );
    T.push_back( L / ( 2 * M_PI * turns * ds ) ); 
  }
  else
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 0 ); 
  }

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// Number of equally long elements lc for section, turn and layer.
int Solenoid::NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v, const int section, const int turn, const int layer)
{
  int section_elements = -1;
  double ds = Solenoid::ds( 0, 0, 0, layer );

  if( std::abs( lc ) > 0 ) section_elements = std::abs( ceil( ds * ( t2v[0] - t1v[0] ) / lc ) );

  return section_elements;
}

/// Number of equally long elements lc for coil.
int Solenoid::NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v)
{
  int elements = 0;

  for(int layer = 0; layer < layers; layer++)
    elements += Solenoid::NElements(lc, t1v, t2v, 0, 0, layer);

  return elements;
}

/// Solenoid layer is modeled with helix and elements are printed for given
/// layer number. For all elements in the helix t1 = 0 and t2 = 2 * Pi * turns. 
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double Solenoid::Elements(const double t1, const double t2, const int section, const int turn, const int layer, const int M, const int test, const bool verbose)
{

  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( t2 - t1 ) / M ;
  double ds = 0;
  double dl;
  double dv;

  dl = Solenoid::dl(dt, 0, 0, 0, layer);
  dv = Solenoid::dv(dt, 0, 0, 0, layer);

  for( int i = 0; i < M; i++ )
  {
    t = t1 + (i + 0.5) * dt;

    len += dl;
    vol += dv;

    P.erase( P.begin(), P.end() );
    P = Solenoid::XYZ(t, 0, 0, layer);

    T.erase( T.begin(), T.end() );
    T = Solenoid::Jv(t, 0, 0, layer);
    if( dt < 0 ) iiflucks::scaleVector( -1.0, T );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dl=" << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

