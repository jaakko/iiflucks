#include "iiflucks.h"
#include <iostream>

Line::~Line() { };

/// Length = ( t2 - t1 ) * R
double Line::Length(const double t1, const double t2)
{
  return std::abs( t2 - t1 );
}

/// dl = dt 
double Line::dl(const double dt, const double t)
{
  return std::abs( dt );
}

/// Volume = A * dt 
double Line::dv(double dt, const double t)
{
  return std::abs( A * dt );
}

/// Volume = pi * (r2^2 - r1^2 ) * dt 
double Line::dv(double dt, const double t, const double r1, const double r2)
{
  return std::abs( M_PI * ( r2 * r2 - r1 * r1 ) * dt );
}

/// Length element dl = ds * dt:
double Line::ds(const double t)
{
  return 1;
}

/// The line position vector P components are:
///    Px = 0
///    Py = 0
///    Pz = t 
std::vector<double> Line::XYZ(const double t)
{
  std::vector<double> P;

  P.erase( P.begin(), P.end() );
  P.push_back( 0 ); 
  P.push_back( 0 ); 
  P.push_back( t ); 

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Line tangential vector T:
///    Tx =   0 
///    Ty =   0 
///    Tz =   1 
std::vector<double> Line::Jv(const double t)
{
  std::vector<double> T;

  T.erase( T.begin(), T.end() );
  T.push_back( 0 ); 
  T.push_back( 0 ); 
  T.push_back( 1 ); 

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// Line goes from (0, 0, _t1_) to (0, 0, _t2_) and is divided into _M_ number
/// of elements.
/// Rotate wire elements around axis ( _ax_ , _ay_ , _az_ ) with angle 
/// _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging.
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum of Jz dv. With any other value the wire length is
/// returned. 
double Line::Elements(const double t1, const double t2, const int M, const int test, const bool verbose)
{

  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( t2 - t1 ) / M ;
  double ds =  1;
  double dl = dt * ds; 
  double dv = A * dt * ds;

  for( int i = 0; i < M; i++ )
  {
    t = t1 + (i + 0.5) * dt;

    len += dl;
    vol += dv;

    P = XYZ( t );
    T = Jv( t );

    if( dt < 0 ) iiflucks::scaleVector( -1.0, T );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dl=" << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

/// Calculation of magnetic field at position P. Return 0 in success.
int Line::B(const double I, const double t1, const double t2, const std::vector<double> P, std::vector<double> &B)
{
  return iiflucks::fieldLine(I, t1, t2, P, B);
}
