#include "iiflucks.h"
#include <iostream>

/// The helix position vector P components are:
///    Px = r cos(t)
///    Py = r sin(t)
///    Pz = t L / ( 2 pi N)
///
/// Tangential vector T:
///    Tx = -r sin(t) / ds
///    Ty =  r cos(t) / ds
///    Tz =  L / ( 2 pi N ds )
///
/// Length element dl = ds * dt:
///    ds = sqrt( r^2 + L^2 / ( 2 pi N )^2 )
///
double iiflucks::helix(const double t, const double t1, const double t2, const double r, const double L, const double N,  std::vector<double> &P, std::vector<double> &T, double &ds)
{
  P.erase( P.begin(), P.end() );
  P.push_back( r * cos( t ) );
  P.push_back( r * sin( t ) );
  P.push_back( t * L / ( 2 * M_PI * N ) ); 

  ds = sqrt( r * r + L * L / ( 4 * M_PI * M_PI * N * N ) ); 
  T.erase( T.begin(), T.end() );
  if( ds > 0 )
  {
    T.push_back( -r * sin( t ) / ds );
    T.push_back(  r * cos( t ) / ds );
    T.push_back( L / ( 2 * M_PI * N * ds ) ); 
  }
  else
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 0 ); 
  }

  return std::abs( ds * ( t2 - t1 ) );
}

/// Helix bottom center is at (0, 0, 0) and wire starts from (_r_, 0, 0).  
/// Helix radius is _r_, length _L_, number of turns _N_ and wire diameter
/// _d_. Current _I_ flows in wire in counter clockwise direction and
/// is divided to _M_ number of volume elements.
/// Rotate wire elements around axis ( _ax_ , _ay_ , _az_ ) with angle 
/// _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
double iiflucks::helixWire(const double r, const double L, const double N, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test)
{
  double J = I / (M_PI * (d/2) * (d/2) );
  double A = M_PI * (d/2) * (d/2);

  std::vector<double> axis, P, Jt; // axis of rotation and vectors to rotate

  iiflucks::makeVector( ax, ay, az, axis );
  iiflucks::normalizeVector( axis );

  Helix *coil = new Helix("helix", r, L, N, A, J, theta, axis, {xt, yt, zt} ); 

  double dl = 0, len = 0, dv = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = 2 * M_PI * N / M ;

  dl = coil->dl( dt, 0 );
  dv = coil->dv( dt, 0 );

  for( int i = 0; i < M; i++ )
  {
    t = 2 * M_PI * (i + 0.5) * N / M;

    len += dl;
    vol += dv;

    P = coil->XYZ( t );
    Jt = coil->Jv( t );

    Jxl += Jt[ 0 ] * dv;
    Jyl += Jt[ 1 ] * dv;
    Jzl += Jt[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dl=" << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << Jt[ 0 ] << " " << Jt[ 1 ] << " " << Jt[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  delete( coil );

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

