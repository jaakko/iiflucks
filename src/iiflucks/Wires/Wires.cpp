#include "Wires.h"

namespace iiflucks {

/// Vertical wire starts from ( _xw_, _yw0_ , _zw_ ), is divided to _Mv_ 
/// longitudinal elements with element length _dl_, has cross section 
/// surface area _A_ and current density _Jw_. The flags _verbose_ and 
/// _test_ are used for debugging.  
double verticalWire(const int Mv, const double xw, const double yw0, const double zw, const double dl, const double A, const double Jw, const bool verbose, const bool test)
{	
  double yw = 0;
  double len = 0;
  double dv = 0;

  for( int j = 0; j < Mv; j++)
  {
    yw = yw0 + (j + 0.5) * dl;
    len += fabs( dl );
    dv = fabs( A * dl );

    if( verbose ) std::cout << "-- j=" << j << " ";

    if( verbose || !test )
    {
      std::cout << xw << " " << yw << " " << zw;
      std::cout << " " << 0 << " " << Jw << " " << 0;
      std::cout << " " << dv << std::endl;
    }
  }

  return len;
}

/// Horizontal wire starts from ( _xw0_, _yw_ , _zw_ ), is divided to _Mh_ 
/// longitudinal elements with element length _dl_, has cross section 
/// surface area _A_ and current density _Jw_. The flags _verbose_ and 
/// _test_ are used for debugging.  
double horizontalWire(const int Mh, const double xw0, const double yw, const double zw, const double dl, const double A, const double Jw, const bool verbose, const bool test)
{	
  double xw = 0;
  double len = 0;
  double dv = 0;

  for( int j = 0; j < Mh; j++)
  {
    xw = xw0 + (j + 0.5) * dl;
    len += fabs( dl );
    dv = fabs( A * dl );

    if( verbose ) std::cout << "-- j=" << j << " ";

    if( verbose || !test )
    {
      std::cout << xw << " " << yw << " " << zw;
      std::cout << " " << Jw << " " << 0 << " " << 0;
      std::cout << " " << dv << std::endl;
    }
  }

  return len;
}

/// Wire arc center is ( _xw0_, _yw0_ , _zw0_ ) and it starts from angle 
/// _phi0_ and has radius _r_ . It is divided to _Mr_ longitudinal elements 
/// with element angular length _dphi_. The wire has cross section surface 
/// area _A_ and current density _Jw_. 
/// The flags _verbose_ and _test_ are used for debugging.  
double arcWire(const int Mr, const double xw0, const double yw0, const double zw0, const double phi0, const double dphi, const double r, const double A, const double Jw, const bool verbose, const bool test)
{	
  double phi = 0;
  double dl = r * dphi;
  double dv = fabs( A * dl );
  double xw = 0;
  double yw = 0;
  double Jx = 0, Jy = 0;

  double len = 0;
  for( int j = 0; j < Mr; j++)
  {
    phi = phi0 + (j + 0.5) * dphi; 
    xw = r * cos( phi ) + xw0;
    yw = r * sin( phi ) + yw0;

    // direction of current density vector along with wire
    Jx = -Jw * sin( phi );
    Jy = Jw * cos( phi );

    len += fabs( dl );

    if( verbose ) std::cout << "-- j=" << j << " ";

    if( verbose || !test )
    {
      std::cout << xw << " " << yw << " " << zw0;
      std::cout << " " << Jx << " " << Jy << " " << 0;
      std::cout << " " << dv << std::endl;
    }
  }

  return len;
}

/// Wire helix center is ( _xw0_, _yw0_ , _zw0_ ) and it starts from angle 
/// _phi0_ and has radius _r_ with element angular length _dphi_. 
/// Number of longitudinal elements is _Mr_.
/// The helix length is given by _L_ and number of turns _N_ in z-direction.
/// The wire has cross section surface area _A_ and current density _Jw_. 
/// The flags _verbose_ and _test_ are used for debugging.  
double linkWire(int Mr, const double xw0, const double yw0, const double zw0, const double phi0, const double dphi, const double r, const double L, const int N, const double A, const double Jw, const bool verbose, const bool test)
{
  double phi = 0;
  double ds = dphi * sqrt( r * r + L * L / ( 4 * M_PI * M_PI * N * N) );
  double dv = fabs( A * ds );
  double xw = 0, yw = 0, zw = zw0;
  double Jx = 0, Jy = 0, Jz = 0;
  double dz = dphi * L * 2 / ( M_PI * N );

  double len = 0;
  for( int j = 0; j < Mr; j++)
  {
    phi = phi0 + (j + 0.5) * dphi; 
    xw = r * cos( phi ) + xw0;
    yw = r * sin( phi ) + yw0;
    zw = (j + 0.5) * dz + zw0;

    len += fabs( ds );

    // direction of current density vector along with wire
    Jx = -Jw * r * sin( phi ) / sqrt( r * r + L * L / ( 4 * M_PI * M_PI * N * N ) );
    Jy = Jw * r * cos( phi ) / sqrt( r * r + L * L / ( 4 * M_PI * M_PI * N * N ) );
    Jz = Jw / sqrt( 1 + 4 * M_PI * M_PI * N * N * r * r/ ( L * L ) );

    if( verbose ) std::cout << "-- j=" << j << " ";

    if( verbose || !test )
    {
      std::cout << xw << " " << yw << " " << zw;
      std::cout << " " << Jx << " " << Jy << " " << Jz;
      std::cout << " " << dv << std::endl;
    }
  }

  return len;

}

}
