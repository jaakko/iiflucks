#include "Racetrackcylinder.h"

// Ring on cylinder surface wire length.
double f (double t, void * params)
{
   double R =  ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];

   double f = sqrt( R * R + R * R * R * R * sin(t) * sin(t) * cos(t) * cos(t) /( R2 * R2 - R * R * cos(t) * cos(t) ) );  
   return f;
}

namespace iiflucks {

/// Straight sections along z-axis have length _h_, curved corners have radius
/// _R_ and curved end sections have length _e_. Radius of cylinder is _R2_.
/// From cylinder central z-axis cross section of winding mandrel has angle
/// _alpha_ ~ (2 _R_ + _e_) /  _R2_. Curved end section has angle 
/// _beta_ = _e_ / _R2_. Layer of coil is wound radially from _R2_ to 
/// _R2_ + _w_. Next new layer is wound on top of the previous layer at
/// distance of _t_ / ( _O_ - 1 ). This is repeated until all layers have been
/// wound.
/// 
/// Wire sections in first turn on first layer are:
///
/// 1. straight
/// _xw_ = sqrt( _Rn_^2 - _R_^2 )
/// _yw_ = _R_ 
/// _zw_ = [-_h_/2, +_h_/2]
///
/// 2. curved arc _t_ = [0, pi/2]
/// _xw_ = sqrt( _Rn_^2 - _R_^2 cos^2( t ) )
/// _yw_ = _R_ cos( t )
/// _zw_ = _R_ sin( t ) + h / 2
///
/// 3. circular _t_ = [ -_beta_ / 2, +_beta_ / 2]
/// _xw_ = _Rn_ cos( _t_ )
/// _yw_ = _Rn_ sin( _t_ )
/// _zw_ = +_h_ / 2 + _R_ 
///
/// 4. curved arc _t_ = [pi/2, pi]
/// _xw_ = sqrt( _Rn_^2 - _R_^2 cos^2( t ) )
/// _yw_ = _R_ cos( t )
/// _zw_ = _R_ sin( t ) + h / 2
///
/// 5. straight
/// _xw_ = sqrt( _Rn_^2 - _R_^2 ) 
/// _yw_ = -_R_ 
/// _zw_ = [+_h_/2, -_h_/2]
///
/// 6. curved arc _t_ = [pi, 3pi/2]
/// _xw_ = sqrt( _Rn_^2 - _R_^2 cos^2( t ) )
/// _yw_ = _R_ cos( t )
/// _zw_ = _R_ sin( t ) - h / 2
///
/// 7. circular _t_ = [ +_beta_ / 2, -_beta_ / 2]
/// _xw_ = _Rn_ cos( _t_ )
/// _yw_ = _Rn_ sin( _t_ )
/// _zw_ = -_h_ / 2 - _R_ 
///
/// 8. curved arc _t_ = [3pi/2, 2pi]
/// _xw_ = sqrt( _Rn_^2 - _R_^2 cos^2( t ) )
/// _yw_ = _R_ cos( t )
/// _zw_ = _R_ sin( t ) - _h_ / 2
///
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// Current flows in counter clockwise direction around x-axis.
/// Length of wire on one layer is 
/// L = N * ( 2*h + 2*e + integral(R^2 + R^4 sin^2(t) cos^2(t) / ( R2^2 - R^2 * cos^2(t) ), t=[0,2pi] ) ).
///
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double raceTrackCylinder(const double R, const double R2, const double h, const double e, const double w, const double t, const double d, const int N, const int O, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test)
{
  double xw = 0, yw = 0, zw = 0;
  double nxw = 0, nyw = 0, nzw = 0;
  double Jx = 0, Jy = 0, Jz = 0;
  double A = (d/2) * (d/2) * M_PI;
  double J = I / A;
  double alpha = ( 2 * R + e ) / R2;
  double beta = e / R2;

  // numerical integration of curved corners wire length
  double clen = 0;
  double clenerr = 0;
  size_t neval = 0;
  int intres = 0;
  double Rk = 0;
  double Rn = 0;
  double params[] = { Rk, Rn };

  gsl_function F;
  F.function = &f;
  F.params = params;

  double dw = 0;
  if( N > 1 ) dw = w / ( N - 1 );

  double dt = 0;
  if( O > 1 ) dt = t / ( O - 1 );

  // O layers each with N turns in radial direction
  if( verbose ) std::cout << "--         Ri " << std::setw(15) << std::setfill(' ') << "Rn" << std::setw(15) << std::setfill(' ') << "clen " << std::setw(15) << std::setfill(' ') << std::endl;

  // sin( alpha ) = R / R2
  // cos( alpha ) = sqrt( 1 - (R / R2)^2 )
  // dy = i * dw * R / R2 + k * dt * R / R2 
  // dx = i * dw * sqrt( 1 - (R / R2)^2 ) - k * dt * sqrt( 1 - (R / R2)^2 )

  // initial straight wire coordinates  
  double xw0 = sqrt( R2 * R2 - R * R );
  double yw0 = R;
  double sin_alpha = R / R2;
  double cos_alpha = sqrt( 1 - R * R / ( R2 * R2 ) );

  double wlen = 0;
  for( int k = 0; k < O; k++)
  {
    for( int i = 0; i < N; i++)
    {
      xw = xw0 + i * dw * cos_alpha - k * dt * sin_alpha;
      yw = yw0 + i * dw * sin_alpha + k * dt * sin_alpha;
      Rk = yw;
      Rn = sqrt( Rk * Rk + xw * xw );

      params[ 0 ] = Rk;
      params[ 1 ] = Rn;
      F.params = params;

      intres = gsl_integration_qng(&F, 0, 2 * M_PI, 0, 1e-5, &clen, &clenerr, &neval); 
      if( intres != 0 ) std::cerr << "** Corner length integration error. " << intres << std::endl;

    if( verbose ) std::cout << std::right << Rk << std::setw(15) << std::setfill(' ') << Rn << std::setw(15) << std::setfill(' ') << clen << std::setw(15) << std::setfill(' ') << std::endl;

      wlen += clen;
    }
  }

  wlen += 2 * O * N* ( h + e  );

  if( verbose ) std::cout << "-- calculated wire length " << wlen << std::endl;

  double lc = wlen / M; // length of volume elements     
  int Mh = ceil( h / lc ); // number of strainght elements along z-axis
  int Me = ceil( e / lc ); // number of circular end elements 
  int Mr = 0; // number of corner elements
  double len = 0;
  double vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;

  std::vector<double> a, v; // axis of rotation and vector to rotate

  double anorm = sqrt( ax*ax + ay*ay + az*az );

  if( anorm > 0 && theta != 0 )
  {
    a.push_back( ax / anorm );
    a.push_back( ay / anorm );
    a.push_back( az / anorm );
  }

  double phi = 0;
  double dphi = 0;
  double dsdt = 0;
  double dl = 0;
  double dv = 0;

  // O layers
  for( int k = 0; k < O; k++)
  {
    // N turns
    for( int i = 0; i < N; i++)
    {
      xw = xw0 + i * dw * cos_alpha - k * dt * sin_alpha;
      yw = yw0 + i * dw * sin_alpha + k * dt * sin_alpha;
      Rk = yw;
      Rn = sqrt( Rk * Rk + xw * xw );

      // right straight wire
      dl = h / Mh;
      dv = A * dl;

      if( verbose ) std::cout << "-- " << Mh << " straight elements" << std::endl;

      for( int j = 0; j < Mh; j++)
      {
        // center of volume element
        xw = sqrt( Rn * Rn - Rk * Rk );
        yw = Rk;
        zw = -h / 2 + (j + 0.5) * dl;

        // direction of current density vector along with wire
        nzw = 1; 

        Jx = 0;
        Jy = 0;
        Jz = J * nzw;

        len += dl;
        vol += dv;

        if( anorm > 0 && theta != 0 )
        {
          v.erase( v.begin(), v.end() );
          v.push_back( xw );
          v.push_back( yw );
          v.push_back( zw );
        
          iiflucks::QuaternionRotation(a, theta, v);

          xw = v[ 0 ];
          yw = v[ 1 ];
          zw = v[ 2 ];

          v.erase( v.begin(), v.end() );
          v.push_back( Jx );
          v.push_back( Jy );
          v.push_back( Jz );
        
          iiflucks::QuaternionRotation(a, theta, v);

          Jx = v[ 0 ];
          Jy = v[ 1 ];
          Jz = v[ 2 ];
        }

        Jxl += Jx * dv;
        Jyl += Jy * dv;
        Jzl += Jz * dv;

        if( verbose || test == 0 )
         {
           std::cout << xw << " " << yw << " " << zw;
           std::cout << " " << Jx << " " << Jy << " " << Jz;
           std::cout << " " << dv << std::endl;
         }
      }
  
      // top right curved arc 
      Mr = ceil( 0.25 * clen /  lc );
      dphi = 0.5 * M_PI / Mr;

      if( verbose ) std::cout << "-- " << Mr << " top right arc elements" << std::endl;

      for( int j = 0; j < Mr; j++)
      {
        phi = (j + 0.5) * dphi;

        // center of volume element
        xw = sqrt( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) );
        yw = Rk * cos( phi );
        zw = Rk * sin( phi ) + h / 2;

        dsdt = sqrt( Rk * Rk * Rk * Rk * cos( phi ) * cos( phi ) * sin( phi ) * sin( phi ) / ( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) ) + Rk * Rk );

        // direction of current density vector along with wire
        nxw = Rk * Rk * cos( phi ) * sin( phi ) / ( dsdt * sqrt( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) ) );
        nyw = -Rk * sin( phi ) / dsdt;
        nzw = +Rk * cos( phi ) / dsdt;

        Jx = J * nxw;
        Jy = J * nyw;
        Jz = J * nzw;

        dl = dphi * dsdt; 
        dv = std::abs( dl * A );

        len += dl;
        vol += dv;

        if( anorm > 0 && theta != 0 )
        {
          v.erase( v.begin(), v.end() );
          v.push_back( xw );
          v.push_back( yw );
          v.push_back( zw );
        
          iiflucks::QuaternionRotation(a, theta, v);

          xw = v[ 0 ];
          yw = v[ 1 ];
          zw = v[ 2 ];

          v.erase( v.begin(), v.end() );
          v.push_back( Jx );
          v.push_back( Jy );
          v.push_back( Jz );
        
          iiflucks::QuaternionRotation(a, theta, v);

          Jx = v[ 0 ];
          Jy = v[ 1 ];
          Jz = v[ 2 ];
        }

        Jxl += Jx * dv;
        Jyl += Jy * dv;
        Jzl += Jz * dv;

        if( verbose ) std::cout << "-- phi=" << phi << " dphi=" << dphi << " ";

        if( verbose || test == 0 )
        {
          std::cout << xw << " " << yw << " " << zw;
  	  std::cout << " " << Jx << " " << Jy << " " << Jz;
  	  std::cout << " " << dv << std::endl;
        }
      }

      // top circular wire
      dphi = beta / Me;
      dl = e / Me;
      dv = A * dl;

      if( verbose ) std::cout << "-- " << Me << " straight elements" << std::endl;

      for( int j = 0; j < Me; j++)
      {
        phi = -beta / 2 + (j + 0.5) * dphi;

      	// center of volume element
        xw = Rn * cos( phi );
        yw = Rn * sin( phi );
        zw = +h / 2 + Rk;

        // direction of current density vector along with wire
        nxw = -sin( phi );
        nyw =  cos( phi );	

        Jx = J * nxw;
        Jy = J * nyw;
        Jz = 0;

        len += dl;
        vol += dv;
  
        if( anorm > 0 && theta != 0 )
        {
          v.erase( v.begin(), v.end() );
          v.push_back( xw );
          v.push_back( yw );
          v.push_back( zw );
        
          iiflucks::QuaternionRotation(a, theta, v);

          xw = v[ 0 ];
          yw = v[ 1 ];
          zw = v[ 2 ];

          v.erase( v.begin(), v.end() );
          v.push_back( Jx );
          v.push_back( Jy );
          v.push_back( Jz );
        
          iiflucks::QuaternionRotation(a, theta, v);

          Jx = v[ 0 ];
          Jy = v[ 1 ];
          Jz = v[ 2 ];
        }

        Jxl += Jx * dv;
        Jyl += Jy * dv;
        Jzl += Jz * dv;

        if( verbose || test == 0 )
         {
           std::cout << xw << " " << yw << " " << zw;
           std::cout << " " << Jx << " " << Jy << " " << Jz;
           std::cout << " " << dv << std::endl;
         }
      }
  
      // top left arc 
      Mr = ceil( 0.25 * clen /  lc );
      dphi = 0.5 * M_PI / Mr;

      if( verbose ) std::cout << "-- " << Mr << " top right arc elements" << std::endl;

      for( int j = 0; j < Mr; j++)
      {
        phi = (j + 0.5) * dphi + 0.5 * M_PI;

        // center of volume element
        xw = sqrt( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) );
        yw = Rk * cos( phi );
        zw = Rk * sin( phi ) + h / 2;

        dsdt = sqrt( Rk * Rk * Rk * Rk * cos( phi ) * cos( phi ) * sin( phi ) * sin( phi ) / ( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) ) + Rk * Rk );

        // direction of current density vector along with wire
        nxw = Rk * Rk * cos( phi ) * sin( phi ) / ( dsdt * sqrt( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) ) );
        nyw = -Rk * sin( phi ) / dsdt;
        nzw = +Rk * cos( phi ) / dsdt;

        Jx = J * nxw;
        Jy = J * nyw;
        Jz = J * nzw;

        dl = dphi * dsdt; 
        dv = std::abs( dl * A );

        len += dl;
        vol += dv;
  
        if( anorm > 0 && theta != 0 )
        {
          v.erase( v.begin(), v.end() );
          v.push_back( xw );
          v.push_back( yw );
          v.push_back( zw );
        
          iiflucks::QuaternionRotation(a, theta, v);
  
          xw = v[ 0 ];
          yw = v[ 1 ];
          zw = v[ 2 ];
  
          v.erase( v.begin(), v.end() );
          v.push_back( Jx );
          v.push_back( Jy );
          v.push_back( Jz );
        
          iiflucks::QuaternionRotation(a, theta, v);

          Jx = v[ 0 ];
          Jy = v[ 1 ];
          Jz = v[ 2 ];
        }
  
        Jxl += Jx * dv;
        Jyl += Jy * dv;
        Jzl += Jz * dv;

        if( verbose ) std::cout << "-- phi=" << phi << " dphi=" << dphi << " ";

        if( verbose || test == 0 )
        {
          std::cout << xw << " " << yw << " " << zw;
          std::cout << " " << Jx << " " << Jy << " " << Jz;
          std::cout << " " << dv << std::endl;
        }
      }

      // left straight wire
      dl = h / Mh;
      dv = A * dl;

      if( verbose ) std::cout << "-- " << Mh << " straight elements" << std::endl;

      for( int j = 0; j < Mh; j++)
      {
        // center of volume element
        xw =  sqrt( Rn * Rn - Rk * Rk );
        yw = -Rk;
        zw = +h / 2 - (j + 0.5) * dl;

	// direction of current density vector along with wire
        nzw = -1; 

        Jx = 0;
        Jy = 0;
        Jz = J * nzw;

        len += dl;
        vol += dv;

        if( anorm > 0 && theta != 0 )
        {
          v.erase( v.begin(), v.end() );
          v.push_back( xw );
          v.push_back( yw );
          v.push_back( zw );
        
          iiflucks::QuaternionRotation(a, theta, v);

          xw = v[ 0 ];
          yw = v[ 1 ];
          zw = v[ 2 ];
  
          v.erase( v.begin(), v.end() );
          v.push_back( Jx );
          v.push_back( Jy );
          v.push_back( Jz );
        
          iiflucks::QuaternionRotation(a, theta, v);
  
          Jx = v[ 0 ];
          Jy = v[ 1 ];
          Jz = v[ 2 ];
        }

        Jxl += Jx * dv;
        Jyl += Jy * dv;
        Jzl += Jz * dv;
  
        if( verbose || test == 0 )
        {
          std::cout << xw << " " << yw << " " << zw;
          std::cout << " " << Jx << " " << Jy << " " << Jz;
          std::cout << " " << dv << std::endl;
        }
      }

      // bottom left arc
      Mr = ceil( 0.25 * clen / lc );
      dphi = 0.5 * M_PI / Mr;

      if( verbose ) std::cout << "-- " << Mr << " arc elements" << std::endl;

      for( int j = 0; j < Mr; j++)
      {
        phi = (j + 0.5) * dphi + M_PI;

        // center of volume element
        xw = sqrt( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) );
        yw = Rk * cos( phi );
        zw = Rk * sin( phi ) - h / 2;

        dsdt = sqrt( Rk * Rk * Rk * Rk * cos( phi ) * cos( phi ) * sin( phi ) * sin( phi ) / ( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) ) + Rk * Rk );

        // direction of current density vector along with wire
        nxw = Rk * Rk * cos( phi ) * sin( phi ) / ( dsdt * sqrt( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) ) );
        nyw = -Rk * sin( phi ) / dsdt;
        nzw = +Rk * cos( phi ) / dsdt;

        Jx = J * nxw;
        Jy = J * nyw;
        Jz = J * nzw;

        dl = dphi * dsdt; 
        dv = std::abs( dl * A );

	len += dl;
        vol += dv;

        if( anorm > 0 && theta != 0 )
        {
          v.erase( v.begin(), v.end() );
          v.push_back( xw );
          v.push_back( yw );
          v.push_back( zw );
        
          iiflucks::QuaternionRotation(a, theta, v);

          xw = v[ 0 ];
          yw = v[ 1 ];
          zw = v[ 2 ];

          v.erase( v.begin(), v.end() );
          v.push_back( Jx );
          v.push_back( Jy );
          v.push_back( Jz );
        
          iiflucks::QuaternionRotation(a, theta, v);

          Jx = v[ 0 ];
          Jy = v[ 1 ];
          Jz = v[ 2 ];
        }

        Jxl += Jx * dv;
        Jyl += Jy * dv;
        Jzl += Jz * dv;
      
        if( verbose ) std::cout << "-- phi=" << phi << " dphi=" << dphi << " ";

        if( verbose || test == 0 )
        {
          std::cout << xw << " " << yw << " " << zw;
          std::cout << " " << Jx << " " << Jy << " " << Jz;
          std::cout << " " << dv << std::endl;
        }
      }

      // bottom circular wire
      dphi = beta / Me;
      dl = e / Me;
      dv = A * dl;

      if( verbose ) std::cout << "-- " << Me << " straight elements" << std::endl;

      for( int j = 0; j < Me; j++)
      {
        phi = +beta / 2 - (j + 0.5) * dphi;

      	// center of volume element
        xw = Rn * cos( phi );
        yw = Rn * sin( phi );
        zw = -h / 2 - Rk;

        // direction of current density vector along with wire
        nxw = -sin( phi );
        nyw =  cos( phi );	

        Jx = J * nxw;
        Jy = J * nyw;
        Jz = 0;

        len += dl;
        vol += dv;
  
        if( anorm > 0 && theta != 0 )
        {
          v.erase( v.begin(), v.end() );
          v.push_back( xw );
          v.push_back( yw );
          v.push_back( zw );
        
          iiflucks::QuaternionRotation(a, theta, v);

          xw = v[ 0 ];
          yw = v[ 1 ];
          zw = v[ 2 ];
  
          v.erase( v.begin(), v.end() );
          v.push_back( Jx );
          v.push_back( Jy );
          v.push_back( Jz );
        
          iiflucks::QuaternionRotation(a, theta, v);

          Jx = v[ 0 ];
          Jy = v[ 1 ];
          Jz = v[ 2 ];
        }

        Jxl += Jx * dv;
        Jyl += Jy * dv;
        Jzl += Jz * dv;

        if( verbose || test == 0 )
        {
          std::cout << xw << " " << yw << " " << zw;
          std::cout << " " << Jx << " " << Jy << " " << Jz;
          std::cout << " " << dv << std::endl;
        }
      }
  
      // bottom right curved arc 
      Mr = ceil( 0.25 * clen /  lc );
      dphi = 0.5 * M_PI / Mr;

      if( verbose ) std::cout << "-- " << Mr << " arc elements" << std::endl;

      for( int j = 0; j < Mr; j++)
      {
        phi = 1.5 * M_PI + (j + 0.5) * dphi;

        // center of volume element
        xw = sqrt( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) );
        yw = Rk * cos( phi );
        zw = Rk * sin( phi ) - h / 2;

        dsdt = sqrt( Rk * Rk * Rk * Rk * cos( phi ) * cos( phi ) * sin( phi ) * sin( phi ) / ( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) ) + Rk * Rk );

        // direction of current density vector along with wire
        nxw = Rk * Rk * cos( phi ) * sin( phi ) / ( dsdt * sqrt( Rn * Rn - Rk * Rk * cos( phi ) * cos( phi ) ) );
        nyw = -Rk * sin( phi ) / dsdt;
        nzw = +Rk * cos( phi ) / dsdt;

        Jx = J * nxw;
        Jy = J * nyw;
        Jz = J * nzw;

        dl = dphi * dsdt; 
        dv = std::abs( dl * A );

	len += dl;
        vol += dv;

        if( anorm > 0 && theta != 0 )
        {
          v.erase( v.begin(), v.end() );
          v.push_back( xw );
          v.push_back( yw );
          v.push_back( zw );
        
          iiflucks::QuaternionRotation(a, theta, v);

          xw = v[ 0 ];
          yw = v[ 1 ];
          zw = v[ 2 ];

          v.erase( v.begin(), v.end() );
          v.push_back( Jx );
          v.push_back( Jy );
          v.push_back( Jz );
        
          iiflucks::QuaternionRotation(a, theta, v);
  
          Jx = v[ 0 ];
          Jy = v[ 1 ];
          Jz = v[ 2 ];
        }
  
        Jxl += Jx * dv;
        Jyl += Jy * dv;
        Jzl += Jz * dv;
      
        if( verbose ) std::cout << "-- phi=" << phi << " dphi=" << dphi << " ";
  
        if( verbose || test == 0 )
        {
          std::cout << xw << " " << yw << " " << zw;
          std::cout << " " << Jx << " " << Jy << " " << Jz;
          std::cout << " " << dv << std::endl;
        }
      }
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

}


