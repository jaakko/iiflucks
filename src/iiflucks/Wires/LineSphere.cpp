#include "iiflucks.h"
#include <iostream>

LineSphere::~LineSphere() { };

/// Length = ( t2 - t1 ) * R2
double LineSphere::Length(const double t1, const double t2)
{
  return std::abs( R2 * ( t2 - t1 ) );
}

/// dl = dt * R2
double LineSphere::dl(const double dt, const double t)
{
  return std::abs( R2 * dt );
}

/// Volume = A * dt * R2 
double LineSphere::dv(double dt, const double t)
{
  return std::abs( A * dt * R2 );
}

/// Volume = pi * ( r2^2 - r1^2 ) * dt * R2
double LineSphere::dv(double dt, const double t, const double r1, const double r2)
{
  return std::abs( M_PI * ( r2 * r2 - r1 * r1 ) * dt * R2 );
}

/// Length element dl = ds * dt:
double LineSphere::ds(const double t)
{
  return R2;
}

/// The line on sphere position vector P components are:
///    Px = R2^2 / sqrt(R1^2 + R2^2) cos(t)
///    Py = R1 R2 / sqrt(R1^2 + R2^2) cos(t)
///    Pz = R2 sin(t) 
std::vector<double> LineSphere::XYZ(const double t)
{
  std::vector<double> P;

  P.erase( P.begin(), P.end() );
  P.push_back( R2 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2 ) );
  P.push_back( R1 * R2 * cos( t ) / sqrt( R1 * R1 + R2 * R2 ) );
  P.push_back( R2 * sin( t ) ); 

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Line on sphere tangential vector T:
///    Tx = -R2 / sqrt( R1^2 + R2^2 ) sin(t)
///    Ty = -R1 / sqrt( R1^2 + R2^2 ) sin(t)
///    Tz = cos(t).
std::vector<double> LineSphere::Jv(const double t)
{
  std::vector<double> T;

  T.erase( T.begin(), T.end() );
  T.push_back( -R2 * sin( t ) / sqrt( R1 * R1 + R2 * R2 ) );
  T.push_back( -R1 * sin( t ) / sqrt( R1 * R1 + R2 * R2 ) );
  T.push_back(  cos( t ) );

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// Line on sphere current flows along meridian with azimuth 
/// sin(phi) = R1 / sqrt( R1^2 + R2^2) from altitude t1 to t2. 
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging.
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double LineSphere::Elements(const double t1, const double t2, const int M, const int test, const bool verbose)
{

  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( t2 - t1 ) / M ;
  double ds =  R2;
  double dl = dt * ds; 
  double dv = A * dt * ds;

  if( verbose ) std::cout << "t dl P[0] P[1] P[2] T[0] T[1] T[2] dv" << std::endl;
  for( int i = 0; i < M; i++ )
  {
    t = t1 + (i + 0.5) * dt;

    len += dl;
    vol += dv;

    P = XYZ( t );
    T = Jv( t );

    if( dt < 0 ) iiflucks::scaleVector( -1.0, T );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << t << " " << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

/// Numerical integration of magnetic field at position P.
int LineSphere::B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  return iiflucks::fieldLineSphere(R1, R2, I, t1, t2, P, epsabs, B, Berr, neval);
}
