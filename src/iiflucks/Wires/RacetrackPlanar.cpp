#include "iiflucks.h"
#include <iostream>

RacetrackPlanar::~RacetrackPlanar() { };

/// Total length for all windings and layers. 
double RacetrackPlanar::CalcLength(std::vector<double> t1v, std::vector<double> t2v, bool verbose)
{
  dw = 0;
  if( turns > 1 ) dw = w / ( turns - 1 );

  dth = 0;
  if( layers > 1 ) dth = th / ( layers - 1 );

//  double length = layers * turns * ( 2 * ( h + e ) + 2 * M_PI * R );
//  length += layers * turns * ( turns - 1 ) * M_PI * dw;

  double length = 0;
  for( int layer = 0; layer < layers; layer++ )
    for( int turn = 0; turn < turns; turn++ )
      for( int section = 0; section < 8; section++ )
         length += RacetrackPlanar::Length(t1v[ section ], t2v[ section ], section, turn, layer);

  return length;
}

  /// Calculate section, turn and layer from length. Return t on parametric curve.
double RacetrackPlanar::CalcTurn(const double length, int &section, int &turn, int &layer)
{
  double t = 0;
  double a = 2 * turns * ( h + e + M_PI * R ) + turns * ( turns - 1 ) * M_PI * dw;
  layer = floor( length / a );
  double len2 = length - layer * a;

  double b;
  if( turns > 1 )
  {
    b = -2 * e - 2 * h + dw * M_PI - 2 * M_PI * R;
    turn = floor( ( b + sqrt( 4 * dw * len2 * M_PI + b * b ) ) / ( 2 * dw * M_PI ));
  }
  else turn = 0;

  double len3 = len2 - 2 * turn * ( h + e + M_PI * R ) - turn * ( turn - 1) * M_PI * dw;

  if( len3 > ( 2 * ( h + e ) + 1.5 * M_PI * ( R + turn * dw ) ) )
  {
    section = 7;
    t = 0;
  }
  else if( len3 > ( 2 * h + e + 1.5 * M_PI * ( R + turn * dw ) ) )
  {
    section = 6;
    t = 0;
  }
  else if( len3 > ( 2 * h + e + M_PI * ( R + turn * dw ) ) )
  {
    section = 5;
    t = 0;
  }
  else if( len3 > ( h + e + M_PI * ( R + turn * dw ) ) )
  {
    section = 4;
    t = 0;
  }
  else if( len3 > ( h + e + 0.5 * M_PI * ( R + turn * dw ) ) )
  {
    section = 3;
    t = 0;
  }
  else if( len3 > ( h + 0.5 * M_PI * ( R + turn * dw ) ) )
  {
    section = 2;
    t = 0;
  }
  else if( len3 > h )
  {
    section = 1;
    t = 0;
  }
  else if( len3 > 0 )
  {
    section = 0;
    t = 0;
  }
  else section = -1;

  return t;
}

  /// Length t = [t1, t2].
double RacetrackPlanar::Length(const double t1, const double t2, const int section, const int turn, const int layer)
{
  double Rw = R + turn * dw;
  double len = 0;

  switch( section )
  {
    case 0:
      len = t2 - t1;
      break;
    case 1:
      len = Rw * ( t2 - t1 );
      break;
    case 2:
      len = t2 - t1;
      break;
    case 3:
      len = Rw * ( t2 - t1 );
      break;
    case 4:
      len = t2 - t1;
      break;
    case 5: 
      len = Rw * ( t2 - t1 );
      break;
    case 6:
      len = t2 - t1;
      break;
    case 7:
      len = Rw * ( t2 - t1 );
      break;
    default:
      std::cerr << "-- undefined section number = " << section << std::endl;
  }

  return std::abs( len ); 
}

/// dl = dt * ds
double RacetrackPlanar::dl(const double dt, const double t, const int section, const int turn, const int layer)
{
  double dl = dt * RacetrackPlanar::ds( t, section, turn, layer );

  return std::abs( dl );
}

/// Volume = A * dt * ds 
double RacetrackPlanar::dv(double dt, const double t, const int section, const int turn, const int layer)
{
  double dv = A * dt * RacetrackPlanar::ds( t, section, turn, layer );

  return std::abs( dv );
}

/// Volume = pi * ( r2^2 - r1^2 ) * dt * ds 
double RacetrackPlanar::dv(double dt, const double t, const double r1, const double r2, const int section, const int turn, const int layer)
{
  double dv = M_PI * ( r2 * r2 - r1 * r1 ) * dt * RacetrackPlanar::ds( t, section, turn, layer );

  return std::abs( dv );
}

/// Length element dl = ds * dt:
double RacetrackPlanar::ds(const double t, const int section, const int turn, const int layer)
{
  double Rw = R + turn * dw;
  double ds = 0;

  switch( section )
  {
    case 0:
      ds = 1;
      break;
    case 1:
      ds = Rw;
      break;
    case 2:
      ds = 1;
      break;
    case 3:
      ds = Rw;
      break;
    case 4:
      ds = 1;
      break;
    case 5: 
      ds = Rw;
      break;
    case 6:
      ds = 1;
      break;
    case 7:
      ds = Rw;
      break;
    default:
      std::cerr << "-- undefined section number = " << section << std::endl;
  }

  return ds;
}

/// The racetrack on cylinder position vector P components.
std::vector<double> RacetrackPlanar::XYZ(const double t, const int section, const int turn, const int layer)
{
  double Rw = R + turn * dw;
  double xl = layer * dth;

  std::vector<double> P;
  P.erase( P.begin(), P.end() );

  if ( section == 0 )
  {
    P.push_back( xl );
    P.push_back( Rw + e / 2 );
    P.push_back( t );
  }
  else if ( section == 1 )
  {
    P.push_back( xl );
    P.push_back( Rw * cos( t ) + e / 2 );
    P.push_back( Rw * sin( t ) + h / 2 );
  }
  else if ( section == 2 )
  {
    P.push_back( xl );
    P.push_back( t );
    P.push_back( Rw + h / 2 );
  }
  else if ( section == 3 )
  {
    P.push_back( xl );
    P.push_back( Rw * cos( t ) - e / 2 );
    P.push_back( Rw * sin( t ) + h / 2 );
  }
  else if ( section == 4 )
  {
    P.push_back( xl );
    P.push_back( -Rw - e / 2 );
    P.push_back( t );
  }
  else if ( section == 5 )
  {
    P.push_back( xl );
    P.push_back( Rw * cos( t ) - e / 2 );
    P.push_back( Rw * sin( t ) - h / 2 );
  }
  else if ( section == 6 )
  {
    P.push_back( xl );
    P.push_back( t );
    P.push_back( -Rw - h / 2 );
  }
  else if ( section == 7 )
  {
    P.push_back( xl );
    P.push_back( Rw * cos( t ) + e / 2 );
    P.push_back( Rw * sin( t ) - h / 2 );
  }
  else
  {
    std::cerr << "-- undefined section number = " << section << std::endl;
  }

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Tangential vector T.
std::vector<double> RacetrackPlanar::Jv(const double t, const int section, const int turn, const int layer)
{
  std::vector<double> T;

  T.erase( T.begin(), T.end() );
  if ( section == 0 )
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 1 );
  }
  else if ( section == 1 )
  {
    T.push_back( 0 ); 
    T.push_back( -sin( t ) );
    T.push_back(  cos( t ) );
  }
  else if ( section == 2 )
  {
    T.push_back( 0 );
    T.push_back( 1 );
    T.push_back( 0 );
  }
  else if ( section == 3 )
  {
    T.push_back( 0 ); 
    T.push_back( -sin( t ) );
    T.push_back(  cos( t ) );
  }
  else if ( section == 4 )
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 1 );
  }
  else if ( section == 5 )
  {
    T.push_back( 0 ); 
    T.push_back( -sin( t ) );
    T.push_back(  cos( t ) );
  }
  else if ( section == 6 )
  {
    T.push_back( 0 );
    T.push_back( 1 );
    T.push_back( 0 );
  }
  else if ( section == 7 )
  {
    T.push_back( 0 ); 
    T.push_back( -sin( t ) );
    T.push_back(  cos( t ) );
  }
  else
  {
    std::cerr << "-- undefined section number = " << section << std::endl;
  }

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// Number of equally long elements lc for section, turn and layer.
int RacetrackPlanar::NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v, const int section, const int turn, const int layer)
{
  int section_elements = -1;

  double Rw = R + turn * dw;

  switch( section )
  {
    case 0:
      section_elements = std::abs( ceil( ( t2v[ 0 ] - t1v[ 0 ] ) / lc ) );
      break;
    case 1:
      section_elements = std::abs( ceil( Rw * ( t2v[ 1 ] - t1v[ 1 ] ) / lc ) );
      break;
    case 2:
      section_elements = std::abs( ceil( ( t2v[ 2 ] - t1v[ 2 ] ) / lc ) );
      break;
    case 3:
      section_elements = std::abs( ceil( Rw * ( t2v[ 3 ] - t1v[ 3 ] ) / lc ) );
      break;
    case 4:
      section_elements = std::abs( ceil( ( t2v[ 4 ] - t1v[ 4 ] ) / lc ) );
      break;
    case 5:
      section_elements = std::abs( ceil( Rw * ( t2v[ 5 ] - t1v[ 5 ] ) / lc ) );
      break;
    case 6:
      section_elements = std::abs( ceil( ( t2v[ 6 ] - t1v[ 6 ] ) / lc ) );
      break;
    case 7:
      section_elements = std::abs( ceil( Rw * ( t2v[ 7 ] - t1v[ 7 ] ) / lc ) );
      break;
    default:
      std::cerr << "-- undefined section number = " << section << std::endl;
  }

  return std::abs( section_elements );

}

/// Number of equally long elements lc for coil.
int RacetrackPlanar::NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v)
{
  int elements = 0;
  double Rw;

  for(int layer = 0; layer < layers; layer++)
  {
    for(int turn = 0; turn < turns; turn++)
    {
      Rw = R + turn * dw;
      elements += std::abs( ceil( ( t2v[ 0 ] - t1v[ 0 ] ) / lc ) );
      elements += std::abs( ceil( Rw * ( t2v[ 1 ] - t1v[ 1 ] ) / lc ) );
      elements += std::abs( ceil( ( t2v[ 2 ] - t1v[ 2 ] ) / lc ) );
      elements += std::abs( ceil( Rw * ( t2v[ 3 ] - t1v[ 3 ] ) / lc ) );
      elements += std::abs( ceil( ( t2v[ 4 ] - t1v[ 4 ] ) / lc ) );
      elements += std::abs( ceil( Rw * ( t2v[ 5 ] - t1v[ 5 ] ) / lc ) );
      elements += std::abs( ceil( ( t2v[ 6 ] - t1v[ 6 ] ) / lc ) );
      elements += std::abs( ceil( Rw * ( t2v[ 7 ] - t1v[ 7 ] ) / lc ) );
    }
  }

  return elements;
}

  /// Wire sections in first turn on first layer are:
/// 0. straight    (0, +_R_ + _e_/2, -_h_/2) to (0, +_R_ + _e_/2, +_h_/2)
/// 1. arc         (0, +_R_ + _e_/2, +_h_/2) to (0, +_e_/2_, _R_ + _h_/2)
/// 2. straight    (0, +_e_/2, _R_ +_h_/2)   to (0, -_e_/2, _R_ +_h_/2)
/// 3. arc         (0, -_e_/2, _R_ +_h_/2)  to (0, -_R_ - _e_/2_, +_h_/2)
/// 4. straight    (0, -_R_ - _e_/2, +_h_/2) to (0, -_R_ - _e_/2_, -_h_/2)
/// 5. arc         (0, -_R_ - _e_/2, -_h_/2) to (0, -_e_/2_, -_R_ -_h_/2)
/// 6. straight    (0, -_e_/2, -_R_ -_h_/2) to (0, +_e_/2_, -_R_ -_h_/2)
/// 7. arc         (0, +_e_/2, -_R_ -_h_/2) to (0, +_R_ + _e_/2_, -_h_/2)
///
/// Length of straight section along z-axis is _h_, _R_ corner radius,
/// straight section along y-axis _e_, width of winding _w_ and _N_ number
/// of turns. Thickness of the on x-axis is _th_ with number of layers _O_.
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// Current flows in counter clockwise direction around x-axis.
/// Length of wire on one layer is L = 2*(h+e)*N + 2*pi*R*N + pi*w*N.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double RacetrackPlanar::Elements(const double t1, const double t2, const int section, const int turn, const int layer, const int M, const int test, const bool verbose)
{

  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( t2 - t1 ) / M ;
  double ds = 0;
  double dl;
  double dv;

  for( int i = 0; i < M; i++ )
  {
    t = t1 + (i + 0.5) * dt;

    dl = RacetrackPlanar::dl(dt, t, section, turn, layer);
    dv = RacetrackPlanar::dv(dt, t, section, turn, layer);

    len += dl;
    vol += dv;

    P.erase( P.begin(), P.end() );
    P = RacetrackPlanar::XYZ(t, section, turn, layer);

    T.erase( T.begin(), T.end() );
    T = RacetrackPlanar::Jv(t, section, turn, layer);
    if( dt < 0 ) iiflucks::scaleVector( -1.0, T );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t= " << t << " dl= " << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

/// Calculation of magnetic field at position P for section, turn and layer.
int RacetrackPlanar::B(const double I, const std::vector<double> t1v, const std::vector<double> t2v, const int section, const int turn, const int layer, const double epsabs, const std::vector<double> P, const bool verbose, std::vector<double> &Bv, std::vector<double> &Berrv, size_t &neval)
{
  int intres = 0;
  neval = 0;

  double Rw = R + turn * dw;
  double xl = layer * dth;
  double xp, yp, zp;

  if( verbose ) std::cout << "section,turn,layer,Rw,xl,t1,t2,xp,yp,zp,Bx,By,Bz,Bxerr,Byerr,Bzerr,intres,neval" << std::endl;
  if( verbose ) std::cout << section << "," << turn << "," << layer << "," << Rw << "," << xl;

  if ( section == 0 )
  {
    xp = P[0] - xl;
    yp = P[1] - Rw - e /2;
    zp = P[2];
    intres = iiflucks::fieldLine(I, t1v[ section ], t2v[ section ], {xp, yp, zp}, Bv);
    std::cout << "," << t1v[ section ] << "," << t2v[ section ];
  }
  else if ( section == 1 )
  {
    xp = P[1] - e / 2;
    yp = P[2] - h / 2;
    zp = P[0] - xl;

    intres = iiflucks::fieldRing(Rw, I, t1v[ section ], t2v[ section ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);
    std::cout << "," << t1v[ section ] << "," << t2v[ section ];

    std::swap( Bv[0], Bv[2] );
    std::swap( Bv[1], Bv[2] );
    std::swap( Berrv[0], Berrv[2] );
    std::swap( Berrv[1], Berrv[2] );
  }
  else if ( section == 2 )
  {
    xp = P[0] - xl;
    yp = P[2] - h / 2 - Rw;
    zp = P[1];

    intres = iiflucks::fieldLine(I, t2v[ section ], t1v[ section ], {xp, yp, zp}, Bv);
    std::cout << "," << t2v[ section ] << "," << t1v[ section ];
    std::swap( Bv[1], Bv[2] );
    std::swap( Berrv[1], Berrv[2] );
  }
  else if ( section == 3 )
  {
    xp = P[1] + e / 2;
    yp = P[2] - h / 2;
    zp = P[0] - xl;

    intres = iiflucks::fieldRing(Rw, I, t1v[ section ], t2v[ section ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);
    std::cout << "," << t1v[ section ] << "," << t2v[ section ];

    std::swap( Bv[0], Bv[2] );
    std::swap( Bv[1], Bv[2] );
    std::swap( Berrv[0], Berrv[2] );
    std::swap( Berrv[1], Berrv[2] );
  }
  else if ( section == 4 )
  {
    xp = P[0] - xl;
    yp = P[1] + Rw + e /2;
    zp = P[2];
    intres = iiflucks::fieldLine(I, t1v[ section ], t2v[ section ], {xp, yp, zp}, Bv);
    std::cout << "," << t1v[ section ] << "," << t2v[ section ];
  }
  else if ( section == 5 )
  {
    xp = P[1] + e / 2;
    yp = P[2] + h / 2;
    zp = P[0] - xl;

    intres = iiflucks::fieldRing(Rw, I, t1v[ section ], t2v[ section ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);
    std::cout << "," << t1v[ section ] << "," << t2v[ section ];

    std::swap( Bv[0], Bv[2] );
    std::swap( Bv[1], Bv[2] );
    std::swap( Berrv[0], Berrv[2] );
    std::swap( Berrv[1], Berrv[2] );
  }
  else if ( section == 6 )
  {
    xp = P[0] - xl;
    yp = P[2] + h / 2 + Rw;
    zp = P[1];

    intres = iiflucks::fieldLine(I, t2v[ section ], t1v[ section ], {xp, yp, zp}, Bv);
    std::cout << "," << t2v[ section ] << "," << t1v[ section ];
    std::swap( Bv[1], Bv[2] );
    std::swap( Berrv[1], Berrv[2] );
  }
  else if ( section == 7 )
  {
    xp = P[1] - e / 2;
    yp = P[2] + h / 2;
    zp = P[0] - xl;

    intres = iiflucks::fieldRing(Rw, I, t1v[ section ], t2v[ section ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);
    std::cout << "," << t1v[ section ] << "," << t2v[ section ];

    std::swap( Bv[0], Bv[2] );
    std::swap( Bv[1], Bv[2] );
    std::swap( Berrv[0], Berrv[2] );
    std::swap( Berrv[1], Berrv[2] );
  }
  else
  {
    std::cerr << "-- undefined section number = " << section << std::endl;
  }

  if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval << std::endl;

  return intres;
}

/// Calculation of magnetic field at position P for coil.
int RacetrackPlanar::B(const double I, const std::vector<double> t1v, const std::vector<double> t2v, const double epsabs, const std::vector<double> P, const bool verbose, std::vector<double> &Bv, std::vector<double> &Berrv, size_t &neval)
{
  int intres = 0;
  neval = 0;

  double Rw, xl;
  double xp, yp, zp;

  if( verbose ) std::cout << "section,turn,layer,Rw,xl,t1,t2,xp,yp,zp,Bx,By,Bz,Bxerr,Byerr,Bzerr,intres,neval,Bxsum,Bysum,Bzsum,Bxsum_err,Bysum_err,Bzsum_err" << std::endl;

  std::vector<double> Bsum, Bsum_err;
  iiflucks::zeroVector( Bsum );
  iiflucks::zeroVector( Bsum_err );
  for( int layer = 0; layer < layers; layer++)
  {
    for( int turn = 0; turn < turns; turn++)
    {
      Rw = R + turn * dw;
      xl = layer * dth;

      xp = P[0] - xl;
      yp = P[1] - Rw - e /2;
      zp = P[2];
      intres += iiflucks::fieldLine(I, t1v[ 0 ], t2v[ 0 ], {xp, yp, zp}, Bv);

      iiflucks::addVectors( Bsum, Bv, Bsum);
      if( verbose ) std::cout << "0," << turn << "," << layer << "," << Rw << "," << xl;
      if( verbose ) std::cout << "," << t1v[0] << "," << t2v[0];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[1] - e / 2;
      yp = P[2] - h / 2;
      zp = P[0] - xl;

      intres = iiflucks::fieldRing(Rw, I, t1v[ 1 ], t2v[ 1 ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);

      std::swap( Bv[0], Bv[2] );
      std::swap( Bv[1], Bv[2] );
      std::swap( Berrv[0], Berrv[2] );
      std::swap( Berrv[1], Berrv[2] );

      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "1," << turn << "," << layer << "," << Rw << "," << xl;
      if( verbose ) std::cout << "," << t1v[1] << "," << t2v[1];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[0] - xl;
      yp = P[2] - h / 2 - Rw;
      zp = P[1];

      intres = iiflucks::fieldLine(I, t2v[ 2 ], t1v[ 2 ], {xp, yp, zp}, Bv);
      std::swap( Bv[1], Bv[2] );
      std::swap( Berrv[1], Berrv[2] );

      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "2," << turn << "," << layer << "," << Rw << "," << xl;
      if( verbose ) std::cout << "," << t2v[2] << "," << t1v[2];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[1] + e / 2;
      yp = P[2] - h / 2;
      zp = P[0] - xl;

      intres = iiflucks::fieldRing(Rw, I, t1v[ 3 ], t2v[ 3 ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);

      std::swap( Bv[0], Bv[2] );
      std::swap( Bv[1], Bv[2] );
      std::swap( Berrv[0], Berrv[2] );
      std::swap( Berrv[1], Berrv[2] );

      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "3," << turn << "," << layer << "," << Rw << "," << xl;
      if( verbose ) std::cout << "," << t1v[3] << "," << t2v[3];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[0] - xl;
      yp = P[1] + Rw + e /2;
      zp = P[2];
      intres = iiflucks::fieldLine(I, t1v[ 4 ], t2v[ 4 ], {xp, yp, zp}, Bv);

      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "4," << turn << "," << layer << "," << Rw << "," << xl;
      if( verbose ) std::cout << "," << t1v[4] << "," << t2v[4];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[1] + e / 2;
      yp = P[2] + h / 2;
      zp = P[0] - xl;

      intres = iiflucks::fieldRing(Rw, I, t1v[ 5 ], t2v[ 5 ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);

      std::swap( Bv[0], Bv[2] );
      std::swap( Bv[1], Bv[2] );
      std::swap( Berrv[0], Berrv[2] );
      std::swap( Berrv[1], Berrv[2] );

      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "5," << turn << "," << layer << "," << Rw << "," << xl;
      if( verbose ) std::cout << "," << t1v[5] << "," << t2v[5];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[0] - xl;
      yp = P[2] + h / 2 + Rw;
      zp = P[1];

      intres = iiflucks::fieldLine(I, t2v[ 6 ], t1v[ 6 ], {xp, yp, zp}, Bv);
      std::swap( Bv[1], Bv[2] );
      std::swap( Berrv[1], Berrv[2] );

      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "6," << turn << "," << layer << "," << Rw << "," << xl;
      if( verbose ) std::cout << "," << t2v[6] << "," << t1v[6];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[1] - e / 2;
      yp = P[2] + h / 2;
      zp = P[0] - xl;

      intres = iiflucks::fieldRing(Rw, I, t1v[ 7 ], t2v[ 7 ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);

      std::swap( Bv[0], Bv[2] );
      std::swap( Bv[1], Bv[2] );
      std::swap( Berrv[0], Berrv[2] );
      std::swap( Berrv[1], Berrv[2] );

      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "7," << turn << "," << layer << "," << Rw << "," << xl;
      if( verbose ) std::cout << "," << t1v[7] << "," << t2v[7];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

    }
  }

  Bv.erase( Bv.begin(), Bv.end() );
  Bv = Bsum;
  Berrv.erase( Berrv.begin(), Berrv.end() );
  Berrv = Bsum_err;

  return intres;

}

