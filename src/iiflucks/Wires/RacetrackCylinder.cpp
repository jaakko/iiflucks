#include <iostream>
#include <iomanip>
#include <gsl/gsl_integration.h>
#include "iiflucks.h"

RacetrackCylinder::~RacetrackCylinder() { };

// Ring on cylinder surface wire length.
double fringcylinder (double t, void * params)
{
   double R =  ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];
   
   double fringcylinder = sqrt( R * R + R * R * R * R * sin(t) * sin(t) * cos(t) * cos(t) /( R2 * R2 - R * R * cos(t) * cos(t) ) );  

   return fringcylinder;
}

/// Total length for all windings and layers. 
double RacetrackCylinder::CalcLength(std::vector<double> t1v, std::vector<double> t2v, bool verbose)
{
  // numerical integration of curved corners wire length
  double clen = 0;
  double clenerr = 0;
  size_t neval = 0;
  int intres = 0;
  double Rk = 0;
  double Rn = 0;
  double params[] = { Rk, Rn };

  gsl_function F;
  F.function = &fringcylinder;
  F.params = params;

  if( turns > 1 ) dw = w / ( turns - 1 );
  if( layers > 1 ) dth = th / ( layers - 1 );

  // O layers each with N turns in radial direction
//  if( verbose ) std::cout << "--         Ri " << std::setw(15) << std::setfill(' ') << "Rn" << std::setw(15) << std::setfill(' ') << "clen " << std::setw(15) << std::setfill(' ') << std::endl;

  // initial straight wire coordinates  
  xw0 = sqrt( R2 * R2 - R * R );
  yw0 = R;
  sin_alpha = R / R2;
  cos_alpha = sqrt( 1 - R * R / ( R2 * R2 ) );

  double xw = 0, yw = 0, zw = 0;
  double wlen = 0;
  for( int layer = 0; layer < layers; layer++)
  {
    for( int turn = 0; turn < turns; turn++)
    {
      xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
      yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
      Rk = yw;
      Rn = sqrt( Rk * Rk + xw * xw );

      params[ 0 ] = Rk;
      params[ 1 ] = Rn;
      F.params = params;

      intres = gsl_integration_qng(&F, 0, 2 * M_PI, 0, 1e-5, &clen, &clenerr, &neval); 
      if( intres != 0 ) std::cerr << "** Corner length integration error. " << intres << std::endl;

      if( verbose ) std::cout << std::right << Rk << std::setw(15) << std::setfill(' ') << Rn << std::setw(15) << std::setfill(' ') << clen << std::setw(15) << std::setfill(' ') << std::endl;

      wlen += clen;
    }
  }

  wlen += 2 * layers * turns * h ;

  return wlen;
}

/// Length t = [t1, t2].
double RacetrackCylinder::Length(const double t1, const double t2, const int section, const int turn, const int layer)
{
  // numerical integration of curved corners wire length
  double clen = 0;
  double clenerr = 0;
  size_t neval = 0;
  int intres = 0;
  double Rk = 0;
  double Rn = 0;
  double params[] = { Rk, Rn };

  double xw, yw;

  gsl_function F;
  F.function = &fringcylinder;
  F.params = params;

  if ( section == 0 )
  {
    clen = t2 - t1;
  }
  else if ( section == 1 )
  {
    xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
    yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
    Rk = yw;
    Rn = sqrt( Rk * Rk + xw * xw );

    params[ 0 ] = Rk;
    params[ 1 ] = Rn;
    F.params = params;

    intres = gsl_integration_qng(&F, t1, t2, 0, 1e-5, &clen, &clenerr, &neval); 

    if( intres != 0 ) std::cerr << "** Corner length integration error. " << intres << std::endl;
  }
  else if ( section == 2 )
  {
    clen = t2 - t1;
  }
  else if ( section == 3 )
  {
    xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
    yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
    Rk = yw;
    Rn = sqrt( Rk * Rk + xw * xw );

    params[ 0 ] = Rk;
    params[ 1 ] = Rn;
    F.params = params;

    intres = gsl_integration_qng(&F, t1 + M_PI, t2 + M_PI, 0, 1e-5, &clen, &clenerr, &neval); 

    if( intres != 0 ) std::cerr << "** Corner length integration error. " << intres << std::endl;
  }
  else
  {
    std::cerr << "-- undefined section number = " << section << std::endl;
  }

  return std::abs( clen ); 
}

/// dl = dt * ds
double RacetrackCylinder::dl(const double dt, const double t, const int section, const int turn, const int layer)
{
  double dl = dt * RacetrackCylinder::ds( t, section, turn, layer );

  return std::abs( dl );
}

/// Volume = A * dt * ds 
double RacetrackCylinder::dv(double dt, const double t, const int section, const int turn, const int layer)
{
  double dv = A * dt * RacetrackCylinder::ds( t, section, turn, layer );

  return std::abs( dv );
}

/// Volume = pi * ( r2^2 - r1^2 ) * dt * ds 
double RacetrackCylinder::dv(double dt, const double t, const double r1, const double r2, const int section, const int turn, const int layer)
{
  double dv = M_PI * ( r2 * r2 - r1 * r1) * dt * RacetrackCylinder::ds( t, section, turn, layer );

  return std::abs( dv );
}

/// Length element dl = ds * dt:
double RacetrackCylinder::ds(const double t, const int section, const int turn, const int layer)
{
  double ds = 0;

  double xw, yw, Rk, Rn;
  if ( section == 0 )
  {
    ds = 1;
  }
  else if ( section == 1 )
  {
    xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
    yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
    Rk = yw;
    Rn = sqrt( Rk * Rk + xw * xw );

    ds =  sqrt( Rk * Rk * Rk * Rk * cos( t ) * cos( t ) * sin( t ) * sin( t ) / ( Rn * Rn - Rk * Rk * cos( t ) * cos( t ) ) + Rk * Rk );
  }
  else if ( section == 2 )
  {
    ds = 1;
  }
  else if ( section == 3 )
  {
    xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
    yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
    Rk = yw;
    Rn = sqrt( Rk * Rk + xw * xw );

    ds =  sqrt( Rk * Rk * Rk * Rk * cos( t + M_PI ) * cos( t + M_PI ) * sin( t + M_PI ) * sin( t + M_PI ) / ( Rn * Rn - Rk * Rk * cos( t + M_PI ) * cos( t + M_PI ) ) + Rk * Rk );
  }
  else
  {
    std::cerr << "-- undefined section number = " << section << std::endl;
  }

  return ds;
}

/// The racetrack on cylinder position vector P components.
std::vector<double> RacetrackCylinder::XYZ(const double t, const int section, const int turn, const int layer)
{
  std::vector<double> P;
  P.erase( P.begin(), P.end() );

  double xw, yw, Rk, Rn;
  xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
  yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
  Rk = yw;
  Rn = sqrt( Rk * Rk + xw * xw );

  if ( section == 0 )
  {
    P.push_back( sqrt( Rn * Rn - Rk * Rk ) );
    P.push_back( Rk );
    P.push_back( t );
  }
  else if ( section == 1 )
  {
    P.push_back( sqrt( Rn * Rn - Rk * Rk * cos( t ) * cos( t ) ) );
    P.push_back( Rk * cos( t ) );
    P.push_back( Rk * sin( t ) + h / 2 );
  }
  else if ( section == 2 )
  {
    P.push_back( sqrt( Rn * Rn - Rk * Rk ) );
    P.push_back( -Rk );
    P.push_back( t );
  }
  else if ( section == 3 )
  {
    P.push_back( sqrt( Rn * Rn - Rk * Rk * cos( t ) * cos( t ) ) );
    P.push_back( Rk * cos( t ) );
    P.push_back( Rk * sin( t ) - h / 2 );
  }
  else
  {
    std::cerr << "-- undefined section number = " << section << std::endl;
  }

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Tangential vector T.
std::vector<double> RacetrackCylinder::Jv(const double t, const int section, const int turn, const int layer)
{
  double ds;
  std::vector<double> T;

  double xw, yw, Rk, Rn;
  xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
  yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
  Rk = yw;
  Rn = sqrt( Rk * Rk + xw * xw );

  T.erase( T.begin(), T.end() );
  if ( section == 0 )
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 1 );
  }
  else if ( section == 1 )
  {
    if( std::abs( R2 ) > std::abs( R ) )
    {
      ds = sqrt( R * R + R * R * R * R * sin( t ) * sin( t ) * cos( t ) * cos( t )/ ( R2 * R2 - R * R * cos( t ) * cos( t ) ) );
    }
    else
    {
      ds = 0;
    }

    if( ds > 0 )
    {
      T.push_back( R * R * cos( t ) * sin( t ) / ( ds * sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) );
      T.push_back( -R * sin( t ) / ds );
      T.push_back(  R * cos( t ) / ds );
    }
    else
    {
      T.push_back( 0 );
      T.push_back( 0 );
      T.push_back( 0 );
    }

  }
  else if ( section == 2 )
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 1 );
  }
  else if ( section == 3 )
  {
    if( std::abs( R2 ) > std::abs( R ) )
    {
      ds = sqrt( R * R + R * R * R * R * sin( t ) * sin( t ) * cos( t ) * cos( t )/ ( R2 * R2 - R * R * cos( t ) * cos( t ) ) );
    }
    else
    {
      ds = 0;
    }

    if( ds > 0 )
    {
      T.push_back( R * R * cos( t ) * sin( t ) / ( ds * sqrt( R2 * R2 - R * R * cos( t ) * cos( t ) ) ) );
      T.push_back( -R * sin( t ) / ds );
      T.push_back(  R * cos( t ) / ds );
    }
    else
    {
      T.push_back( 0 );
      T.push_back( 0 );
      T.push_back( 0 );
    }
  }
  else
  {
    std::cerr << "-- undefined section number = " << section << std::endl;
  }

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// Number of equally long elements lc for section, turn and layer.
int RacetrackCylinder::NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v, const int section, const int turn, const int layer)
{
  int section_elements = -1;

  // numerical integration of curved corners wire length
  double clen = 0;
  double clenerr = 0;
  size_t neval = 0;
  int intres = 0;
  double Rk = 0;
  double Rn = 0;
  double params[] = { Rk, Rn };

  double xw, yw;

  gsl_function F;
  F.function = &fringcylinder;
  F.params = params;

  switch( section )
  {
    case 0:
      section_elements = std::abs( ceil( ( t2v[ section ] - t1v[ section ] ) / lc ) );
      break;
    case 1:
      xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
      yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
      Rk = yw;
      Rn = sqrt( Rk * Rk + xw * xw );

      params[ 0 ] = Rk;
      params[ 1 ] = Rn;
      F.params = params;

      intres = gsl_integration_qng(&F, t1v[ section ], t2v[ section ], 0, 1e-5, &clen, &clenerr, &neval);

      if( intres != 0 ) std::cerr << "** Corner length integration error. " << intres << std::endl;
      section_elements = std::abs( ceil( clen / lc ) );
      break;
    case 2:
      section_elements = std::abs( ceil( ( t2v[ section ] - t1v[ section ] ) / lc ) );
      break;
    case 3:
      xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
      yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
      Rk = yw;
      Rn = sqrt( Rk * Rk + xw * xw );

      params[ 0 ] = Rk;
      params[ 1 ] = Rn;
      F.params = params;

      intres = gsl_integration_qng(&F, t1v[ section ], t2v[ section ], 0, 1e-5, &clen, &clenerr, &neval);

      if( intres != 0 ) std::cerr << "** Corner length integration error. " << intres << std::endl;
      section_elements = std::abs( ceil( clen / lc ) );
      break;
    default:
      std::cerr << "-- undefined section number = " << section << std::endl;
  }

  return section_elements;
}

/// Number of equally long elements lc for coil.
int RacetrackCylinder::NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v)
{
  int elements = 0;

  for( int layer = 0; layer < layers; layer++)
  {
    for( int turn = 0; turn < turns; turn++)
    {
      elements += RacetrackCylinder::NElements(lc, t1v, t2v, 0, turn, layer);
      elements += RacetrackCylinder::NElements(lc, t1v, t2v, 1, turn, layer);
      elements += RacetrackCylinder::NElements(lc, t1v, t2v, 2, turn, layer);
      elements += RacetrackCylinder::NElements(lc, t1v, t2v, 3, turn, layer);
    }
  }

  return elements;
}

/// Calculation of magnetic field at position P for section, turn and layer.
int RacetrackCylinder::B(const double I, const std::vector<double> t1v, const std::vector<double> t2v, const int section, const int turn, const int layer, const double epsabs, const std::vector<double> P, const bool verbose, std::vector<double> &Bv, std::vector<double> &Berrv, size_t &neval)
{
  int intres = 0;
  neval = 0;

  double xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
  double yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
  double Rk = yw;
  double Rn = sqrt( Rk * Rk + xw * xw );
  double xp, yp, zp;

  if( verbose ) std::cout << "section,turn,layer,Rn,Rk,xw,yw,xp,yp,zp,Bx,By,Bz,Bxerr,Byerr,Bzerr,intres,neval" << std::endl;
  if( verbose ) std::cout << section << "," << turn << "," << layer << "," << Rn<< "," << Rk << "," << xw << "," << yw;

  switch( section )
  {
    case 0:
      xp = P[0] - sqrt( Rn * Rn - Rk * Rk );
      yp = P[1] - Rk;
      zp = P[2];
      intres = iiflucks::fieldLine(I, t1v[ section ], t2v[ section ], {xp, yp, zp}, Bv);
      break;
    case 1:
      xp = P[0];
      yp = P[1];
      zp = P[2] - h / 2;
      intres = iiflucks::fieldRingCylinder(Rk, Rn, I, t1v[ section ], t2v[ section ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);
      break;
    case 2:
      xp = P[0] - sqrt( Rn * Rn - Rk * Rk );
      yp = P[1] + Rk;
      zp = P[2];
      intres = iiflucks::fieldLine(I, t1v[ section ], t2v[ section ], {xp, yp, zp}, Bv);
      break;
    case 3:
      xp = P[0];
      yp = P[1];
      zp = P[2] + h / 2;
      intres = iiflucks::fieldRingCylinder(R, R2, I, t1v[ section ], t2v[ section ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);
      break;
    default:
      std::cerr << "-- undefined section number = " << section << std::endl;
  }
  if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval << std::endl;

  return intres;
}

/// Calculation of magnetic field at position P for coil.
int RacetrackCylinder::B(const double I, const std::vector<double> t1v, const std::vector<double> t2v, const double epsabs, const std::vector<double> P, const bool verbose, std::vector<double> &Bv, std::vector<double> &Berrv, size_t &neval)
{
  int intres = 0;
  neval = 0;

  double xw, yw, Rk, Rn;
  double xp, yp, zp;

  if( verbose ) std::cout << "section,turn,layer,Rn,Rk,xw,yw,t1,t2,xp,yp,zp,Bx,By,Bz,Bxerr,Byerr,Bzerr,intres,neval,Bxsum,Bysum,Bzsum,Bxsum_err,Bysum_err,Bzsum_err" << std::endl;

  std::vector<double> Bsum, Bsum_err;
  iiflucks::zeroVector( Bsum );
  iiflucks::zeroVector( Bsum_err );
  for( int layer = 0; layer < layers; layer++)
  {
    for( int turn = 0; turn < turns; turn++)
    {
      xw = xw0 + turn * dw * cos_alpha - layer * dth * sin_alpha;
      yw = yw0 + turn * dw * sin_alpha + layer * dth * sin_alpha;
      Rk = yw;
      Rn = sqrt( Rk * Rk + xw * xw );

      xp = P[0] - sqrt( Rn * Rn - Rk * Rk );
      yp = P[1] - Rk;
      zp = P[2];
      intres += iiflucks::fieldLine(I, t1v[ 0 ], t2v[ 0 ], {xp, yp, zp}, Bv);
      iiflucks::addVectors( Bsum, Bv, Bsum);
      if( verbose ) std::cout << "0," << turn << "," << layer << "," << Rn<< "," << Rk << "," << xw << "," << yw;
      if( verbose ) std::cout << "," << t1v[0] << "," << t2v[0];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[0];
      yp = P[1];
      zp = P[2] - h / 2;
      intres += iiflucks::fieldRingCylinder(Rk, Rn, I, t1v[ 1 ], t2v[ 1 ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);
      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "1," << turn << "," << layer << "," << Rn<< "," << Rk << "," << xw << "," << yw;
      if( verbose ) std::cout << "," << t1v[1] << "," << t2v[1];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[0] - sqrt( Rn * Rn - Rk * Rk );
      yp = P[1] + Rk;
      zp = P[2];
      intres += iiflucks::fieldLine(I, t1v[ 2 ], t2v[ 2 ], {xp, yp, zp}, Bv);
      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "2," << turn << "," << layer << "," << Rn<< "," << Rk << "," << xw << "," << yw;
      if( verbose ) std::cout << "," << t1v[2] << "," << t2v[2];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;

      xp = P[0];
      yp = P[1];
      zp = P[2] + h / 2;
      intres += iiflucks::fieldRingCylinder(R, R2, I, t1v[ 3 ], t2v[ 3 ], {xp, yp, zp}, epsabs, Bv, Berrv, neval);
      iiflucks::addVectors( Bsum, Bv, Bsum);
      iiflucks::addVectors( Bsum_err, Berrv, Bsum_err);
      if( verbose ) std::cout << "3," << turn << "," << layer << "," << Rn<< "," << Rk << "," << xw << "," << yw;
      if( verbose ) std::cout << "," << t1v[3] << "," << t2v[3];
      if( verbose ) std::cout << "," << xp << "," << yp << "," << zp << "," << Bv[0] << "," << Bv[1] << "," << Bv[2] << "," << Berrv[0] << "," << Berrv[1] << "," << Berrv[2] << "," << intres << "," << neval;
      if( verbose ) std::cout << "," << Bsum[0] << "," << Bsum[1] << "," << Bsum[2] << "," << Bsum_err[0] << "," << Bsum_err[1] << "," << Bsum_err[2] << std::endl;
    }
  }

  Bv.erase( Bv.begin(), Bv.end() );
  Bv = Bsum;
  Berrv.erase( Berrv.begin(), Berrv.end() );
  Berrv = Bsum_err;

  return intres;

}

/// Straight sections along z-axis have length _h_, curved corners have radius
/// _R_. Radius of cylinder is _R2_. From cylinder central z-axis cross
/// section of winding mandrel has angle _alpha_ ~ (2 _R_ ) /  _R2_. 
/// Layer of coil is wound radially from _R2_ to _R2_ + _w_. Next new layer
/// is wound on top of the previous layer at distance of _th_ / ( _layers_ - 1 ).
/// This is repeated until all layers have been wound.
/// 
/// Wire sections in first turn on first layer are:
///
/// 0. straight
/// _xw_ = sqrt( _Rn_^2 - _R_^2 )
/// _yw_ = _R_ 
/// _zw_ = [-_h_/2, +_h_/2]
///
/// 1. curved arc _t_ = [0, pi]
/// _xw_ = sqrt( _Rn_^2 - _R_^2 cos^2( t ) )
/// _yw_ = _R_ cos( t )
/// _zw_ = _R_ sin( t ) + h / 2
///
/// 2. straight
/// _xw_ = sqrt( _Rn_^2 - _R_^2 ) 
/// _yw_ = -_R_ 
/// _zw_ = [+_h_/2, -_h_/2]
///
/// 3. curved arc _t_ = [pi, 2 pi]
/// _xw_ = sqrt( _Rn_^2 - _R_^2 cos^2( t ) )
/// _yw_ = _R_ cos( t )
/// _zw_ = _R_ sin( t ) - h / 2
///
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// Current flows in counter clockwise direction around x-axis.
/// Length of wire on one layer is 
/// L = turns * ( 2*h + 2*e + integral(R^2 + R^4 sin^2(t) cos^2(t) / ( R2^2 - R^2 * cos^2(t) ), t=[0,2pi] ) ).
///
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double RacetrackCylinder::Elements(const double t1, const double t2, const int section, const int turn, const int layer, const int M, const int test, const bool verbose)
{

  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( t2 - t1 ) / M ;
  double ds = 0;
  double dl;
  double dv;

  for( int i = 0; i < M; i++ )
  {
    t = t1 + (i + 0.5) * dt;

    dl = RacetrackCylinder::dl(dt, t, section, turn, layer);
    dv = RacetrackCylinder::dv(dt, t, section, turn, layer);

    len += dl;
    vol += dv;

    P.erase( P.begin(), P.end() );
    P = RacetrackCylinder::XYZ(t, section, turn, layer);

    T.erase( T.begin(), T.end() );
    T = RacetrackCylinder::Jv(t, section, turn, layer);
    if( dt < 0 ) iiflucks::scaleVector( -1.0, T );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dl=" << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

