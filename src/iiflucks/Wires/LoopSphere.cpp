#include "iiflucks.h"
#include <gsl/gsl_integration.h>
#include <iostream>

LoopSphere::~LoopSphere() { };

// Loop on spherical surface wire length.
double floopsphere(double t, void * params)
{
   double theta1 = ( (double *) params)[ 0 ];
   double dtheta = ( (double *) params)[ 1 ];
   double dphi   = ( (double *) params)[ 2 ];
   double R2     = ( (double *) params)[ 3 ];

   double floopsphere = sqrt( 0.25 * dphi * dphi * R2 * R2 * cos( theta1 + ( 1 - t * t * t * t ) * dtheta ) * cos( theta1 + ( 1 - t * t * t * t ) * dtheta ) + 16 * dtheta * dtheta * R2 * R2 * t * t * t * t * t * t );

   return floopsphere;
}

/// Length t = [t1, t2].
double LoopSphere::Length(const double t1, const double t2)
{
  double params[] = { theta1, dtheta, dphi, R2 };
  double len = 0, lenerr = 0;
  int intres = 0;
  size_t neval = 0;

  gsl_function F;
  F.function = &floopsphere;
  F.params = params;

  if( t1 != t2 )
  {
    intres = gsl_integration_qng(&F, t1, t2, 0, 1e-5, &len, &lenerr, &neval); 
    len = std::abs( len );
    if( intres != 0 ) len = -1;
  }

  return len;
}


/// dl = dt * ds
double LoopSphere::dl(const double dt, const double t)
{
  return std::abs( dt * ds( t ) );
}

/// Volume = A * dt * ds 
double LoopSphere::dv(double dt, const double t)
{
  return std::abs( A * dt * ds( t ) );
}

/// Volume = pi * ( r2^2 - r1^2 ) * dt * ds 
double LoopSphere::dv(double dt, const double t, const double r1, const double r2)
{

  return std::abs( M_PI * ( r2 * r2 - r1 * r1 ) * dt * ds( t ) );
}

/// Length element dl = ds * dt:
double LoopSphere::ds(const double t)
{
  double ds = sqrt( 0.25 * dphi * dphi * R2 * R2 * cos( theta1 + ( 1 - t * t * t * t ) * dtheta ) * cos( theta1 + ( 1 - t * t * t * t ) * dtheta ) + 16 * dtheta * dtheta * R2 * R2 * t * t * t * t * t * t );

  return ds;
}

/// The loop on sphere position vector P components are:
///   Px = R2 cos( theta1 + ( 1 - t^4 ) * dtheta) cos( dphi * ( t + 1 ) / 2 + phi1 )
///   Py = R2 cos( theta1 + ( 1 - t^4 ) * dtheta) sin( dphi * ( t + 1 ) / 2 + phi1 )
///   Pz = R2 sin( theta1 + ( 1 - t^4 ) * dtheta ). 
std::vector<double> LoopSphere::XYZ(const double t)
{
  std::vector<double> P;

  P.erase( P.begin(), P.end() );
  P.push_back( R2 * cos( theta1 + ( 1 - t*t*t*t ) * dtheta) * cos( dphi * ( t + 1 ) / 2 + phi1 ) );
  P.push_back( R2 * cos( theta1 + ( 1 - t*t*t*t ) * dtheta) * sin( dphi * ( t + 1 ) / 2 + phi1 ) );
  P.push_back( R2 * sin( theta1 + ( 1 - t*t*t*t ) * dtheta ) ); 

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Loop on sphere tangential vector T:
/// ds*Tx = -0.5 dphi R2 cos( theta1 + ( 1 - t^4 ) * dtheta) sin( dphi * ( t + 1 ) / 2 + phi1 ) + 4 dtheta R2 t^3 cos( phi1 + 0.5 dphi ( 1 + t) ) sin( theta1 + dtheta ( 1 - t^4 ) )
/// ds*Ty = 0.5 dphi R2 cos( theta1 + ( 1 - t^4 ) * dtheta) cos( dphi * ( t + 1 ) / 2 + phi1 ) + 4 dtheta R2 t^3 sin( phi1 + 0.5 dphi ( 1 + t) ) sin( theta1 + dtheta ( 1 - t^4 ) )
/// ds*Tz = -4 dtheta R2 t^3 cos( theta1 + ( 1 - t^4 ) * dtheta ). 
std::vector<double> LoopSphere::Jv(const double t)
{
  double lx = -0.5 * dphi * R2 * cos( theta1 + ( 1 - t*t*t*t ) * dtheta ) * sin( dphi * ( t + 1 ) / 2 + phi1 ) + 4 * dtheta * R2 * t*t*t * cos( phi1 + 0.5 * dphi * ( 1 + t) ) * sin( theta1 + dtheta * ( 1 - t*t*t*t ) );
  lx /= ds(t);
  double ly = 0.5 * dphi * R2 * cos( theta1 + ( 1 - t*t*t*t ) * dtheta ) * cos( dphi * ( t + 1 ) / 2 + phi1 ) + 4 * dtheta * R2 * t*t*t * sin( phi1 + 0.5 * dphi * ( 1 + t) ) * sin( theta1 + dtheta * ( 1 - t*t*t*t ) );
  ly /= ds(t);
  double lz = -4 * dtheta * R2 * t*t*t * cos( theta1 + ( 1 - t*t*t*t ) * dtheta ); 
  lz /= ds(t);
      
  std::vector<double> T;
  T.erase( T.begin(), T.end() );
  T.push_back( lx );
  T.push_back( ly );
  T.push_back( lz );

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging.
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double LoopSphere::Elements(const double t1, const double t2, const int M, const int test, const bool verbose)
{
  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( t2 - t1 ) / M ;
  double dl = 0;
  double dv = 0;

  if( verbose ) std::cout << "t dl P[0] P[1] P[2] T[0] T[1] T[2] dv" << std::endl;
  for( int i = 0; i < M; i++ )
  {
    t = t1 + (i + 0.5) * dt;

    dl = dt * ds( t );
    dv = A * dt * ds( t );
    len += dl;
    vol += dv;

    P = XYZ( t );

    T = Jv( t );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << t << " " << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

/// Numerical integration of magnetic field at position P.
int LoopSphere::B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  return iiflucks::fieldLoopSphere(R2, theta1, phi1, dtheta, dphi, I, t1, t2, P, epsabs, B, Berr, neval);
}
