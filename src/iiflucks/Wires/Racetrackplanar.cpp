#include "Racetrackplanar.h"

namespace iiflucks {

/// Wire sections in first turn on first layer are:
/// 1. straight    (0, +_R_, -_h_/2) to (0, +_R_, +_h_/2)
/// 2. half circle (0, +_R_, +_h_/2) to (0, -_R_, +_h_/2)
/// 3. straight    (0, -_R_, +_h_/2) to (0, -_R_, -_h_/2)
/// 4. half circle (0, -_R_, -_h_/2) to (0, +_R_, -_h_/2)
///
/// Length of straight section along z-axis us _h_, _R_ corner radius,
/// straight section along y-axis _e_, width of winding _w_ and _N_ number
/// of turns. Thickness of the on x-axis is _th_ with number of layers _O_.
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// Current flows in counter clockwise direction around x-axis.
/// Length of wire on one layer is L = 2*(h+e)*N + 2*pi*R*N + pi*w*N.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double raceTrackPlanar(const double R, const double h, const double e, const double w, const double th, const double d, const int N, const int O, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test)
{
  double A = (d/2) * (d/2) * M_PI;
  double J = I / A;

  std::vector<double> a, P, Q, T, U; // axis of rotation and vectors to rotate

  double anorm = sqrt( ax*ax + ay*ay + az*az );

  if( anorm > 0 && theta != 0 )
  {
    a.push_back( ax / anorm );
    a.push_back( ay / anorm );
    a.push_back( az / anorm );
  }

  double wlen = O * ( 2 * ( h + e ) * N + 2 * M_PI * R * N + M_PI * w * N );
  double lc = wlen / M;      
  int Mh = ceil( h / lc );
  int Me = ceil( e / lc );
  int Mr = 0;

  double Xo = 0;
  double Rn = 0;
  double dz = 0;
  double dphi = 0;
  double dw = 0;
  double dt = 0;

  double ds = 0;
  double dl = 0, len = 0, dv = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t, t1, t2;

  if( N > 1 ) dw = w / ( N - 1 );
  if( O > 1 ) dt = th / ( O - 1 );

  // O layers
  for( int k = 0; k < O; k++)
  {
    Xo = k * dt;

    // N turns
    for( int i = 0; i < N; i++)
    {
      Rn = R + i * dw;

      // right straight wire
      dz = h / Mh;

      if( verbose ) std::cout << "-- " << Mh << " straight elements" << std::endl;

      for( int j = 0; j < Mh; j++)
      {
        t = (j + 0.5) * dz;
        t1 = j  * dz;
        t2 = (j + 1.0) * dz;

        dl = iiflucks::straightline(t, t1, t2, P, T, ds);

        dv = dl * A;

        len += dl;
        vol += dv;

        // center of volume element
        P[ 0 ] += Xo;
        P[ 1 ] += Rn + e / 2;
        P[ 2 ] += -h / 2;

        if( anorm > 0 && theta != 0 )
        {
          iiflucks::QuaternionRotation(a, theta, P);
          iiflucks::QuaternionRotation(a, theta, T);
        }

        P[ 0 ] += xt;
        P[ 1 ] += yt;
        P[ 2 ] += zt; 

        T[ 0 ] *= J;
        T[ 1 ] *= J;
        T[ 2 ] *= J; 

        Jxl += T[ 0 ] * dv;
        Jyl += T[ 1 ] * dv;
        Jzl += T[ 2 ] * dv;

        if( verbose ) std::cout << "-- t=" << t << " dz=" << dz << " ";

        if( verbose || !test )
        {
          std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
          std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
          std::cout << " " << dv << std::endl;
        }
      }

      // top right arc 
      Mr = ceil( 0.5 * M_PI * Rn /  lc );
      dphi = 0.5 * M_PI / Mr;

      if( verbose ) std::cout << "-- " << Mr << " top right arc elements" << std::endl;

      for( int j = 0; j < Mr; j++)
      {
        t = (j + 0.5) * dphi;
        t1 = j  * dphi;
        t2 = (j + 1.0) * dphi;

        dl = iiflucks::ring(t, t1, t2, Rn, P, T, ds);

        dv = dl * A;

        len += dl;
        vol += dv;

        P[ 0 ] += Xo;
	P[ 1 ] += e / 2;
	P[ 2 ] += h / 2;

        if( anorm > 0 && theta != 0 )
        {
          iiflucks::QuaternionRotation(a, theta, P);
          iiflucks::QuaternionRotation(a, theta, T);
        }

        P[ 0 ] += xt;
        P[ 1 ] += yt;
        P[ 2 ] += zt; 

        T[ 0 ] *= J;
        T[ 1 ] *= J;
        T[ 2 ] *= J; 

        Jxl += T[ 0 ] * dv;
        Jyl += T[ 1 ] * dv;
        Jzl += T[ 2 ] * dv;

        if( verbose ) std::cout << "-- t=" << t << " dphi=" << dphi << " ";

        if( verbose || !test )
        {
          std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
          std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
          std::cout << " " << dv << std::endl;
        }
      }

      // top straight wire
      dz = e / Me;

      if( verbose ) std::cout << "-- " << Me << " straight elements" << std::endl;

      for( int j = 0; j < Me; j++)
      {
        t = (j + 0.5) * dz;
        t1 = j  * dz;
        t2 = (j + 1.0) * dz;

        dl = iiflucks::straightline(t, t1, t2, Q, U, ds);
      
        dv = dl * A;

        len += dl;
        vol += dv;

        // center of volume element
        P[ 0 ] = Xo;
        P[ 1 ] = e / 2 - Q[ 2 ];
        P[ 2 ] = h / 2 + Rn;

        // direction of current density vector along with wire
        T[ 0 ] = 0;
	T[ 1 ] = -U[ 2 ];
        T[ 2 ] = 0;

        if( anorm > 0 && theta != 0 )
        {
          iiflucks::QuaternionRotation(a, theta, P);
          iiflucks::QuaternionRotation(a, theta, T);
        }
 
        P[ 0 ] += xt;
        P[ 1 ] += yt;
        P[ 2 ] += zt; 

        T[ 0 ] *= J;
        T[ 1 ] *= J;
        T[ 2 ] *= J; 

        Jxl += T[ 0 ] * dv;
        Jyl += T[ 1 ] * dv;
        Jzl += T[ 2 ] * dv;

        if( verbose ) std::cout << "-- t=" << t << " dz=" << dz << " ";

        if( verbose || !test )
        {
          std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
          std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
          std::cout << " " << dv << std::endl;
        }
      }

    // top left arc 
      Mr = ceil( 0.5 * M_PI * Rn /  lc );
      dphi = 0.5 * M_PI / Mr;

      if( verbose ) std::cout << "-- " << Mr << " top right arc elements" << std::endl;

      for( int j = 0; j < Mr; j++)
      {
        t = (j + 0.5) * dphi + 0.5 * M_PI;
	t1 = j  * dphi + 0.5 * M_PI;
        t2 = (j + 1.0) * dphi + 0.5 * M_PI;

        dl = iiflucks::ring(t, t1, t2, Rn, P, T, ds);

        dv = dl * A;

        len += dl;
        vol += dv;

        P[ 0 ] += Xo;
	P[ 1 ] += -e / 2;
	P[ 2 ] += h / 2;

        if( anorm > 0 && theta != 0 )
        {
          iiflucks::QuaternionRotation(a, theta, P);
          iiflucks::QuaternionRotation(a, theta, T);
        }

        P[ 0 ] += xt;
        P[ 1 ] += yt;
        P[ 2 ] += zt; 

        T[ 0 ] *= J;
        T[ 1 ] *= J;
        T[ 2 ] *= J; 

        Jxl += T[ 0 ] * dv;
        Jyl += T[ 1 ] * dv;
        Jzl += T[ 2 ] * dv;

        if( verbose ) std::cout << "-- t=" << t << " dphi=" << dphi << " ";

        if( verbose || !test )
        {
          std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
          std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
          std::cout << " " << dv << std::endl;
        }
      }

      // left straight wire
      dz = h / Mh;

      if( verbose ) std::cout << "-- " << Mh << " straight elements" << std::endl;

      for( int j = 0; j < Mh; j++)
      {
        t = (j + 0.5) * dz;
        t1 = j  * dz;
        t2 = (j + 1.0) * dz;

        dl = iiflucks::straightline(t, t1, t2, P, T, ds);

        dv = dl * A;

        len += dl;
        vol += dv;

        // center of volume element
        P[ 0 ] += Xo;
        P[ 1 ] += -Rn - e / 2;
	P[ 2 ] = -P[ 2 ];
        P[ 2 ] += h / 2;

        // direction of current density vector along with wire
        T[ 2 ] = -T[ 2 ];

	if( anorm > 0 && theta != 0 )
        {
          iiflucks::QuaternionRotation(a, theta, P);
          iiflucks::QuaternionRotation(a, theta, T);
        }

        P[ 0 ] += xt;
        P[ 1 ] += yt;
        P[ 2 ] += zt; 

        T[ 0 ] *= J;
        T[ 1 ] *= J;
        T[ 2 ] *= J; 

        Jxl += T[ 0 ] * dv;
        Jyl += T[ 1 ] * dv;
        Jzl += T[ 2 ] * dv;

        if( verbose ) std::cout << "-- t=" << t << " dz=" << dz << " ";
  
        if( verbose || !test )
        {
          std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
          std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
          std::cout << " " << dv << std::endl;
        }
      }

      // bottom left arc
      Mr = ceil( 0.5 * M_PI * Rn / lc );
      dphi = 0.5 * M_PI / Mr;

      if( verbose ) std::cout << "-- " << Mr << " arc elements" << std::endl;

      for( int j = 0; j < Mr; j++)
      {
        t = (j + 0.5) * dphi + M_PI;
	t1 = j  * dphi + M_PI;
        t2 = (j + 1.0) * dphi + M_PI;

        dl = iiflucks::ring(t, t1, t2, Rn, P, T, ds);

        dv = dl * A;

        len += dl;
        vol += dv;

        P[ 0 ] += Xo;
	P[ 1 ] += -e / 2;
	P[ 2 ] += -h / 2;

        if( anorm > 0 && theta != 0 )
        {
          iiflucks::QuaternionRotation(a, theta, P);
          iiflucks::QuaternionRotation(a, theta, T);
        }

        P[ 0 ] += xt;
        P[ 1 ] += yt;
        P[ 2 ] += zt; 

        T[ 0 ] *= J;
        T[ 1 ] *= J;
        T[ 2 ] *= J; 

        Jxl += T[ 0 ] * dv;
        Jyl += T[ 1 ] * dv;
        Jzl += T[ 2 ] * dv;

        if( verbose ) std::cout << "-- t=" << t << " dphi=" << dphi << " ";

        if( verbose || !test )
        {
          std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
          std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
          std::cout << " " << dv << std::endl;
        }
      }

      // bottom straight wire
      dz = e / Me;

      if( verbose ) std::cout << "-- " << Me << " straight elements" << std::endl;

      for( int j = 0; j < Me; j++)
      {
        t = (j + 0.5) * dz;
        t1 = j  * dz;
        t2 = (j + 1.0) * dz;

        dl = iiflucks::straightline(t, t1, t2, Q, U, ds);

        dv = dl * A;

        len += dl;
        vol += dv;

        // center of volume element
        P[ 0 ] = Xo;
        P[ 1 ] = -e / 2 + Q[ 2 ];
        P[ 2 ] = -h / 2 - Rn;

        // direction of current density vector along with wire
        T[ 0 ] = 0;
	T[ 1 ] = U[ 2 ];
        T[ 2 ] = 0;

        if( anorm > 0 && theta != 0 )
        {
          iiflucks::QuaternionRotation(a, theta, P);
          iiflucks::QuaternionRotation(a, theta, T);
        }
 
        P[ 0 ] += xt;
        P[ 1 ] += yt;
        P[ 2 ] += zt; 

        T[ 0 ] *= J;
        T[ 1 ] *= J;
        T[ 2 ] *= J; 

        Jxl += T[ 0 ] * dv;
        Jyl += T[ 1 ] * dv;
        Jzl += T[ 2 ] * dv;

        if( verbose ) std::cout << "-- t=" << t << " dz=" << dz << " ";

        if( verbose || !test )
        {
          std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
          std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
          std::cout << " " << dv << std::endl;
        }
      }
  
      // bottom right arc
      Mr = ceil( 0.5 * M_PI * Rn / lc );
      dphi = 0.5 * M_PI / Mr;

      if( verbose ) std::cout << "-- " << Mr << " arc elements" << std::endl;

      for( int j = 0; j < Mr; j++)
      {
        t = (j + 0.5) * dphi + 1.5 * M_PI;
	t1 = j  * dphi + 1.5 * M_PI;
        t2 = (j + 1.0) * dphi + 1.5 * M_PI;

        dl = iiflucks::ring(t, t1, t2, Rn, P, T, ds);

        dv = dl * A;

        len += dl;
        vol += dv;

        P[ 0 ] += Xo;
	P[ 1 ] +=  e / 2;
	P[ 2 ] += -h / 2;

        if( anorm > 0 && theta != 0 )
        {
          iiflucks::QuaternionRotation(a, theta, P);
          iiflucks::QuaternionRotation(a, theta, T);
        }

        P[ 0 ] += xt;
        P[ 1 ] += yt;
        P[ 2 ] += zt; 

        T[ 0 ] *= J;
        T[ 1 ] *= J;
        T[ 2 ] *= J; 

        Jxl += T[ 0 ] * dv;
        Jyl += T[ 1 ] * dv;
        Jzl += T[ 2 ] * dv;

        if( verbose ) std::cout << "-- t=" << t << " dphi=" << dphi << " ";

        if( verbose || !test )
        {
          std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
          std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
          std::cout << " " << dv << std::endl;
        }
      }
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

}


