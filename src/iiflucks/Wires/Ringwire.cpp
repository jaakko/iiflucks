#include "Ringwire.h"

// Ring on cylinder surface wire length.
double frcyl (double t, void * params)
{
   double R =  ( (double *) params)[ 0 ];
   double R2 = ( (double *) params)[ 1 ];

   double frcyl = sqrt( R * R + R * R * R * R * sin(t) * sin(t) * cos(t) * cos(t) /( R2 * R2 - R * R * cos(t) * cos(t) ) );  

   return frcyl;
}

namespace iiflucks {

/// The ring position vector P components are:
///    Px = r cos(t)
///    Py = r sin(t)
///    Pz = 0
///
/// Tangential vector T:
///    Tx = -sin(t)
///    Ty =  cos(t)
///    Tz = 0
///
/// Length element dl = ds * dt:
///    ds = r
///
double ring(const double t, const double t1, const double t2, const double r, std::vector<double> &P, std::vector<double> &T, double &ds)
{
  P.erase( P.begin(), P.end() );
  P.push_back( r * cos( t ) );
  P.push_back( r * sin( t ) );
  P.push_back( 0 ); 

  ds = r;
  T.erase( T.begin(), T.end() );
  if( ds > 0 )
  {
    T.push_back( -sin( t ) );
    T.push_back(  cos( t ) );
    T.push_back( 0 ); 
  }
  else
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 0 ); 
  }

  return std::abs( ds * ( t2 - t1 ) );
}

/// Ring or arc center is at (0, 0, 0) and current flows in counter clockwise
/// direction. The arc starts at ( _R_ cos( _phi0_ ) , _R_ sin( _phi0_ ), 0 )
/// and ends at ( _R_ cos( _phi1_ ) , _R_ sin( _phi1_ ), 0 ). 
/// Sweep angle _phi1_ - _phi0_ = 2 pi gives complete ring. 
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging.
/// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
/// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
/// returned.
double ringWire(const double R, const double phi0, const double phi1, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test)
{
  double J = I / (M_PI * (d/2) * (d/2) );
  double A = (d/2) * (d/2) * M_PI;

  std::vector<double> axis, P, Jt; // axis of rotation and vectors to rotate

  iiflucks::makeVector( ax, ay, az, axis );
  iiflucks::normalizeVector( axis );

  Ring *coil = new Ring("ring", R, A, J, theta, axis, {xt, yt, zt});

  double dl = 0, len = 0, dv = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = ( phi1 - phi0 ) / M ;

  dl = coil->dl( dt, 0 );
  dv = coil->dv( dt, 0 );

  for( int i = 0; i < M; i++ )
  {
    t = phi0 + (i + 0.5) * dt;

    len += dl;
    vol += dv;

    P = coil->XYZ( t );
    Jt = coil->Jv( t );

    Jxl += Jt[ 0 ] * dv;
    Jyl += Jt[ 1 ] * dv;
    Jzl += Jt[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dl=" << dl << " ";
  
    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << Jt[ 0 ] << " " << Jt[ 1 ] << " " << Jt[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }
  
  delete( coil );

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}


/// The ring on cylinder position vector P components are:
///    Px = sqrt( r2^2 - r^2 cos^2(t) )
///    Py = r cos(t)
///    Pz = r sin(t) 
///
/// Tangential vector T:
///    Tx = r^2 cos(t) sin(t) / ( ds * sqrt( r2^2 - r^2 cos^2(t) ) )
///    Ty = -r sin(t) / ds
///    Tz =  r cos(t) / ds
///
/// Length element dl = ds * dt:
///    ds = sqrt( r^2 + r^4 sin^2(t) cos^2(t) / ( r2^2 - r^2 cos^2(t) ) ) 
///
double ringcylinder(const double t, const double t1, const double t2, const double r, const double r2, std::vector<double> &P, std::vector<double> &T, double &ds)
{
  P.erase( P.begin(), P.end() );
  P.push_back( sqrt( r2 * r2 - r * r * cos( t ) * cos( t ) ) );
  P.push_back( r * cos( t ) );
  P.push_back( r * sin( t ) ); 

  if( std::abs( r2 ) > std::abs( r ) )
  {
    ds = sqrt( r * r + r * r * r * r * sin( t ) * sin( t ) * cos( t ) * cos( t )/ ( r2 * r2 - r * r * cos( t ) * cos( t ) ) );
  }
  else
  {
    ds = 0;
  }

  T.erase( T.begin(), T.end() );
  if( ds > 0 )
  {
    T.push_back( r * r * cos( t ) * sin( t ) / ( ds * sqrt( r2 * r2 - r * r * cos( t ) * cos( t ) ) ) );
    T.push_back( -r * sin( t ) / ds );
    T.push_back(  r * cos( t ) / ds ); 
  }
  else
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 0 ); 
  }

  double params[] = { r, r2 };
  double len = 0, lenerr = 0;
  int intres = 0;
  size_t neval = 0;

  gsl_function F;
  F.function = &frcyl;
  F.params = params;

  if( t1 != t2 )
  {
    intres = gsl_integration_qng(&F, t1, t2, 0, 1e-5, &len, &lenerr, &neval); 
    len = std::abs( len );
    if( intres != 0 ) len = -1;
  }

  return len;
}
  

/// Ring or arc on cylinder center is at (_R2_, 0, 0) and current flows in counter clockwise
/// direction. The arc starts at ( sqrt( _R2_^2 -_R_^2 cos^2( _phi0_ ) ), _R_ cos( _phi0_ ), _R_ sin( _phi0_ ) )
/// and ends at ( sqrt( _R2_^2 -_R_^2 cos^2( _phi1_ ) ), _R_ cos( _phi1_ ), _R_ sin( _phi1_ ) ).
/// Sweep angle _phi1_ - _phi0_ = 2 pi gives complete ring. 
/// _d_ is wire diameter, _M_ number of volume elements and _I_ current.
/// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
/// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
double ringCylinderWire(const double R, const double R2, const double phi0, const double phi1, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test)
{
  double Jphi = I / (M_PI * (d/2) * (d/2) );
  double dphi = ( phi1 - phi0 ) / M;
  double A = (d/2) * (d/2) * M_PI;

  std::vector<double> a, P, T; // axis of rotation and vectors to rotate

  double anorm = sqrt( ax*ax + ay*ay + az*az );

  if( anorm > 0 && theta != 0 )
  {
    a.push_back( ax / anorm );
    a.push_back( ay / anorm );
    a.push_back( az / anorm );
  }

  double ds = 0;
  double dl = 0, len = 0, dv = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t, t1, t2;
  for( int i = 0; i < M; i++ )
  {
    t = (i + 0.5) * dphi + phi0;
    t1 = i  * dphi + phi0;
    t2 = (i + 1.0) * dphi + phi0;

    dl = iiflucks::ringcylinder(t, t1, t2, R, R2, P, T, ds);

    dv = dl * A;

    len += dl;
    vol += dv;

    if( anorm > 0 && theta != 0 )
    {
      iiflucks::QuaternionRotation(a, theta, P);
      iiflucks::QuaternionRotation(a, theta, T);
    }
    
    P[ 0 ] += xt;
    P[ 1 ] += yt;
    P[ 2 ] += zt; 
    
    T[ 0 ] *= Jphi;
    T[ 1 ] *= Jphi;
    T[ 2 ] *= Jphi; 

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dphi=" << dphi << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ] ;
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

}



