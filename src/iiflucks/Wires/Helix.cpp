#include "iiflucks.h"
#include <iostream>

Helix::~Helix() { };

/// Length = ( t2 - t1 ) * sqrt( R^2 + L^2 / ( 2 pi N )^2 )
double Helix::Length(const double t1, const double t2)
{
  return std::abs( ( t2 - t1 ) * sqrt( R * R + L * L / ( 4 * M_PI * M_PI * N * N ) ) );
}

/// dl = dt * sqrt( R^2 + L^2 / ( 2 pi N )^2 )
double Helix::dl(const double dt, const double t)
{
  return std::abs( dt * ds( 0 ) );
}

/// Volume = A * dt * sqrt( R^2 + L^2 / ( 2 pi N )^2 )
double Helix::dv(double dt, const double t)
{
  return std::abs( A * dt * ds( 0 ) );
}

/// Volume = pi * ( r2^2 - r1^2 ) * dt * sqrt( r^2 + L^2 / ( 2 pi N )^2 )
double Helix::dv(double dt, const double t, const double r1, const double r2)
{
  return std::abs( M_PI * ( r2 * r2 - r1 * r1 ) * dt * ds( 0 ) );
}

/// Length element dl = ds * dt:
///    ds = sqrt( R^2 + L^2 / ( 2 pi N )^2 )
double Helix::ds(const double t)
{
  double ds = sqrt( R * R + L * L / ( 4 * M_PI * M_PI * N * N ) ); 

  return ds;
}

/// The helix position vector P components are:
///    Px = R cos(t)
///    Py = R sin(t)
///    Pz = t L / ( 2 pi N)
std::vector<double> Helix::XYZ(const double t)
{
  std::vector<double> P;

  P.erase( P.begin(), P.end() );
  P.push_back( R * cos( t ) );
  P.push_back( R * sin( t ) );
  P.push_back( t * L / ( 2 * M_PI * N ) ); 

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, P);
  }

  P[ 0 ] += tr[ 0 ];
  P[ 1 ] += tr[ 1 ];
  P[ 2 ] += tr[ 2 ];

  return P;
}

/// Helix tangential vector T:
///    Tx = -R sin(t) / ds
///    Ty =  R cos(t) / ds
///    Tz =  L / ( 2 pi N ds )
std::vector<double> Helix::Jv(const double t)
{
  std::vector<double> T;
  if( ds( t ) > 0 )
  {
    T.push_back( -R * sin( t ) / ds( 0 ) );
    T.push_back(  R * cos( t ) / ds( 0 ) );
    T.push_back( L / ( 2 * M_PI * N * ds( 0 ) ) );
  }
  else
  {
    T.push_back( 0 );
    T.push_back( 0 );
    T.push_back( 0 );
  }

  if( std::abs( iiflucks::vectorLength( axis ) - 1 ) < 1e-5 && theta != 0 )
  {
    iiflucks::QuaternionRotation(axis, theta, T);
  }

  T[ 0 ] *= J;
  T[ 1 ] *= J;
  T[ 2 ] *= J;

  return T;
}

/// Helix bottom center is at (0, 0, 0) and wire starts from (_r_, 0, 0).  
/// Helix radius is _r_, length _L_, number of turns _N_ and wire diameter
/// _d_. Current _I_ flows in wire in counter clockwise direction and
/// is divided to _M_ number of volume elements.
/// Rotate wire elements around axis ( _ax_ , _ay_ , _az_ ) with angle 
/// _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
/// The flags _verbose_ and _test_ are used for debugging. 
double Helix::Elements(const int M, const int test, const bool verbose)
{

  std::vector<double> P, T; // position and tangent vectors

  double len = 0, vol = 0;
  double Jxl = 0, Jyl = 0, Jzl = 0;
  double t;
  double dt = 2 * M_PI * N / M ;
  double dl = dt * ds( 0 );
  double dv = A * dt * ds( 0 );

  for( int i = 0; i < M; i++ )
  {
    t = 2 * M_PI * (i + 0.5) * N / M;

    len += dl;
    vol += dv;

    P = XYZ( t );

    if( ds( 0 ) > 0 ) T = Jv( t ); else iiflucks::zeroVector( T );

    Jxl += T[ 0 ] * dv;
    Jyl += T[ 1 ] * dv;
    Jzl += T[ 2 ] * dv;

    if( verbose ) std::cout << "-- t=" << t << " dl=" << dl << " ";

    if( verbose || !test )
    {
      std::cout << P[ 0 ] << " " << P[ 1 ] << " " << P[ 2 ];
      std::cout << " " << T[ 0 ] << " " << T[ 1 ] << " " << T[ 2 ];
      std::cout << " " << dv << std::endl;
    }
  }

  if( test == 1 )
  {
    return len;
  }
  else if( test == 2 )
  {
    return vol;
  }
  else if( test == 3 )
  {
    return Jxl;
  }
  else if( test == 4 )
  {
    return Jyl;
  }
  else if( test == 5 )
  {
    return Jzl;
  }
  else
  {
    return len;
  }
}

/// Numerical integration of magnetic field at position P.
int Helix::B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval)
{
  return iiflucks::fieldHelix(R, L, N, I, t1, t2, P, epsabs, B, Berr, neval);
}
