#include "Histograms.h"

Histograms::~Histograms() { };

/// Update histogram minimum and maximum values from yaml node. 
void Histograms::ParseRangesYaml(const YAML::Node histo_minmax)
{
  if( histo_minmax["x_range"] )
  {
    minx = histo_minmax["x_range"][0].as<double>();
    maxx = histo_minmax["x_range"][1].as<double>();
  }

  if( histo_minmax["y_range"] )
  {
    miny = histo_minmax["y_range"][0].as<double>();
    maxy = histo_minmax["y_range"][1].as<double>();
  }

  if( histo_minmax["z_range"] )
  {
    minz = histo_minmax["z_range"][0].as<double>();
    maxz = histo_minmax["z_range"][1].as<double>();
  }

  if( histo_minmax["r12_range"] )
  {
    mndst = histo_minmax["r12_range"][0].as<double>();
    mxdst = histo_minmax["r12_range"][1].as<double>();
  }

  if( histo_minmax["vol1_range"] )
  {
    mnvol1 = histo_minmax["vol1_range"][0].as<double>();
    mxvol1 = histo_minmax["vol1_range"][1].as<double>();
  }

  if( histo_minmax["vol2_range"] )
  {
    mnvol2 = histo_minmax["vol2_range"][0].as<double>();
    mxvol2 = histo_minmax["vol2_range"][1].as<double>();
  }

  if( histo_minmax["prob1_range"] )
  {
    mnprob1 = histo_minmax["prob1_range"][0].as<double>();
    mxprob1 = histo_minmax["prob1_range"][1].as<double>();
  }

  if( histo_minmax["prob2_range"] )
  {
    mnprob2 = histo_minmax["prob2_range"][0].as<double>();
    mxprob2 = histo_minmax["prob2_range"][1].as<double>();
  }

  if( histo_minmax["J1_range"] )
  {
    minJ1 = histo_minmax["J1_range"][0].as<double>();
    maxJ1 = histo_minmax["J1_range"][1].as<double>();
  }

  if( histo_minmax["J1x_range"] )
  {
    minJ1x = histo_minmax["J1x_range"][0].as<double>();
    maxJ1x = histo_minmax["J1x_range"][1].as<double>();
  }

  if( histo_minmax["J1y_range"] )
  {
    minJ1y = histo_minmax["J1y_range"][0].as<double>();
    maxJ1y = histo_minmax["J1y_range"][1].as<double>();
  }

  if( histo_minmax["J1z_range"] )
  {
    minJ1z = histo_minmax["J1z_range"][0].as<double>();
    maxJ1z = histo_minmax["J1z_range"][1].as<double>();
  }

  if( histo_minmax["J2_range"] )
  {
    minJ2 = histo_minmax["J2_range"][0].as<double>();
    maxJ2 = histo_minmax["J2_range"][1].as<double>();
  }

  if( histo_minmax["J2x_range"] )
  {
    minJ2x = histo_minmax["J2x_range"][0].as<double>();
    maxJ2x = histo_minmax["J2x_range"][1].as<double>();
  }

  if( histo_minmax["J2y_range"] )
  {
    minJ2y = histo_minmax["J2y_range"][0].as<double>();
    maxJ2y = histo_minmax["J2y_range"][1].as<double>();
  }

  if( histo_minmax["J2z_range"] )
  {
    minJ2z = histo_minmax["J2z_range"][0].as<double>();
    maxJ2z = histo_minmax["J2z_range"][1].as<double>();
  }

  if( histo_minmax["Jrnd_range"] )
  {
    minJrnd = histo_minmax["Jrnd_range"][0].as<double>();
    maxJrnd = histo_minmax["Jrnd_range"][1].as<double>();
  }

  if( histo_minmax["Fx_range"] )
  {
    minFx = histo_minmax["Fx_range"][0].as<double>();
    maxFx = histo_minmax["Fx_range"][1].as<double>();
  }

  if( histo_minmax["Fy_range"] )
  {
    minFy = histo_minmax["Fy_range"][0].as<double>();
    maxFy = histo_minmax["Fy_range"][1].as<double>();
  }

  if( histo_minmax["Fz_range"] )
  {
    minFz = histo_minmax["Fz_range"][0].as<double>();
    maxFz = histo_minmax["Fz_range"][1].as<double>();
  }

  if( histo_minmax["F_range"] )
  {
    minF = histo_minmax["F_range"][0].as<double>();
    maxF = histo_minmax["F_range"][1].as<double>();
  }

  if( histo_minmax["Bx_range"] )
  {
    minBx = histo_minmax["Bx_range"][0].as<double>();
    maxBx = histo_minmax["Bx_range"][1].as<double>();
  }

  if( histo_minmax["By_range"] )
  {
    minBy = histo_minmax["By_range"][0].as<double>();
    maxBy = histo_minmax["By_range"][1].as<double>();
  }

  if( histo_minmax["Bz_range"] )
  {
    minBz = histo_minmax["Bz_range"][0].as<double>();
    maxBz = histo_minmax["Bz_range"][1].as<double>();
  }

  if( histo_minmax["B_range"] )
  {
    minB = histo_minmax["B_range"][0].as<double>();
    maxB = histo_minmax["B_range"][1].as<double>();
  }

  if( histo_minmax["M12_range"] )
  {
    for( std::size_t i = 0; i < histo_minmax["M12_range"].size() && i < nbin_log ; i++)
    {
      log_range[ i ] = histo_minmax["M12_range"][ i ].as<double>();
    }
  }
}

// Adjust position histogram ranges.
void AdjustRangesXYZ(double &minx, double &maxx)
{
  if( std::abs( minx - maxx ) < constants::equal )
  {
    if( minx != 0 )
    {
      if( minx > 0 ) minx *= 0.9; else minx *= 1.1;
      if( maxx > 0 ) maxx *= 1.1; else maxx *= 0.9;
    }
    else
    {
      minx = -0.1;
      maxx = +0.1;
    }
  }
  else
  {
    minx -= 0.01*fabs( maxx - minx );
    maxx += 0.01*fabs( maxx - minx );
  }
}

// Adjust probability histogram ranges.
void AdjustRangesProb(double &mnprob, double &mxprob)
{
  if( ( mnprob - mxprob ) < constants::equal )
  {
    if( mnprob != 0 )
    {
      mnprob *= 0.9;
      mxprob *= 1.1;
    }
    else
    {
      mnprob = 0;
      mxprob = 1;
    }
  }
  else
  {
    mnprob -= 0.01*fabs( mxprob - mnprob );
    mxprob += 0.01*fabs( mxprob - mnprob );
  }
}

// Adjust volume histogram ranges.
void AdjustRangesVol(double &mnvol, double &mxvol)
{
  if( std::abs( mnvol - mxvol ) < constants::equal )
  {
    if( mnvol != 0 )
    {
      mnvol *= 0.9;
      mxvol *= 1.1;
    }
    else
    {
      mnvol = 0;
      mxvol = 0.1;
    }
  }
  else
  {
    mnvol -= 0.01 * fabs( mxvol - mnvol );
    mxvol += 0.01 * fabs( mxvol - mnvol );
  }
}

// Adjust current density histogram ranges.
void AdjustRangesJ(double &minJ, double &maxJ)
{
  if( std::abs( minJ - maxJ ) < constants::equal )
  {
    if( minJ != 0 )
    {
      if( minJ > 0 ) minJ *= 0.9; else minJ *= 1.1;
      if( maxJ > 0 ) maxJ *= 1.1; else maxJ *= 0.9;
    }
    else
    {
      minJ = -0.1;
      maxJ = +0.1;
    }
  }
  else
  {
    minJ -= 0.01 * fabs( maxJ - minJ );
    maxJ += 0.01 * fabs( maxJ - minJ );
  }
}

// Adjust force histogram ranges.
void AdjustRangesF(double &minF, double &maxF)
{
  if( std::abs ( minF - maxF ) < constants::equal )
  {
    if( minF != 0 )
    {
      if( minF > 0 ) minF *= 0.9; else minF *= 1.1;
      if( maxF > 0 ) maxF *= 1.1; else maxF *= 0.9;
    }
    else
    {
      minF = -0.1;
      maxF = +0.1;
    }
  }
  else
  {
    minF -= 0.01*fabs( maxF - minF );
    maxF += 0.01*fabs( maxF - minF );
  }
}

// Adjust field histogram ranges.
void AdjustRangesB(double &minB, double &maxB)
{
  if( std::abs( minB - maxB ) < constants::equal )
  {
    if( minB != 0 )
    {
      if( minB > 0 ) minB *= 0.9; else minB *= 1.1;
      if( maxB > 0 ) maxB *= 1.1; else maxB *= 0.9;
    }
    else
    {
      minB = -0.1;
      maxB = +0.1;
    }
  }
  else
  {
    minB -= 0.01*fabs( maxB - minB );
    maxB += 0.01*fabs( maxB - minB );
  }
}

/// Set histogram ranges.
void Histograms::SetRanges(const bool default_ranges)
{
  bool verbose = false;

  AdjustRangesXYZ( minx, maxx );
  AdjustRangesXYZ( miny, maxy );
  AdjustRangesXYZ( minz, maxz );

  AdjustRangesProb( mnprob1, mxprob1 );
  AdjustRangesProb( mnprob2, mxprob2 );

  AdjustRangesVol( mnvol1, mxvol1 );
  AdjustRangesVol( mnvol2, mxvol2 );

  AdjustRangesJ( minJ1, maxJ1 );
  AdjustRangesJ( minJ1x, maxJ1x );
  AdjustRangesJ( minJ1y, maxJ1y );
  AdjustRangesJ( minJ1z, maxJ1z );
  AdjustRangesJ( minJ2, maxJ2 );
  AdjustRangesJ( minJ2x, maxJ2x );
  AdjustRangesJ( minJ2y, maxJ2y );
  AdjustRangesJ( minJ2z, maxJ2z );

  if( minJrnd == 1e20 && maxJrnd == -1e20 )
  {
    minJrnd = -10;
    maxJrnd = +10;
  }
  else if( minJrnd == maxJrnd )
  {
    if( minJrnd > 0 ) minJrnd *= 0.9; else minJrnd *= 1.1;
    if( maxJrnd > 0 ) maxJrnd *= 1.1; else maxJrnd *= 0.9;
  }

  AdjustRangesF( minFx, maxFx );
  AdjustRangesF( minFy, maxFz );
  AdjustRangesF( minFz, maxFz );
  AdjustRangesF( minF, maxF );

  AdjustRangesB( minBx, maxBx );
  AdjustRangesB( minBy, maxBz );
  AdjustRangesB( minBz, maxBz );
  AdjustRangesB( minB, maxB );

  if( verbose ) 
  {
    std::cout << "-- probability1 histogram [" << mnprob1 << "," << mxprob1 << "]" << std::endl;
    std::cout << "-- probability2 histogram [" << mnprob2 << "," << mxprob2 << "]" << std::endl;
    std::cout << "-- volume1 histogram [" << mnvol1 << "," << mxvol2 << "]" << std::endl;
    std::cout << "-- volume2 histogram [" << mnvol2 << "," << mxvol2 << "]" << std::endl;
    std::cout << "-- distance histogram [" << mndst << "," << mxdst << "]" << std::endl;
    std::cout << "-- x histogram [" << minx << "," << maxx << "]" << std::endl;
    std::cout << "-- y histogram [" << miny << "," << maxy << "]" << std::endl;
    std::cout << "-- z histogram [" << minz << "," << maxz << "]" << std::endl;
    std::cout << "-- J1 histogram [" << minJ1 << "," << maxJ1 << "]" << std::endl;
    std::cout << "-- J1x histogram [" << minJ1x << "," << maxJ1x << "]" << std::endl;
    std::cout << "-- J1y histogram [" << minJ1y << "," << maxJ1y << "]" << std::endl;
    std::cout << "-- J1z histogram [" << minJ1z << "," << maxJ1z << "]" << std::endl;
    std::cout << "-- J2 histogram [" << minJ2 << "," << maxJ2 << "]" << std::endl;
    std::cout << "-- J2x histogram [" << minJ2x << "," << maxJ2x << "]" << std::endl;
    std::cout << "-- J2y histogram [" << minJ2y << "," << maxJ2y << "]" << std::endl;
    std::cout << "-- J2z histogram [" << minJ2z << "," << maxJ2z << "]" << std::endl;
    std::cout << "-- Jrnd histogram [" << minJrnd << "," << maxJrnd << "]" << std::endl;
    std::cout << "-- dM12 histogram [" << min_dM12 << "," << max_dM12 << "]" << std::endl;
    std::cout << "-- Fx histogram [" << minFx << "," << maxFx << "]" << std::endl;
    std::cout << "-- Fy histogram [" << minFy << "," << maxFy << "]" << std::endl;
    std::cout << "-- Fz histogram [" << minFz << "," << maxFz << "]" << std::endl;
    std::cout << "-- F histogram [" << minF << "," << maxF << "]" << std::endl;
    std::cout << "-- Bx histogram [" << minBx << "," << maxBx << "]" << std::endl;
    std::cout << "-- By histogram [" << minBy << "," << maxBy << "]" << std::endl;
    std::cout << "-- Bz histogram [" << minBz << "," << maxBz << "]" << std::endl;
    std::cout << "-- B histogram [" << minB << "," << maxB << "]" << std::endl;
  }
  
  if( !default_ranges && mxprob1 > mnprob1 ) gsl_histogram_set_ranges_uniform ( hprob1, mnprob1, mxprob1);
  else gsl_histogram_set_ranges_uniform ( hprob1, 0, 1);

  if( !default_ranges && mxprob2 > mnprob2 ) gsl_histogram_set_ranges_uniform ( hprob2, mnprob2, mxprob2);
  else gsl_histogram_set_ranges_uniform ( hprob2, 0, 1);

  if( !default_ranges && mxvol1 > mnvol1 ) gsl_histogram_set_ranges_uniform ( hvol1, mnvol1, mxvol1 );
  else gsl_histogram_set_ranges_uniform ( hvol1, 0, 1);

  if( !default_ranges && mxvol2 > mnvol2 ) gsl_histogram_set_ranges_uniform ( hvol2, mnvol2, mxvol2 );
  else gsl_histogram_set_ranges_uniform ( hvol2, 0, 1);

  if( !default_ranges && mxdst > mndst ) gsl_histogram_set_ranges_uniform ( hdst, mndst, mxdst );
  else gsl_histogram_set_ranges_uniform ( hdst, 0, 1);

  if( !default_ranges && maxx > minx ) gsl_histogram_set_ranges_uniform ( hx, minx, maxx);
  else gsl_histogram_set_ranges_uniform ( hx, -1, +1);

  if( !default_ranges && maxy > miny ) gsl_histogram_set_ranges_uniform ( hy, miny, maxy);
  else gsl_histogram_set_ranges_uniform ( hy, -1, +1);

  if( !default_ranges && maxz > minz ) gsl_histogram_set_ranges_uniform ( hz, minz, maxz);
  else gsl_histogram_set_ranges_uniform ( hz, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny )gsl_histogram2d_set_ranges_uniform ( hxy, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hxy, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxz > minz ) gsl_histogram2d_set_ranges_uniform ( hxz, minx, maxx, minz, maxz);
  else gsl_histogram2d_set_ranges_uniform ( hxz, -1, +1, -1, +1);

  if( !default_ranges && maxy > miny && maxz > minz ) gsl_histogram2d_set_ranges_uniform ( hyz, miny, maxy, minz, maxz);
  else gsl_histogram2d_set_ranges_uniform ( hyz, -1, +1, -1, +1);

  if( !default_ranges && maxJ1 > minJ1 ) gsl_histogram_set_ranges_uniform ( hJ1, minJ1, maxJ1);
  else gsl_histogram_set_ranges_uniform ( hJ1, 0, 1);

  if( !default_ranges && maxJ1x > minJ1x ) gsl_histogram_set_ranges_uniform ( hJ1x, minJ1x, maxJ1x);
  else gsl_histogram_set_ranges_uniform ( hJ1x, 0, 1);

  if( !default_ranges && maxJ1y > minJ1y ) gsl_histogram_set_ranges_uniform ( hJ1y, minJ1y, maxJ1y);
  else gsl_histogram_set_ranges_uniform ( hJ1y, 0, 1);

  if( !default_ranges && maxJ1z > minJ1z ) gsl_histogram_set_ranges_uniform ( hJ1z, minJ1z, maxJ1z);
  else gsl_histogram_set_ranges_uniform ( hJ1z, 0, 1);

  if( !default_ranges && maxJ2 > minJ2 ) gsl_histogram_set_ranges_uniform ( hJ2, minJ2, maxJ2);
  else gsl_histogram_set_ranges_uniform ( hJ2, 0, 1);

  if( !default_ranges && maxJ2x > minJ2x ) gsl_histogram_set_ranges_uniform ( hJ2x, minJ2x, maxJ2x);
  else gsl_histogram_set_ranges_uniform ( hJ2x, 0, 1);

  if( !default_ranges && maxJ2y > minJ2y ) gsl_histogram_set_ranges_uniform ( hJ2y, minJ2y, maxJ2y);
  else gsl_histogram_set_ranges_uniform ( hJ2y, 0, 1);

  if( !default_ranges && maxJ2z > minJ2z ) gsl_histogram_set_ranges_uniform ( hJ2z, minJ2z, maxJ2z);
  else gsl_histogram_set_ranges_uniform ( hJ2z, 0, 1);

  if( !default_ranges && maxJrnd > minJrnd ) gsl_histogram_set_ranges_uniform ( hJrnd, minJrnd, maxJrnd);
  else gsl_histogram_set_ranges_uniform ( hJrnd, 0, 1);

  if( !default_ranges && max_dM12 > min_dM12 ) gsl_histogram_set_ranges_uniform ( hdM12, min_dM12, max_dM12);
  else gsl_histogram_set_ranges_uniform ( hdM12, 0, 1);

  if( !default_ranges && maxFx > minFx ) gsl_histogram_set_ranges_uniform ( hFx, minFx, maxFx);
  else gsl_histogram_set_ranges_uniform ( hFx, -1, +1);

  if( !default_ranges && maxFy > minFy ) gsl_histogram_set_ranges_uniform ( hFy, minFy, maxFy);
  else gsl_histogram_set_ranges_uniform ( hFy, -1, +1);

  if( !default_ranges && maxFz > minFz ) gsl_histogram_set_ranges_uniform ( hFz, minFz, maxFz);
  else gsl_histogram_set_ranges_uniform ( hFz, -1, +1);

  if( !default_ranges && maxF > minF ) gsl_histogram_set_ranges_uniform ( hF, minF, maxF);
  else gsl_histogram_set_ranges_uniform ( hF, -1, +1);

  if( !default_ranges && maxB > minB ) gsl_histogram_set_ranges_uniform ( hB, minB, maxB);
  else gsl_histogram_set_ranges_uniform ( hB, -1, +1);

  if( !default_ranges && maxBx > minBx ) gsl_histogram_set_ranges_uniform ( hBx, minBx, maxBx);
  else gsl_histogram_set_ranges_uniform ( hBx, -1, +1);

  if( !default_ranges && maxBy > minBy ) gsl_histogram_set_ranges_uniform ( hBy, minBy, maxBy);
  else gsl_histogram_set_ranges_uniform ( hBy, -1, +1);

  if( !default_ranges && maxBz > minBz ) gsl_histogram_set_ranges_uniform ( hBz, minBz, maxBz);
  else gsl_histogram_set_ranges_uniform ( hBz, -1, +1);

  if( !default_ranges && maxB > minB ) gsl_histogram_set_ranges_uniform ( hB, minB, maxB);
  else gsl_histogram_set_ranges_uniform ( hB, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hFxy1, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hFxy1, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hFxy2, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hFxy2, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxz > minz ) gsl_histogram2d_set_ranges_uniform ( hFxz1, minx, maxx, minz, maxz);
  else gsl_histogram2d_set_ranges_uniform ( hFxz1, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxz > minz )gsl_histogram2d_set_ranges_uniform ( hFxz2, minx, maxx, minz, maxz);
  else gsl_histogram2d_set_ranges_uniform ( hFxz2, -1, +1, -1, +1);

  if( !default_ranges && maxy > miny && maxz > minz ) gsl_histogram2d_set_ranges_uniform ( hFyz1, miny, maxy, minz, maxz);
  else gsl_histogram2d_set_ranges_uniform ( hFxz1, -1, +1, -1, +1);

  if( !default_ranges && maxy > miny && maxz > minz ) gsl_histogram2d_set_ranges_uniform ( hFyz2, miny, maxy, minz, maxz);
  else gsl_histogram2d_set_ranges_uniform ( hFxz2, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1x_xy, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1x_xy, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1y_xy, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1y_xy, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1z_xy, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1z_xy, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1x_xz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1x_xz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1y_xz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1y_xz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1z_xz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1z_xz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1x_yz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1x_yz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1y_yz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1y_yz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ1z_yz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ1z_yz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2x_xy, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2x_xy, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2y_xy, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2y_xy, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2z_xy, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2z_xy, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2x_xz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2x_xz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2y_xz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2y_xz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2z_xz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2z_xz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2x_yz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2x_yz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2y_yz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2y_yz, -1, +1, -1, +1);

  if( !default_ranges && maxx > minx && maxy > miny ) gsl_histogram2d_set_ranges_uniform ( hJ2z_yz, minx, maxx, miny, maxy);
  else gsl_histogram2d_set_ranges_uniform ( hJ2z_yz, -1, +1, -1, +1);

  gsl_histogram_set_ranges( hdM12, log_range, nbin_log + 1);
}

/// Scale F and J histogram values with xy-, xz- and yz-position histograms.
void Histograms::ScaleValues()
{
  double xlower, xupper, ylower, yupper;

  gsl_histogram2d_memcpy( scale_xy, hxy );

  // pad bins with zero content with 1 for histogram division
  for(size_t j = 0; j < nbin_xy; j++)
  {
    for(size_t i = 0; i < nbin_xy; i++)
    {
      if( gsl_histogram2d_get( scale_xy, i, j ) == 0 )
      {
        gsl_histogram2d_get_xrange( scale_xy, i, &xlower, &xupper);
        gsl_histogram2d_get_yrange( scale_xy, j, &ylower, &yupper);
        gsl_histogram2d_increment( scale_xy, ( xlower + xupper ) / 2, ( ylower +yupper ) / 2 );
      }
    }
  }

  gsl_histogram2d_memcpy( scale_xz, hxz );

  for(size_t j = 0; j < nbin_xy; j++)
  {
    for(size_t i = 0; i < nbin_xy; i++)
    {
      if( gsl_histogram2d_get( scale_xz, i, j ) == 0 )
      {
        gsl_histogram2d_get_xrange( scale_xz, i, &xlower, &xupper);
        gsl_histogram2d_get_yrange( scale_xz, j, &ylower, &yupper);
        gsl_histogram2d_increment( scale_xz, ( xlower + xupper ) / 2, ( ylower +yupper ) / 2 );
      }
    }
  }

  gsl_histogram2d_memcpy( scale_yz, hyz );

  for(size_t j = 0; j < nbin_xy; j++)
  {
    for(size_t i = 0; i < nbin_xy; i++)
    {
      if( gsl_histogram2d_get( scale_yz, i, j ) == 0 )
      {
        gsl_histogram2d_get_xrange( scale_yz, i, &xlower, &xupper);
        gsl_histogram2d_get_yrange( scale_yz, j, &ylower, &yupper);
        gsl_histogram2d_increment( scale_yz, ( xlower + xupper ) / 2, ( ylower +yupper ) / 2 );
      }
    }
  }

  std::cout << "binning prevents scaling:";
  if( gsl_histogram2d_equal_bins_p( hJ1x_xy, scale_xy ) == 1 ) gsl_histogram2d_div( hJ1x_xy, scale_xy ); else std::cout << " hJ1x_xy";

  if( gsl_histogram2d_equal_bins_p( hJ1y_xy, scale_xy ) == 1 ) gsl_histogram2d_div( hJ1y_xy, scale_xy ); else std::cout << " hJ1y_xy";

  if( gsl_histogram2d_equal_bins_p( hJ1z_xy, scale_xy ) == 1 ) gsl_histogram2d_div( hJ1z_xy, scale_xy ); else std::cout << " hJ1z_xy";

  if( gsl_histogram2d_equal_bins_p( hJ2x_xy, scale_xy ) == 1 ) gsl_histogram2d_div( hJ2x_xy, scale_xy ); else std::cout << " hJ2x_xy";

  if( gsl_histogram2d_equal_bins_p( hJ2y_xy, scale_xy ) == 1 ) gsl_histogram2d_div( hJ2y_xy, scale_xy ); else std::cout << " hJ2y_xy";

  if( gsl_histogram2d_equal_bins_p( hJ2z_xy, scale_xy ) == 1 ) gsl_histogram2d_div( hJ2z_xy, scale_xy ); else std::cout << " hJ2z_xy";

  if( gsl_histogram2d_equal_bins_p( hJ1x_xz, scale_xz ) == 1 ) gsl_histogram2d_div( hJ1x_xz, scale_xz ); else std::cout << " hJ1x_xz";

  if( gsl_histogram2d_equal_bins_p( hJ1y_xz, scale_xz ) == 1 ) gsl_histogram2d_div( hJ1y_xz, scale_xz ); else std::cout << " hJ1y_xz";

  if( gsl_histogram2d_equal_bins_p( hJ1z_xz, scale_xz ) == 1 ) gsl_histogram2d_div( hJ1z_xz, scale_xz ); else std::cout << " hJ1z_xz";

  if( gsl_histogram2d_equal_bins_p( hJ2x_xz, scale_xz ) == 1 ) gsl_histogram2d_div( hJ2x_xz, scale_xz ); else std::cout << " hJ2x_xz";

  if( gsl_histogram2d_equal_bins_p( hJ2y_xz, scale_xz ) == 1 ) gsl_histogram2d_div( hJ2y_xz, scale_xz ); else std::cout << " hJ2y_xz";

  if( gsl_histogram2d_equal_bins_p( hJ2z_xz, scale_xz ) == 1 ) gsl_histogram2d_div( hJ2z_xz, scale_xz ); else std::cout << " hJ2z_xz";

  if( gsl_histogram2d_equal_bins_p( hJ1x_yz, scale_yz ) == 1 ) gsl_histogram2d_div( hJ1x_yz, scale_yz ); else std::cout << " hJ1x_yz";

  if( gsl_histogram2d_equal_bins_p( hJ1y_yz, scale_yz ) == 1 ) gsl_histogram2d_div( hJ1y_yz, scale_yz ); else std::cout << " hJ1y_yz";

  if( gsl_histogram2d_equal_bins_p( hJ1z_yz, scale_yz ) == 1 ) gsl_histogram2d_div( hJ1z_yz, scale_yz ); else std::cout << " hJ1z_yz";

  if( gsl_histogram2d_equal_bins_p( hJ2x_yz, scale_yz ) == 1 ) gsl_histogram2d_div( hJ2x_yz, scale_yz ); else std::cout << " hJ2x_yz";

  if( gsl_histogram2d_equal_bins_p( hJ2y_yz, scale_yz ) == 1 ) gsl_histogram2d_div( hJ2y_yz, scale_yz ); else std::cout << " hJ2y_yz";

  if( gsl_histogram2d_equal_bins_p( hJ2z_yz, scale_yz ) == 1 ) gsl_histogram2d_div( hJ2z_yz, scale_yz ); else std::cout << " hJ2z_yz";

  if( gsl_histogram2d_equal_bins_p( hFxy1, scale_xy ) == 1 ) gsl_histogram2d_div( hFxy1, scale_xy ); else std::cout << " hFxy1";

  if( gsl_histogram2d_equal_bins_p( hFxy2, scale_xy ) == 1 ) gsl_histogram2d_div( hFxy2, scale_xy ); else std::cout << " hFxy2";

  if( gsl_histogram2d_equal_bins_p( hFxz1, scale_xz ) == 1 ) gsl_histogram2d_div( hFxz1, scale_xz ); else std::cout << " hFxz1";

  if( gsl_histogram2d_equal_bins_p( hFxz2, scale_xz ) == 1 ) gsl_histogram2d_div( hFxz2, scale_xz ); else std::cout << " hFxz2";

  if( gsl_histogram2d_equal_bins_p( hFyz1, scale_yz ) == 1 ) gsl_histogram2d_div( hFyz1, scale_yz ); else std::cout << " hFyz1";

  if( gsl_histogram2d_equal_bins_p( hFyz2, scale_yz ) == 1 ) gsl_histogram2d_div( hFyz2, scale_yz ); else std::cout << " hFyz2";

  std::cout << std::endl;
}

// Save histogram to file.
void SaveHistogram(const std::string name, const std::string prefix, const gsl_histogram *histo)
{
  std::string fname = prefix + name;

  FILE *outhprob;

  outhprob = fopen( fname.c_str(), "w" );

  if( outhprob == NULL )
  {
    std::cerr << "Could not write " << fname << " file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhprob, histo, "%g", "%g");
    fclose( outhprob );
  }
}

// Save 2d histogram to file.
void SaveHistogram2d(const std::string name, const std::string prefix, const gsl_histogram2d *histo2)
{
  std::string fname = prefix + name;
  FILE *outh2;
  outh2 = fopen( fname.c_str() , "w");
  if( outh2 == NULL )
  {
    std::cerr << "Could not write " << fname << " file" << std::endl;
  }
  else
  {
    gsl_histogram2d_fprintf( outh2, histo2, "%g", "%g");
    fclose( outh2 );
  }
}

/// Save histograms.
void Histograms::SaveHistograms(const std::string prefix)
{
  SaveHistogram("hprob1.txt", prefix, hprob1);
  SaveHistogram("hprob2.txt", prefix, hprob2);
  SaveHistogram("hvol1.txt", prefix, hvol1);
  SaveHistogram("hvol2.txt", prefix, hvol2);
  SaveHistogram("hdst.txt", prefix, hdst);
  SaveHistogram("hx.txt", prefix, hx);
  SaveHistogram("hy.txt", prefix, hy);
  SaveHistogram("hz.txt", prefix, hz);
  SaveHistogram("hJ1.txt", prefix, hJ1);
  SaveHistogram("hJ1x.txt", prefix, hJ1x);
  SaveHistogram("hJ1y.txt", prefix, hJ1y);
  SaveHistogram("hJ1z.txt", prefix, hJ1z);
  SaveHistogram("hJ2.txt", prefix, hJ2);
  SaveHistogram("hJ2x.txt", prefix, hJ2x);
  SaveHistogram("hJ2y.txt", prefix, hJ2y);
  SaveHistogram("hJ2z.txt", prefix, hJ2z);
  SaveHistogram("hJrnd.txt", prefix, hJrnd);
  SaveHistogram("hdM12.txt", prefix, hdM12);
  SaveHistogram("hF.txt", prefix, hF);
  SaveHistogram("hFx.txt", prefix, hFx);
  SaveHistogram("hFy.txt", prefix, hFy);
  SaveHistogram("hFz.txt", prefix, hFz);
  SaveHistogram2d("hxy.txt", prefix, hxy);
  SaveHistogram2d("hxz.txt", prefix, hxz);
  SaveHistogram2d("hyz.txt", prefix, hyz);
  SaveHistogram2d("hFxy1.txt", prefix, hFxy1);
  SaveHistogram2d("hFxy2.txt", prefix, hFxy2);
  SaveHistogram2d("hFxz1.txt", prefix, hFxz1);
  SaveHistogram2d("hFxz2.txt", prefix, hFxz2);
  SaveHistogram2d("hFyz1.txt", prefix, hFyz1);
  SaveHistogram2d("hFyz2.txt", prefix, hFyz2);
  SaveHistogram2d("hJ1x_xy.txt", prefix, hJ1x_xy);
  SaveHistogram2d("hJ1y_xy.txt", prefix, hJ1y_xy);
  SaveHistogram2d("hJ1z_xy.txt", prefix, hJ1z_xy);
  SaveHistogram2d("hJ1x_xz.txt", prefix, hJ1x_xz);
  SaveHistogram2d("hJ1y_xz.txt", prefix, hJ1y_xz);
  SaveHistogram2d("hJ1z_xz.txt", prefix, hJ1z_xz);
  SaveHistogram2d("hJ1x_yz.txt", prefix, hJ1x_yz);
  SaveHistogram2d("hJ1y_yz.txt", prefix, hJ1y_yz);
  SaveHistogram2d("hJ1z_yz.txt", prefix, hJ1z_yz);
  SaveHistogram2d("hJ2x_xy.txt", prefix, hJ2x_xy);
  SaveHistogram2d("hJ2y_xy.txt", prefix, hJ2y_xy);
  SaveHistogram2d("hJ2z_xy.txt", prefix, hJ2z_xy);
  SaveHistogram2d("hJ2x_xz.txt", prefix, hJ2x_xz);
  SaveHistogram2d("hJ2y_xz.txt", prefix, hJ2y_xz);
  SaveHistogram2d("hJ2z_xz.txt", prefix, hJ2z_xz);
  SaveHistogram2d("hJ2x_yz.txt", prefix, hJ2x_yz);
  SaveHistogram2d("hJ2y_yz.txt", prefix, hJ2y_yz);
  SaveHistogram2d("hJ2z_yz.txt", prefix, hJ2z_yz);

}

/// Save histograms for field calculations.
void Histograms::SaveHistogramsB(const std::string prefix)
{
  SaveHistogram("hB.txt", prefix, hB);
  SaveHistogram("hBx.txt", prefix, hBx);
  SaveHistogram("hBy.txt", prefix, hBy);
  SaveHistogram("hBz.txt", prefix, hBz);
}

/// Delete histograms.
void Histograms::Delete()
{
  gsl_histogram_free( hprob1 );
  gsl_histogram_free( hprob2 );
  gsl_histogram_free( hvol1 );
  gsl_histogram_free( hvol2 );
  gsl_histogram_free( hdst );
  gsl_histogram_free( hx );
  gsl_histogram_free( hy );
  gsl_histogram_free( hz );
  gsl_histogram_free( hJ1 );
  gsl_histogram_free( hJ1x );
  gsl_histogram_free( hJ1y );
  gsl_histogram_free( hJ1z );
  gsl_histogram_free( hJ2 );
  gsl_histogram_free( hJ2x );
  gsl_histogram_free( hJ2y );
  gsl_histogram_free( hJ2z );
  gsl_histogram_free( hJrnd );
  gsl_histogram_free( hdM12 );
  gsl_histogram_free( hFx );
  gsl_histogram_free( hFy );
  gsl_histogram_free( hFz );
  gsl_histogram_free( hF );
  gsl_histogram_free( hBx );
  gsl_histogram_free( hBy );
  gsl_histogram_free( hBz );
  gsl_histogram_free( hB );
  gsl_histogram2d_free( hxy );
  gsl_histogram2d_free( hxz );
  gsl_histogram2d_free( hyz );
  gsl_histogram2d_free( hFxy1 );
  gsl_histogram2d_free( hFxy2 );
  gsl_histogram2d_free( hFxz1 );
  gsl_histogram2d_free( hFxz2 );
  gsl_histogram2d_free( hFyz1 );
  gsl_histogram2d_free( hFyz2 );
  gsl_histogram2d_free( hJ1x_xy );
  gsl_histogram2d_free( hJ1y_xy );
  gsl_histogram2d_free( hJ1z_xy );
  gsl_histogram2d_free( hJ1x_xz );
  gsl_histogram2d_free( hJ1y_xz );
  gsl_histogram2d_free( hJ1z_xz );
  gsl_histogram2d_free( hJ1x_yz );
  gsl_histogram2d_free( hJ1y_yz );
  gsl_histogram2d_free( hJ1z_yz );
  gsl_histogram2d_free( hJ2x_xy );
  gsl_histogram2d_free( hJ2y_xy );
  gsl_histogram2d_free( hJ2z_xy );
  gsl_histogram2d_free( hJ2x_xz );
  gsl_histogram2d_free( hJ2y_xz );
  gsl_histogram2d_free( hJ2z_xz );
  gsl_histogram2d_free( hJ2x_yz );
  gsl_histogram2d_free( hJ2y_yz );
  gsl_histogram2d_free( hJ2z_yz );

}

/// Update probability histogram values.
void Histograms::UpdateProbability(const double prob1)
{
  if( prob1 > mxprob1 ) mxprob1 = prob1;
  if( prob1 < mnprob1 ) mnprob1 = prob1;
  gsl_histogram_increment( hprob1, prob1);
}

/// Update probability histogram values.
void Histograms::UpdateProbability(const double prob1, const double prob2)
{
  if( prob1 > mxprob1 ) mxprob1 = prob1;
  if( prob1 < mnprob1 ) mnprob1 = prob1;
  gsl_histogram_increment( hprob1, prob1);

  if( prob2 > mxprob2 ) mxprob2 = prob2;
  if( prob2 < mnprob2 ) mnprob2 = prob2;
  gsl_histogram_increment( hprob2, prob2);
}


/// Update volume histogram values.
void Histograms::UpdateVolume(const double dv1)
{
  if( dv1 > mxvol1 ) mxvol1 = dv1;
  if( dv1 < mnvol1 ) mnvol1 = dv1;
  gsl_histogram_increment( hvol1, dv1 );
}

/// Update volume histogram values.
void Histograms::UpdateVolume(const double dv1, const double dv2)
{
  if( dv1 > mxvol1 ) mxvol1 = dv1;
  if( dv1 < mnvol1 ) mnvol1 = dv1;
  gsl_histogram_increment( hvol1, dv1 );

  if( dv2 > mxvol2 ) mxvol2 = dv2;
  if( dv2 < mnvol2 ) mnvol2 = dv2;
  gsl_histogram_increment( hvol2, dv2 );
}

/// Update distance histogram values.
void Histograms::UpdateDistance(const double r12)
{
  if( r12 > mxdst ) mxdst = r12;
  if( r12 < mndst ) mndst = r12;
  gsl_histogram_increment( hdst, r12 );
}

/// Update current density histogram values.
void Histograms::UpdateCurrent(const double J1, const std::vector<double> J1v, const std::vector<double> P1)
{
  if( J1 > maxJ1 ) maxJ1 = J1;
  if( J1 < minJ1 ) minJ1 = J1;
  gsl_histogram_increment( hJ1, J1 );

  if( J1v[0] > maxJ1x ) maxJ1x = J1v[0];
  if( J1v[0] < minJ1x ) minJ1x = J1v[0];
  gsl_histogram_increment( hJ1x, J1v[0] );

  if( J1v[1] > maxJ1y ) maxJ1y = J1v[1];
  if( J1v[1] < minJ1y ) minJ1y = J1v[1];
  gsl_histogram_increment( hJ1y, J1v[1] );

  if( J1v[2] > maxJ1z ) maxJ1z = J1v[2];
  if( J1v[2] < minJ1z ) minJ1z = J1v[2];
  gsl_histogram_increment( hJ1z, J1v[2] );

  if( !P1.empty() )
  {
    gsl_histogram2d_accumulate( hJ1x_xy, P1[ 0 ], P1[ 1 ], J1v[0]);
    gsl_histogram2d_accumulate( hJ1y_xy, P1[ 0 ], P1[ 1 ], J1v[1]);
    gsl_histogram2d_accumulate( hJ1z_xy, P1[ 0 ], P1[ 1 ], J1v[2]);

    gsl_histogram2d_accumulate( hJ1x_xz, P1[ 0 ], P1[ 2 ], J1v[0]);
    gsl_histogram2d_accumulate( hJ1y_xz, P1[ 0 ], P1[ 2 ], J1v[1]);
    gsl_histogram2d_accumulate( hJ1z_xz, P1[ 0 ], P1[ 2 ], J1v[2]);

    gsl_histogram2d_accumulate( hJ1x_yz, P1[ 1 ], P1[ 2 ], J1v[0]);
    gsl_histogram2d_accumulate( hJ1y_yz, P1[ 1 ], P1[ 2 ], J1v[1]);
    gsl_histogram2d_accumulate( hJ1z_yz, P1[ 1 ], P1[ 2 ], J1v[2]);
  }
}

/// Update current density histogram values.
void Histograms::UpdateCurrent(const double J1, const double J2, const std::vector<double> J1v, const std::vector<double> J2v, const std::vector<double> P1, const std::vector<double> P2)
{
  if( J1 > maxJ1 ) maxJ1 = J1;
  if( J1 < minJ1 ) minJ1 = J1;
  gsl_histogram_increment( hJ1, J1 );

  if( J1v[0] > maxJ1x ) maxJ1x = J1v[0];
  if( J1v[0] < minJ1x ) minJ1x = J1v[0];
  gsl_histogram_increment( hJ1x, J1v[0] );

  if( J1v[1] > maxJ1y ) maxJ1y = J1v[1];
  if( J1v[1] < minJ1y ) minJ1y = J1v[1];
  gsl_histogram_increment( hJ1y, J1v[1] );

  if( J1v[2] > maxJ1z ) maxJ1z = J1v[2];
  if( J1v[2] < minJ1z ) minJ1z = J1v[2];
  gsl_histogram_increment( hJ1z, J1v[2] );

  if( J2 > maxJ2 ) maxJ2 = J2;
  if( J2 < minJ2 ) minJ2 = J2;
  gsl_histogram_increment( hJ2, J2 );

  if( J2v[0] > maxJ2x ) maxJ2x = J2v[0];
  if( J2v[0] < minJ2x ) minJ2x = J2v[0];
  gsl_histogram_increment( hJ2x, J2v[0] );

  if( J2v[1] > maxJ2y ) maxJ2y = J2v[1];
  if( J2v[1] < minJ2y ) minJ2y = J2v[1];
  gsl_histogram_increment( hJ2y, J2v[1] );

  if( J2v[2] > maxJ2z ) maxJ2z = J2v[2];
  if( J2v[2] < minJ2z ) minJ2z = J2v[2];
  gsl_histogram_increment( hJ2z, J2v[2] );

  if( !P1.empty() && !P2.empty() )
  {
    gsl_histogram2d_accumulate( hJ1x_xy, P1[ 0 ], P1[ 1 ], J1v[0]);
    gsl_histogram2d_accumulate( hJ1y_xy, P1[ 0 ], P1[ 1 ], J1v[1]);
    gsl_histogram2d_accumulate( hJ1z_xy, P1[ 0 ], P1[ 1 ], J1v[2]);

    gsl_histogram2d_accumulate( hJ1x_xz, P1[ 0 ], P1[ 2 ], J1v[0]);
    gsl_histogram2d_accumulate( hJ1y_xz, P1[ 0 ], P1[ 2 ], J1v[1]);
    gsl_histogram2d_accumulate( hJ1z_xz, P1[ 0 ], P1[ 2 ], J1v[2]);

    gsl_histogram2d_accumulate( hJ1x_yz, P1[ 1 ], P1[ 2 ], J1v[0]);
    gsl_histogram2d_accumulate( hJ1y_yz, P1[ 1 ], P1[ 2 ], J1v[1]);
    gsl_histogram2d_accumulate( hJ1z_yz, P1[ 1 ], P1[ 2 ], J1v[2]);

    gsl_histogram2d_accumulate( hJ2x_xy, P2[ 0 ], P2[ 1 ], J2v[0]);
    gsl_histogram2d_accumulate( hJ2y_xy, P2[ 0 ], P2[ 1 ], J2v[1]);
    gsl_histogram2d_accumulate( hJ2z_xy, P2[ 0 ], P2[ 1 ], J2v[2]);

    gsl_histogram2d_accumulate( hJ2x_xz, P2[ 0 ], P2[ 2 ], J2v[0]);
    gsl_histogram2d_accumulate( hJ2y_xz, P2[ 0 ], P2[ 2 ], J2v[1]);
    gsl_histogram2d_accumulate( hJ2z_xz, P2[ 0 ], P2[ 2 ], J2v[2]);

    gsl_histogram2d_accumulate( hJ2x_yz, P2[ 1 ], P2[ 2 ], J2v[0]);
    gsl_histogram2d_accumulate( hJ2y_yz, P2[ 1 ], P2[ 2 ], J2v[1]);
    gsl_histogram2d_accumulate( hJ2z_yz, P2[ 1 ], P2[ 2 ], J2v[2]);
  }
}

/// Update inductance histogram values.
void Histograms::UpdateInductance(const double M12)
{
  if( M12 > max_dM12 ) max_dM12 = M12;
  if( M12 < min_dM12 ) min_dM12 = M12;
  gsl_histogram_increment( hdM12, M12 );
}

/// Update position histogram values.
void Histograms::UpdateXYZ(const std::vector<double> P1)
{
  if( !P1.empty() )
  {
    if( P1[ 0 ] > maxx ) maxx = P1[ 0 ];
    if( P1[ 0 ] < minx ) minx = P1[ 0 ];
    gsl_histogram_increment( hx, P1[ 0 ] );
    if( P1[ 1 ] > maxy ) maxy = P1[ 1 ];
    if( P1[ 1 ] < miny ) miny = P1[ 1 ];
    gsl_histogram_increment( hy, P1[ 1 ] );
    if( P1[ 2 ] > maxz ) maxz = P1[ 2 ];
    if( P1[ 2 ] < minz ) minz = P1[ 2 ];
    gsl_histogram_increment( hz, P1[ 2 ] );
  }
}

/// Update position histogram values.
void Histograms::UpdateXYZ(const std::vector<double> P1, const std::vector<double> P2)
{
  if( !P1.empty() )
  {
    if( P1[ 0 ] > maxx ) maxx = P1[ 0 ];
    if( P1[ 0 ] < minx ) minx = P1[ 0 ];
    gsl_histogram_increment( hx, P1[ 0 ] );
    if( P1[ 1 ] > maxy ) maxy = P1[ 1 ];
    if( P1[ 1 ] < miny ) miny = P1[ 1 ];
    gsl_histogram_increment( hy, P1[ 1 ] );
    if( P1[ 2 ] > maxz ) maxz = P1[ 2 ];
    if( P1[ 2 ] < minz ) minz = P1[ 2 ];
    gsl_histogram_increment( hz, P1[ 2 ] );
  }

  if( !P2.empty() )
  {
    if( P2[ 0 ] > maxx ) maxx = P2[ 0 ];
    if( P2[ 0 ] < minx ) minx = P2[ 0 ];
    gsl_histogram_increment( hx, P2[ 0 ] );
    if( P2[ 1 ] > maxy ) maxy = P2[ 1 ];
    if( P2[ 1 ] < miny ) miny = P2[ 1 ];
    gsl_histogram_increment( hy, P2[ 1 ] );
    if( P2[ 2 ] > maxz ) maxz = P2[ 2 ];
    if( P2[ 2 ] < minz ) minz = P2[ 2 ];
    gsl_histogram_increment( hz, P2[ 2 ] );
  }
}

/// Update force histogram values.
void Histograms::UpdateForce(const double F, const std::vector<double> F12, const std::vector<double> P1, const std::vector<double> P2)
{
  if( !F12.empty() )
  {
    if( F12[ 0 ] > maxFx ) maxFx = F12[ 0 ];
    if( F12[ 0 ] < minFx ) minFx = F12[ 0 ];
    gsl_histogram_increment( hFx, F12[ 0 ] );
    if( F12[ 1 ] > maxFy ) maxFy = F12[ 1 ];
    if( F12[ 1 ] < minFy ) minFy = F12[ 1 ];
    gsl_histogram_increment( hFy, F12[ 1 ] );
    if( F12[ 2 ] > maxFz ) maxFz = F12[ 2 ];
    if( F12[ 2 ] < minFz ) minFz = F12[ 2 ];
    gsl_histogram_increment( hFz, F12[ 2 ] );
  }

  if( F > maxF ) maxF = F;
  if( F < minF ) minF = F;
  gsl_histogram_increment( hF, F );

  if( !P1.empty() && !P2.empty() )
  {
    gsl_histogram2d_increment( hxy, P1[ 0 ], P1[ 1 ] );
    gsl_histogram2d_increment( hxy, P2[ 0 ], P2[ 1 ] );

    gsl_histogram2d_increment( hxz, P1[ 0 ], P1[ 2 ] );
    gsl_histogram2d_increment( hxz, P2[ 0 ], P2[ 2 ] );

    gsl_histogram2d_increment( hyz, P1[ 1 ], P1[ 2 ] );
    gsl_histogram2d_increment( hyz, P2[ 1 ], P2[ 2 ] );

    gsl_histogram2d_accumulate( hFxy1, P1[ 0 ], P1[ 1 ], F);
    gsl_histogram2d_accumulate( hFxy2, P2[ 0 ], P2[ 1 ], -F);

    gsl_histogram2d_accumulate( hFxz1, P1[ 0 ], P1[ 2 ], F);
    gsl_histogram2d_accumulate( hFxz2, P2[ 0 ], P2[ 2 ], -F);

    gsl_histogram2d_accumulate( hFyz1, P1[ 1 ], P1[ 2 ], F);
    gsl_histogram2d_accumulate( hFyz2, P2[ 1 ], P2[ 2 ], -F);
  }
}

/// Update histograms and minimum and maximum values for field calculation.
void Histograms::UpdateField(const std::vector<double> dBv, const double dB)
{
  if( dB > maxB ) maxB = dB;
  if( dB < minB ) minB = dB;
  gsl_histogram_increment( hB, dB );

  if( !dBv.empty() )
  {
    if( dBv[ 0 ] > maxBx ) maxBx = dBv[ 0 ];
    if( dBv[ 0 ] < minBx ) minBx = dBv[ 0 ];
    gsl_histogram_increment( hBx, dBv[ 0 ] );

    if( dBv[ 1 ] > maxBy ) maxBy = dBv[ 1 ];
    if( dBv[ 1 ] < minBy ) minBy = dBv[ 1 ];
    gsl_histogram_increment( hBy, dBv[ 1 ] );

    if( dBv[ 2 ] > maxBz ) maxBz = dBv[ 2 ];
    if( dBv[ 2 ] < minBz ) minBz = dBv[ 2 ];
    gsl_histogram_increment( hBz, dBv[ 2 ] );
  }
}

/// Emit Yaml for histogram ranges. 
void Histograms::SaveRangesYaml(const std::string file_name, const bool test)
{
  YAML::Emitter out;

  out << YAML::BeginMap;
  out << YAML::Key << "nbin";
  out << YAML::Value << nbin;
  out << YAML::Comment("bin number can not be edited here");
  out << YAML::Key << "nbin_x";
  out << YAML::Value << nbin_x;
  out << YAML::Key << "nbin_xy";
  out << YAML::Value << nbin_xy;
  out << YAML::Key << "x_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minx << maxx << YAML::EndSeq;
  out << YAML::Comment("histogram ranges can be edited here");
  out << YAML::Key << "y_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << miny << maxy << YAML::EndSeq;
  out << YAML::Key << "z_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minz << maxz << YAML::EndSeq;
  out << YAML::Key << "r12_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << mndst << mxdst << YAML::EndSeq;
  out << YAML::Key << "vol1_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << mnvol1 << mxvol1 << YAML::EndSeq;
  out << YAML::Key << "vol2_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << mnvol2 << mxvol2 << YAML::EndSeq;
  out << YAML::Key << "prob1_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << mnprob1 << mxprob1 << YAML::EndSeq;
  out << YAML::Key << "prob2_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << mnprob2 << mxprob2 << YAML::EndSeq;
  out << YAML::Key << "J1_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJ1 << maxJ1 << YAML::EndSeq;
  out << YAML::Key << "J1x_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJ1x << maxJ1x << YAML::EndSeq;
  out << YAML::Key << "J1y_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJ1y << maxJ1y << YAML::EndSeq;
  out << YAML::Key << "J1z_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJ1z << maxJ1z << YAML::EndSeq;
  out << YAML::Key << "J2_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJ2 << maxJ2 << YAML::EndSeq;
  out << YAML::Key << "J2x_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJ2x << maxJ2x << YAML::EndSeq;
  out << YAML::Key << "J2y_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJ2y << maxJ2y << YAML::EndSeq;
  out << YAML::Key << "J2z_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJ2z << maxJ2z << YAML::EndSeq;
  out << YAML::Key << "Jrnd_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minJrnd << maxJrnd << YAML::EndSeq;
  out << YAML::Key << "Fx_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minFx << maxFx << YAML::EndSeq;
  out << YAML::Key << "Fy_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minFy << maxFy << YAML::EndSeq;
  out << YAML::Key << "Fz_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minFz << maxFz << YAML::EndSeq;
  out << YAML::Key << "F_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minF << maxF << YAML::EndSeq;
  out << YAML::Key << "Bx_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minBx << maxBx << YAML::EndSeq;
  out << YAML::Key << "By_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minBy << maxBy << YAML::EndSeq;
  out << YAML::Key << "Bz_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minBz << maxBz << YAML::EndSeq;
  out << YAML::Key << "B_range";
  out << YAML::Value << YAML::Flow << YAML::BeginSeq << minB << maxB << YAML::EndSeq;
  out << YAML::Key << "nbin_log";
  out << YAML::Value << nbin_log;
// https://github.com/jbeder/yaml-cpp/issues/761
//  out << YAML::Key << "M12_range";
//  out << YAML::Value << YAML::Flow << YAML::BeginSeq;
//  for( size_t i = 0; i < nbin_log; i++ ) out << log_range[ i ];
//  out << YAML::EndSeq;
  out << YAML::EndMap;

  if( test )
  {
    std::cout << out.c_str();
    std::cout << std::endl;
    std::cout << "M12_range: [";
    for( size_t i = 0; i < nbin_log - 1; i++ ) std::cout << log_range[ i ] << ", ";
    std::cout << log_range[ nbin_log - 1 ] << "]" << std::endl;
    std::cout << "# dM12_min = " << min_dM12 << ", dM12_max = " << max_dM12 << std::endl;
  }
  else
  {
    std::ofstream outfile( file_name );

    if( !outfile.good() )
    {
      std::cerr << "Could not write " << file_name << std::endl;
    }
    else
    {
      outfile << out.c_str();
      outfile << std::endl;
      outfile << "M12_range: [";
      for( size_t i = 0; i < nbin_log - 1; i++ ) outfile << log_range[ i ] << ", ";
      outfile << log_range[ nbin_log - 1 ] << "]" << std::endl;
      outfile << "# dM12_min = " << min_dM12 << " dM12_max = " << max_dM12 << std::endl;
      outfile.close();
    } 
  }
}
