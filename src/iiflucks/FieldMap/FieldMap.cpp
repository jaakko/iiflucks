#include "FieldMap.h"
#include "iiflucks.h"

FieldMap::~FieldMap() { };

/// Cartesian coordinate system calculation grid. Initialize B and Berr
/// to zero. Calculate magnetic field at grid positions
/// P = (x0 + l * dx, y0 + k * dy, z0 + j * dz).
/// l = [-steps[0], steps[0]+1], k = [-steps[1],steps[1]+1], j = [-steps[2],steps[2]+1].
size_t FieldMap::Cartesian(std::vector<double> grid_center, std::vector<double> grid_steps, std::vector<int> steps)
{
  this->grid_center = grid_center;
  this->grid_steps = grid_steps;
  this->steps = steps;

  int nxe = steps[0] +1, nye = steps[1] +1, nze = steps[2] +1;
  int nxs = steps[0], nys = steps[1], nzs = steps[2];

  if( grid_steps[ 0 ] == 0 )
  {
    nxs = 0;
    nxe = 1;
  }
  if( grid_steps[ 1 ] == 0 )
  {
    nys = 0;
    nye = 1;
  }
  if( grid_steps[ 2 ] == 0 )
  {
    nzs = 0;
    nze = 1;
  }

  if( debug ) std::cout << "l,k,j,index,x,y,z" << std::endl;
  std::string key;
  double x, y, z;
  map_size = 0;
  for(int j = -nzs; j < nze; j++)
  {
    for(int k = -nys; k < nye; k++)
    {
      for(int l = -nxs; l < nxe; l++)
      {
        key = std::to_string( l ) + "," + std::to_string( k ) + "," + std::to_string( j );
        grid[ key ] = map_size;

        if( debug ) std::cout << key << "," << map_size << ",";

        x = grid_center[ 0 ] + l * grid_steps[ 0 ];
        y = grid_center[ 1 ] + k * grid_steps[ 1 ];
        z = grid_center[ 2 ] + j * grid_steps[ 2 ];

        if( verbose ) std::cout << x << "," << y << "," << z;
        if( verbose ) std::cout << std::endl;

        iiflucks::makeVector(l, k, j, P[ map_size ]);

	iiflucks::zeroVector( B[ map_size ] );
        iiflucks::zeroVector( Berr[ map_size ] );

	if( map_size >= constants::field_map_size )
        {
          if( verbose ) std::cerr << "-- maximum map size " << constants::field_map_size << std::endl;
	  return -1;
        }

        map_size++;
      }
    }
  }

  cylindrical = false;
  spherical = false;

  return map_size;
}

/// Return coordinate vector on field map grid (l, k, j).
std::vector<double> FieldMap::Coordinates(std::vector<int> P)
{
  std::vector<double> P2;
  P2.push_back( grid_center[ 0 ] + P[ 0 ] * grid_steps[ 0 ] );
  P2.push_back( grid_center[ 1 ] + P[ 1 ] * grid_steps[ 1 ] );
  P2.push_back( grid_center[ 2 ] + P[ 2 ] * grid_steps[ 2 ] );

  return P2;
}

/// Cylindrical coordinate system calculation grid. Initialize B and Berr
/// to zero. Calculate magnetic field at grid positions
/// P = (r0 + l * dr, phi0 + k * dphi, z0 + j * dz).
/// l = [-steps, steps+1], k = [-steps,steps], j = [-steps,steps+1].
size_t FieldMap::Cylindrical(std::vector<double> grid_center, std::vector<double> grid_steps, std::vector<int> steps)
{
  this->grid_center = grid_center;
  this->grid_steps = grid_steps;
  this->steps = steps;

  int nre = steps[0] +1, nphie = steps[1], nze = steps[2] +1;
  int nrs = steps[0], nphis = steps[1], nzs = steps[2];

  if( grid_steps[ 0 ] == 0 )
  {
    nrs = 0;
    nre = 1;
  }
  if( grid_steps[ 1 ] == 0 )
  {
    nphis = 0;
    nphie = 1;
  }
  if( grid_steps[ 2 ] == 0 )
  {
    nzs = 0;
    nze = 1;
  }

  if( debug ) std::cout << "l,k,j,index,r,phi,z" << std::endl;
  std::string key;
  double r, phi, z;
  map_size = 0;
  for(int j = -nzs; j < nze; j++)
  {
    for(int k = -nphis; k < nphie; k++)
    {
      for(int l = -nrs; l < nre; l++)
      {
        key = std::to_string( l ) + "," + std::to_string( k ) + "," + std::to_string( j );
        grid[ key ] = map_size;

        if( debug ) std::cout << key << "," << map_size << ",";

        r   = grid_center[ 0 ] + l * grid_steps[ 0 ];
        phi = grid_center[ 1 ] + k * grid_steps[ 1 ];
        z   = grid_center[ 2 ] + j * grid_steps[ 2 ];

        if( verbose ) std::cout << r << "," << phi << "," << z;
        if( verbose ) std::cout << std::endl;

        iiflucks::makeVector(l, k, j, P[ map_size ]);

	iiflucks::zeroVector( B[ map_size ] );
        iiflucks::zeroVector( Berr[ map_size ] );

	if( map_size >= constants::field_map_size )
        {
          if( verbose ) std::cerr << "-- maximum map size " << constants::field_map_size << std::endl;
	  return -1;
        }

        map_size++;
      }
    }
  }

  cylindrical = true;
  spherical = false;

  return map_size;
}

/// Spherical coordinate system calculation grid. Initialize B and Berr
/// to zero. Calculate magnetic field at grid positions
/// P = (r0 + l * dr, theta0 + k * dtheta, phi0 + j * dphi).
/// l = [-steps, steps+1], k = [-steps,steps], j = [-steps,steps].
size_t FieldMap::Spherical(std::vector<double> grid_center, std::vector<double> grid_steps, std::vector<int> steps)
{
  this->grid_center = grid_center;
  this->grid_steps = grid_steps;
  this->steps = steps;

  int nre = steps[0] +1, nthetae = steps[1], nphie = steps[2];
  int nrs = steps[0], nthetas = steps[1], nphis = steps[2];

  if( grid_steps[ 0 ] == 0 )
  {
    nrs = 0;
    nre = 1;
  }
  if( grid_steps[ 1 ] == 0 )
  {
    nthetas = 0;
    nthetae = 1;
  }
  if( grid_steps[ 2 ] == 0 )
  {
    nphis = 0;
    nphie = 1;
  }

  if( debug ) std::cout << "l,k,j,index,r,theta,phi" << std::endl;
  std::string key;
  double r, theta, phi;
  map_size = 0;
  for(int j = -nphis; j < nphie; j++)
  {
    for(int k = -nthetas; k < nthetae; k++)
    {
      for(int l = -nrs; l < nre; l++)
      {
        key = std::to_string( l ) + "," + std::to_string( k ) + "," + std::to_string( j );
        grid[ key ] = map_size;

        if( debug ) std::cout << key << "," << map_size << ",";

	r     = grid_center[ 0 ] + l * grid_steps[ 0 ];
        theta = grid_center[ 1 ] + k * grid_steps[ 1 ];
        phi   = grid_center[ 2 ] + j * grid_steps[ 2 ];

        if( verbose ) std::cout << r << "," << theta << "," << phi;
        if( verbose ) std::cout << std::endl;

        iiflucks::makeVector(l, k, j, P[ map_size ]);
        iiflucks::zeroVector( B[ map_size ] );
        iiflucks::zeroVector( Berr[ map_size ] );

	if( map_size >= constants::field_map_size )
        {
          if( verbose ) std::cerr << "-- maximum map size " << constants::field_map_size << std::endl;
	  return -1;
        }

        map_size++;
      }
    }
  }

  cylindrical = false;
  spherical = true;

  return map_size;
}

/// Transform Cartesian field vectors to cylindrical field vectors.
void FieldMap::CartesianCylindrical()
{
  std::vector<double> Bcyl;

  for( size_t i = 0; i < map_size; i++)
  {
    iiflucks::cartesianCylindrical( B[ i ], Bcyl );

    B[ i ][ 0 ] = Bcyl[ 0 ];
    B[ i ][ 1 ] = Bcyl[ 1 ];
    B[ i ][ 2 ] = Bcyl[ 2 ];

    iiflucks::cartesianCylindrical( Berr[ i ], Bcyl );

    Berr[ i ][ 0 ] = Bcyl[ 0 ];
    Berr[ i ][ 1 ] = Bcyl[ 1 ];
    Berr[ i ][ 2 ] = Bcyl[ 2 ];
  }
}

/// Transform Cartesian field vectors to spherical field vectors.
void FieldMap::CartesianSpherical()
{
  std::vector<double> Bsph;

  for( size_t i = 0; i < map_size; i++)
  {
    iiflucks::cartesianSpherical( B[ i ], Bsph );

    B[ i ][ 0 ] = Bsph[ 0 ];
    B[ i ][ 1 ] = Bsph[ 1 ];
    B[ i ][ 2 ] = Bsph[ 2 ];

    iiflucks::cartesianSpherical( Berr[ i ], Bsph );

    Berr[ i ][ 0 ] = Bsph[ 0 ];
    Berr[ i ][ 1 ] = Bsph[ 1 ];
    Berr[ i ][ 2 ] = Bsph[ 2 ];
  }
}

/// Copy field map if empty.
int FieldMap::Add(FieldMap *map, const double scale)
{
  std::vector<double> B2;

  if( map_size == 0 )
  {
    for( size_t i = 0; i < map->map_size; i++)
    {
      P[i].erase(P[i].begin(), P[i].end());
      P[i].push_back( map->P[i][0] );
      P[i].push_back( map->P[i][1] );
      P[i].push_back( map->P[i][2] );

      B[i].erase(B[i].begin(), B[i].end());
      B[i].push_back( map->B[i][0] );
      B[i].push_back( map->B[i][1] );
      B[i].push_back( map->B[i][2] );
      B[i].push_back( map->B[i][3] );

      Berr[i].erase(Berr[i].begin(), Berr[i].end());
      Berr[i].push_back( map->Berr[i][0] );
      Berr[i].push_back( map->Berr[i][1] );
      Berr[i].push_back( map->Berr[i][2] );
    }

    map_size = map->map_size;
    spherical = map->spherical;
    cylindrical = map->cylindrical;
    grid_center = map->grid_center;
    grid_steps = map->grid_steps;
    steps = map->steps;
    grid = map->grid;
  }
  else if( map_size != map->map_size )
  {
    std::cerr << "Add() field map sizes are different." << std::endl;
    return -1;
  }
  else
  {
    for( size_t i = 0; i < map_size; i++)
    {
      if( iiflucks::distancePoints( map->P[i], P[i] ) == 0 )
      {
        B2 = map->B[i];
        iiflucks::scaleVector( scale, B2 );
        iiflucks::addVectors( B2, B[i], B[i] );
        B[ i ][ 3 ] = iiflucks::vectorLength( B[ i ] );
        B2 = map->Berr[i];
        iiflucks::scaleVector( scale, B2 );
        iiflucks::addVectors( B2, Berr[i], Berr[i] );
      }
      else
      {
        std::cerr << "Grid positions do not match." << std::endl;
        return -2;
      }
    }
  }

  return 0;
}

/// Rotate cylindrically symmetric field maps. Return 0 in success.
int FieldMap::Rotate(FieldMap *map, const int angle, const int full_rotation)
{
  int index_phi;
  std::string key;
  double alpha;
  std::vector<int> P2;
  std::vector<double> B2;
  size_t index;
  if( map_size == 0 )
  {
    std::cerr << "Rotate() field map is empty." << std::endl;
    return -1;
  }
  else if( map_size != map->map_size )
  {
    std::cerr << "Rotate() field map sizes are different." << std::endl;
    return -2;
  }
  else
  {
    alpha = angle * map->grid_steps[ 1 ];
    if( debug ) std::cout << angle << "," << alpha << std::endl;

    if( debug ) std::cout << "l,k,j,l,k,j,index,B[0],B[1],B[2],B[3],B[0],B[1],B[2],B[3]" << std::endl;
    for( size_t i = 0; i < map_size; i++)
    {
      if( iiflucks::distancePoints( map->P[i], P[i] ) == 0 )
      {
        P2 = map->P[i];
        if( debug ) std::cout << P2[ 0 ] << "," << P2[ 1 ] << "," << P2[ 2 ];
        P2[ 1 ] -= map->P[ 0 ][ 1 ];
        P2[ 1 ] += angle;

        P2[ 1 ] %= full_rotation;
        if( P2[ 1 ] >= full_rotation / 2 )
        {
          P2[ 1 ] -= full_rotation / 2;
          P2[ 0 ] = -P2[ 0 ];
          P2[ 1 ] += map->P[ 0 ][ 1 ];
        }
        else
        {
          P2[ 1 ] += map->P[ 0 ][ 1 ];
        }

        key = std::to_string( P2[ 0 ] ) + "," + std::to_string( P2[ 1 ] ) + "," + std::to_string( P2[ 2 ] );
        index = grid[ key ];
        if( debug ) std::cout << "," << key << "," << index;

        if( debug ) std::cout << "," << map->B[i][0] << "," << map->B[i][1] << "," << map->B[i][2] << "," << map->B[i][3];

        B[ index ] = map->B[ i ];
        iiflucks::rotateXY( alpha, B[ index ] );
        if( debug ) std::cout << "," << map->B[ index ][0] << "," << map->B[ index ][1] << "," << map->B[ index ][2] << "," << map->B[ index ][3];

        Berr[ index ] = map->Berr[ i ];
        iiflucks::rotateXY( alpha, Berr[ index ] );

      if( debug ) std::cout << std::endl;
      }
      else
      {
        std::cerr << "Grid positions do not match." << std::endl;
        return -3;
      }
    }
  }

  return 0;
}

/// Print field map.
void FieldMap::Print()
{
  std::vector<double> P2;

  if( debug ) std::cout << "l,k,j,P[0],P[1],P[2],B[0],B[1],B[2],B[3],Berr[0],Berr[1],Berr[2]" << std::endl;
  for( size_t i = 0; i < map_size; i++)
  {
    P2 = Coordinates( P[ i ] );
    if( debug ) std::cout << P[i][0] << "," << P[i][1] << "," << P[i][2] << ",";
    std::cout << P2[0] << "," << P2[1] << "," << P2[2];
    std::cout << "," << B[i][0] << "," << B[i][1] << "," << B[i][2] << "," << B[i][3];
    std::cout << "," << Berr[i][0] << "," << Berr[i][1] << "," << Berr[i][2];
    std::cout << std::endl;
  }
}

/// Only positive radius values are printed. 
//void FieldMap::PrintCylindrical()
//{
//  std::vector<double> P2;
//
//  if( debug ) std::cout << "l,k,j,P[0],P[1],P[2],B[0],B[1],B[2],B[3],Berr[0],Berr[1],Berr[2]" << std::endl;
//  for( size_t i = 0; i < map_size; i++)
//  {
//     if( P[i][0] >= 0 )
//     {
//       P2 = Coordinates( P[ i ] );
//       if( debug ) std::cout << P[i][0] << "," << P[i][1] << "," << P[i][2] << ",";
//       std::cout << P2[0] << "," << P2[1] << "," << P2[2];
//       std::cout << "," << B[i][0] << "," << B[i][1] << "," << B[i][2] << "," << B[i][3];
//       std::cout << "," << Berr[i][0] << "," << Berr[i][1] << "," << Berr[i][2];
//       std::cout << std::endl;
//    }
//  }
//
//  std::vector<int> Pn;
//  for( size_t i = 0; i < map_size; i++)
//  {
//    if( P[i][0] <= 0 )
//    {
//      Pn = P[ i ];
//      Pn[ 0 ] = -Pn[ 0 ];
//      P2 = Coordinates( Pn );
//      if( debug ) std::cout << Pn[0] << "," << Pn[1] << "," << Pn[2] << ",";
//      std::cout << P2[0] << "," << P2[1] + M_PI << "," << P2[2] << ","; 
//      std::cout << B[i][0] << "," << B[i][1] << "," << B[i][2] << "," << B[i][3] << ",";
//      std::cout << Berr[i][0] << "," << Berr[i][1] << "," << Berr[i][2];
//      std::cout << std::endl;
//    }
//  }
//}
