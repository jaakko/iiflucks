
#include "Quaternions.h"

namespace iiflucks {

/// _q_ = (_q11_ + _q12_ __i__ + _q13_ __j__ + _q14_ __k__)(_q21_ + _q22_ __i__ + _q23_ __j__ + _q24_ __k__),
/// where _q1_ = _q11_ + _q12_ __i__ + _q13_ __j__ + _q14_ __k__ and _q2_ = _q21_ + _q22_ __i__ + q23 __j__ + q24 __k__.
void HamiltonProduct(const std::vector<double> &q1, const std::vector<double> &q2, std::vector<double> &q)
{
  q.erase( q.begin(), q.end() );
  q.push_back( q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2] - q1[3] * q2[3] );
  q.push_back( q1[0] * q2[1] + q1[1] * q2[0] + q1[2] * q2[3] - q1[3] * q2[2] );
  q.push_back( q1[0] * q2[2] - q1[1] * q2[3] + q1[2] * q2[0] + q1[3] * q2[1] );
  q.push_back( q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1] + q1[3] * q2[0] );
}

/// _q_ = _q1*_ = _q11_ - _q12_ __i__ - _q13_ __j__ - _q14_ __k__. 
void ConjugateQuaternion(const std::vector<double> &q1, std::vector<double> &q)
{
  q.erase( q.begin(), q.end() );
  q.push_back( q1[0] );
  q.push_back( -q1[1] );
  q.push_back( -q1[2] );
  q.push_back( -q1[3] );
}

/// The rotation is done with quaternion conjugation and Hamilton product. 
/// _q_ = cos( _theta_ / 2 ) + ( _ax_ __i__ + _ay_ __j__ + _az_ __k__ ) sin( _theta_ / 2 ) and
/// _p'_ = _q_ _p_ _q_^-1.
void QuaternionRotation(const std::vector<double> &a, const double theta, std::vector<double> &r)
{
  std::vector<double> p, pr, q, qc, h;

  p.push_back( 0 );
  p.push_back( r[0] );
  p.push_back( r[1] );
  p.push_back( r[2] );

  q.push_back( cos( theta/2 ) );
  q.push_back( a[0] * sin( theta/2 ) );
  q.push_back( a[1] * sin( theta/2 ) );
  q.push_back( a[2] * sin( theta/2 ) );

  ConjugateQuaternion(q, qc);
  HamiltonProduct(p, qc, h);
  HamiltonProduct(q, h, pr);

  r.erase( r.begin(), r.end() );
  r.push_back( pr[1] );
  r.push_back( pr[2] );
  r.push_back( pr[3] );

}

}

