#include "Test_geometry_calculations.h"

namespace iiflucks {

/// Calculate distance between two points, test if point is on plane,
/// test if vector is on any plane defined by normal vector,
/// calculate distance from point to line,
/// triangle area and normal vector,
/// triangle centroid,
/// quadrangle area,
/// tetrahedron volume,
/// prism volume, 
/// and pyramid volume.  
void testGeometry(const bool verbose)
{
  std::vector<double> P0, P1, P2, P3, Pc, nv;

  // test distance between points
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(1); P1.push_back(0);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") and";
    std::cout << " (" << P1[0] << "," << P1[1] << "," << P1[2] << ") distance=";
  }

  std::cout << iiflucks::distancePoints(P0, P1);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test point is on plane
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  nv.erase (nv.begin(), nv.end());
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(1); P1.push_back(0);
  nv.push_back(1); nv.push_back(1); nv.push_back(0);

  if( verbose )
  {
    std::cout << "-- plane n=(" << nv[0] << "," << nv[1] << "," << nv[2] << ") with point ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << "), is point ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") on plane? ";
  }

  if( iiflucks::pointOnPlane(nv, P0, P1) ) std::cout << "yes"; else std::cout << "no";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test vector P0 is on any plane defined by normal vector nv
  P0.erase (P0.begin(), P0.end());
  nv.erase (nv.begin(), nv.end());
  P0.push_back(1); P0.push_back(-1); P0.push_back(1);
  nv.push_back(1); nv.push_back(1); nv.push_back(0);

  if( verbose )
  {
    std::cout << "-- plane n=(" << nv[0] << "," << nv[1] << "," << nv[2] << ") is vector ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") on any plane? ";
  }

  if( iiflucks::vectorAnyPlane(nv, P0) ) std::cout << "yes"; else std::cout << "no";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // calculate P1 distance from line with nv and P0
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  nv.erase (nv.begin(), nv.end());
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(1); P1.push_back(0);
  nv.push_back(1); nv.push_back(1); nv.push_back(0);

  if( verbose )
  {
    std::cout << "-- (" << P1[0] << "," << P1[1] << "," << P1[2] << ") distance to ";
    std::cout << "line v=(" << nv[0] << "," << nv[1] << "," << nv[2] << ") with point ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") =";
  }

  std::cout << iiflucks::distancePointLine(nv, P0, P1);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test P0 distance from plane spanned by two vectors P2 and P3
  // P1 is on the plane
  P0.push_back(0); P0.push_back(1); P0.push_back(0); 
  P1.push_back(0); P1.push_back(0); P1.push_back(1); 
  P2.push_back(0); P2.push_back(0); P2.push_back(1); 
  P3.push_back(1); P3.push_back(1); P3.push_back(0); 

  if( verbose )
  {
    std::cout << "-- plane vectors ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << "point on plane ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "distance to ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") = ";
  }

  std::cout << iiflucks::distancePointPlane(P0, P1, P2, P3);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test triangle area and normal calculation
  P0.push_back(0); P0.push_back(1); P0.push_back(0);
  P1.push_back(0); P1.push_back(0); P1.push_back(1);
  P2.push_back(1); P2.push_back(0); P2.push_back(0);

  if( verbose )
  {
    std::cout << "-- triangle ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "area ";
  }

  std::cout << iiflucks::triangleAreaNormal(P0, P1, P2, nv);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  if( verbose ) std::cout << "-- triangle normal pseudovector ";

  std::cout << "(" << nv[0] << "," << nv[1] << "," << nv[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test triangle centroid calculation
  iiflucks::triangleCentroid(P0, P1, P2, Pc);

  if( verbose ) std::cout << "-- triangle centroid ";

  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test quadrangle area calculation
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());
  P0.push_back(0); P0.push_back(0); P0.push_back(0);
  P1.push_back(3); P1.push_back(0); P1.push_back(0);
  P2.push_back(4); P2.push_back(2); P2.push_back(0);
  P3.push_back(-2); P3.push_back(2); P3.push_back(0);

  if( verbose )
  {
    std::cout << "-- quadrangle ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << "area=";
  }

  std::cout << iiflucks::quadrangleArea(P0, P1, P2, P3);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test tetrahedron volume calculation
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());
  P0.push_back(0); P0.push_back(0); P0.push_back(0); 
  P1.push_back(0); P1.push_back(0); P1.push_back(1); 
  P2.push_back(1); P2.push_back(0); P2.push_back(1); 
  P3.push_back(0); P3.push_back(1); P3.push_back(1); 

  if( verbose )
  {
    std::cout << "-- tetrahedron ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- volume ";
  }

  std::cout << iiflucks::tetrahedronVolume(P0, P1, P2, P3);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  Pc.erase (Pc.begin(), Pc.end());

  iiflucks::tetrahedronCentroid(P0, P1, P2, P3, Pc);

  if( verbose ) std::cout << "-- centroid ";

  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test prism volume calculation
  std::vector<double> P4, P5;
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());
  Pc.erase (Pc.begin(), Pc.end());
  P0.push_back(1); P0.push_back(0); P0.push_back(0); 
  P1.push_back(1); P1.push_back(1); P1.push_back(0); 
  P2.push_back(0); P2.push_back(0); P2.push_back(0); 
  P3.push_back(1); P3.push_back(0); P3.push_back(1); 
  P4.push_back(1); P4.push_back(1); P4.push_back(2); 
  P5.push_back(0); P5.push_back(0); P5.push_back(2); 

  if( verbose )
  {
    std::cout << "-- prism ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << "(" << P4[0] << "," << P4[1] << "," << P4[2] << ") ";
    std::cout << "(" << P5[0] << "," << P5[1] << "," << P5[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- volume approximate ";
  }

  std::cout << iiflucks::prismVolume(P0, P1, P2, P3, P4, P5);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  iiflucks::prismCentroid(P0, P1, P2, P3, P4, P5, Pc);

  if( verbose ) std::cout << "-- centroid ";

  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());
  P4.erase (P4.begin(), P4.end());
  Pc.erase (Pc.begin(), Pc.end());

  // test pyramid volume calculation
  P0.push_back(+1); P0.push_back(+1); P0.push_back(0); 
  P1.push_back(-1); P1.push_back(+1); P1.push_back(0); 
  P2.push_back(-1); P2.push_back(-1); P2.push_back(0); 
  P3.push_back(+1); P3.push_back(-1); P3.push_back(0); 
  P4.push_back(0); P4.push_back(0); P4.push_back(1);

  if( verbose )
  {
    std::cout << "-- pyramid ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << "(" << P4[0] << "," << P4[1] << "," << P4[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- volume ";
  }

  std::cout << iiflucks::pyramidVolume(P0, P1, P2, P3, P4);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  iiflucks::pyramidCentroid(P0, P1, P2, P3, P4, Pc);

  if( verbose ) std::cout << "-- centroid ";

  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";

  std::cout << std::endl;

}

}

