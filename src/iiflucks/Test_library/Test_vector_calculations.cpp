#include "Test_vector_calculations.h"

namespace iiflucks {

/// Calculate vector length, dot product of two vectors, angle between
/// two vectors, vector product and vector triple product.
void testVector(const bool verbose)
{
  std::vector<double> P0, P1, P2;

  // test vector length calculation
  P0.push_back(1); P0.push_back(2); P0.push_back(3);

  if( verbose )
  {
    std::cout << "-- vector ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") length=";
  }

  std::cout << iiflucks::vectorLength( P0 );

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());

  // test dot product calculation
  P0.push_back(1); P0.push_back(2); P0.push_back(3);
  P1.push_back(4); P1.push_back(5); P1.push_back(6);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") .";
    std::cout << " (" << P1[0] << "," << P1[1] << "," << P1[2] << ") = ";
  }

  std::cout << iiflucks::dotProduct(P0, P1);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test angle between vectors
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(1); P1.push_back(0);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") and";
    std::cout << " (" << P1[0] << "," << P1[1] << "," << P1[2] << ") angle=";
  }

  std::cout << iiflucks::angleVectors(P0, P1);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test vector product calculation
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(-1); P1.push_back(-1);

  if( verbose )
  {
    std::cout << "-- vector product ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") x ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") = ";
  }

  iiflucks::crossProduct(P0, P1, P2);

  std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());

  // test triple product calculation
  P0.push_back(0); P0.push_back(2); P0.push_back(0);
  P1.push_back(1); P1.push_back(0); P1.push_back(0);
  P2.push_back(0); P2.push_back(0); P2.push_back(5);

  if( verbose )
  {
    std::cout << "-- triple product ";
    std::cout << "[(" << P0[0] << "," << P0[1] << "," << P0[2] << "), ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << "), ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")] = ";
  }

  std::cout << iiflucks::tripleProduct(P0, P1, P2);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test vector subtraction
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(-1); P1.push_back(-1);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") -";
    std::cout << " (" << P1[0] << "," << P1[1] << "," << P1[2] << ") = ";
  }

  iiflucks::subtractVectors( P0, P1, P2);
  std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test zero vector
  iiflucks::zeroVector( P2 );

  if( verbose ) std::cout << "-- zero vector ";
  std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test scaling of vector
  P0.erase (P0.begin(), P0.end());
  P0.push_back(1); P0.push_back(2); P0.push_back(3);

  if( verbose ) std::cout << "-- scale by 2 (" << P0[0] << "," << P0[1] << "," << P0[2] << ") = ";

  iiflucks::scaleVector(2, P0);
  std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test vector addition 
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(-1); P1.push_back(-1);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") +";
    std::cout << " (" << P1[0] << "," << P1[1] << "," << P1[2] << ") = ";
  }

  iiflucks::addVectors( P0, P1, P2);
  std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test vector element squared addition 
  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(-2); P1.push_back(-3);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") +";
    std::cout << " square_elements(" << P1[0] << "," << P1[1] << "," << P1[2] << ") = ";
  }

  iiflucks::squareAddVector( P0, P1, P2);
  std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P0.push_back(0); P0.push_back(6); P0.push_back(8);
  if( verbose )
  {
    std::cout << "-- normalize (" << P0[0] << "," << P0[1] << "," << P0[2] << ") = ";
  }
  iiflucks::normalizeVector( P0 );
  std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test cylindrical coordinates to Cartesian transformation
  P0.erase (P0.begin(), P0.end());
  P0.push_back( sqrt(2) ); P0.push_back( M_PI/4 ); P0.push_back( 1 );
  if( verbose )
  {
    std::cout << "-- cylindrical(" << P0[0] << "," << P0[1] << "," << P0[2] << ") = cartesian";
  }
  iiflucks::cylindricalCartesian( P0, P1 );
  std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test spherical coordinates to Cartesian transformation
  P0.erase (P0.begin(), P0.end());
  P0.push_back( sqrt(3) ); P0.push_back( acos( 1 / sqrt( 3 ) ) ); P0.push_back( M_PI / 4 );
  if( verbose )
  {
    std::cout << "-- spherical(" << P0[0] << "," << P0[1] << "," << P0[2] << ") = cartesian";
  }
  iiflucks::sphericalCartesian( P0, P1 );
  std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test Cartesian coordinates to cylindrical transformation
  P0.erase (P0.begin(), P0.end());
  P0.push_back( 1 ); P0.push_back( 1 ); P0.push_back( 1 );
  if( verbose )
  {
    std::cout << "-- cartesian(" << P0[0] << "," << P0[1] << "," << P0[2] << ") = cylindrical";
  }
  iiflucks::cartesianCylindrical( P0, P1 );
  std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test Cartesian coordinates to spherical transformation
  P0.erase (P0.begin(), P0.end());
  P0.push_back( 1 ); P0.push_back( 1 ); P0.push_back( 1 );
  if( verbose )
  {
    std::cout << "-- cartesian(" << P0[0] << "," << P0[1] << "," << P0[2] << ") = spherical";
  }
  iiflucks::cartesianSpherical( P0, P1 );
  std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ")";

  std::cout << std::endl;

}

}

