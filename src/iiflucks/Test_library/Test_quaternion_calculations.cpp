#include "Test_quaternion_calculations.h"

namespace iiflucks {

/// Conjugate quaternion, calculate Hamilton product of two quartenions,
/// and rotate vector around axis by given angle.
void testQuaternion(const bool verbose)
{

  std::vector<double> q, q2, qc, qh; // test quartenions
  std::vector<double> a, v; // axis of rotation and vector to rotate
  double theta = 0; // rotation angle around axis a

  // conjugate quaternion
  q.push_back( 1 );
  q.push_back( 2 );
  q.push_back( 3 );
  q.push_back( 4 );

  if( verbose ) std::cout << "-- q = {" << q[0] << "," << q[1] << "," << q[2] << "," << q[3] << "} conjugate ";

  iiflucks::ConjugateQuaternion(q, qc);

  std::cout << "{" << qc[0] << "," << qc[1] << "," << qc[2] << "," << qc[3] << "}";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // Hamilton product q and q2
  q2.push_back( 5 );
  q2.push_back( 6 );
  q2.push_back( 7 );
  q2.push_back( 8 );

  if( verbose ) std::cout << "-- q2 = {" << q[0] << "," << q[1] << "," << q[2] << "," << q[3] << "} Hamilton product q q2 = ";

  iiflucks::HamiltonProduct(q, q2, qh);

  std::cout << "{" << qh[0] << "," << qh[1] << "," << qh[2] << "," << qh[3] << "}";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // rotate r around axis a by angle theta
  v.push_back( 1 );
  v.push_back( 1 );
  v.push_back( 1 );

  a.push_back( 1/sqrt(2) );
  a.push_back( 1/sqrt(2) );
  a.push_back( 0 );

  theta = M_PI;

  if( verbose ) std::cout << "-- rotate v = (" << v[0] << "," << v[1] << "," << v[2] << ") around axis (" << a[0] << "," << a[1] << "," << a[2] << ") by angle " << theta << " =";

  iiflucks::QuaternionRotation(a, theta, v);

  std::cout << "(" << v[0] << "," << v[1] << "," << v[2] << ")";

  std::cout << std::endl;

}

}

