#include "Test_files.h"

namespace iiflucks {

/// Test creating Helix object from yaml file.
void testYamlHelix(const bool verbose)
{
  if( verbose ) std::cout << "-- test creating Helix object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate: {dt: 0.001, t1: 0, t2: 62.83185307179586477, dr_m: 0.0, r1_m: 0.0, r2_m: 0.0}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], helix: {radius_m: 0.005, length_m: 0.01, turns: 10.0} } }");

  double dt, t1, t2, I;
  double dr, r1, r2;
  Helix * helix = parseHelix(coil, dt, t1, t2, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( helix != nullptr )
  {
    std::cout << "dt = " << dt << std::endl;
    std::cout << "t1 = " << t1 << std::endl;
    std::cout << "t2 = " << t2 << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << helix->GetName() << std::endl;
    std::cout << "A = " << helix->GetA() << " m^2" << std::endl;
    std::cout << "J = " << helix->GetJ() << " A/m^2" << std::endl;
    std::cout << "theta = " << helix->GetTheta() << " rad" << std::endl;
    axis = helix->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = helix->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
    std::cout << "R = " << helix->GetR() << " m" << std::endl;
    std::cout << "L = " << helix->GetL() << " m" << std::endl;
    std::cout << "N = " << helix->GetN() << " turns" << std::endl;
  }
  else
  {
    std::cout << "-- no helix object" << std::endl;
  }

}

/// Test creating Ring object from yaml file.
void testYamlRing(const bool verbose)
{
  if( verbose ) std::cout << "-- test creating Ring object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate: {dt: 0.001, t1: 0, t2: 6.283185307179586477, dr_m: 0.0, r1_m: 0.0, r2_m: 0.0}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], ring: {radius_m: 0.005} } }");

  double dt, t1, t2, I;
  double dr, r1, r2;
  Ring * ring = parseRing(coil, dt, t1, t2, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( ring != nullptr )
  {
    std::cout << "dt = " << dt << std::endl;
    std::cout << "t1 = " << t1 << std::endl;
    std::cout << "t2 = " << t2 << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << ring->GetName() << std::endl;
    std::cout << "A = " << ring->GetA() << " m^2" << std::endl;
    std::cout << "J = " << ring->GetJ() << " A/m^2" << std::endl;
    std::cout << "theta = " << ring->GetTheta() << " rad" << std::endl;
    axis = ring->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = ring->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
    std::cout << "R = " << ring->GetR() << " m" << std::endl;
  }
  else
  {
    std::cout << "-- no ring object" << std::endl;
  }
}

/// Test creating RingCylinder object from yaml file.
void testYamlRingCylinder(const bool verbose)
{
  if( verbose ) std::cout << "-- test creating RingCylinder object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate: {dt: 0.001, t1: 0, t2: 6.283185307179586477, dr_m: 0.0, r1_m: 0.0, r2_m: 0.0}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], ringcylinder: {radius_m: 0.005, cylinder_radius_m: 0.04} } }");

  double dt, t1, t2, I;
  double dr, r1, r2;
  RingCylinder * ringcyl = parseRingCylinder(coil, dt, t1, t2, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( ringcyl != nullptr )
  {
    std::cout << "dt = " << dt << std::endl;
    std::cout << "t1 = " << t1 << std::endl;
    std::cout << "t2 = " << t2 << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << ringcyl->GetName() << std::endl;
    std::cout << "A = " << ringcyl->GetA() << " m^2" << std::endl;
    std::cout << "J = " << ringcyl->GetJ() << " A/m^2" << std::endl;
    std::cout << "theta = " << ringcyl->GetTheta() << " rad" << std::endl;
    axis = ringcyl->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = ringcyl->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
    std::cout << "R = " << ringcyl->GetR() << " m" << std::endl;
    std::cout << "R2 = " << ringcyl->GetR2() << " m" << std::endl;
  }
  else
  {
    std::cout << "-- no ringcyl object" << std::endl;
  }
}

/// Test creating Line object from yaml file.
void testYamlLine(const bool verbose)
{
  if( verbose ) std::cout << "-- test creating Line object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate: {dt: 0.001, t1: 0, t2: 6.283185307179586477, dr_m: 0.0, r1_m: 0.0, r2_m: 0.0}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], line: } }");

  double dt, t1, t2, I;
  double dr, r1, r2;
  Line * line = parseLine(coil, dt, t1, t2, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( line != nullptr )
  {
    std::cout << "dt = " << dt << std::endl;
    std::cout << "t1 = " << t1 << std::endl;
    std::cout << "t2 = " << t2 << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << line->GetName() << std::endl;
    std::cout << "A = " << line->GetA() << " m^2" << std::endl;
    std::cout << "J = " << line->GetJ() << " A/m^2" << std::endl;
    std::cout << "theta = " << line->GetTheta() << " rad" << std::endl;
    axis = line->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = line->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
  }
  else
  {
    std::cout << "-- no line object" << std::endl;
  }

}

/// Test creating LineSphere object from yaml file.
void testYamlLineSphere(const bool verbose)
{
  if( verbose ) std::cout << "-- test creating LineSphere object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate: {dt: 1e-5, t1: -0.785398163397, t2: 0.785398163397}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], linesphere: {radius_m: 0.01, sphere_radius_m: 0.02} } }");

  double dt, t1, t2, I;
  double dr, r1, r2;
  LineSphere * lsphere = parseLineSphere(coil, dt, t1, t2, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( lsphere != nullptr )
  {
    std::cout << "dt = " << dt << std::endl;
    std::cout << "t1 = " << t1 << std::endl;
    std::cout << "t2 = " << t2 << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << lsphere->GetName() << std::endl;
    std::cout << "A = " << lsphere->GetA() << " m^2" << std::endl;
    std::cout << "J = " << lsphere->GetJ() << " A/m^2" << std::endl;
    std::cout << "theta = " << lsphere->GetTheta() << " rad" << std::endl;
    axis = lsphere->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = lsphere->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
    std::cout << "R1 = " << lsphere->GetR1() << " m" << std::endl;
    std::cout << "R2 = " << lsphere->GetR2() << " m" << std::endl;
  }
  else
  {
    std::cout << "-- no line on sphere object" << std::endl;
  }
}

/// Test creating LoopSphere object from yaml file.
void testYamlLoopSphere(const bool verbose)
{
  if( verbose ) std::cout << "test creating LoopSphere object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate: {dt: 1e-5, t1: -1.0, t2: +1.0}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], loopsphere: {sphere_radius_m: 0.02, altitude_rad: 0.785398, azimuth_rad: 0.392699, altitude_length_rad: 0.392699, azimuth_length_rad: 0.785398} } }");

  double dt, t1, t2, I;
  double dr, r1, r2;
  LoopSphere * loopsphere = parseLoopSphere(coil, dt, t1, t2, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( loopsphere != nullptr )
  {
    std::cout << "dt = " << dt << std::endl;
    std::cout << "t1 = " << t1 << std::endl;
    std::cout << "t2 = " << t2 << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << loopsphere->GetName() << std::endl;
    std::cout << "A = " << loopsphere->GetA() << " m^2" << std::endl;
    std::cout << "J = " << loopsphere->GetJ() << " A/m^2" << std::endl;
    std::cout << "rotation = " << loopsphere->GetTheta() << " rad" << std::endl;
    axis = loopsphere->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = loopsphere->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
    std::cout << "R2 = " << loopsphere->GetR2() << " m" << std::endl;
    std::cout << "theta1 = " << loopsphere->Get_theta1() << " rad" << std::endl;
    std::cout << "phi1 = " << loopsphere->Get_phi1() << " rad" << std::endl;
    std::cout << "dtheta = " << loopsphere->Get_dtheta() << " rad" << std::endl;
    std::cout << "dphi = " << loopsphere->Get_dphi() << " rad" << std::endl;
  }
  else
  {
    std::cout << "-- no loop on sphere object" << std::endl;
  }
}

/// Test creating RacetrackCylinder object from yaml file.
void testYamlRacetrackCylinder(const bool verbose)
{
  if( verbose ) std::cout << "-- test creating RacetrackCylinder object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate_sections: {lc: 1e-5, t1: [0.0, 0.0, 0.0, 0.0], t2: [0.36, 3.14159265358979, 0.36, 3.14159265358979], dr_m: 0.0, r1_m: 0.0, r2_m: 0.0 }, coil: {refdes: L1, wire_current_A: 150, wire_diameter_m: 5e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], racetrack_cylinder: {radius_m: 0.0113, cylinder_radius_m: 0.059, length_m: 0.36, radial_width_m: 0.012, thickness_m: 0.018, layer_turns: 19, layers: 18} } }");

  std::vector<double> t1v, t2v;
  double dr, r1, r2, lc;
  double I;
  RacetrackCylinder * rtcyl = parseRacetrackCylinder(coil, lc, t1v, t2v, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( rtcyl != nullptr )
  {
    std::cout << "lc = " << lc << std::endl;
    for(std::size_t i = 0; i < t1v.size(); i++)
      std::cout << "t1[" << i << "] = " << t1v[ i ] << std::endl;
    for(std::size_t i = 0; i < t2v.size(); i++)
      std::cout << "t2[" << i << "] = " << t2v[ i ] << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << rtcyl->GetName() << std::endl;
    std::cout << "A = " << rtcyl->GetA() << " m^2" << std::endl;
    std::cout << "J = " << rtcyl->GetJ() << " A/m^2" << std::endl;
    std::cout << "theta = " << rtcyl->GetTheta() << " rad" << std::endl;
    axis = rtcyl->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = rtcyl->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
    std::cout << "R = " << rtcyl->GetR() << " m" << std::endl;
    std::cout << "R2 = " << rtcyl->GetR2() << " m" << std::endl;
    std::cout << "h = " << rtcyl->Geth() << " m" << std::endl;
    std::cout << "w = " << rtcyl->Getw() << " m" << std::endl;
    std::cout << "th = " << rtcyl->Getth() << " m" << std::endl;
    std::cout << "turns on layer = " << rtcyl->GetTurns() << std::endl;
    std::cout << "layers = " << rtcyl->GetLayers() << std::endl;
  }
  else
  {
    std::cout << "-- no racetrack on cylinder object" << std::endl;
  }
}

/// Test creating RacetrackPlanar object from yaml file.
void testYamlRacetrackPlanar(const bool verbose)
{
  if( verbose ) std::cout << "-- test creating RacetrackPlanar object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate_sections: {lc: 1e-5, t1: [0.0,  0.0, 0.0, 0.0, 0.0,  0.0, 0.0,  0.0], t2: [0.3, 1.57079632679, 0.0, 1.57079632679, 0.3, 1.57079632679, 0.0, 1.57079632679], dr_m: 0.0, r1_m: 0.0, r2_m: 0.0 }, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], racetrack_planar: {radius_m: 0.005, length_m: 0.3, end_length_m: 0.0, winding_width_m: 0.01, thickness_m: 0.018, layer_turns: 20, layers: 20} } }");

  std::vector<double> t1v, t2v;
  double dr, r1, r2, lc;
  double I;
  RacetrackPlanar * rtplanar = parseRacetrackPlanar(coil, lc, t1v, t2v, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( rtplanar != nullptr )
  {
    std::cout << "lc = " << lc << std::endl;
    for(std::size_t i = 0; i < t1v.size(); i++)
      std::cout << "t1[" << i << "] = " << t1v[ i ] << std::endl;
    for(std::size_t i = 0; i < t2v.size(); i++)
      std::cout << "t2[" << i << "] = " << t2v[ i ] << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << rtplanar->GetName() << std::endl;
    std::cout << "A = " << rtplanar->GetA() << " m^2" << std::endl;
    std::cout << "J = " << rtplanar->GetJ() << " A/m^2" << std::endl;
    std::cout << "theta = " << rtplanar->GetTheta() << " rad" << std::endl;
    axis = rtplanar->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = rtplanar->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
    std::cout << "R = " << rtplanar->GetR() << " m" << std::endl;
    std::cout << "h = " << rtplanar->Geth() << " m" << std::endl;
    std::cout << "e = " << rtplanar->Gete() << " m" << std::endl;
    std::cout << "w = " << rtplanar->Getw() << " m" << std::endl;
    std::cout << "th = " << rtplanar->Getth() << " m" << std::endl;
    std::cout << "turns on layer = " << rtplanar->GetTurns() << std::endl;
    std::cout << "layers = " << rtplanar->GetLayers() << std::endl;
  }
  else
  {
    std::cout << "-- no planar racetrack object" << std::endl;
  }

}

/// Test creating Solenoid object from yaml file.
void testYamlSolenoid(const bool verbose)
{
  if( verbose ) std::cout << "-- test creating Solenoid object from yaml file." << std::endl;

  YAML::Node coil = YAML::Load("{integrate_sections: {lc: 1e-5, t1: [0.0], t2: [31.4159265358979], dr_m: 0.0, r1_m: 0.0, r2_m: 0.0}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], solenoid: {radius_m: 0.005, length_m: 0.01, thickness_m: 0.001, layer_turns: 10, layers: 1} } }");

  std::vector<double> t1v, t2v;
  double dr, r1, r2, lc;
  double I;
  Solenoid * solenoid = parseSolenoid(coil, lc, t1v, t2v, dr, r1, r2, I, verbose);

  std::vector<double> axis, tr;
  if( solenoid != nullptr )
  {
    std::cout << "lc = " << lc << std::endl;
    for(std::size_t i = 0; i < t1v.size(); i++)
      std::cout << "t1[" << i << "] = " << t1v[ i ] << std::endl;
    for(std::size_t i = 0; i < t2v.size(); i++)
      std::cout << "t2[" << i << "] = " << t2v[ i ] << std::endl;
    std::cout << "dr = " << dr << " m" << std::endl;
    std::cout << "r1 = " << r1 << " m" << std::endl;
    std::cout << "r2 = " << r2 << " m" << std::endl;
    std::cout << "I = " << I << " A" << std::endl;
    std::cout << "Reference designator = " << solenoid->GetName() << std::endl;
    std::cout << "A = " << solenoid->GetA() << " m^2" << std::endl;
    std::cout << "J = " << solenoid->GetJ() << " A/m^2" << std::endl;
    std::cout << "theta = " << solenoid->GetTheta() << " rad" << std::endl;
    axis = solenoid->GetRotationAxis();
    std::cout << "rotation axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
    tr = solenoid->GetTranslation();
    std::cout << "translation = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
    std::cout << "R = " << solenoid->GetR() << " m" << std::endl;
    std::cout << "L = " << solenoid->GetL() << " m" << std::endl;
    std::cout << "th = " << solenoid->Getth() << " m" << std::endl;
    std::cout << "turns on layer = " << solenoid->GetTurns() << std::endl;
    std::cout << "layers = " << solenoid->GetLayers() << std::endl;
  }
  else
  {
    std::cout << "-- no planar solenoid object" << std::endl;

  }
}

/// Test histogram ranges from yaml node.
void testYamlHistogramRanges(const bool verbose)
{
  if( verbose ) std::cout << "-- test parsing histogram ranges from yaml file." << std::endl;

  YAML::Node hist_ranges = YAML::Load("{nbin: 100, nbin_x: 200, nbin_xy: 400, x_range: [-1.0, 1.0], y_range: [-1.0, 1.0], z_range: [-1.0, 1.0], r12_range: [0.0, 1.0], vol1_range: [0.0, 1.0], vol2_range: [0.0, 1.0], prob1_range: [0.0, 1.0], prob2_range: [0.0, 1.0], J1_range: [-1.0, 1.0], J2_range: [-1.0, 1.0], Jrnd_range: [-1.0, 1.0], Fx_range: [-1.0, 1.0], Fy_range: [-1.0, 1.0], Fz_range: [-1.0, 1.0], F_range: [-1.0, 1.0], Bx_range: [-1.0, 1.0], By_range: [-1.0, 1.0], Bz_range: [-1.0, 1.0], B_range: [-1.0, 1.0], nbin_log: 15, M12_range: [-1.0e-1, -1.0e-2, -1.0e-3, -1.0e-4, -1.0e-5, -1.0e-6, -1e-7, -1e-8, +1e-8, +1e-7, +1e-6, +1e-5, +1e-4, +1e-3, +1e-2, +1e-1]}");

  Histograms * hist = new Histograms("Default ranges");

  hist->ParseRangesYaml( hist_ranges );
//  hist->SetRanges( false );
  std::cout << hist->GetName() << std::endl;
  hist->SaveRangesYaml( "", true );

}

}
