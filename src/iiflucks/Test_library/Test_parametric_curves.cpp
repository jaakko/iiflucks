#include "Test_parametric_curves.h"

namespace iiflucks {

/// Calculate position and tangential vectors on curves with element parametric
/// length ds and swept length t=[t1, t2]. The parametric curves are helix and 
/// ring.  
void testParametricCurves(const int test, const bool verbose)
{

  std::vector<double> P, J; // position and current density vectors 
  double length, ds;
  double t, t1, t2;
  double R, L, N;

  t = 0.003;
  t1 = 0.005;
  t2 = 0.001;

// test straight line parametric curve
  Line *line = new Line("line", 0.1, 1, 0, {0, 0, 1}, {0, 0, 0});
  if( test == 1 )
  {

    if( verbose ) std::cout << "-- straight line t = " << t << ", t=[" << t1 << "," << t2 << "] ";
  //  length = iiflucks::straightline(t, t1, t2, P, J, ds);

    P = line->XYZ( t );
    J = line->Jv( t );
    length = line->Length( t1, t2 );
    ds = line->ds( 0 );

    std::cout << "(" << P[0] << "," << P[1] << "," << P[2] << ")";
    std::cout << " (" << J[0] << "," << J[1] << "," << J[2] << ")";
    std::cout << " " << length << " " << ds << std::endl;
  }

  delete( line );

  // test ring parametric curve

  if( test == 2 )
  {
    R = 0.01;
    t = 0.9 * M_PI;
    t1 = 1.1 * M_PI;
    t2 = 0.8 * M_PI;

    if( verbose ) std::cout << "-- ring R = " << R << ", t = " << t << ", t=[" << t1 << "," << t2 << "] ";

    length = iiflucks::ring(t, t1, t2, R, P, J, ds);

    std::cout << "(" << P[0] << "," << P[1] << "," << P[2] << ")";
    std::cout << " (" << J[0] << "," << J[1] << "," << J[2] << ")";
    std::cout << " " << length << " " << ds << std::endl;
  }

// test helix parametric curve
  R = 0.01;
  L = 0.01;
  N = 4;

  Helix *coil = new Helix("helix", R, L, N, 0.1, 1, 0, {0, 0, 1}, {0, 0, 0});

  if( test == 3 )
  {
    t = 2.5 * M_PI;
    t1 = 3.0 * M_PI;
    t2 = 2.0 * M_PI;

    P = coil->XYZ( t );
    J = coil->Jv( t );
    std::cout << "(" << P[0] << "," << P[1] << "," << P[2] << ")";
    std::cout << " (" << J[0] << "," << J[1] << "," << J[2] << ")";
    std::cout << " " << coil->Length( t1, t2 );
    std::cout << " " << coil->ds( 0 );
    std::cout << std::endl;

    if( verbose ) std::cout << "-- helix R = " << R << ", L = " << L << ", N = " << N << ", t = " << t << ", t=[" << t1 << "," << t2 << "] ";

    length = iiflucks::helix(t, t1, t2, R, L, N, P, J, ds);

    std::cout << "(" << P[0] << "," << P[1] << "," << P[2] << ")";
    std::cout << " (" << J[0] << "," << J[1] << "," << J[2] << ")";
    std::cout << " " << length << " " << ds << std::endl;

  }

  delete( coil );

// test ring on cylinder parametric curve
  R =  0.01;
  double R2 = 0.04;

  RingCylinder *ringcyl = new RingCylinder("ring_on_cylinder", R, R2, 0.1, 1, 0, {0, 0, 1}, {0, 0, 0});

  if( test == 4 )
  {
    t = 0.9 * M_PI;
    t1 = 1.1 * M_PI;
    t2 = 0.8 * M_PI;

    if( verbose ) std::cout << "-- ring R = " << R << " on cylinder R2 = " << R2 << ", t = " << t << ", t=[" << t1 << "," << t2 << "] ";

    P = ringcyl->XYZ( t );
    J = ringcyl->Jv( t );
    length = ringcyl->Length( t1, t2 );
    ds = ringcyl->ds( t );

    std::cout << "(" << P[0] << "," << P[1] << "," << P[2] << ")";
    std::cout << " (" << J[0] << "," << J[1] << "," << J[2] << ")";
    std::cout << " " << length << " " << ds << std::endl;
  }

  delete( ringcyl );

// test line on sphere parametric curve
  double R1 = 0.01;
  R2 = 0.02;

  LineSphere *lsphere = new LineSphere("line_on_sphere", R1, R2, 0.1, 1, 0, {0, 0, 1}, {0, 0, 0});

  if( test == 5 )
  {
    t = M_PI/4;
    t1 = -M_PI/4;
    t2 = M_PI/4;

    if( verbose ) std::cout << "-- line at R1 = y = " << R1 << " on sphere R2 = " << R2 << ", t = " << t << ", t=[" << t1 << "," << t2 << "] ";

    P = lsphere->XYZ( t );
    J = lsphere->Jv( t );
    length = lsphere->Length( t1, t2 );
    ds = lsphere->ds( t );

    std::cout << "(" << P[0] << "," << P[1] << "," << P[2] << ")";
    std::cout << " (" << J[0] << "," << J[1] << "," << J[2] << ")";
    std::cout << " " << length << " " << ds << std::endl;
  }

  delete( lsphere );

// test loop on sphere parametric curve
  R2 = 0.02;
  double theta1 = M_PI/4;
  double dtheta = M_PI/8;
  double phi1 = M_PI/8;
  double dphi = M_PI/4;

  LoopSphere *loopsphere = new LoopSphere("loop_on_sphere", R2, theta1, phi1, dtheta, dphi, 0.1, 1, 0, {0, 0, 1}, {0, 0, 0});

  if( test == 6 )
  {
    t = 0;
    t1 = -1;
    t2 = 1;

    if( verbose )
    {
      std::cout << "R2, " << R2 << std::endl;
      std::cout << "theta1," << theta1 << std::endl;
      std::cout << "phi1," << phi1 << std::endl;
      std::cout << "dtheta," << dtheta << std::endl;
      std::cout << "dphi," << dphi << std::endl;
      std::cout << "t," << t << std::endl;
      std::cout << "t1," << t1 << std::endl;
      std::cout << "t2," << t2 << std::endl;
    }

    P = loopsphere->XYZ( t );
    J = loopsphere->Jv( t );
    length = loopsphere->Length( t1, t2 );
    ds = loopsphere->ds( t );

    std::cout << P[0] << "," << P[1] << "," << P[2] << std::endl;
    std::cout << J[0] << "," << J[1] << "," << J[2] << std::endl;
    std::cout << length << "," << ds << std::endl;
  }

  delete( loopsphere );

}

}

