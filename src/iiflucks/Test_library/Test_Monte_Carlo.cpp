#include "Test_Monte_Carlo.h"

namespace iiflucks {

/// Calculate inductance for parametric curves. 
void testMonteCarlo(const int test, const bool verbose)
{

  // helix parametric curve
  double R = 0.005;
  double L = 0.01;
  double N = 10;
  double A = M_PI * 5e-5 * 5e-5;
  double J = 1e-3 / A;

  Helix *coil = new Helix("helix", R, L, N, A, J, 0, {0, 0, 1}, {0, 0, 0});

  YAML::Node hist_ranges = YAML::Load("{nbin: 100, nbin_x: 200, nbin_xy: 400, x_range: [-1.0, 1.0], y_range: [-1.0, 1.0], z_range: [-1.0, 1.0], r12_range: [0.0, 1.0], vol1_range: [0.0, 1.0], vol2_range: [0.0, 1.0], prob1_range: [0.0, 1.0], prob2_range: [0.0, 1.0], J1_range: [-1.0, 1.0], J2_range: [-1.0, 1.0], Jrnd_range: [-1.0, 1.0], Fx_range: [-1.0, 1.0], Fy_range: [-1.0, 1.0], Fz_range: [-1.0, 1.0], F_range: [-1.0, 1.0], Bx_range: [-1.0, 1.0], By_range: [-1.0, 1.0], Bz_range: [-1.0, 1.0], B_range: [-1.0, 1.0], nbin_log: 15, M12_range: [-1.0e-1, -1.0e-2, -1.0e-3, -1.0e-4, -1.0e-5, -1.0e-6, -1e-7, -1e-8, +1e-8, +1e-7, +1e-6, +1e-5, +1e-4, +1e-3, +1e-2, +1e-1]}");

  Histograms *histograms = new Histograms("histograms");
  histograms->ParseRangesYaml( hist_ranges );
  MonteCarlo *calc = new MonteCarlo("Inductance", 6, 1e-3, 1e-3, histograms);

  double t1 = 0;
  double t2 = 2.0 * M_PI * N;
  double dt = ( t2 - t1 ) / 1000;
  double dr, r1, r2;
  int nzero = 0;

  if( test == 1 )
  {
//  calc->Debug();
    nzero = calc->Run<Helix, Helix>(coil, coil, dt, t1, t2, dt, t1, t2);

    if( verbose )
    {
      std::cout << "-- Helix self-inductance\n";
      std::cout << "-- zero distance " << nzero << " times" << std::endl;
      std::cout << "-- V1 = " << calc->V1 << " m^3, V2 = " << calc->V2 << " m^3" << std::endl;
      std::cout << "-- E12 " << calc->E12  << " +- " << calc->E12err << " J" << std::endl;
      std::cout << "-- Flux12 " << calc->Flux12 << " +- " << calc->Flux12err << " Wb" << std::endl;
      std::cout << "-- M12 = " << calc->M12 << " +- " << calc->M12err << std::endl;
      std::cout << "-- F21 (" << calc->F12[ 0 ] << " N, " << calc->F12[ 1 ] << " N, " << calc->F12[ 2 ] << " N)"	<< std::endl;
      std::cout << "-- +-(" << calc->F12err[ 0 ] << " N, " << calc->F12err[ 1 ] << " N, " << calc->F12err[ 2 ] << " N)"	<< std::endl;
    }
    else
    {
      std::cout << calc->V1 << " " << calc->V2 << " ";
      std::cout << calc->M12 << " " << calc->M12err << " ";
      std::cout << calc->F12[ 0 ] << " " << calc->F12err[ 0 ] << " ";
      std::cout << calc->F12[ 1 ] << " " << calc->F12err[ 1 ] << " ";
      std::cout << calc->F12[ 2 ] << " " << calc->F12err[ 2 ];
      std::cout << std::endl;
    }
    histograms->SaveHistograms("helix");
  }


  MonteCarlo *calc2 = new MonteCarlo("Mutual inductance", 6, 1e-3, 1e-3, histograms);

  if( test == 2 )
  {
    calc2->TestCalc();
    nzero = calc2->Run("", "", 0, {0, 0, 1}, {0, 0, 0});

    if( verbose )
    {
      std::cout << "-- Rings mutual inductance\n";
      std::cout << "-- zero distance " << nzero << " times" << std::endl;
      std::cout << "-- V1 = " << calc2->V1 << " m^3, V2 = " << calc2->V2 << " m^3" << std::endl;
      std::cout << "-- E12 " << calc2->E12  << " +- " << calc2->E12err << " J" << std::endl;
      std::cout << "-- Flux12 " << calc2->Flux12 << " +- " << calc2->Flux12err << " Wb" << std::endl;
      std::cout << "-- M12 = " << calc2->M12 << " +- " << calc2->M12err << std::endl;
      std::cout << "-- F21 (" << calc2->F12[ 0 ] << " N, " << calc2->F12[ 1 ] << " N, " << calc2->F12[ 2 ] << " N)"	<< std::endl;
      std::cout << "-- +-(" << calc2->F12err[ 0 ] << " N, " << calc2->F12err[ 1 ] << " N, " << calc2->F12err[ 2 ] << " N)"	<< std::endl;
    }
    else
    {
      std::cout << calc2->V1 << " " << calc2->V2 << " ";
      std::cout << calc2->M12 << " " << calc2->M12err << " ";
      std::cout << calc2->F12[ 0 ] << " " << calc2->F12err[ 0 ] << " ";
      std::cout << calc2->F12[ 1 ] << " " << calc2->F12err[ 1 ] << " ";
      std::cout << calc2->F12[ 2 ] << " " << calc2->F12err[ 2 ];
      std::cout << std::endl;
    }

    histograms->SaveHistograms("rings");
  }


  MonteCarlo *calc3 = new MonteCarlo("Mutual inductance", 6, 1e-3, 1e-3, histograms);

  if( test == 3 )
  {
    calc3->TestCalc();
    nzero = calc3->Run_pdv("", coil, dt, t1, t2, 0, {0, 0, 1}, {0, 0, 0});

    if( verbose )
    {
      std::cout << "-- Ring-helix mutual inductance\n";
      std::cout << "-- zero distance " << nzero << " times" << std::endl;
      std::cout << "-- V1 = " << calc3->V1 << " m^3, V2 = " << calc3->V2 << " m^3" << std::endl;
      std::cout << "-- E12 " << calc3->E12  << " +- " << calc3->E12err << " J" << std::endl;
      std::cout << "-- Flux12 " << calc3->Flux12 << " +- " << calc3->Flux12err << " Wb" << std::endl;
      std::cout << "-- M12 = " << calc3->M12 << " +- " << calc3->M12err << std::endl;
      std::cout << "-- F21 (" << calc3->F12[ 0 ] << " N, " << calc3->F12[ 1 ] << " N, " << calc3->F12[ 2 ] << " N)"	<< std::endl;
      std::cout << "-- +-(" << calc3->F12err[ 0 ] << " N, " << calc3->F12err[ 1 ] << " N, " << calc3->F12err[ 2 ] << " N)"	<< std::endl;
    }
    else
    {
      std::cout << calc3->V1 << " " << calc3->V2 << " ";
      std::cout << calc3->M12 << " " << calc3->M12err << " ";
      std::cout << calc3->F12[ 0 ] << " " << calc3->F12err[ 0 ] << " ";
      std::cout << calc3->F12[ 1 ] << " " << calc3->F12err[ 1 ] << " ";
      std::cout << calc3->F12[ 2 ] << " " << calc3->F12err[ 2 ];
      std::cout << std::endl;
    }

    histograms->SaveHistograms("ring_helix");
  }

  // two parallel 10 cm long wire 1 cm apart from each other 
  Line *wire1 = new Line("line", A, J, 0, {0, 0, 1}, {0, 0, 0});
  Line *wire2 = new Line("line", A, J, 0, {0, 0, 1}, {0, 0.01, 0});

  MonteCarlo *calc4 = new MonteCarlo("Parallel wires", 6, 1e-3, 1e-3, histograms);

  if( test == 4 )
  {
    t1 = 0;
    t2 = 0.1;
    dt = ( t2 - t1 ) / 1000;
//  calc->Debug();
    nzero = calc4->Run<Line, Line>(wire1, wire2, dt, t1, t2, dt, t1, t2);

    if( verbose )
    {
      std::cout << "-- Two parallel 10 cm long wires 1 cm apart with 1 mA current\n";
      std::cout << "-- zero distance " << nzero << " times" << std::endl;
      std::cout << "-- V1 = " << calc4->V1 << " m^3, V2 = " << calc4->V2 << " m^3" << std::endl;
      std::cout << "-- E12 " << calc4->E12  << " +- " << calc4->E12err << " J" << std::endl;
      std::cout << "-- Flux12 " << calc4->Flux12 << " +- " << calc4->Flux12err << " Wb" << std::endl;
      std::cout << "-- M12 = " << calc4->M12 << " +- " << calc4->M12err << std::endl;
      std::cout << "-- F21 (" << calc4->F12[ 0 ] << " N, " << calc4->F12[ 1 ] << " N, " << calc4->F12[ 2 ] << " N)"	<< std::endl;
      std::cout << "-- +-(" << calc4->F12err[ 0 ] << " N, " << calc4->F12err[ 1 ] << " N, " << calc4->F12err[ 2 ] << " N)"	<< std::endl;
    }
    else
    {
      std::cout << calc4->V1 << " " << calc4->V2 << " ";
      std::cout << calc4->M12 << " " << calc4->M12err << " ";
      std::cout << calc4->F12[ 0 ] << " " << calc4->F12err[ 0 ] << " ";
      std::cout << calc4->F12[ 1 ] << " " << calc4->F12err[ 1 ] << " ";
      std::cout << calc4->F12[ 2 ] << " " << calc4->F12err[ 2 ];
      std::cout << std::endl;
    }
  }

  // ring with radius of 5 mm on cylinder with radius 4 cm
  RingCylinder *ringcyl = new RingCylinder("RingCyl", 0.005, 0.04, A, J, 0, {0, 0, 1}, {0, 0, 0});

  MonteCarlo *calc5 = new MonteCarlo("Ring on cylindrical surface inductance", 6, 1e-3, 1e-3, histograms);

  if( test == 5 )
  {
    t1 = 0;
    t2 = 2.0 * M_PI;
    dt = ( t2 - t1 ) / 1000000;

//  calc->Debug();
    nzero = calc5->Run<RingCylinder, RingCylinder>(ringcyl, ringcyl, dt, t1, t2, dt, t1, t2);

    if( verbose )
    {
      std::cout << "-- 1 cm ring on 4 cm radius surface with 1 mA current\n";
      std::cout << "-- zero distance " << nzero << " times" << std::endl;
      std::cout << "-- V1 = " << calc5->V1 << " m^3, V2 = " << calc5->V2 << " m^3" << std::endl;
      std::cout << "-- E12 " << calc5->E12  << " +- " << calc5->E12err << " J" << std::endl;
      std::cout << "-- Flux12 " << calc5->Flux12 << " +- " << calc5->Flux12err << " Wb" << std::endl;
      std::cout << "-- M12 = " << calc5->M12 << " +- " << calc5->M12err << std::endl;
      std::cout << "-- F21 (" << calc5->F12[ 0 ] << " N, " << calc5->F12[ 1 ] << " N, " << calc5->F12[ 2 ] << " N)"	<< std::endl;
      std::cout << "-- +-(" << calc5->F12err[ 0 ] << " N, " << calc5->F12err[ 1 ] << " N, " << calc5->F12err[ 2 ] << " N)"	<< std::endl;
    }
    else
    {
      std::cout << calc5->V1 << " " << calc5->V2 << " ";
      std::cout << calc5->M12 << " " << calc5->M12err << " ";
      std::cout << calc5->F12[ 0 ] << " " << calc5->F12err[ 0 ] << " ";
      std::cout << calc5->F12[ 1 ] << " " << calc5->F12err[ 1 ] << " ";
      std::cout << calc5->F12[ 2 ] << " " << calc5->F12err[ 2 ];
      std::cout << std::endl;
    }
  }

  // Four parallel 10 cm long wires along z-axis at (0.01, 0.01), 
  // (0.01, -0.01), (-0.01, -0.01) and (-0.01, 0.01) each with current
  // 100 mA with opposite signs for adjacent wires. Wire diameter 1 mm. 
  std::string s1 = "{integrate: {dt: 1e-7, t1: 0, t2: 0.1}, coil: {refdes: L1, wire_current_A: -0.1, wire_diameter_m: 1e-3, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [-0.01,0.01,0.0], line: } }";
  std::string s2 = "{integrate: {dt: 1e-7, t1: 0, t2: 0.1}, coil: {refdes: L1, wire_current_A: 0.1, wire_diameter_m: 1e-3, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.01,0.01,0.0], line: } }";
  std::string s3 = "{integrate: {dt: 1e-7, t1: 0, t2: 0.1}, coil: {refdes: L1, wire_current_A: 0.1, wire_diameter_m: 1e-3, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [-0.01,-0.01,0.0], line: } }";
  std::string s4 = "{integrate: {dt: 1e-7, t1: 0, t2: 0.1}, coil: {refdes: L1, wire_current_A: -0.1, wire_diameter_m: 1e-3, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.01,-0.01,0.0], line: } }";

  std::string s = s1 + " " + s2 + " " + s3 + " " + s4;

  std::vector<YAML::Node> four_wires = YAML::LoadAll( s );

  Line * line1 = nullptr;
  Line * line2 = nullptr;
  double u1, u2, du;
  double I1, I2;

  MonteCarlo *calc7 = new MonteCarlo("Four parallel wires", 6, 0.1, 0.1, nullptr);

  int nzerom[ 4 ][ 4  ]; // nzero matrix
  double V1m[ 4 ][ 4  ]; // V1 matrix
  double V2m[ 4 ][ 4 ]; // V2 matrix
  double E12m[ 4 ][ 4 ]; // E12 matrix
  double E12errm[ 4 ][ 4 ]; // E12 error matrix
  double Flux12m[ 4 ][ 4 ]; // Flux12 matrix
  double Flux12errm[ 4 ][ 4 ]; // Flux12 error matrix
  double M12m[ 4 ][ 4 ]; // M12 matrix
  double M12errm[ 4 ][ 4 ]; // M12 error matrix
  double Fx12m[ 4 ][ 4 ]; // Fx12 matrix
  double Fx12errm[ 4 ][ 4 ]; // Fx12 error matrix
  double Fy12m[ 4 ][ 4 ]; // Fy12 matrix
  double Fy12errm[ 4 ][ 4 ]; // Fy12 error matrix
  double Fz12m[ 4 ][ 4 ]; // Fz12 matrix
  double Fz12errm[ 4 ][ 4 ]; // Fz12 error matrix
  int row = 0, nrow = 0, col = 0;
  bool file1 = false, file2 = false;
  if( test == 7 )
  {
    for(std::vector<YAML::Node>::const_iterator coil1 = four_wires.begin(); coil1 != four_wires.end(); ++coil1)
    {
      if( (*coil1)["coil"]["line"] )
      {
        if( verbose ) std::cout << "-- parse line1" << std::endl;
        line1 = iiflucks::parseLine( *coil1, dt, t1, t2, dr, r1, r2, I1, false );
      }

      calc7->SetI1( I1 );

      for(std::vector<YAML::Node>::const_iterator coil2 = four_wires.begin(); coil2 != four_wires.end(); ++coil2)
      {
        if( (*coil2)["coil"]["line"] )
        {
          if( verbose ) std::cout << "-- parse line2" << std::endl;
          line2 = iiflucks::parseLine( *coil2, du, u1, u2, dr, r1, r2, I2, false );
        }
  
        calc7->SetI2( I2 );

//        std::cout << "-- row = " << row << " col = " << col << std::endl;

        if( line1 && line2 ) nzero = calc7->Run<Line, Line>(line1, line2, dt, t1, t2, du, u1, u2);

        if( verbose )
        {
          std::cout << "-- zero distance " << nzero << " times" << std::endl;
          std::cout << "-- V1 = " << calc7->V1 << " m^3, V2 = " << calc7->V2 << " m^3" << std::endl;
          std::cout << "-- E12 = " << calc7->E12  << " +- " << calc7->E12err << " J" << std::endl;
          std::cout << "-- Flux12 = " << calc7->Flux12 << " +- " << calc7->Flux12err << " Wb" << std::endl;
          std::cout << "-- M12 = " << calc7->M12 << " +- " << calc7->M12err << std::endl;
          std::cout << "-- F12 = (" << calc7->F12[ 0 ] << " N, " << calc7->F12[ 1 ] << " N, " << calc7->F12[ 2 ] << " N)"	<< std::endl;
          std::cout << "-- +-(" << calc7->F12err[ 0 ] << " N, " << calc7->F12err[ 1 ] << " N, " << calc7->F12err[ 2 ] << " N)"	<< std::endl;
        }
//        else
//        {
//          std::cout << calc7->V1 << " " << calc7->V2 << " ";
//          std::cout << calc7->M12 << " " << calc7->M12err << " ";
//          std::cout << calc7->F12[ 0 ] << " " << calc7->F12err[ 0 ] << " ";
//          std::cout << calc7->F12[ 1 ] << " " << calc7->F12err[ 1 ] << " ";
//          std::cout << calc7->F12[ 2 ] << " " << calc7->F12err[ 2 ];
//          std::cout << std::endl;
//        }

        nzerom[ row ][ col ] = calc7->nzero;
        V1m[ row ][ col ] = calc7->V1;
        V2m[ row ][ col ] = calc7->V2;
        E12m[ row ][ col ] = calc7->E12;
        E12errm[ row ][ col ] = calc7->E12err;
        Flux12m[ row ][ col ] = calc7->Flux12;
        Flux12errm[ row ][ col ] = calc7->Flux12err;
        M12m[ row ][ col ] = calc7->M12;
        M12errm[ row ][ col ] = calc7->M12err;
        Fx12m[ row ][ col ] = calc7->F12[ 0 ];
        Fx12errm[ row ][ col ] = calc7->F12err[ 0 ];
        Fy12m[ row ][ col ] = calc7->F12[ 1 ];
        Fy12errm[ row ][ col ] = calc7->F12err[ 1 ];
        Fz12m[ row ][ col ] = calc7->F12[ 2 ];
        Fz12errm[ row ][ col ] = calc7->F12err[ 2 ];

        row++;
      }
      nrow = row;
      row = 0;
      col++;
    }

    if( verbose ) std::cout << "----- zero distance -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << nzerom[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- M12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << M12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fx12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fx12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fy12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fy12m[ i ][ j ];
      std::cout << std::endl;
    }

    if( verbose ) std::cout << "----- Fz12 -----" << std::endl;
    for(int i = 0; i < nrow; i++)
    {
      for(int j = 0; j < col; j++) std::cout << " " << Fz12m[ i ][ j ];
      std::cout << std::endl;
    }
  }

  // racetrack coil on cylinder
  // I = 150 A, d = 0.5 mm, R = 0.0113 m, R2 = 0.059 m, h = 0.36 m, w = 0.012 m
  // th = 0.018 m, N = 19, O = 18
  YAML::Node coil2 = YAML::Load("{integrate_sections: {lc: 1e-5, t1: [-0.18, 0.0, 0.18, 3.14159265358979], t2: [0.18, 3.14159265358979, -0.18, 6.28318530717959]}, coil: {refdes: L1, wire_current_A: 150, wire_diameter_m: 5e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], racetrack_cylinder: {radius_m: 0.0113, cylinder_radius_m: 0.059, length_m: 0.36, radial_width_m: 0.012, thickness_m: 0.018, layer_turns: 19, layers: 18} } }");

  std::vector<double> t1v, t2v;
  double I, lc;
  RacetrackCylinder * rtcyl = parseRacetrackCylinder(coil2, lc, t1v, t2v, dr, r1, r2, I, verbose);

  MonteCarlo *calc8 = new MonteCarlo("RT coil on cylinder", 6, 150, 150, nullptr);

  if( test == 8 )
  {
    calc8->SetI1( I );
    calc8->SetI2( I );

    if( rtcyl ) nzero = calc7->Run<RacetrackCylinder, RacetrackCylinder>(rtcyl, rtcyl, lc, t1v, t2v, lc, t1v, t2v);

    if( verbose )
    {
      std::cout << "-- RT coil with 150 A current\n";
      std::cout << "-- zero distance " << nzero << " times" << std::endl;
      std::cout << "-- V1 = " << calc8->V1 << " m^3, V2 = " << calc8->V2 << " m^3" << std::endl;
      std::cout << "-- E12 " << calc8->E12  << " +- " << calc8->E12err << " J" << std::endl;
      std::cout << "-- Flux12 " << calc8->Flux12 << " +- " << calc8->Flux12err << " Wb" << std::endl;
      std::cout << "-- M12 = " << calc8->M12 << " +- " << calc8->M12err << std::endl;
      std::cout << "-- F21 (" << calc8->F12[ 0 ] << " N, " << calc8->F12[ 1 ] << " N, " << calc8->F12[ 2 ] << " N)"	<< std::endl;
      std::cout << "-- +-(" << calc8->F12err[ 0 ] << " N, " << calc8->F12err[ 1 ] << " N, " << calc8->F12err[ 2 ] << " N)"	<< std::endl;
    }
    else
    {
      std::cout << calc8->V1 << " " << calc8->V2 << " ";
      std::cout << calc8->M12 << " " << calc8->M12err << " ";
      std::cout << calc8->F12[ 0 ] << " " << calc8->F12err[ 0 ] << " ";
      std::cout << calc8->F12[ 1 ] << " " << calc8->F12err[ 1 ] << " ";
      std::cout << calc8->F12[ 2 ] << " " << calc8->F12err[ 2 ];
      std::cout << std::endl;
    }
  }

  YAML::Node coil3 = YAML::Load("{integrate: {dt: 1e-5, t1: -0.785398163397, t2: 0.785398163397}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], linesphere: {radius_m: 0.01, sphere_radius_m: 0.02} } }");

  LineSphere * lsphere = parseLineSphere(coil3, dt, t1, t2, dr, r1, r2, I, verbose);

  MonteCarlo *calc6 = new MonteCarlo("Meridian line on sphere", 4, I, I, nullptr);

  if( test == 6 )
  {
    calc6->SetI1( I );
    calc6->SetI2( I );

    if( lsphere ) nzero = calc6->Run(lsphere, lsphere, dt, t1, t2, dt, t1, t2);

    if( verbose )
    {
      std::cout << "-- meridian line with 1 mA current\n";
      std::cout << "-- zero distance " << nzero << " times" << std::endl;
      std::cout << "-- V1 = " << calc6->V1 << " m^3, V2 = " << calc6->V2 << " m^3" << std::endl;
      std::cout << "-- E12 " << calc6->E12  << " +- " << calc6->E12err << " J" << std::endl;
      std::cout << "-- Flux12 " << calc6->Flux12 << " +- " << calc6->Flux12err << " Wb" << std::endl;
      std::cout << "-- M12 = " << calc6->M12 << " +- " << calc6->M12err << std::endl;
      std::cout << "-- F21 (" << calc6->F12[ 0 ] << " N, " << calc6->F12[ 1 ] << " N, " << calc6->F12[ 2 ] << " N)"	<< std::endl;
      std::cout << "-- +-(" << calc6->F12err[ 0 ] << " N, " << calc6->F12err[ 1 ] << " N, " << calc6->F12err[ 2 ] << " N)"	<< std::endl;
    }
    else
    {
      std::cout << calc6->V1 << " " << calc6->V2 << " ";
      std::cout << calc6->M12 << " " << calc6->M12err << " ";
      std::cout << calc6->F12[ 0 ] << " " << calc6->F12err[ 0 ] << " ";
      std::cout << calc6->F12[ 1 ] << " " << calc6->F12err[ 1 ] << " ";
      std::cout << calc6->F12[ 2 ] << " " << calc6->F12err[ 2 ];
      std::cout << std::endl;
    }
  }

  delete( calc );
  delete( calc2 );
  delete( calc3 );
  delete( calc4 );
  delete( calc5 );
  delete( calc6 );
  delete( calc7 );
  delete( calc8 );
  delete( histograms );
  delete( coil );
  delete( wire1 );
  delete( wire2 );
  delete( ringcyl );
  delete( lsphere );

}

}

