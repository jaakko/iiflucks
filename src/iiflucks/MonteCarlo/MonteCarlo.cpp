#include "MonteCarlo.h"

MonteCarlo::~MonteCarlo() { };

/// This resets accumulated values to zero for inductance calculation.
void MonteCarlo::Init()
{
  // probability to select dv1 and dv2 segments
  dv1 = V1 / np1;
  dv2 = V2 / np2;
  prob1 = dv1 / V1;
  prob2 = dv2 / V2;            
  prob = prob1 * prob2;

//  if( verbose ) std::cout << "-- prob1 = " << prob1 << " prob2 = " << prob2 << " prob = " << prob << std::endl;
      
  M12 = 0;
  Msqsum = 0;
  nzero = 0; 

  iiflucks::zeroVector( F12 );
  iiflucks::zeroVector( Fsqsum );
  nrand = pow(10, Nrnd);
}

/// This resets accumulated values to zero for field calculation.
void MonteCarlo::InitB()
{
  // probability to select dv1 and dv2 segments
  dv1 = V1 / np1;
  prob1 = dv1 / V1;

//  if( verbose ) std::cout << "-- prob1 = " << prob1 << std::endl;

  iiflucks::zeroVector( B );
  iiflucks::zeroVector( Bsqsum );
  nrand = pow(10, Nrnd);
}

void MonteCarlo::ThermalNoise( const int np, const double dv, double Jx[], double Jy[], double Jz[])
{
// random direction vector of unit length
//	gsl_ran_dir_3d(r, &xrnd, &yrnd, &zrnd);
//        Jrms = sqrt( 4 * sigma * GSL_CONST_MKSA_BOLTZMANN * T / dv2[ k ]);
//      	Jrnd = gsl_ran_gaussian(r, Jrms);
//      	J2x[ k ] = Jrnd * xrnd;
//      	J2y[ k ] = Jrnd * yrnd;
//      	J2z[ k ] = Jrnd * zrnd;
//
//	if( verbose ) std::cout << Jrms << ": " << J2x[ k ] << " " << J2y[ k ] << " " << J2z[ k ] << std::endl;
//
//	if( Jrnd > maxJrnd ) maxJrnd = Jrnd;
 //       if( Jrnd < minJrnd ) minJrnd = Jrnd;
  //      gsl_histogram_increment( hJrnd, Jrnd );
   //   }
}

/// Inner loop calculations for inductance Monte Carlo
void MonteCarlo::Innerloop()
{
  J1 = iiflucks::vectorLength( J1v );
  J2 = iiflucks::vectorLength( J2v );

  rij = iiflucks::distancePoints(P1, P2); 

  iiflucks::subtractVectors( P2, P1, P21 );

  dM12 = 0;

  iiflucks::zeroVector( dF12 );

  if( rij > 0 ) 
  {
    dM12 = dv1 * dv2 * iiflucks::dotProduct(J1v, J2v) / ( rij * prob );
    iiflucks::crossProduct( J1v, P21, J1xr );
    iiflucks::crossProduct( J2v, J1xr, dF12 );
    iiflucks::scaleVector( dv1 * dv2 / ( rij * rij * rij * prob ), dF12 );
  }
  else nzero++;

  M12 += dM12;
  Msqsum += dM12 * dM12;

  dF = iiflucks::vectorLength( dF12 );

  iiflucks::addVectors( F12, dF12, F12 ); 
  iiflucks::squareAddVector( Fsqsum, dF12, Fsqsum );

  if( histograms )
  {
    histograms->UpdateXYZ( P1, P2 );
    histograms->UpdateDistance( rij );
    histograms->UpdateProbability( prob1, prob2 );
    histograms->UpdateVolume( dv1, dv2 );
    histograms->UpdateCurrent( J1, J2, J1v, J2v, P1, P2 );
    histograms->UpdateForce( dF, dF12, P1, P2 );
    histograms->UpdateInductance( dM12 );
  } 

}

/// Inner loop calculations for field Monte Carlo
void MonteCarlo::InnerloopB()
{
  J1 = iiflucks::vectorLength( J1v );
  rij = iiflucks::distancePoints( P1, P2 );
  iiflucks::subtractVectors( P2, P1, P21 );

  iiflucks::crossProduct( J1v, P21, dBv );

  if( rij > 0 )
  {
    iiflucks::scaleVector( dv1 / ( rij * rij * rij * prob1 ), dBv);
  }
  else nzero++;

  dB = iiflucks::vectorLength( dBv );

  iiflucks::addVectors( B, dBv, B );
  iiflucks::squareAddVector( Bsqsum, dBv, Bsqsum );

  if( histograms )
  {
    histograms->UpdateXYZ( P1 );
    histograms->UpdateDistance( rij );
    histograms->UpdateProbability( prob1 );
    histograms->UpdateVolume( dv1 );
    histograms->UpdateCurrent( J1, J1v, P1 );
    histograms->UpdateField( dBv, dB );
  }
}

/// Calculate E12, F12, M12 and F12 with maximum errors from accumulated values.
void MonteCarlo::Calculate()
{
  nrand -= nzero; // correct for zero distance samples

//  if( verbose ) std::cout << "-- " << nrand << " random samples" << std::endl;

  var = sqrt( Msqsum/nrand - (M12/nrand)*(M12/nrand) );

  M12 /= nrand;
  M12err = 3 * var / sqrt( (double)nrand );

  iiflucks::zeroVector( Fvar );
  Fvar[ 0 ] = sqrt( Fsqsum[ 0 ] / nrand - ( F12[ 0 ] / nrand ) * ( F12[ 0 ] / nrand ) );
  Fvar[ 1 ] = sqrt( Fsqsum[ 1 ] / nrand - ( F12[ 1 ] / nrand ) * ( F12[ 1 ] / nrand ) );
  Fvar[ 2 ] = sqrt( Fsqsum[ 2 ] / nrand - ( F12[ 2 ] / nrand ) * ( F12[ 2 ] / nrand ) );

  iiflucks::scaleVector( 1 / (double)nrand , F12 );

  F12err = Fvar;
  iiflucks::scaleVector( 3 / sqrt( (double)nrand ), F12err );

  E12 = 0.5 * 1e-7 * sqrt( ur1 * ur2 ) * M12;
  E12err = 0.5 * 1e-7 * sqrt( ur1 * ur2 ) * M12err;
//  if( verbose ) std::cout << "-- E12 " << E12 << " +- " << E12err << " J" << std::endl;

  Flux12 = 1e-7 * sqrt( ur1 * ur2 ) * M12 / I1;
  Flux12err = 1e-7 * sqrt( ur1 * ur2 ) * M12err / I1;
//  if( verbose ) std::cout << "-- Flux12 " << Flux12 << " +- " << Flux12err << " Wb" << std::endl;

  M12 *= 1e-7 * sqrt( ur1 * ur2 ) /( I1 * I2 );

  M12err *= 1e-7 * sqrt( ur1 * ur2 ) /( I1 * I2);

  iiflucks::scaleVector( 1e-7 * sqrt( ur1 * ur2 ), F12 ); 

  iiflucks::scaleVector( 1e-7 * sqrt( ur1 * ur2 ), F12err ); 

//  if( verbose ) std::cout << "-- zero distance " << nzero << " times" << std::endl;
//  if( verbose ) std::cout << "-- M12 = " << M12 << " +- " << M12err << " H " << std::endl;

}

/// Calculate B with maximum errors from accumulated values.
void MonteCarlo::CalculateB()
{
  nrand -= nzero; // correct for zero distance samples

//  if( verbose ) std::cout << "-- " << nrand << " random samples" << std::endl;

  iiflucks::zeroVector( Bvar );
  Bvar[ 0 ] = sqrt( Bsqsum[ 0 ] / nrand - ( B[ 0 ] / nrand ) * ( B[ 0 ] / nrand ) );
  Bvar[ 1 ] = sqrt( Bsqsum[ 1 ] / nrand - ( B[ 1 ] / nrand ) * ( B[ 1 ] / nrand ) );
  Bvar[ 2 ] = sqrt( Bsqsum[ 2 ] / nrand - ( B[ 2 ] / nrand ) * ( B[ 2 ] / nrand ) );

  iiflucks::scaleVector( 1 / (double)nrand , B );
  Berr = Bvar;
  iiflucks::scaleVector( 3 / sqrt( (double)nrand ), Berr );
  iiflucks::scaleVector( 1e-7 , B );
  iiflucks::scaleVector( 1e-7 , Berr );

//  if( verbose ) std::cout << "-- zero distance " << nzero << " times" << std::endl;
}

/// General 'xc yc zc Jx Jy Jz dv' file Monte Carlo.
int MonteCarlo::Run(const std::string file1, const std::string file2, const double theta, const std::vector<double> axis, const std::vector<double> tr)
{
  std::vector<double> a = axis;

  if( theta != 0 ) iiflucks::normalizeVector( a ); 

  if( test )
  {
    np1 = 20;
    np2 = 20;
    V1 = 0;
    V2 = 0;
    for( int i = 0; i < np1 ; i++ ) V1 += dv1a[ i ];
    for( int i = 0; i < np2 ; i++ ) V2 += dv2a[ i ];
  }
  else
  {
    np1 = iiflucks::totalVolume( file1, V1 );
    np2 = iiflucks::totalVolume( file2, V2 );
  }

  Init(); // calculate probabilities and reset cumulative variables 

//  InitHistograms(); // initialize histograms

  const gsl_rng_type * U;
  gsl_rng * rnd;

  U = gsl_rng_default;
  rnd = gsl_rng_alloc (U);

  if( np1 < 1 || np2 < 1 || V1 <= 0 || V2 <= 0 )
  {
    std::cerr << "Failed to read files " << file1 << " and " << file2 << " V1 = " << V1 << " and V2 = " << V2 << std::endl;

    return -1;
  }
  else if( np1 >= constants::maxpoints || np2 >= constants::maxpoints )
  {
    std::cerr << "Maximum number of points " << constants::maxpoints << std::endl;
    return -2;
  }
  else
  {
    if( !test )
    {
      np1 = iiflucks::readFile( file1, x1c, y1c, z1c, J1x, J1y, J1z, dv1a );
      np2 = iiflucks::readFile( file2, x2c, y2c, z2c, J2x, J2y, J2z, dv2a );
    }

    iiflucks::rotateTranslate(theta, axis, tr, np1, x1c, y1c, z1c, J1x, J1y, J1z); 

    if( verbose ) // CSV printing
    {
      std::cout << "np1, " << np1 << std::endl;
      std::cout << "np2, " << np2 << std::endl;
      std::cout << "V1, " << V1 << std::endl;
      std::cout << "V2, " << V2 << std::endl;
      std::cout << "M12, " << M12 << std::endl;
      std::cout << "Msqsum, " << Msqsum << std::endl;
      std::cout << "F12, " << F12[0] << "," << F12[1] << "," << F12[2] << std::endl;
      std::cout << "Fsqsum, " << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
      std::cout << "nrand, " << nrand << std::endl;
      std::cout << "i,n1,dv1,prob1,n2,dv2,prob2,P1x,P1y,P1z,P2x,P2y,P2z,J1vx,J1vy,J1vz,J2vx,J2vy,J2vz,rij,dM12,M12,Msqsum,dF12x,dF12y,dF12z,F12x,F12y,F12z,Fsqsumx,Fsqsumy,Fsqsumz" << std::endl;
    }

    for( int i = 0; i < nrand; i++)
    {
      n1 = (int)( gsl_ran_flat (rnd, 0, np1) );
      n2 = (int)( gsl_ran_flat (rnd, 0, np2) );

      // probability to select n1 and n2 segments
      dv1 = dv1a[ n1 ];
      dv2 = dv2a[ n2 ];
      prob1 = dv1 / V1;
      prob2 = dv2 / V2;            
      prob = prob1 * prob2;

      iiflucks::makeVector( x1c[ n1 ], y1c[ n1 ], z1c[ n1 ], P1 );
      iiflucks::makeVector( x2c[ n2 ], y2c[ n2 ], z2c[ n2 ], P2 );
      iiflucks::makeVector( J1x[ n1 ], J1y[ n1 ], J1z[ n1 ], J1v );
      iiflucks::makeVector( J2x[ n2 ], J2y[ n2 ], J2z[ n2 ], J2v );

      if( verbose )
      {
        std::cout << i << "," << n1 << "," << dv1 << "," << prob1;
        std::cout << "," << n2 << "," << dv2 << "," << prob2;
        std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
        std::cout << "," << P2[0] << "," << P2[1] << "," << P2[2];
        std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
        std::cout << "," << J2v[0] << "," << J2v[1] << "," << J2v[2];
      }

      Innerloop();

      if( verbose )
      {
        std::cout << "," << rij;
        std::cout << "," << dM12 << "," << M12 << "," << Msqsum;
        std::cout << "," << dF12[0] << "," << dF12[1] << "," << dF12[2];
        std::cout << "," << F12[0] << "," << F12[1] << "," << F12[2];
        std::cout << "," << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
      }
    }

    Calculate(); // calculate values and errors from accumulated values
  }

  return nzero;
}

/// Run Monte Carlo integration of field for position-current-density-dv file at P.
int MonteCarlo::Run(const std::string file, const double theta, const std::vector<double> axis, const std::vector<double> tr, const std::vector<double> P)
{
  P2 = P;
  const gsl_rng_type * U;
  gsl_rng * rnd;

  U = gsl_rng_default;
  rnd = gsl_rng_alloc (U);

  std::vector<double> a = axis;

  if( theta != 0 ) iiflucks::normalizeVector( a );

  if( test )
  {
    np1 = 20;
    V1 = 0;
    for( int i = 0; i < np1 ; i++ ) V1 += dv1a[ i ];
  }
  else
  {
    np1 = iiflucks::totalVolume( file, V1 );
  }

  if( np1 < 1 || V1 <= 0 )
  {
    std::cerr << "Failed to read file " << file << " V1 = " << V1 << std::endl;
    return -1;
  }
  else if( np1 >= constants::maxpoints )
  {
    std::cerr << "Maximum number of points " << constants::maxpoints << std::endl;
    return -2;
  }
  else
  {
    if( !test )
    {
      np1 = iiflucks::readFile( file, x1c, y1c, z1c, J1x, J1y, J1z, dv1a );
    }

    iiflucks::rotateTranslate(theta, axis, tr, np1, x1c, y1c, z1c, J1x, J1y, J1z);

    iiflucks::zeroVector( B );
    iiflucks::zeroVector( Bsqsum );
    nrand = pow(10, Nrnd);

    if( verbose ) // CSV printing
    {
      std::cout << "np1, " << np1 << std::endl;
      std::cout << "V1, " << V1 << std::endl;
      std::cout << "P2, " << P2[0] << "," << P2[1] << "," << P2[2] << std::endl;
      std::cout << "B, " << B[0] << "," << B[1] << "," << B[2] << std::endl;
      std::cout << "Bsqsum, " << Bsqsum[0] << "," << Bsqsum[1] << "," << Bsqsum[2] << std::endl;
      std::cout << "nrand, " << nrand << std::endl;
      std::cout << "i,n1,dv1,prob1,P1x,P1y,P1z,J1vx,J1vy,J1vz,rij,dBx,dBy,dBz,Bx,By,Bz" << std::endl;
    }

    nzero = 0;
    for( int i = 0; i < nrand; i++)
    {
      n1 = (int)( gsl_ran_flat (rnd, 0, np1) );

      // probability to select n1 and n2 segments
      dv1 = dv1a[ n1 ];
      prob1 = dv1 / V1;

      iiflucks::makeVector( x1c[ n1 ], y1c[ n1 ], z1c[ n1 ], P1 );
      iiflucks::makeVector( J1x[ n1 ], J1y[ n1 ], J1z[ n1 ], J1v );

      if( verbose )
      {
        std::cout << i << "," << n1 << "," << dv1 << "," << prob1;
        std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
        std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
      }

      InnerloopB();

      if( verbose )
      {
        std::cout << "," << rij;
        std::cout << "," << dBv[0] << "," << dBv[1] << ","<< dBv[2];
        std::cout << "," << B[0] << "," << B[1] << "," << B[2] << std::endl;
      }
    }

    CalculateB(); // calculate values and errors from accumulated values

  }

  return nzero;
}

