#include "Files.h"

namespace iiflucks {

/// Return number of points in the file.
int totalVolume(const std::string fname, double &V)
{
  int np = 0;
  double x, dv, vol = 0;

  std::ifstream infile( fname );
  if( !infile.good() )
  {
    std::cerr << "Could not open " << fname << std::endl; 
    return -1;
  }
  else
  {
    while( true )
    {
      infile >> x >> x >> x;
      infile >> x >> x >> x;
      infile >> dv;

      if( !infile.good() ) break;

      vol += dv;
      np++;
    }

    V = vol;
  }

  return np;
}

/// Return number of points in the file.
int readFile(const std::string fname, double xc[], double yc[], double zc[], double Jx[], double Jy[], double Jz[], double dv[])
{
  int np = 0;

  std::ifstream infile( fname );
  if( !infile.good() )
  {
    std::cerr << "Could not open " << fname << std::endl; 
    return -1;
  }
  else
  {
    while( true )
    {
      infile >> xc[ np ] >> yc[ np ] >> zc[ np ];
      infile >> Jx[ np ] >> Jy[ np ] >> Jz[ np ];
      infile >> dv[ np ];

      if( np >= constants::maxpoints ) break;
      if( !infile.good() ) break;

      np++;
    }
  } 

  return np;
}

/// Rotate position-current-density-dv data around axis by angle theta and translate by tr.
void rotateTranslate(const double theta, const std::vector<double> axis, const std::vector<double> tr, const int np, double xc[], double yc[], double zc[], double Jx[], double Jy[], double Jz[])
{
  std::vector<double> v;

  for( int i = 0; i < np; i++)
  {
    if( theta != 0 )
    {
//    if( !test || verbose ) std::cout << "-- rotate first volume around axis (" << ax << "," << ay << "," << az << ") by angle " << theta << std::endl;
      v.erase( v.begin(), v.end() );
      v.push_back( xc[ i ] );
      v.push_back( yc[ i ] );
      v.push_back( zc[ i ] );

      iiflucks::QuaternionRotation(axis, theta, v);
   
      xc[ i ] = v[ 0 ];
      yc[ i ] = v[ 1 ];
      zc[ i ] = v[ 2 ];

      v.erase( v.begin(), v.end() );
      v.push_back( Jx[ i ] );
      v.push_back( Jy[ i ] );
      v.push_back( Jz[ i ] );

      iiflucks::QuaternionRotation(axis, theta, v);

      Jx[ i ] = v[ 0 ]; 
      Jy[ i ] = v[ 1 ];
      Jz[ i ] = v[ 2 ];
    }

    xc[ i ] += tr[ 0 ];
    yc[ i ] += tr[ 1 ];
    zc[ i ] += tr[ 2 ];
  }

}

/// Calculate field from position-current-density-dv data.
std::vector<double> calculateB(const int np, double xc[], double yc[], double zc[], double Jx[], double Jy[], double Jz[], double dv[], const std::vector<double> P)
{
  double r21;
  std::vector<double> r, dB, B, J;

  iiflucks::zeroVector( B );
  for(int i = 0; i < np; i++)
  {
    iiflucks::makeVector( Jx[ i ], Jy[ i ], Jz[ i ], J );
    iiflucks::makeVector( xc[ i ], yc[ i ], zc[ i ], r );

    iiflucks::subtractVectors( P, r, r);
    r21 = iiflucks::vectorLength( r );

    iiflucks::crossProduct( J, r, dB );
    iiflucks::scaleVector( dv[ i ] / ( r21 * r21 * r21 ), dB );
    iiflucks::addVectors( B, dB, B);
  }

  iiflucks::scaleVector( 1e-7, B);

  return B;
}

/// Coil integration is done with t=[t1,t2] and dt. Radial integration is with
/// r=[r1, r2] and dr. Coil rotation and translation is returned from this
/// function with wire diameter and current.
bool parseYaml(const YAML::Node coil, const bool verbose, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, std::string &refdes, double &I, double &d, double &theta, std::vector<double> &axis, std::vector<double> &tr)
{
  if (coil["integrate"]["dt"] ) 
  {
    dt = coil["integrate"]["dt"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- integrate dt missing" << std::endl;
    return false;
  }

  if (coil["integrate"]["t1"]) 
  {
    t1 = coil["integrate"]["t1"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- integrate t1 missing" << std::endl;
    return false;
  }

  if (coil["integrate"]["t2"]) 
  {
    t2 = coil["integrate"]["t2"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- integrate t2 missing" << std::endl;
    return false;
  }

  dr = 0;
  r1 = 0;
  r2 = 0;
  if (coil["integrate"]["dr_m"] ) 
  {
    dr = coil["integrate"]["dr_m"].as<double>();

    if (coil["integrate"]["r1_m"] ) 
    {
      r1 = coil["integrate"]["r1_m"].as<double>();
    }
    else
    {
      if( verbose ) std::cerr << "-- integrate r1_m missing" << std::endl;
      return false;
    }

    if (coil["integrate"]["r2_m"] ) 
    {
      r2 = coil["integrate"]["r2_m"].as<double>();
    }
    else
    {
      if( verbose ) std::cerr << "-- integrate r2_m missing" << std::endl;
      return false;
    }
  }

  if (coil["coil"]["refdes"])
  {
    refdes = coil["coil"]["refdes"].as<std::string>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil refdes missing" << std::endl;
    return false;
  }

  if (coil["coil"]["wire_current_A"]) 
  {
    I = coil["coil"]["wire_current_A"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil wire_current_A missing" << std::endl;
    return false;
  }

  if (coil["coil"]["wire_diameter_m"]) 
  {
    d = coil["coil"]["wire_diameter_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil wire_diameter_m missing" << std::endl;
    return false;
  }

  if (coil["coil"]["rotation_angle"]) 
  {
    theta = coil["coil"]["rotation_angle"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil rotation angle missing" << std::endl;
    return false;
  }

  if (coil["coil"]["rotation_axis"]) 
  {
    axis.push_back( coil["coil"]["rotation_axis"][0].as<double>() );
    axis.push_back( coil["coil"]["rotation_axis"][1].as<double>() );
    axis.push_back( coil["coil"]["rotation_axis"][2].as<double>() );
//    std::cout << "-- axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- coil rotation axis missing" << std::endl;
    return false;
  }

  if (coil["coil"]["translation_m"]) 
  {
    tr.push_back( coil["coil"]["translation_m"][0].as<double>() );
    tr.push_back( coil["coil"]["translation_m"][1].as<double>() );
    tr.push_back( coil["coil"]["translation_m"][2].as<double>() );
//    std::cout << "-- tr = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- coil translation missing" << std::endl;
    return false;
  }

  return true;
}

/// Coil integration is done with t=[t1,t2] and lc for each section. Radial
/// integration is with r=[r1, r2] and dr. Coil rotation and translation is
/// returned from this function with wire diameter and current.
bool parseCoilYaml(const YAML::Node coil, const bool verbose, double &lc, std::vector<double> &t1v, std::vector<double> &t2v, double &dr, double &r1, double &r2, std::string &refdes, double &I, double &d, double &theta, std::vector<double> &axis, std::vector<double> &tr)
{
  if (coil["integrate_sections"]["lc"] )
  {
    lc = coil["integrate_sections"]["lc"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- integrate_sections lc missing" << std::endl;
    return false;
  }

  if (coil["integrate_sections"]["t1"] ) 
  {
    for(std::size_t i = 0; i < coil["integrate_sections"]["t1"].size(); i++) t1v.push_back( coil["integrate_sections"]["t1"][ i ].as<double>() );
  }
  else
  {
    if( verbose ) std::cerr << "-- integrate_sections t1 missing" << std::endl;
    return false;
  }

  if (coil["integrate_sections"]["t2"] ) 
  {
    for(std::size_t i = 0; i < coil["integrate_sections"]["t2"].size(); i++) t2v.push_back( coil["integrate_sections"]["t2"][ i ].as<double>() );
  }
  else
  {
    if( verbose ) std::cerr << "-- integrate_sections t2 missing" << std::endl;
    return false;
  }

  if (coil["integrate_sections"]["dr_m"] ) 
  {
    dr = coil["integrate_sections"]["dr_m"].as<double>();

    if (coil["integrate_sections"]["r1_m"] ) 
    {
      r1 = coil["integrate_sections"]["r1_m"].as<double>();
    }
    else
    {
      if( verbose ) std::cerr << "-- integrate_sections r1_m missing" << std::endl;
      return false;
    }

    if (coil["integrate_sections"]["r2_m"] ) 
    {
      r2 = coil["integrate_sections"]["r2_m"].as<double>();
    }
    else
    {
      if( verbose ) std::cerr << "-- integrate_sections r2_m missing" << std::endl;
      return false;
    }
  }

  if (coil["coil"]["refdes"])
  {
    refdes = coil["coil"]["refdes"].as<std::string>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil refdes missing" << std::endl;
    return false;
  }

  if (coil["coil"]["wire_current_A"]) 
  {
    I = coil["coil"]["wire_current_A"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil wire_current_A missing" << std::endl;
    return false;
  }

  if (coil["coil"]["wire_diameter_m"]) 
  {
    d = coil["coil"]["wire_diameter_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil wire_diameter_m missing" << std::endl;
    return false;
  }

  if (coil["coil"]["rotation_angle"]) 
  {
    theta = coil["coil"]["rotation_angle"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil rotation angle missing" << std::endl;
    return false;
  }

  if (coil["coil"]["rotation_axis"]) 
  {
    axis.push_back( coil["coil"]["rotation_axis"][0].as<double>() );
    axis.push_back( coil["coil"]["rotation_axis"][1].as<double>() );
    axis.push_back( coil["coil"]["rotation_axis"][2].as<double>() );
//    std::cout << "-- axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- coil rotation axis missing" << std::endl;
    return false;
  }

  if (coil["coil"]["translation_m"]) 
  {
    tr.push_back( coil["coil"]["translation_m"][0].as<double>() );
    tr.push_back( coil["coil"]["translation_m"][1].as<double>() );
    tr.push_back( coil["coil"]["translation_m"][2].as<double>() );
//    std::cout << "-- tr = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- coil translation missing" << std::endl;
    return false;
  }

  return true;
}  

/// Test yaml node for current distribution data.
bool isCurrentYaml(const YAML::Node current)
{
  if( current.Type() != YAML::NodeType::Map ) return false;

  if( current["current"] )
  {
    if( current["current"]["file"] ) return true;
  }

  return false;
}

/// End-to-end current of current distribution with rotation and translation. 
bool parseCurrent(const YAML::Node current, const bool verbose, double &I, double &theta, std::vector<double> &axis, std::vector<double> &tr, std::string &file)
{
//  if (current["current"]["refdes"])
//  {
//    refdes = current["current"]["refdes"].as<std::string>();
//  }
//  else
//  {
//    if( verbose ) std::cerr << "-- current refdes missing" << std::endl;
//    return false;
//  }

  if (current["current"]["current_A"]) 
  {
    I = current["current"]["current_A"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- current current_A missing" << std::endl;
    return false;
  }

  if (current["current"]["rotation_angle"]) 
  {
    theta = current["current"]["rotation_angle"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- current rotation angle missing" << std::endl;
    return false;
  }

  if (current["current"]["rotation_axis"]) 
  {
    axis.push_back( current["current"]["rotation_axis"][0].as<double>() );
    axis.push_back( current["current"]["rotation_axis"][1].as<double>() );
    axis.push_back( current["current"]["rotation_axis"][2].as<double>() );
//    std::cout << "-- axis = (" << axis[ 0 ] << "," << axis[ 1 ] << "," << axis[ 2 ] << ")" << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- current rotation axis missing" << std::endl;
    return false;
  }

  if (current["current"]["translation_m"]) 
  {
    tr.push_back( current["current"]["translation_m"][0].as<double>() );
    tr.push_back( current["current"]["translation_m"][1].as<double>() );
    tr.push_back( current["current"]["translation_m"][2].as<double>() );
//    std::cout << "-- tr = (" << tr[ 0 ] << "," << tr[ 1 ] << "," << tr[ 2 ] << ")" << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- current translation missing" << std::endl;
    return false;
  }

  if (current["current"]["file"])
  {
    file = current["current"]["file"].as<std::string>();
  }
  else
  {
    if( verbose ) std::cerr << "-- current file missing" << std::endl;
    return false;
  }

  return true;
}

/// Test yaml node for Helix data.
bool isHelixYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["helix"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt and current I. 
Helix * parseHelix(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes; 
  double d;
  double theta;
  std::vector<double> axis, tr;
  double R;
  double L;
  double N;

  Helix * helix_pointer = nullptr;

  if( verbose ) std::cout << "-- test is Map type" << std::endl;
  if( coil.Type() != YAML::NodeType::Map ) return helix_pointer;

  if( verbose ) std::cout << "-- parse coil" << std::endl;
  if( !parseYaml(coil, verbose, dt, t1, t2, dr, r1, r2, refdes, I, d, theta, axis, tr) ) return helix_pointer;

  if( verbose ) std::cout << "-- dt = " << dt << " t1 = " << t1 << " t2 = " << t2 << std::endl;

  if( verbose ) std::cout << "-- parse helix radius" << std::endl;
  if (coil["coil"]["helix"]["radius_m"]) 
  {
    R = coil["coil"]["helix"]["radius_m"].as<double>();
    if( verbose ) std::cout << "-- R = " << R << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- coil helix radius missing" << std::endl;
    return helix_pointer;
  }

  if (coil["coil"]["helix"]["length_m"]) 
  {
    L = coil["coil"]["helix"]["length_m"].as<double>();
    if( verbose ) std::cout << "-- L = " << L << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- helix length missing" << std::endl;
    return helix_pointer;
  }

  if (coil["coil"]["helix"]["turns"]) 
  {
    N = coil["coil"]["helix"]["turns"].as<double>();
    if( verbose ) std::cout << "-- N = " << N << std::endl;
  }
  else
  {
    if( verbose ) std::cerr << "-- helix turns missing" << std::endl;
    return helix_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  helix_pointer = new Helix(refdes, R, L, N, A, J, theta, axis, tr);

  return helix_pointer;
};

/// Test yaml node for Ring data.
bool isRingYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["ring"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt and current I. 
Ring * parseRing(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes; 
  double d;
  double theta;
  std::vector<double> axis, tr;
  double R;

  Ring * ring_pointer = nullptr;

  if( coil.Type() != YAML::NodeType::Map ) return ring_pointer;

  if( !parseYaml(coil, verbose, dt, t1, t2, dr, r1, r2, refdes, I, d, theta, axis, tr) ) return ring_pointer;

  if (coil["coil"]["ring"]["radius_m"]) 
  {
    R = coil["coil"]["ring"]["radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil ring radius missing" << std::endl;
    return ring_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  ring_pointer = new Ring(refdes, R, A, J, theta, axis, tr);

  return ring_pointer;
}

/// Test yaml node for RingCylinder data.
bool isRingCylinderYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["ringcylinder"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt and current I. 
RingCylinder * parseRingCylinder(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes;
  double d;
  double theta;
  std::vector<double> axis, tr;
  double R;
  double R2;

  RingCylinder * ringcyl_pointer = nullptr;

  if( coil.Type() != YAML::NodeType::Map ) return ringcyl_pointer;

  if( !parseYaml(coil, verbose, dt, t1, t2, dr, r1, r2, refdes, I, d, theta, axis, tr) ) return ringcyl_pointer;

  if (coil["coil"]["ringcylinder"]["radius_m"])
  {
    R = coil["coil"]["ringcylinder"]["radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil ringcylinder radius missing" << std::endl;
    return ringcyl_pointer;
  }

  if (coil["coil"]["ringcylinder"]["cylinder_radius_m"])
  {
    R2 = coil["coil"]["ringcylinder"]["cylinder_radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil ringcylinder cylinder_radius missing" << std::endl;
    return ringcyl_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  ringcyl_pointer = new RingCylinder(refdes, R, R2, A, J, theta, axis, tr);

  return ringcyl_pointer;
}

/// Test yaml node for Line data.
bool isLineYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["line"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt and current I. 
Line * parseLine(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes; 
  double d;
  double theta;
  std::vector<double> axis, tr;

  Line * line_pointer = nullptr;

  if( coil.Type() != YAML::NodeType::Map ) return line_pointer;

  if( !parseYaml(coil, verbose, dt, t1, t2, dr, r1, r2, refdes, I, d, theta, axis, tr) ) return line_pointer;

  if ( coil["coil"]["line"] )
  {
  }
  else
  {
    if( verbose ) std::cerr << "-- coil line definition missing" << std::endl;
    return line_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  line_pointer = new Line(refdes, A, J, theta, axis, tr);

  return line_pointer;
}

/// Test yaml node for LineSphere data.
bool isLineSphereYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["linesphere"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt and current I.
LineSphere * parseLineSphere(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes;
  double d;
  double theta;
  std::vector<double> axis, tr;
  double R1, R2;

  LineSphere * lsphere_pointer = nullptr;

  if( coil.Type() != YAML::NodeType::Map ) return lsphere_pointer;

  if( !parseYaml(coil, verbose, dt, t1, t2, dr, r1, r2, refdes, I, d, theta, axis, tr) ) return lsphere_pointer;

  if (coil["coil"]["linesphere"]["radius_m"])
  {
    R1 = coil["coil"]["linesphere"]["radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil linesphere radius_m missing" << std::endl;
    return lsphere_pointer;
  }

  if (coil["coil"]["linesphere"]["sphere_radius_m"])
  {
    R2 = coil["coil"]["linesphere"]["sphere_radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil linesphere sphere_radius_m missing" << std::endl;
    return lsphere_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  lsphere_pointer = new LineSphere(refdes, R1, R2, A, J, theta, axis, tr);

  return lsphere_pointer;
}

/// Test yaml node for LoopSphere data.
bool isLoopSphereYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["loopsphere"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt and current I.
LoopSphere * parseLoopSphere(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes;
  double d;
  double rotation;
  std::vector<double> axis, tr;
  double R2;
  double theta1, phi1, dtheta, dphi;

  LoopSphere * loopsphere_pointer = nullptr;

  if( coil.Type() != YAML::NodeType::Map ) return loopsphere_pointer;

  if( !parseYaml(coil, verbose, dt, t1, t2, dr, r1, r2, refdes, I, d, rotation, axis, tr) ) return loopsphere_pointer;

  if (coil["coil"]["loopsphere"]["sphere_radius_m"])
  {
    R2 = coil["coil"]["loopsphere"]["sphere_radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil loopsphere sphere_radius_m missing" << std::endl;
    return loopsphere_pointer;
  }

  if (coil["coil"]["loopsphere"]["altitude_rad"])
  {
    theta1 = coil["coil"]["loopsphere"]["altitude_rad"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil loopsphere altitude_rad missing" << std::endl;
    return loopsphere_pointer;
  }

  if (coil["coil"]["loopsphere"]["azimuth_rad"])
  {
    phi1 = coil["coil"]["loopsphere"]["azimuth_rad"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil loopsphere azimuth_rad missing" << std::endl;
    return loopsphere_pointer;
  }

  if (coil["coil"]["loopsphere"]["altitude_length_rad"])
  {
    dtheta = coil["coil"]["loopsphere"]["altitude_length_rad"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil loopsphere altitude_length_rad missing" << std::endl;
    return loopsphere_pointer;
  }

  if (coil["coil"]["loopsphere"]["azimuth_length_rad"])
  {
    dphi = coil["coil"]["loopsphere"]["azimuth_length_rad"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil loopsphere azimuth_length_rad missing" << std::endl;
    return loopsphere_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  loopsphere_pointer = new LoopSphere(refdes, R2, theta1, phi1, dtheta, dphi, A, J, rotation, axis, tr);

  return loopsphere_pointer;
}

/// Test yaml node for RacetrackCylinder data.
bool isRacetrackCylinderYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["racetrack_cylinder"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt for each section and current I. 
RacetrackCylinder * parseRacetrackCylinder(const YAML::Node coil, double &lc, std::vector<double> &t1v, std::vector<double> &t2v, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes; 
  double d;
  double theta;
  std::vector<double> axis, tr;
  double R;
  double R2;
  double h;
  double w;
  double th;
  int N;
  int O;

  RacetrackCylinder * rtcyl_pointer = nullptr;

  if( coil.Type() != YAML::NodeType::Map ) return rtcyl_pointer;

  if( !parseCoilYaml(coil, verbose, lc, t1v, t2v, dr, r1, r2, refdes, I, d, theta, axis, tr) ) return rtcyl_pointer;

  if (coil["coil"]["racetrack_cylinder"]["radius_m"])
  {
    R = coil["coil"]["racetrack_cylinder"]["radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_cylinder radius missing" << std::endl;
    return rtcyl_pointer;
  }

  if (coil["coil"]["racetrack_cylinder"]["cylinder_radius_m"])
  {
    R2 = coil["coil"]["racetrack_cylinder"]["cylinder_radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_cylinder cylinder_radius missing" << std::endl;
    return rtcyl_pointer;
  }

  if (coil["coil"]["racetrack_cylinder"]["length_m"])
  {
    h = coil["coil"]["racetrack_cylinder"]["length_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_cylinder length_m missing" << std::endl;
    return rtcyl_pointer;
  }

  if (coil["coil"]["racetrack_cylinder"]["radial_width_m"])
  {
    w = coil["coil"]["racetrack_cylinder"]["radial_width_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_cylinder radial_width_m missing" << std::endl;
    return rtcyl_pointer;
  }

  if (coil["coil"]["racetrack_cylinder"]["thickness_m"])
  {
    th = coil["coil"]["racetrack_cylinder"]["thickness_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_cylinder thickness_m missing" << std::endl;
    return rtcyl_pointer;
  }

  if (coil["coil"]["racetrack_cylinder"]["layer_turns"])
  {
    N = coil["coil"]["racetrack_cylinder"]["layer_turns"].as<int>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_cylinder layer_turns missing" << std::endl;
    return rtcyl_pointer;
  }

  if (coil["coil"]["racetrack_cylinder"]["layers"])
  {
    O = coil["coil"]["racetrack_cylinder"]["layers"].as<int>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_cylinder layers missing" << std::endl;
    return rtcyl_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  rtcyl_pointer = new RacetrackCylinder(refdes, R, R2, h, w, th, N, O, A, J, theta, axis, tr);

  return rtcyl_pointer;
}

/// Test yaml node for RacetrackPlanar data.
bool isRacetrackPlanarYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["racetrack_planar"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt for each section and current I.
RacetrackPlanar * parseRacetrackPlanar(const YAML::Node coil, double &lc, std::vector<double> &t1v, std::vector<double> &t2v, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes;
  double d;
  double theta;
  std::vector<double> axis, tr;
  double R;
  double h;
  double e;
  double w;
  double th;
  int turns;
  int layers;

  RacetrackPlanar * rtplanar_pointer = nullptr;

  if( coil.Type() != YAML::NodeType::Map ) return rtplanar_pointer;

  if( !parseCoilYaml(coil, verbose, lc, t1v, t2v, dr, r1, r2, refdes, I, d, theta, axis, tr) ) return rtplanar_pointer;

  if (coil["coil"]["racetrack_planar"]["radius_m"])
  {
    R = coil["coil"]["racetrack_planar"]["radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_planar radius_m missing" << std::endl;
    return rtplanar_pointer;
  }

  if (coil["coil"]["racetrack_planar"]["length_m"])
  {
    h = coil["coil"]["racetrack_planar"]["length_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_planar length_m missing" << std::endl;
    return rtplanar_pointer;
  }

  if (coil["coil"]["racetrack_planar"]["end_length_m"])
  {
    e = coil["coil"]["racetrack_planar"]["end_length_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_planar end_length_m missing" << std::endl;
    return rtplanar_pointer;
  }

  if (coil["coil"]["racetrack_planar"]["winding_width_m"])
  {
    w = coil["coil"]["racetrack_planar"]["winding_width_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_planar winding_width_m missing" << std::endl;
    return rtplanar_pointer;
  }

  if (coil["coil"]["racetrack_planar"]["thickness_m"])
  {
    th = coil["coil"]["racetrack_planar"]["thickness_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_planar thickness_m missing" << std::endl;
    return rtplanar_pointer;
  }

  if (coil["coil"]["racetrack_planar"]["layer_turns"])
  {
    turns = coil["coil"]["racetrack_planar"]["layer_turns"].as<int>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_planar layer_turns missing" << std::endl;
    return rtplanar_pointer;
  }

  if (coil["coil"]["racetrack_planar"]["layers"])
  {
    layers = coil["coil"]["racetrack_planar"]["layers"].as<int>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil racetrack_planar layers missing" << std::endl;
    return rtplanar_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  rtplanar_pointer = new RacetrackPlanar(refdes, R, h, e, w, th, turns, layers, A, J, theta, axis, tr);

  return rtplanar_pointer;
}

/// Test yaml node for Solenoid data.
bool isSolenoidYaml(const YAML::Node coil)
{
  if( coil.Type() != YAML::NodeType::Map ) return false;

  if( coil["coil"] )
  {
    if( coil["coil"]["solenoid"] ) return true;
  }

  return false;
}

/// Return integration range t=[t1, t2] with dt for all layers and current I.
Solenoid * parseSolenoid(const YAML::Node coil, double &lc, std::vector<double> &t1v, std::vector<double> &t2v, double &dr, double &r1, double &r2, double &I, bool verbose)
{
  std::string refdes;
  double d;
  double theta;
  std::vector<double> axis, tr;
  double R;
  double L;
  double th;
  int turns;
  int layers;

  Solenoid * solenoid_pointer = nullptr;

  if( coil.Type() != YAML::NodeType::Map ) return solenoid_pointer;

  if( !parseCoilYaml(coil, verbose, lc, t1v, t2v, dr, r1, r2, refdes, I, d, theta, axis, tr) ) return solenoid_pointer;

  if (coil["coil"]["solenoid"]["radius_m"])
  {
    R = coil["coil"]["solenoid"]["radius_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil solenoid radius_m missing" << std::endl;
    return solenoid_pointer;
  }

  if (coil["coil"]["solenoid"]["length_m"])
  {
    L = coil["coil"]["solenoid"]["length_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil solenoid length_m missing" << std::endl;
    return solenoid_pointer;
  }

  if (coil["coil"]["solenoid"]["thickness_m"])
  {
    th = coil["coil"]["solenoid"]["thickness_m"].as<double>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil solenoid thickness_m missing" << std::endl;
    return solenoid_pointer;
  }

  if (coil["coil"]["solenoid"]["layer_turns"])
  {
    turns = coil["coil"]["solenoid"]["layer_turns"].as<int>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil solenoid layer_turns missing" << std::endl;
    return solenoid_pointer;
  }

  if (coil["coil"]["solenoid"]["layers"])
  {
    layers = coil["coil"]["solenoid"]["layers"].as<int>();
  }
  else
  {
    if( verbose ) std::cerr << "-- coil solenoid layers missing" << std::endl;
    return solenoid_pointer;
  }

  double A = M_PI * d * d / 4;
  double J = I / A;

  solenoid_pointer = new Solenoid(refdes, R, L, th, turns, layers, A, J, theta, axis, tr);

  return solenoid_pointer;
}

}
