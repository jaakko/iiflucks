//
// Read gmsh surface mesh file and print centroid of each triangle and 
// quadrangle element followed by surface current vector and area.
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// The file.msh has Gmsh 2D mesh to model surface of weakly magnetized volume
// with M = (Mx, My, Mz). Verbosed printing of file processing can be seen
// with option 'v'. Details on how the mesh file describes the surface can
// be found from Gmsh Reference Manual in section File formats. The section
// Gmsh API documents programming interface to library libgmsh.
//
// File holes.txt is list of holes that are defined by central axis vector
// (v1x, v1y, v1z), point on axis (p1x, p1y, p1z) and bounding box radius
// Rb and height Lb. Here +-Lb/2 is taken along the central axis vector
// with (p1x, p1y, p1z) at center.
//
// v1x v1y v1z p1x p1y p1z Rb Lb
// ...
//
// File planes.txt is list of known planes with outward normal vector.
// The plane is defined by normal vector (n1x, n1y, n1z) and point
// on plane (p1x, p1y, p1z). After this there is outward normal vector
// of the plane (nn1x, nn1y, nn1z).
//
// n1x n1y n1z p1x p1y p1z nn1x nn1y nn1z
// ...
//
// File axis.txt is list of symmetry axes defined by direction vector
// (v1x, v1y, v1z) and point on axis (p1x, p1y, p1z).
//
// v1x v1y v1z p1x p1y p1z
// ...
//
// Node tags and coordinates from mesh are read in with function call
//
// gmsh::model::mesh::getNodes()
//
// List (vector) of 2D elements (triangles) are read in with
//
// gmsh::model::mesh::getElements()
//
// List of elements is used to get node coordinates (vertices) of each 
// triangle. Centroid Pc = (xc, yc, zc) and area da of each triangle is 
// calculated from these coordinates. The point Pc is tested to see if it
// is on one of the planes given in file planes.txt. This is then used
// to define outward direction for the pseudovector normal calculated
// from triangle edges nv = (P1 - P0) x (P2 - P0).
//
// If the point Pc does not belong to one of the known planes the nv
// is tested to see if it is one any plane perpendicular to axis in file 
// axis.txt. Distance of Pc is calculated from symmetry axis. New vector
// is calculated from centroid by adding triangle normal vector scaled
// by square root of triangle area. Distance of this new point is calculated
// from symmetry axis. If distance is larger than the first distance the
// normal is pointing outwards for outer surface. 
//
// Outer surface current is jm = M x nv. This data is printed as list
// 
// xc yc zc jmx jmy jmz da
// ...
//
// for each surface element.
//
// With option 'view' Gmsh postprocessing data is printed. This file can be
// merged in Gmsh with the mesh file to see calculate jm vector field.
//
// With option 'plane' two unit length vectors to define area element are
// printed instead of surface current
//
// xc yc zc ux uy uz vx vy vz da
// ...
//
// This is useful for random surface current simulations.
//
// With option '--test v' some test results are printed.
//
// Tue Apr 23 14:44:37 CDT 2019
// Edit: Fri Sep 13 15:09:51 CDT 2019
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string.h>
#include <gmsh.h>
#include <gsl/gsl_histogram.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: mesh2da file.msh scale holes.txt planes.txt axis.txt Mx My Mz [v|view|plane]" << std::endl;
}

void printversion()
{
  std::cout << "mesh2da v. 20190913, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Read gmsh surface mesh file and print centroid of each triangle and quadrangle element followed by surface current vector and area." << std::endl;
  std::cout << std::endl;
  std::cout << "The file.msh has Gmsh 2D mesh to model surface of weakly magnetized volume with M = (Mx, My, Mz). Verbosed printing of file processing can be seen with option 'v'." << std::endl;
  std::cout << std::endl;
  std::cout << "File holes.txt is list of holes that are defined by central axis vector (v1x, v1y, v1z), point on axis (p1x, p1y, p1z) and bounding box radius Rb and height Lb. Here +-Lb/2 is taken along the central axis vector with (p1x, p1y, p1z) at center." << std::endl;
  std::cout << std::endl;
  std::cout << "v1x v1y v1z p1x p1y p1z Rb Lb" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "File planes.txt is list of known planes with outward normal vector. The plane is defined by normal vector (n1x, n1y, n1z) and point on plane (p1x, p1y, p1z). After this there is outward normal vector of the plane (nn1x, nn1y, nn1z)." << std::endl;
  std::cout << std::endl;
  std::cout << "n1x n1y n1z p1x p1y p1z nn1x nn1y nn1z" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "File axis.txt is list of symmetry axes defined by direction vector (v1x, v1y, v1z) and point on axis (p1x, p1y, p1z)." << std::endl;
  std::cout << std::endl;
  std::cout << "v1x v1y v1z p1x p1y p1z" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "Node tags and coordinates from mesh are read in with function call" << std::endl;
  std::cout << std::endl;
  std::cout << "gmsh::model::mesh::getNodes()" << std::endl;
  std::cout << std::endl;
  std::cout << "List (vector) of 2D elements (triangles) are read in with" << std::endl;
  std::cout << "gmsh::model::mesh::getElements()" << std::endl;
  std::cout << std::endl;
  std::cout << "List of elements is used to get node coordinates (vertices) of each triangle. Centroid Pc = (xc, yc, zc) and area da of each triangle is calculated from these coordinates. The point Pc is tested to see if it is on one of the planes given in file planes.txt. This is then used to define outward direction for the pseudovector normal calculated from triangle edges nv = (P1 - P0) x (P2 - P0)." << std::endl;
  std::cout << std::endl;
  std::cout << "If the point Pc is not inside hole bounding box and does not belong to one of the known planes the nv is tested to see if it is on any plane perpendicular to axis in file axis.txt. Distance of Pc is calculated from symmetry axis. New vector is calculated from centroid by adding triangle normal vector scaled by square root of triangle area. Distance of this new point is calculated from symmetry axis. If distance is larger than the first distance the normal is pointing outwards for outer surface." << std::endl; 
  std::cout << std::endl;
  std::cout << "Outer surface current is jm = M x nv. This data is printed as list" << std::endl;
  std::cout << std::endl;
  std::cout << "xc yc zc jmx jmy jmz da" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "for each surface element." << std::endl;
  std::cout << std::endl;
  std::cout << "With option 'view' Gmsh postprocessing data is printed. This file can be merged in Gmsh with the mesh file to see calculate jm vector field." << std::endl;
  std::cout << std::endl;
  std::cout << "The 'scale' is used for linear scaling of dimensions." << std::endl;
  std::cout << std::endl;
  std::cout << "With option 'plane' two unit length vectors to define area element are printed instead of surface current" << std::endl;
  std::cout << std::endl;
  std::cout << "xc yc zc ux uy uz vx vy vz da" << std::endl;
  std::cout << "..." << std::endl;
  std::cout << std::endl;
  std::cout << "This is useful for random surface current simulations." << std::endl;
}

void testFunctions(bool verbose)
{
  std::vector<double> P0, P1, P2, P3, Pc, nv;

  // test triangle area and normal calculation
  P0.push_back(0); P0.push_back(1); P0.push_back(0);
  P1.push_back(0); P1.push_back(0); P1.push_back(1);
  P2.push_back(1); P2.push_back(0); P2.push_back(0);

  if( verbose )
  {
    std::cout << "-- triangle ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << std::endl;
    std::cout << "-- area ";
  }

  std::cout << iiflucks::triangleAreaNormal(P0, P1, P2, nv);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  if( verbose ) std::cout << "-- triangle normal pseudovector ";

  std::cout << "(" << nv[0] << "," << nv[1] << "," << nv[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  // test triangle centroid calculation
  iiflucks::triangleCentroid(P0, P1, P2, Pc);

  if( verbose ) std::cout << "-- triangle centroid ";

  std::cout << "(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());

  // test vector length calculation
  P0.push_back(1); P0.push_back(2); P0.push_back(3);

  if( verbose )
  {
    std::cout << "-- vector ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") length=";
  }

  std::cout << iiflucks::vectorLength( P0 );

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());

  // test dot product calculation
  P0.push_back(1); P0.push_back(2); P0.push_back(3);
  P1.push_back(4); P1.push_back(5); P1.push_back(6);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") .";
    std::cout << " (" << P1[0] << "," << P1[1] << "," << P1[2] << ") = ";
  }

  std::cout << iiflucks::dotProduct(P0, P1);

  if( verbose ) std::cout << std::endl; else std::cout << " ";


  // test angle between vectors
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(1); P1.push_back(0);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") and";
    std::cout << " (" << P1[0] << "," << P1[1] << "," << P1[2] << ") angle=";
  }

  std::cout << iiflucks::angleVectors(P0, P1);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());

  // test distance between points
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(1); P1.push_back(0);

  if( verbose )
  {
    std::cout << "-- (" << P0[0] << "," << P0[1] << "," << P0[2] << ") and";
    std::cout << " (" << P1[0] << "," << P1[1] << "," << P1[2] << ") distance=";
  }

  std::cout << iiflucks::distancePoints(P0, P1);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  nv.erase (nv.begin(), nv.end());

  // test point is on plane
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(1); P1.push_back(0);
  nv.push_back(1); nv.push_back(1); nv.push_back(0);

  if( verbose )
  {
    std::cout << "-- plane n=(" << nv[0] << "," << nv[1] << "," << nv[2] << ") with point ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << "), is point ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") on plane? ";
  }

  if( iiflucks::pointOnPlane(nv, P0, P1) ) std::cout << "yes"; else std::cout << "no";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  nv.erase (nv.begin(), nv.end());

  // test vector P0 is on any plane defined by normal vector nv
  P0.push_back(1); P0.push_back(-1); P0.push_back(1);
  nv.push_back(1); nv.push_back(1); nv.push_back(0);

  if( verbose )
  {
    std::cout << "-- plane n=(" << nv[0] << "," << nv[1] << "," << nv[2] << ") is vector ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") on any plane? ";
  }

  if( iiflucks::vectorAnyPlane(nv, P0) ) std::cout << "yes"; else std::cout << "no";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());

  // test vector product calculation
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(-1); P1.push_back(-1);

  if( verbose )
  {
    std::cout << "-- vector product ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") x ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") = ";
  }

  iiflucks::crossProduct(P0, P1, P2);

  std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ")";

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  nv.erase (nv.begin(), nv.end());

  // calculate P1 distance from line with nv and P0
  P0.push_back(1); P0.push_back(1); P0.push_back(1);
  P1.push_back(1); P1.push_back(1); P1.push_back(0);
  nv.push_back(1); nv.push_back(1); nv.push_back(0);

  if( verbose )
  {
    std::cout << "-- (" << P1[0] << "," << P1[1] << "," << P1[2] << ") distance to ";
    std::cout << "line v=(" << nv[0] << "," << nv[1] << "," << nv[2] << ") with point ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") =";
  }

  std::cout << iiflucks::distancePointLine(nv, P0, P1);

  if( verbose ) std::cout << std::endl; else std::cout << " ";

  P0.erase (P0.begin(), P0.end());
  P1.erase (P1.begin(), P1.end());
  P2.erase (P2.begin(), P2.end());
  P3.erase (P3.begin(), P3.end());

  // test quadrangle area calculation
  P0.push_back(0); P0.push_back(0); P0.push_back(0);
  P1.push_back(3); P1.push_back(0); P1.push_back(0);
  P2.push_back(4); P2.push_back(2); P2.push_back(0);
  P3.push_back(-2); P3.push_back(2); P3.push_back(0);

  if( verbose )
  {
    std::cout << "-- quadrangle ";
    std::cout << "(" << P0[0] << "," << P0[1] << "," << P0[2] << ") ";
    std::cout << "(" << P1[0] << "," << P1[1] << "," << P1[2] << ") ";
    std::cout << "(" << P2[0] << "," << P2[1] << "," << P2[2] << ") ";
    std::cout << "(" << P3[0] << "," << P3[1] << "," << P3[2] << ") ";
    std::cout << "area=";
  }

  std::cout << iiflucks::quadrangleArea(P0, P1, P2, P3);

  std::cout << std::endl;
}

int main(int argc, char **argv)
{
  const int maxholes = 100; // maximum number of holes
  const int maxplanes = 20; // maximum number of planes
  const int maxaxis = 20; // maximum number of symmetry axes
  bool debug = false;
  bool verbose = false;
  bool printview = false; // print post processing view file for gmsh
  bool printplane = false; // print vectors u and v to define plane

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-geom", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    iiflucks::testGeometry( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-vect", 11 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    iiflucks::testVector( verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test", 6 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    testFunctions( verbose );
    return 0;
  }

  if( argc < 9 )
  {
    printusage();
    return 0;
  }

//  int physicalGroup = atoi( argv[ 2 ] );
  double scale = atof( argv[ 2 ] );

  std::vector<double> M;
  M.push_back( atof( argv[ 6 ] ) );
  M.push_back( atof( argv[ 7 ] ) );
  M.push_back( atof( argv[ 8 ] ) );

  if( argc == 10 ) 
  {
    if( strncmp( argv[ 9 ], "view", 4) == 0 ) printview = true;
    else if( strncmp( argv[ 9 ], "plane", 5) == 0 ) printplane = true;
    else verbose = true;
  }

  int nplanes = 0; // number of planes
  double planenx[ maxplanes ], planeny[ maxplanes ], planenz[ maxplanes ];
  double planepx[ maxplanes ], planepy[ maxplanes ], planepz[ maxplanes ];
  double planennx[ maxplanes ], planenny[ maxplanes ], planennz[ maxplanes ];

  std::ifstream infile1( argv[ 4 ] );
  if( !infile1.good() )
  {
    std::cout << "could not open " << argv[ 4 ] << "\n";
  }

  while( nplanes < maxplanes )
  {
    infile1 >> planenx[ nplanes ] >> planeny[ nplanes ] >> planenz[ nplanes ];
    infile1 >> planepx[ nplanes ] >> planepy[ nplanes ] >> planepz[ nplanes ];
    infile1 >> planennx[ nplanes ] >> planenny[ nplanes ] >> planennz[ nplanes ];
    if( !infile1.good() ) break;

    if( verbose )
    {
      std::cout << "plane n(" << planenx[ nplanes ] << "," << planeny[ nplanes ]
	      << "," << planenz[ nplanes ] << ")";
      std::cout << " P(" << planepx[ nplanes ] << "," << planepy[ nplanes ]
	      << "," << planepz[ nplanes ] << ")";
      std::cout << " out(" << planennx[ nplanes ] << "," << planenny[ nplanes ]
	      << "," << planennz[ nplanes ] << ")";
      std::cout << std::endl;
    }

    nplanes++;
  }

  int naxis = 0; // number of axes 
  double vaxisx[ maxaxis ], vaxisy[ maxaxis ], vaxisz[ maxaxis ];
  double paxisx[ maxaxis ], paxisy[ maxaxis ], paxisz[ maxaxis ];

  std::ifstream infile2( argv[ 5 ] );
  if( !infile2.good() )
  {
    std::cout << "could not open " << argv[ 5 ] << "\n";
  }

  while( naxis < maxaxis )
  {
    infile2 >> vaxisx[ naxis ] >> vaxisy[ naxis ] >> vaxisz[ naxis ];
    infile2 >> paxisx[ naxis ] >> paxisy[ naxis ] >> paxisz[ naxis ];

    if( !infile2.good() ) break;

    if( verbose )
    {
      std::cout << "axis v(" << vaxisx[ naxis ] << "," << vaxisy[ naxis ]
	      << "," << vaxisz[ naxis ] << ")";
      std::cout << " p(" << paxisx[ naxis ] << "," << paxisy[ naxis ]
	      << "," << paxisz[ naxis ] << ")";
      std::cout << std::endl;
    }

    naxis++;
  }

  int nholes = 0; // number of holes
  double vhaxisx[ maxholes ], vhaxisy[ maxholes ], vhaxisz[ maxholes ];
  double phaxisx[ maxholes ], phaxisy[ maxholes ], phaxisz[ maxholes ];
  double Rb[ maxholes ], Lb[ maxholes ];

  std::ifstream infile3( argv[ 3 ] );
  if( !infile3.good() )
  {
    std::cout << "could not open " << argv[ 3 ] << "\n";
  }

  while( nholes < maxholes )
  {
    infile3 >> vhaxisx[ nholes ] >> vhaxisy[ nholes ] >> vhaxisz[ nholes ];
    infile3 >> phaxisx[ nholes ] >> phaxisy[ nholes ] >> phaxisz[ nholes ];
    infile3 >> Rb[ nholes ] >> Lb[ nholes ];

    if( !infile3.good() ) break;

    if( verbose )
    {
      std::cout << "hole axis v(" << vhaxisx[ nholes ] << "," << vhaxisy[ nholes ] << "," << vhaxisz[ nholes ] << ")";
      std::cout << " p(" << phaxisx[ nholes ] << "," << phaxisy[ nholes ]
	      << "," << phaxisz[ nholes] << ") Rb=" << Rb[ nholes ] 
	      << " Lb=" << Lb[ nholes ] << std::endl;
      std::cout << std::endl;
    }
  }

  gmsh::initialize();

  if( verbose ) gmsh::option::setNumber("General.Terminal", 1);

  gmsh::open( argv[ 1 ] );

  // get 2D physical groups
  std::vector<std::pair<int, int> > physicalGroups;
  gmsh::model::getPhysicalGroups(physicalGroups, 2);
  if( verbose ) std::cout << "-- physical groups " << std::endl;
  std::string physicalGroupName = "";
  if( verbose )
    for( unsigned int i = 0; i < physicalGroups.size(); i++)
    {
      std::cout << "-- " << physicalGroups[ i ].first << " " << physicalGroups[ i ].second;
      gmsh::model::getPhysicalName(2, physicalGroups[ i ].second, physicalGroupName);
     std::cout << " " << physicalGroupName << std::endl;
    } 

  // get all node coordinates
  std::vector<std::size_t> nodeTags;
  std::vector<double> nodeCoords, nodeParams;

//  if( physicalGroup > 0 )
//  {
//    gmsh::model::getPhysicalName(2, physicalGroup, physicalGroupName);
//    std::cout << "-- get nodes and coordinates for " << physicalGroup 
//           << " " << physicalGroupName << std::endl;
//    gmsh::model::mesh::getNodesForPhysicalGroup(2, 1, nodeTags, nodeCoords);
//  }
//  else
//  {

//  std::cout << "-- get nodes and coordinates" << std::endl;
  gmsh::model::mesh::getNodes(nodeTags, nodeCoords, nodeParams, -1, -1);
//  std::cout << "-- node params size " << nodeParams.size() << std::endl;

//  }

  // hash to get correct node tag fast
  if( verbose ) std::cout << "-- create node hash" << std::endl;
  unsigned int max_node_tag = nodeTags.size()+1;
  unsigned int nodeHash[ max_node_tag ];
  for( unsigned int i = 0; i < max_node_tag; i++)
  {
    if( nodeTags[ i ] >= 1 && nodeTags[ i ] <= max_node_tag )
    {
      nodeHash[ nodeTags[ i ] ] = i;
      if( verbose ) std::cout << nodeTags[ i ] << " -> " << i << std::endl;
    }
  }

  // get all 2D elements
  std::vector<int> elemTypes;
  std::vector<std::vector<std::size_t> > elemTags, elemNodeTags;
  gmsh::model::mesh::getElements(elemTypes, elemTags, elemNodeTags, -1, -1);

//  std::cout << "-- " << elemTypes.size() << " element types" << std::endl;

  if( verbose )
  {
    std::cout << "element type   number of element tags" << std::endl;
    for( unsigned int i = 0; i < elemTags.size(); i++)
      std::cout << elemTypes[i] << "              " 
	        << elemTags[ i ].size() << std::endl; 

    std::cout <<  "element type   number of element node tags" << std::endl;
    for( unsigned int i = 0; i < elemNodeTags.size(); i++)
      std::cout << elemTypes[i] << "              " 
                << elemNodeTags[ i ].size() << std::endl; 

//    std::cout << "-- mesh element tags: ";
//    for( unsigned int i = 0; i < 4; i++)
//      std::cout << " " << elemTags[1][i];
//    std::cout << std::endl;

//    std::cout << "-- sample of mesh element type 2 (triangle) tags: " << std::endl;
//    for( unsigned int i = 0; i < 4; i++)
//      if( elemTypes[ i ] == 2 )
//      {
//        for( unsigned int j = 0; j < 10; j++)
//      		std::cout << " " << elemTags[i][j];
//        std::cout << std::endl;
//      }

//    std::cout << "-- sample of mesh element type 2 (triangle) node tags: " 
//	      << std::endl;
//    for( unsigned int i = 0; i < 4; i++)
//      if( elemTypes[ i ] == 2 )
//      {
//        for( unsigned int j = 0; j < 10; j++)
//      		std::cout << " " << elemNodeTags[i][j];
//        std::cout << std::endl;
//      }

  }

  std::vector<int> viewTags;
  gmsh::view::getTags( viewTags );

  std::vector<std::string> dataTypes;
  std::vector<int> numElements;
  std::vector<std::vector<double> > data;
  if( viewTags.size() == 1)
  {
    gmsh::view::getListData(viewTags[0], dataTypes, numElements, data);
    if( verbose )
    {
      std::cout << "-- " << numElements[0] << " data elements" << std::endl;
      for( unsigned int i = 0; i < dataTypes.size(); i++)
        std::cout << dataTypes[ i ] << " ";
      for( unsigned int i = 0; i < numElements.size(); i++)
        std::cout << numElements[ i ] << " ";
      std::cout << std::endl;
    }
  }

  int nbin = 400;
  double mindist = 0, maxdist = 0.1;
  double minangle = 0, maxangle = M_PI;

  if( mindist == maxdist )
  {
    mindist *= 0.9;
    maxdist *= 1.1;
  }
  if( minangle == maxangle )
  {
    minangle *= 0.9;
    maxangle *= 1.1;
  }

  gsl_histogram * hdist = gsl_histogram_alloc( nbin );
  gsl_histogram_set_ranges_uniform ( hdist, mindist, maxdist );

  gsl_histogram * hangle = gsl_histogram_alloc( nbin );
  gsl_histogram_set_ranges_uniform ( hangle, minangle, maxangle);

  if( printview ) std::cout << "View \"Surface current jm[A/m]\" {" << std::endl;  
  // area weight center coordinates
  double xc = 0, yc = 0, zc =0;

  int row2 = -1; 
  for( unsigned int i = 0; i < elemTags.size(); i++) 
    if( elemTypes[ i ] == 2 ) row2 = i;

  std::vector<double> P0, P1, P2, P3, Pc, Pd, Pc2, nv, nv2, jm, jm2, tv;
  std::vector<double> npln, ppln, nnpln, axisv, axisp;
  std::vector<double> u, v;
  Pc2.push_back(0);
  Pc2.push_back(0);
  Pc2.push_back(0);
  jm2.push_back(0);
  jm2.push_back(0);
  jm2.push_back(0);
  nv2.push_back(0);
  nv2.push_back(0);
  nv2.push_back(1);

  double da=-1; // element area 
  double A = 0; // total area 
  double angle = 0; // angle between jm vectors
  int nangle = 0; // number of time high angle > pi/2 was seen
  double dist = 0; // distance between centroids
  double daxis = 0; // distance to symmetry axis
  double daxis2 = 0; // distance to symmetry axis moved by surface normal
  double dpoints = 0; // distance between two points
  double donaxis = 0; // distance along hole symmetry axis
  double len = 0; // vector length
  bool holefound = false;
  bool planefound = false;
  bool axisfound = false;
  int k = 0;
  double vdir = 0;
  double jmsign = 0;
  double chir = 0;

  unsigned int elemNodeTag = 0;
  unsigned int elemNodeTagHash = 0;
  double nodex = 0, nodey = 0, nodez = 0;

  // minimum and maximum coordinates for bounding box
  double xmin = 1e20, xmax = -1e20;
  double ymin = 1e20, ymax = -1e20;
  double zmin = 1e20, zmax = -1e20;

  int ntriangle = 0;
  if( row2 < 0 ) 
  {
    std::cout << "-- no triangle found" << std::endl;
  }
  else
  {
    if( verbose ) std::cout << "-- " << elemNodeTags[ row2 ].size() 
	    << " element node tags\n";

    for( unsigned int j = 0; j < elemNodeTags[ row2 ].size() ; j += 3)
    {
      elemNodeTag = elemNodeTags[ row2 ][ j ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug )
        std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P0.erase (P0.begin(), P0.end());
      P0.push_back( nodex/scale );
      P0.push_back( nodey/scale );
      P0.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row2 ][ j + 1 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash;

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P1.erase (P1.begin(), P1.end());
      P1.push_back( nodex/scale );
      P1.push_back( nodey/scale );
      P1.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row2 ][ j + 2 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash;

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P2.erase (P2.begin(), P2.end());
      P2.push_back( nodex/scale );
      P2.push_back( nodey/scale );
      P2.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      Pc.erase (Pc.begin(), Pc.end());
      iiflucks::triangleCentroid(P0, P1, P2, Pc);

      nv.erase (nv.begin(), nv.end());
      da = iiflucks::triangleAreaNormal(P0, P1, P2, nv);

      jm.erase (jm.begin(), jm.end());
      iiflucks::crossProduct(M, nv, jm);
      dist = iiflucks::distancePoints( Pc, Pc2 );

      // calculate two vectors u and v defining plane
      u.erase (u.begin(), u.end());
      u.push_back( P1[ 0 ] - P0[ 0 ] );
      u.push_back( P1[ 1 ] - P0[ 1 ] );
      u.push_back( P1[ 2 ] - P0[ 2 ] );

      len = iiflucks::vectorLength( u );

      u.erase (u.begin(), u.end());
      u.push_back( ( P1[ 0 ] - P0[ 0 ] ) / len );
      u.push_back( ( P1[ 1 ] - P0[ 1 ] ) / len );
      u.push_back( ( P1[ 2 ] - P0[ 2 ] ) / len );

      v.erase (v.begin(), v.end());
      iiflucks::crossProduct(u, nv, v);

// test if point is inside hole bounding box to get correct inward surface
// normal direction
      holefound = false;
      k = 0;
      while( k < nholes && !holefound )
      {
        axisv.erase (axisv.begin(), axisv.end());
        axisv.push_back( vhaxisx[ k ] );
        axisv.push_back( vhaxisy[ k ] );
        axisv.push_back( vhaxisz[ k ] );

        axisp.erase (axisp.begin(), axisp.end());
        axisp.push_back( phaxisx[ k ] );
        axisp.push_back( phaxisy[ k ] );
        axisp.push_back( phaxisz[ k ] );

        daxis = iiflucks::distancePointLine(axisv, axisp, Pc);

	if( daxis < Rb[ k ] )
        {
          Pd.erase (Pd.begin(), Pd.end());
          Pd.push_back( phaxisx[ k ] );
          Pd.push_back( phaxisy[ k ] );
          Pd.push_back( phaxisz[ k ] );

          dpoints = iiflucks::distancePoints(Pd, Pc);

          donaxis = sqrt( dpoints * dpoints - daxis * daxis );

          if( donaxis < Lb[ k ] )
          {
            if( verbose ) std::cout << " hole=" << k << " ";
            holefound = true;

            Pd.erase (Pd.begin(), Pd.end());
            Pd.push_back( Pc[ 0 ] + 0.1 * nv[ 0 ] * Rb[ k ] );
            Pd.push_back( Pc[ 1 ] + 0.1 * nv[ 1 ] * Rb[ k ] );
            Pd.push_back( Pc[ 2 ] + 0.1 * nv[ 2 ] * Rb[ k ] );

            daxis2 = iiflucks::distancePointLine(axisv, axisp, Pd);

            if( daxis2 > daxis  ) 
            {
              if( verbose ) std::cout << "hole axis=" << k << " outward ";
              jmsign = -1;
            }   
            else if( daxis2 < daxis )
            {
              if( verbose ) std::cout << "hole axis=" << k << " inward ";   
              jmsign = +1;
            }
          }
	}

	k++;
      }

// test if point is on one of the predefined planes to get correct
// outward surface normal direction
      planefound = false;
      k = 0;
      while( k < nplanes && !planefound ) 
      {
        npln.erase (npln.begin(), npln.end());
        npln.push_back( planenx[ k ] );
        npln.push_back( planeny[ k ] );
        npln.push_back( planenz[ k ] );

	ppln.erase (ppln.begin(), ppln.end());
        ppln.push_back( planepx[ k ] );
        ppln.push_back( planepy[ k ] );
        ppln.push_back( planepz[ k ] );

	if( iiflucks::pointOnPlane( npln, ppln, Pc) )
        {
          if( verbose ) std::cout << " plane=" << k << " ";
          planefound = true;

          nnpln.erase (nnpln.begin(), nnpln.end());
          nnpln.push_back( planennx[ k ] );
          nnpln.push_back( planenny[ k ] );
          nnpln.push_back( planennz[ k ] );

	  vdir = iiflucks::dotProduct( nv, nnpln );
	  if( vdir < 0 ) 
          {
            if( verbose ) std::cout << "opposite dir ";
            jmsign = -1;
	  }   
	  else if( vdir > 0 )
          {
            if( verbose ) std::cout << "same dir ";   
            jmsign = +1;
	  }
	}

	k++;
      }

// if point does not belong to predefined plane or is not inside hole
// bounding box, test for symmetry axis
      if( !( planefound || holefound ) )
      {
        axisfound = false;
        k = 0;
        while( k < naxis && !axisfound ) 
        {
          axisv.erase (axisv.begin(), axisv.end());
          axisv.push_back( vaxisx[ k ] );
          axisv.push_back( vaxisy[ k ] );
          axisv.push_back( vaxisz[ k ] );

          if( iiflucks::vectorAnyPlane(axisv, nv) )
          {
            axisfound = true;
            axisp.erase (axisp.begin(), axisp.end());
            axisp.push_back( paxisx[ k ] );
            axisp.push_back( paxisy[ k ] );
            axisp.push_back( paxisz[ k ] );

            daxis = iiflucks::distancePointLine(axisv, axisp, Pc);

            Pd.erase (Pd.begin(), Pd.end());
            Pd.push_back( Pc[ 0 ] + nv[ 0 ]*sqrt( da ) );
            Pd.push_back( Pc[ 1 ] + nv[ 1 ]*sqrt( da ) );
            Pd.push_back( Pc[ 2 ] + nv[ 2 ]*sqrt( da ) );

            daxis2 = iiflucks::distancePointLine(axisv, axisp, Pd);

            if( daxis2 > daxis  ) 
            {
              if( verbose ) std::cout << "axis=" << k << " outward ";
              jmsign = +1;
            }   
            else if( daxis2 < daxis )
            {
              if( verbose ) std::cout << "axis=" << k << " inward ";   
              jmsign = -1;
            }

	  }
          k++;
        }
      }

      if( printview )
      {
        std::cout << "VP(" << Pc[0] << "," << Pc[1] << "," << Pc[2] << ")";
        std::cout << "{" << jmsign*jm[0] << "," << jmsign*jm[1] << "," << jmsign*jm[2] << "};" << std::endl;
      }
      else if( printplane )
      {
        std::cout << Pc[0] << " " << Pc[1] << " " << Pc[2] << " ";
        std::cout << u[0] << " " << u[1] << " " << u[2] << " ";
        std::cout << v[0] << " " << v[1] << " " << v[2] << " ";
        std::cout << da;
      }
      else
      {
        std::cout << Pc[0] << " " << Pc[1] << " " << Pc[2] << " ";
        std::cout << jmsign*jm[0] << " " << jmsign*jm[1] << " " << jmsign*jm[2] << " " << da;
        if( verbose ) std::cout << " " << dist;

        if( verbose ) std::cout << " " << nv[0] << " " << nv[1] << " " << nv[2];
      }

      angle = iiflucks::angleVectors(nv, nv2);

      gsl_histogram_increment( hdist, dist );
      gsl_histogram_increment( hangle, angle );

      if( angle != -10 )
      {
        if( fabs( angle ) > M_PI_2 ) 
	{
          if( verbose ) std::cout << " angle=" << angle;
          nangle++;
	}
      }

      jm2.erase (jm2.begin(), jm2.end());
      jm2.push_back( jm[ 0 ] );
      jm2.push_back( jm[ 1 ] );
      jm2.push_back( jm[ 2 ] );

      Pc2.erase (Pc2.begin(), Pc2.end());
      Pc2.push_back( Pc[ 0 ] );
      Pc2.push_back( Pc[ 1 ] );
      Pc2.push_back( Pc[ 2 ] );

      nv2.erase (nv2.begin(), nv2.end());
      nv2.push_back( nv[ 0 ] );
      nv2.push_back( nv[ 1 ] );
      nv2.push_back( nv[ 2 ] );

      if( !printview ) std::cout << std::endl;

      xc += da * Pc[0];
      yc += da * Pc[1];
      zc += da * Pc[2];

      A += da;

      ntriangle++;
    }
  }

  int row3 = -1; 
  for( unsigned int i = 0; i < elemTags.size(); i++) 
    if( elemTypes[ i ] == 3 ) row3 = i;

  int nquadrangle = 0;
  if( row3 < 0 ) 
  {
    if( verbose ) std::cout << "-- no quadrangle found" << std::endl;
  }
  else
  {
    if( verbose ) std::cout << "-- " << elemNodeTags[ row3 ].size() 
	    << " element node tags\n";

    for( unsigned int j = 0; j < elemNodeTags[ row3 ].size() ; j += 4)
    {
//      jx = 0; jy = 0; jz = 0;

      elemNodeTag = elemNodeTags[ row3 ][ j ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug )
        std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash; 

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P0.erase (P0.begin(), P0.end());
      P0.push_back( nodex/scale );
      P0.push_back( nodey/scale );
      P0.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row3 ][ j + 1 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash;

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P1.erase (P1.begin(), P1.end());
      P1.push_back( nodex/scale );
      P1.push_back( nodey/scale );
      P1.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row3 ][ j + 2 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash;

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P2.erase (P2.begin(), P2.end());
      P2.push_back( nodex/scale );
      P2.push_back( nodey/scale );
      P2.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      elemNodeTag = elemNodeTags[ row3 ][ j + 3 ];
      elemNodeTagHash = nodeHash[ elemNodeTag ];
      if( debug ) std::cout << "-- " << j << " " << elemNodeTag << " -> "
		  << elemNodeTagHash;

      nodex = nodeCoords[ 3 * elemNodeTagHash ];
      nodey = nodeCoords[ 3 * elemNodeTagHash + 1 ];
      nodez = nodeCoords[ 3 * elemNodeTagHash + 2 ];
      if( debug ) std::cout << " (" << nodex << "," << nodey << "," 
	      << nodez << ")" << std::endl;

      P3.erase (P3.begin(), P3.end());
      P3.push_back( nodex/scale );
      P3.push_back( nodey/scale );
      P3.push_back( nodez/scale );

      if( nodex < xmin ) xmin = nodex;
      if( nodex > xmax ) xmax = nodex;
      if( nodey < ymin ) ymin = nodey;
      if( nodey > ymax ) ymax = nodey;
      if( nodez < zmin ) zmin = nodez;
      if( nodez > zmax ) zmax = nodez;

      Pc.erase (Pc.begin(), Pc.end());
      iiflucks::quadrangleCentroid(P0, P1, P2, P3, Pc);
      da = iiflucks::quadrangleArea(P0, P1, P2, P3);
      std::cout << Pc[0] << " " << Pc[1] << " " << Pc[2] << " ";
      std::cout << 0 << " " << 0 << " " << 0 << " " << da << std::endl;

      A += da;
      nquadrangle++;

    }
  }

  xc /= A;
  yc /= A;
  zc /= A;

  xmin /= scale; xmax /= scale;
  ymin /= scale; ymax /= scale;
  zmin /= scale; zmax /= scale;

  if( verbose )
  {
    std::cout << std::endl;
    std::cout << ntriangle << " triangle and " << nquadrangle << " quadrangle" << std::endl;
    std::cout << "Total area " << A << std::endl;
    std::cout << "Area center (" << xc << "," << yc << "," << zc << ")" << std::endl;
    std::cout << "Bounding box x[" << xmin << "," << xmax << "] ";
    std::cout << "y[" << ymin << "," << ymax << "] ";
    std::cout << "z[" << zmin << "," << zmax << "]" << std::endl;
    std::cout << "jm-jm2 angle > pi/2 " << nangle << " times" << std::endl;
  }

  gmsh::finalize();

  if( printview ) std::cout << "};" << std::endl;  

  FILE *outhdist;
  outhdist = fopen( "hdist.txt", "w");
  if( outhdist == NULL )
  {
    std::cout << "Could not write 'hdist.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhdist, hdist, "%g", "%g");
    fclose( outhdist );
  }
  gsl_histogram_free( hdist );

  FILE *outhangle;
  outhangle = fopen( "hangle.txt", "w");
  if( outhangle == NULL )
  {
    std::cout << "Could not write 'hangle.txt' file" << std::endl;
  }
  else
  {
    gsl_histogram_fprintf( outhangle, hangle, "%g", "%g");
    fclose( outhangle );
  }
  gsl_histogram_free( hangle );

  return 0;
}
