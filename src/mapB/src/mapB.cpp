//
// Read position-current_density-volume file (coil) and calculate magnetic
// field map.
//
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Tue Mar 26 12:10:49 PM CDT 2024
// Edit: Thu Jul  4 10:52:23 AM CDT 2024
//
// Jaakko Koivuniemi
//

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"
#include "constants.h"
#include "Histograms.h"
#include "MonteCarlo.h"
#include "FieldMap.h"

void printusage()
{
  std::cout << "Usage: mapB file.yaml x0/r0 y0/phi0 z0 dx/dr dy/dphi dz steps Nrand [cyl|mc] [v|d]" << std::endl;
}

void printversion()
{
  std::cout << "mapB v. 20240704, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Read yaml-file (coil) and calculate magnetic field at grid positions r2 = (x0 + l * dx, y0 + k * dy, z0 + j * dz)." << std::endl;
  std::cout << "j = [-steps,steps+1], k = [-steps,steps+1], l = [-steps, steps+1]." << std::endl;
  std::cout << "For one or two dimensional map set dx, dy or dz to zero." << std::endl;
  std::cout << std::endl;
  std::cout << "Model for fractional field is" << std::endl;
  std::cout << std::endl;
  std::cout << "dB(r2) = u0/4*pi * J(r1) x (r2 - r1) * dv/|r2 - r1|^3" << std::endl;
  std::cout << std::endl;
  std::cout << "For position-current-density-dv files sum of dB(r2) is taken for the total field. With parametric coil models 10^Nrand random samples are calculated to estimate total field B(r2)." << std::endl;
  std::cout << "Here J = (Jx, Jy, Jz) is current density vector in volume dv at position r1." << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  bool debug = false;
  bool montecarlo_pdv = false;
  int test = 0;
  bool cylindrical = false; // cylindrical grid

  std::vector<double> axis;
  std::vector<double> tr;
  std::vector<double> Bv;
  std::vector<double> Berrv;
  double theta = 0;
  double x0 = 0; // center position where field is calculated 
  double y0 = 0;
  double z0 = 0;
  double dx2 = 0; // step field map grid
  double dy2 = 0;
  double dz2 = 0.01;
  double steps = 1; // steps in field map grid
  double nr = 0; // exponent for number of random samples
  std::string fname = ""; // pdv-file name
  std::string opt = ""; // optional switch for cylindrical coordinates

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-map1", 11 ) == 0 )
  {
    // test circle of diameter 2 cm with 100 um wire and 1 mA current
    test = 1;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;
  }
  else if( strncmp( argv[ 1 ], "--test-map2", 11 ) == 0 )
  {
    // test helix 1 cm diameter, 1 cm long and 10 turns with 1 mA current
    test = 2;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    x0 = 0; // center position where field is calculated  
    y0 = 0;
    z0 = 5e-3;
    dx2 = 0; // step field map grid 
    dy2 = 0;
    dz2 = 5e-3;
    steps = 0; // steps in field map grid 
    nr = 6; // exponent for number of random samples  
  }
  else if( strncmp( argv[ 1 ], "--test-map3", 11 ) == 0 )
  {
    // test line 10 cm long with 1 mA current
    test = 3;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    x0 = 0.01; // center position where field is calculated
    y0 = 0;
    z0 = 0.05;
    dx2 = 0; // step field map grid
    dy2 = 0;
    dz2 = 0;
    steps = 0; // steps in field map grid
    nr = 5; // exponent for number of random samples
  }
  else if( strncmp( argv[ 1 ], "--test-map4", 11 ) == 0 )
  {
    // test 1 cm ring on 4 cm radius surface with 1 mA current
    test = 4;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    x0 = 0.04; // center position where field is calculated
    y0 = 0;
    z0 = 0;
    dx2 = 0; // step field map grid
    dy2 = 0;
    dz2 = 0;
    steps = 0; // steps in field map grid
    nr = 5; // exponent for number of random samples
  }
  else if( strncmp( argv[ 1 ], "--test-map5", 11 ) == 0 )
  {
    // test one turn planar RT 2 cm long, 1 cm end, 5 mm corners with 1 mA current
    test = 5;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    x0 = 0; // center position where field is calculated
    y0 = 0;
    z0 = 0;
    dx2 = 0; // step field map grid
    dy2 = 0;
    dz2 = 0;
    steps = 0; // steps in field map grid
    nr = 5; // exponent for number of random samples
  }
  else if( strncmp( argv[ 1 ], "--test-map6", 11 ) == 0 )
  {
    // test one turn cylindrical RT 4 cm long on 4 cm radius surface with 5 mm end radius and 1 mA current
    test = 6;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    x0 = 0.04; // center position where field is calculated
    y0 = 0;
    z0 = 0;
    dx2 = 0; // step field map grid
    dy2 = 0;
    dz2 = 0;
    steps = 0; // steps in field map grid
    nr = 5; // exponent for number of random samples
  }
  else if( strncmp( argv[ 1 ], "--test-map7", 11 ) == 0 )
  {
    // test 10 cm line with 1 mA current 1 cm from origin
    test = 7;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    x0 = 0; // center position where field is calculated
    y0 = 0;
    z0 = 0;
    dx2 = 0; // step field map grid
    dy2 = 0;
    dz2 = 0;
    steps = 0; // steps in field map grid
    nr = 5; // exponent for number of random samples
  }
  else if( strncmp( argv[ 1 ], "--test-map8", 11 ) == 0 )
  {
    // line on 2 cm radius sphere t=[-pi/4,pi/4], azimuth 0.46365, 1 mA current
    test = 8;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    x0 = 0; // center position where field is calculated
    y0 = 0;
    z0 = 0;
    dx2 = 0; // step field map grid
    dy2 = 0;
    dz2 = 0;
    steps = 0; // steps in field map grid
    nr = 5; // exponent for number of random samples
  }
  else if( strncmp( argv[ 1 ], "--test-map9", 11 ) == 0 )
  {
    // half loop on 2 cm radius sphere with 1 mA current
    test = 9;
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    x0 = 0; // center position where field is calculated
    y0 = 0;
    z0 = 0;
    dx2 = 0; // step field map grid
    dy2 = 0;
    dz2 = 0;
    steps = 0; // steps in field map grid
    nr = 5; // exponent for number of random samples
  }
  else if( strncmp( argv[ 1 ],  "--test-quat", 11 ) == 0)
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    iiflucks::testQuaternion( verbose );

    return 0;
  }
  else if( strncmp( argv[ 1 ],  "--test-vect", 11 ) == 0)
  {
    if( argc == 3 ) verbose = true;
    if( argc == 3 && strncmp( argv[ 2 ], "d", 1 ) == 0 ) debug = true;

    iiflucks::testVector( verbose );

    return 0;
  }
  else
  {
    if( argc < 10 )
    {
      printusage();
      return 0;
    }

    if( argc == 11 )
    {
      opt = argv[ 10 ];
      if( opt == "cyl" )
      {
        cylindrical = true;
      }
      else if( opt == "mc" )
      {
        montecarlo_pdv = true;
      }
      else verbose = true;
      if( opt == "d" ) debug = true;
    }

    if( argc == 12 )
    {
      verbose = true;
      opt = argv[ 10 ];
      if( opt == "cyl" )
      {
        cylindrical = true;
      }
      else if( opt == "mc" )
      {
        montecarlo_pdv = true;
      }
      if( strncmp( argv[ 11 ], "d", 1 ) == 0 ) debug = true;
    }

    x0 = atof( argv[ 2 ] ); // center position where field is calculated 
    y0 = atof( argv[ 3 ] );
    z0 = atof( argv[ 4 ] );
    dx2 = atof( argv[ 5 ] ); // step field map grid
    dy2 = atof( argv[ 6 ] );
    dz2 = atof( argv[ 7 ] );
    steps = atoi( argv[ 8 ] ); // steps in field map grid
    nr = atof( argv[ 9 ] ); // exponent for number of random samples
  }

  // position coordinates for small volume dv1, initialize with test data
  // 2 cm diameter ring at (0, 0, 0), 100 um wire and current 1 mA
  double x1c[ constants::maxpoints ] = {0.00987688, 0.00891007, 0.00707107, 0.0045399, 0.00156434, -0.00156434, -0.0045399, -0.00707107, -0.00891007, -0.00987688, -0.00987688, -0.00891007, -0.00707107, -0.0045399, -0.00156434, 0.00156434, 0.0045399, 0.00707107, 0.00891007, 0.00987688}; 

  double y1c[ constants::maxpoints ] = { 0.00156434, 0.0045399, 0.00707107, 0.00891007, 0.00987688, 0.00987688, 0.00891007, 0.00707107, 0.0045399, 0.00156434, -0.00156434, -0.0045399, -0.00707107,  -0.00891007, -0.00987688, -0.00987688, -0.00891007, -0.00707107, -0.0045399, -0.00156434}; 

  double z1c[ constants::maxpoints ] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 

  double dv1[ constants::maxpoints ] = {2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11, 2.4674e-11};

  // vector field with same position coordinates, initialize with test data
  double J1x[ constants::maxpoints ] = {-19917.9, -57803.9, -90031.6, -113446, -125756, -125756, -113446, -90031.6, -57803.9, -19917.9, 19917.9, 57803.9, 90031.6, 113446, 125756, 125756, 113446, 90031.6, 57803.9, 19917.9}; 

  double J1y[ constants::maxpoints ] = {125756, 113446, 90031.6, 57803.9, 19917.9, -19917.9, -57803.9, -90031.6, -113446, -125756, -125756, -113446, -90031.6, -57803.9, -19917.9, 19917.9, 57803.9, 90031.6, 113446, 125756}; 

  double J1z[ constants::maxpoints ] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 

  double x = 0, y = 0, z = 0;
  int nxe = steps +1, nye = steps +1, nze = steps +1;
  int nxs = steps, nys = steps, nzs = steps;

  if( cylindrical )
  {
    nxs = 0;
    nye = steps;
  }
  if( dx2 == 0 ) 
  {
    nxs = 0;
    nxe = 1;
  }
  if( dy2 == 0 )
  {
    nys = 0;
    nye = 1;
  }
  if( dz2 == 0 )
  {
    nzs = 0;
    nze = 1;
  }

  int np1 = 0;
  double V1 = 0;
  if( test == 1 )
  {
    np1 = 20;

    for( int i = 0; i < np1; i++)
    {
      if( verbose )
      {
        std::cout << x1c[i] << " " << y1c[i] << " " << z1c[i] << " ";
        std::cout << J1x[i] << " " << J1y[i] << " " << J1z[i] << " ";
        std::cout << dv1[i] << std::endl;
      }
  
      V1 += dv1[ i ];
    }

    if( verbose ) std::cout << "" << np1 << " test points with volume ";
    std::cout << V1 << std::endl;
  } 

  Histograms *histograms = new Histograms("Inductance calculation");

  YAML::Node hist_ranges;
  bool ranges = true;
  try
  {
    hist_ranges = YAML::LoadFile( ".indmtrx.yaml" );
  }

  catch(...)
  {
    std::cerr << "Parsing '.indmtrx.yaml' failed, finding histogram ranges from data." << std::endl;
    ranges = false;
  }

  if( ranges ) histograms->ParseRangesYaml( hist_ranges );
  histograms->SetRanges( false );

  MonteCarlo *calc = new MonteCarlo("Magnetic field", nr, 0, 0, histograms);
  if( debug ) calc->Debug();

  std::vector<YAML::Node> coils;

  if( test == 1 )
  {
    coils = YAML::LoadAll("{current: {refdes: L1, current_A: 1e-3, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], file: test} }");
  }
  else if( test == 2 )
  {
    coils = YAML::LoadAll("{integrate: {dt: 1e-5, t1: 0, t2: 62.83185307179586477}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], helix: {radius_m: 0.005, length_m: 0.01, turns: 10.0 } } }");
  }
  else if( test == 3 )
  {
    coils = YAML::LoadAll("{integrate: {dt: 1e-7, t1: 0, t2: 0.1, dr_m: 1e-7, r1_m: 0, r2_m: 5e-5}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], line: } }");
  }
  else if( test == 4 )
  {
    coils = YAML::LoadAll("{integrate: {dt: 0.001, t1: 0, t2: 6.283185307179586477}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], ringcylinder: {radius_m: 0.005, cylinder_radius_m: 0.04} } }");
  }
  else if( test == 5 )
  {
    coils = YAML::LoadAll("{integrate_sections: {lc: 1e-5, t1: [-0.01, 0.0, 0.005, 1.57079632679, 0.01, 3.14159265359, -0.005, 4.71238898038], t2: [0.01, 1.57079632679, -0.005, 3.14159265359, -0.01, 4.71238898038, 0.005, 6.28318530718]}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], racetrack_planar: {radius_m: 0.005, length_m: 0.02, end_length_m: 0.01, winding_width_m: 0.0, thickness_m: 0.0, layer_turns: 1, layers: 1} } }");
  }
  else if( test == 6 )
  {
    coils = YAML::LoadAll("{integrate_sections: {lc: 1e-5, t1: [-0.02, 0.0, 0.02, 3.14159265358979], t2: [0.02, 3.14159265358979, -0.02, 6.28318530717959]}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], racetrack_cylinder: {radius_m: 0.005, cylinder_radius_m: 0.04, length_m: 0.04, radial_width_m: 0.01, thickness_m: 0.018, layer_turns: 1, layers: 1} } }");
  }
  else if( test == 7 )
  {
    coils = YAML::LoadAll("{integrate: {dt: 1e-5, t1: -0.05, t2: 0.05, dr_m: 1e-7, r1_m: 0, r2_m: 5e-5}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.01,0.0,0.0], line: } }");
  }
  else if( test == 8 )
  {
    coils = YAML::LoadAll("{integrate: {dt: 1e-5, t1: -0.785398163397, t2: 0.785398163397}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], linesphere: {radius_m: 0.01, sphere_radius_m: 0.02} } }");
  }
  else if( test == 9 )
  {
    coils = YAML::LoadAll("{integrate: {dt: 1e-5, t1: -1.0, t2: +1.0}, coil: {refdes: L1, wire_current_A: 1e-3, wire_diameter_m: 1e-4, rotation_axis: [0.0,0.0,1.0], rotation_angle: 0.0, translation_m: [0.0,0.0,0.0], loopsphere: {sphere_radius_m: 0.02, altitude_rad: 0.785398, azimuth_rad: 0.392699, altitude_length_rad: 0.392699, azimuth_length_rad: 0.785398} } }");
  }
  else
  {
    coils = YAML::LoadAllFromFile( argv[ 1 ] );
  }

  Helix * helix = nullptr;
  Ring * ring = nullptr;
  RingCylinder * ringcyl = nullptr;
  Line * line = nullptr;
  LineSphere * lsphere = nullptr;
  LoopSphere * loopsphere = nullptr;
  RacetrackCylinder * rtcyl = nullptr;
  RacetrackPlanar * rtplanar = nullptr;
  Solenoid * solenoid = nullptr;

  double B; // total field
  double length; // wire length [m]
  double I1; // current from yaml-file, not used in calculations
  double dt, t1, t2; // coil parametric curves
  double dr, r1, r2; // radial integration on cross section
  double lc; // integration length scale
  std::vector<double> t1v, t2v;
  std::vector<double> P;
  int nzero = 0;
  bool file = false, montecarlo = true;

  for(std::vector<YAML::Node>::const_iterator coil = coils.begin(); coil != coils.end(); ++coil)
  {
    if( iiflucks::isHelixYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse helix" << std::endl;
      helix = iiflucks::parseHelix( *coil, dt, t1, t2, dr, r1, r2, I1, false );
      std::cout << "# " << helix->GetName() << std::endl;
    }
    else if( iiflucks::isRingYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse ring" << std::endl;
      ring = iiflucks::parseRing( *coil, dt, t1, t2, dr, r1, r2, I1, false );
      std::cout << "# " << ring->GetName() << std::endl;
    }
    else if( iiflucks::isRingCylinderYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse ring on cylindrical surface" << std::endl;
      ringcyl = iiflucks::parseRingCylinder( *coil, dt, t1, t2, dr, r1, r2, I1, false );
      std::cout << "# " << ringcyl->GetName() << std::endl;
    }
    else if( iiflucks::isLineYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse line" << std::endl;
      line = iiflucks::parseLine( *coil, dt, t1, t2, dr, r1, r2, I1, false );
      std::cout << "# " << line->GetName() << std::endl;
    }
    else if( iiflucks::isLineSphereYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse line on sphere" << std::endl;
      lsphere = iiflucks::parseLineSphere( *coil, dt, t1, t2, dr, r1, r2, I1, false );
      std::cout << "# " << lsphere->GetName() << std::endl;
    }
    else if( iiflucks::isLoopSphereYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse loop on sphere" << std::endl;
      loopsphere = iiflucks::parseLoopSphere( *coil, dt, t1, t2, dr, r1, r2, I1, false );
      std::cout << "# " << loopsphere->GetName() << std::endl;
    }
    else if( iiflucks::isRacetrackCylinderYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse racetrack_cylinder" << std::endl;
      rtcyl = iiflucks::parseRacetrackCylinder( *coil, lc, t1v, t2v, dr, r1, r2, I1, true);
      length = rtcyl->CalcLength( t1v, t2v, verbose ); // necessary to initialize model
      std::cout << "# " << rtcyl->GetName() << std::endl;
      if( verbose ) std::cout << "wire length = " << length << std::endl;
    }
    else if( iiflucks::isRacetrackPlanarYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse racetrack_planar" << std::endl;
      rtplanar = iiflucks::parseRacetrackPlanar( *coil, lc, t1v, t2v, dr, r1, r2, I1, false );
      length = rtplanar->CalcLength( t1v, t2v, verbose ); // necessary to initialize model
      std::cout << "# " << rtplanar->GetName() << std::endl;
      if( verbose ) std::cout << "wire length = " << length << std::endl;
    }
    else if( iiflucks::isSolenoidYaml( *coil ) )
    {
      if( verbose ) std::cout << "parse solenoid" << std::endl;
      solenoid = iiflucks::parseSolenoid( *coil, lc, t1v, t2v, dr, r1, r2, I1, false );
      std::cout << "# " << solenoid->GetName() << std::endl;
    }
    else if( iiflucks::isCurrentYaml( *coil ) && test == 0 )
    {
      if( verbose ) std::cout << "parse pdv-file" << std::endl;
      file = iiflucks::parseCurrent( *coil, true, I1, theta, axis, tr, fname );

      if( verbose ) std::cout << "yaml-file " << argv[ 1 ] << std::endl;
      if( verbose ) std::cout << "I1 = " << I1 << std::endl;
      if( verbose ) std::cout << "theta = " << theta << std::endl;
      if( verbose ) std::cout << "axis = (" << axis[0] << "," << axis[1] << "," << axis[2] << ")" << std::endl;
      if( verbose ) std::cout << "pdv-file " << fname << std::endl;

      np1 = iiflucks::totalVolume( fname, V1);
      if( np1 < 1 || V1 <= 0 )
      {
        std::cerr << "Failed to read file " << fname << " V1 = " << V1 << std:: endl;
        return -1;
      }
      else if( np1 >= constants::maxpoints )
      {
        std::cerr << "Maximum number of points " << constants::maxpoints << std::endl;
        return -2;
      }
      else
      {
        if( verbose ) std::cout << "np1 = " << np1 << " V1 = " << V1 << std::endl;
        np1 = iiflucks::readFile(fname , x1c, y1c, z1c, J1x, J1y, J1z, dv1);
        iiflucks::rotateTranslate(theta, axis, tr, np1, x1c, y1c, z1c, J1x, J1y, J1z);
        std::cout << "# " << fname << std::endl;
      }
    }
    else if( test == 1 ) file = true;

    montecarlo = true;
    iiflucks::zeroVector( Bv );
    for(int j = -nzs; j < nze; j++)
    {
      for(int k = -nys; k < nye; k++)
      {
        for(int l = -nxs; l < nxe; l++)
        {
          if( cylindrical )
          {
            x = ( x0 + l * dx2 ) * cos( y0 + k * dy2 );
            y = ( x0 + l * dx2 ) * sin( y0 + k * dy2 );
            z = z0 + j * dz2;
          }
          else
          {
            x = x0 + l * dx2;
            y = y0 + k * dy2;
            z = z0 + j * dz2;
          }

          if( verbose ) std::cout << "(";
          std::cout << x << "," << y << "," << z; 
          if( verbose ) std::cout << ")" << std::endl;

          iiflucks::makeVector(x, y, z, P);

	  if( iiflucks::isHelixYaml( *coil ) ) nzero = calc->Run(helix, dt, t1, t2, P);
          else if( iiflucks::isRingYaml( *coil ) ) nzero = calc->Run(ring, dt, t1, t2, P);
          else if( iiflucks::isRingCylinderYaml( *coil ) ) nzero = calc->Run(ringcyl, dt, t1, t2, P);
          else if( iiflucks::isLineYaml( *coil ) ) nzero = calc->Run(line, dt, t1, t2, P);
          else if( iiflucks::isLineSphereYaml( *coil ) ) nzero = calc->Run(lsphere, dt, t1, t2, P);
          else if( iiflucks::isLoopSphereYaml( *coil ) ) nzero = calc->Run(loopsphere, dt, t1, t2, P);
          else if( iiflucks::isRacetrackCylinderYaml( *coil ) ) nzero = calc->Run(rtcyl, lc, t1v, t2v, P);
          else if( iiflucks::isRacetrackPlanarYaml( *coil ) ) nzero = calc->Run(rtplanar, lc, t1v, t2v, P);
          else if( iiflucks::isSolenoidYaml( *coil ) ) nzero = calc->Run(solenoid, lc, t1v, t2v, P);
          else if( iiflucks::isCurrentYaml( *coil ) || test == 1 )
          {
            if( montecarlo_pdv ) nzero = calc->Run( fname, theta, axis, tr, P);
            else
            {
              Bv = iiflucks::calculateB(np1, x1c, y1c, z1c, J1x, J1y, J1z, dv1, P);
              iiflucks::zeroVector( Berrv );
              montecarlo = false;
	    }
          }

          if( montecarlo )
          {
            Bv = calc->B;
            Berrv = calc->Berr;
            histograms->SaveRangesYaml(".indmtrx.yaml", false);
            histograms->SaveHistograms("");
            histograms->SaveHistogramsB("");
          }

	  B = iiflucks::vectorLength( Bv );
          if( verbose ) std::cout << " field (";
          if( !verbose ) std::cout << ",";
          std::cout << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ] << "," << B;
          if( verbose ) std::cout << ")";
          std::cout << std::endl;

	}
      }
    }
  }

  FieldMap * Bmap = new FieldMap( calc );
//  std::cout << "Maximum field map size " << Bmap->MaxSize() << std::endl;
//  Bmap->Verbose(true);

  int intres;
  size_t neval;
  if( test == 1 )
  {
    ring = new Ring("test1", 0.01, 0, 0);
    intres = ring->B(1e-3, 0, 2 * M_PI, {0, 0, 0}, 1e-5, Bv, Berrv, neval);
    std::cout << "0,0,0," << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ];
    std::cout << "," << Berrv[ 0 ] << "," << Berrv[ 1 ] << "," << Berrv[ 2 ];
    std::cout << "," << neval << std::endl;

    Bmap->Cartesian({0, 0, 0}, {0, 0, 0}, {0, 0, 0});
//    std::cout << "Map size " << Bmap->map_size << std::endl;
    Bmap->Add(ring, 0, 2 * M_PI, 1e-3, 1e-5);
  }

  if( test == 2 )
  {
    intres = helix->B(1e-3, t1, t2, {0, 0, 0.005}, 1e-5, Bv, Berrv, neval);
    std::cout << "0,0,0.005," << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ];
    std::cout << "," << Berrv[ 0 ] << "," << Berrv[ 1 ] << "," << Berrv[ 2 ];
    std::cout << "," << neval << std::endl;

    Bmap->Spherical({0, M_PI/2, 0}, {0.02, M_PI/2, M_PI/2}, {1, 1, 1});
//    std::cout << "Map size " << Bmap->map_size << std::endl;
    Bmap->Add(helix, dt, t1, t2);
    Bmap->CartesianSpherical();
    Bmap->Print();
  }

  if( test == 3 )
  {
    intres = line->B(1e-3, t1, t2, {0.01, 0, 0.05}, Bv );
    std::cout << "0.01,0,0.05," << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ] << std::endl;

    Bmap->Cylindrical({0, 0, 0.05}, {0.01, M_PI/2, 0}, {1, 1, 0});
//    std::cout << "Map size " << Bmap->map_size << std::endl;
    Bmap->Add(line, dt, t1, t2);
    Bmap->CartesianCylindrical();
    Bmap->Print();
  }

  if( test == 4 )
  {
    intres = ringcyl->B(1e-3, 0, 2 * M_PI, {0.04, 0, 0}, 1e-5, Bv, Berrv, neval);
    std::cout << "0.04,0,0," << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ];
    std::cout << "," << Berrv[ 0 ] << "," << Berrv[ 1 ] << "," << Berrv[ 2 ];
    std::cout << "," << neval << std::endl;
  }

  if( test == 5 )
  {
    intres = rtplanar->B(1e-3, t1v, t2v, 1e-5, {0, 0, 0}, false, Bv, Berrv, neval);
    std::cout << "0,0,0," << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ];
    std::cout << "," << Berrv[ 0 ] << "," << Berrv[ 1 ] << "," << Berrv[ 2 ];
    std::cout << "," << neval << std::endl;
    Bmap->Cartesian({0, 0, 0}, {0, 0, 0}, {0, 0, 0});
//    std::cout << "Map size " << Bmap->map_size << std::endl;
    Bmap->Add(rtplanar, t1v, t2v, 1e-3, 1e-5);
  }

  if( test == 6 )
  {
    intres = rtcyl->B(1e-3, t1v, t2v, 1e-5, {0.04, 0, 0}, false, Bv, Berrv, neval);
    std::cout << "0.04,0,0," << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ];
    std::cout << "," << Berrv[ 0 ] << "," << Berrv[ 1 ] << "," << Berrv[ 2 ];
    std::cout << "," << neval << std::endl;
    Bmap->Cartesian({0.04, 0, 0}, {0, 0, 0}, {0, 0, 0});
//    std::cout << "Map size " << Bmap->map_size << std::endl;
    Bmap->Add(rtcyl, lc, t1v, t2v);
  }

  FieldMap * Bmap2 = new FieldMap();
//  std::cout << "Maximum field map size " << Bmap2->MaxSize() << std::endl;
//  Bmap2->Verbose(true);
  if( test == 7 )
  {
    Bmap->Cylindrical({0, 0, 0}, {0.01, M_PI/4, 0}, {2,2,0});
    Bmap->Add(line, dt, t1, t2);
    std::cout << "Map size " << Bmap->map_size << std::endl;
    Bmap->Print();
    Bmap2->Add( Bmap, 1 );
    Bmap2->Print();
    std::cout << "Map 2 size " << Bmap2->map_size << std::endl;
    Bmap2->Rotate( Bmap, 1, 8 );
//    Bmap2->PrintCylindrical();
    Bmap2->Print();
  }

  if( test == 8 )
  {
    lsphere = new LineSphere("test8", 0.01, 0.02, 0, 0);
    intres = lsphere->B(1e-3, -M_PI/4, M_PI/4, {0, 0, 0}, 1e-5, Bv, Berrv, neval);
    std::cout << "0,0,0," << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ];
    std::cout << "," << Berrv[ 0 ] << "," << Berrv[ 1 ] << "," << Berrv[ 2 ];
    std::cout << "," << neval << std::endl;

    Bmap->Cartesian({0, 0, 0}, {0, 0, 0}, {0, 0, 0});
//    std::cout << "Map size " << Bmap->map_size << std::endl;
    Bmap->Add(lsphere, -M_PI/4, M_PI/4, 1e-3, 1e-5);
    Bmap->Print();
  }

  if( test == 9 )
  {
    loopsphere = new LoopSphere("test9", 0.02, M_PI/4, M_PI/8, M_PI/8, M_PI/4, 0, 0);
    intres = loopsphere->B(1e-3, -1, 1, {0, 0, 0}, 1e-5, Bv, Berrv, neval);
    std::cout << "0,0,0," << Bv[ 0 ] << "," << Bv[ 1 ] << "," << Bv[ 2 ];
    std::cout << "," << Berrv[ 0 ] << "," << Berrv[ 1 ] << "," << Berrv[ 2 ];
    std::cout << "," << neval << std::endl;

    Bmap->Cartesian({0, 0, 0}, {0, 0, 0}, {0, 0, 0});
//    std::cout << "Map size " << Bmap->map_size << std::endl;
    Bmap->Add(loopsphere, -1, 1, 1e-3, 1e-5);
    Bmap->Print();
  }

  return 0;
}
