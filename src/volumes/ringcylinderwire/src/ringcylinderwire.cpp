
// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Create volume elements for wire ring. Ring center is at (R2, 0, 0) and
// current flows in counter clockwise direction.
//
// - R ring radius 
// - R2 cylinder radius 
// - phi0 start angle 
// - phi1 end angle 
// - d wire diameter
// - M number of volume elements
// - I current
//
// Thu Mar 14 10:49:47 AM CDT 2024
// Edit: Sun Mar 17 07:20:11 PM CDT 2024
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: ringcylinderwire R R2 phi0 phi1 d M I ax ay az theta xt yt zt [v]" << std::endl;
}

void printversion()
{
  std::cout << "ringcylinderwire v. 20240317, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create volume elements for wire ring or arc on cylinder surface. Ring or arc center is at (R2, 0, 0) and current flows in counter clockwise direction around x-axis. The arc starts at ( sqrt( R2^2 - R^2 cos^2( phi0 ) ), R cos( phi0 ), R sin( phi0 ) ) and ends at ( sqrt( R2^2 - R^2 cos^2( phi1 ) ), R cos( phi1 ), R sin( phi1 ) ). "; 
  std::cout << "Sweep angle phi1 - phi0 = 2 pi gives complete ring." << std::endl;
  std::cout << std::endl;
  std::cout << "- R ring radius" << std::endl;
  std::cout << "- R2 cylinder radius" << std::endl;
  std::cout << "- phi0 start angle" << std::endl;
  std::cout << "- phi1 end angle" << std::endl;
  std::cout << "- d wire diameter" << std::endl;
  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
  std::cout << std::endl;
  std::cout << "Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt)." << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  int test = 0;

  // initial values for testing
  double R = 0.01;
  double R2 = 0.04;
  double phi0 = 0;
  double phi1 = 2 * M_PI;
  double d = 0.0001;
  int M = 2000;
  double I = 1e-3;
  double ax = 0, ay = 0, az = 1;
  double theta = 0;
  double xt = 0, yt = 0, zt = 0;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-length", 13 ) == 0 )
  {
    test = 1;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-volume", 13 ) == 0 )
  {
    test = 2;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jxl", 10 ) == 0 )
  {
    test = 3;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jyl", 10 ) == 0 )
  {
    test = 4;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jzl", 10 ) == 0 )
  {
    test = 5;
    if( argc == 3 ) verbose = true;
  }
  else
  {
    if( argc < 15 )
    {
      printusage();
      return 0;
    }

    if( argc == 16 ) verbose = true;
  
    R = atof( argv[ 1 ] );
    R2 = atof( argv[ 2 ] );
    phi0 = atof( argv[ 3 ] );
    phi1 = atof( argv[ 4 ] );
    d = atof( argv[ 5 ] );
    M = atoi( argv[ 6 ] );
    I = atof( argv[ 7 ] );
    ax = atof( argv[ 8 ] );
    ay = atof( argv[ 9 ] );
    az = atof( argv[ 10 ] );
    theta = atof( argv[ 11 ] );
    xt = atof( argv[ 12 ] );
    yt = atof( argv[ 13 ] );
    zt = atof( argv[ 14 ] );
  }

  if( verbose )
  {
    std::cout << "-- ring radius " << R << std::endl;
    std::cout << "-- cylinder radius " << R2 << std::endl;
    std::cout << "-- ring start angle " << phi0 << std::endl;
    std::cout << "-- ring end angle " << phi1 << std::endl;
    std::cout << "-- wire diameter " << d << std::endl;
    std::cout << "-- volume elements " << M << std::endl;
    std::cout << "-- current " << I << std::endl;
  }

  double len = iiflucks::ringCylinderWire(R, R2, phi0, phi1, d, M, I, ax, ay, az, theta, xt, yt, zt, verbose, test);

  if( verbose ) std::cout << "-- length/volume/Jxl/Jyl/Jxl " << len << std::endl;
  else std::cout << len << std::endl;

}
