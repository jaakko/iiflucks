// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
// 
// Create volume elements for wire making continuous '8' loops gradually
// spacing wire along z-axis. Point (0, 0, 0) is between two bobbins
// with symmetry z-axes on points (0, +b/2, 0) and (0, -b/2, 0).
// Here +dz is given by coil length L and number of turns N. When winding
// the gradiometer on two parallel bobbins each with diameter 
// 2*r there are straight wire sections in between that move gradually
// along z-axis too. The wire starts from (0, 0, 0) and is would 
// counter clockwise around first bobbin and then clockwise around second
// bobbin. 
//
// - r bobbin radius
// - b distance between two gradiometer bobbin centers
// - L gradiometer coil length
// - d wire diameter
// - N number of turns
// - M number of volume elements
// - I current
//
// Tue May  7 13:12:26 CDT 2019
// Edit: Thu May 23 21:45:43 CDT 2019
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <string.h>
#include <gsl/gsl_const_mksa.h>


void printusage()
{
  std::cout << "Usage: gradient8wire r b L d N M I [v]" << std::endl;
}

void printversion()
{
  std::cout << "gradient8wire v. 20190523, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create volume elements for wire making continuous '8' loops gradually spacing wire along z-axis. Point (0, 0, 0) is between two bobbins with symmetry z-axes on points (0, +b/2, 0) and (0, -b/2, 0). Here +dz is given by coil length L and number of turns N. When winding the gradiometer on two parallel bobbins each with diameter 2*r there are straight wire sections in between that move gradually along z-axis too. The wire starts from (0, 0, 0) and is would counter clockwise around first bobbin and then clockwise around second bobbin." << std::endl; 
  std::cout << std::endl;
  std::cout << "- r bobbin radius" << std::endl;
  std::cout << "- b distance between two gradiometer bobbin centers" << std::endl;
  std::cout << "- L gradiometer coil length" << std::endl;
  std::cout << "- d wire diameter" << std::endl;
  std::cout << "- N number of turns" << std::endl;
  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  bool test = false;
  bool printcircular = false; // circular elements only

  // initial values for testing
  double r = 0.016; 
  double b = 0.04; 
  double L = 0.001;
  double d = 0.0001; 
  int N = 2;
  int M = 2000;
  double I = 1e-3;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test", 6 ) == 0 )
  {
    test = true;
    if( argc == 3 ) verbose = true;
  }
  else
  {
    if( argc < 8 )
    {
      printusage();
      return 0;
    }

    if( argc == 9 ) verbose = true;

    r = atof( argv[ 1 ] );
    b = atof( argv[ 2 ] );
    L = atof( argv[ 3 ] );
    d = atof( argv[ 4 ] );
    N = atoi( argv[ 5 ] );
    M = atoi( argv[ 6 ] );
    I = atof( argv[ 7 ] );

  }

  if( verbose )
  {
    std::cout << "-- bobbin radius " << r << std::endl;
    std::cout << "-- bobbin separation " << b << std::endl;
    std::cout << "-- coil length " << L << std::endl;
    std::cout << "-- wire diameter " << d << std::endl;
    std::cout << "-- number of turns " << N << std::endl;
    std::cout << "-- volume elements " << M << std::endl;
    std::cout << "-- current " << I << std::endl;
  }

  double J = I / (M_PI * (d/2) * (d/2) );
  double Jx = 0, Jy = 0, Jz = 0;
  double J2x = 0, J2y = 0, J2z = 0;
  double JVx = 0, JVy = 0, JVz = 0;
  double dphi = ( 2 * M_PI * N ) / M;
  double phi = 0;
  double phi0 = acos( 2 * r / b );
  double xw = 0, yw = 0, zw =0;
  double nxw = 0, nyw = 0, nzw = 0, lnw = 0;
  double s = 0;
  double dlx = 0, dly = 0, dlz = 0, dl = 0;
  double len = 0;
  double dv = 0;
  double vol = 0;
  double l1 = 0, l2 = 0;
  double tanangle = 0, cosangle = 0, angle = 0;
  int loopno = 0;
  bool circular = false; 
 
  for( int i = 0; i < M; i++ )
  {
    phi = 2 * M_PI * (i + 0.5) * N / M;

    loopno = trunc( phi / (2 * M_PI) );

    if( verbose ) std::cout << "-- loopno=" << loopno << " phi=" << phi;

    if( loopno % 2 == 0 )
    {
      if( phi - loopno * 2 * M_PI < phi0 )
      {
        circular = false;

	if( verbose ) std::cout << " straight wire1";
        s = 1 - r*tan( phi0 - phi )/sqrt( b * b / 4 - r * r);
        xw = s * r * sin( phi0 );
	yw = s * (b / 2 - r * cos( phi0 ));
        zw = phi * L / ( 2 * M_PI * N );

	// direction vector
        nxw = r * sin( phi0 );
        nyw = b/2 - r * cos( phi0 );
        nzw = L / ( 2 * M_PI * N );

        // element length
	dlx = r * r * sin( phi0 ) / ( cos(phi - phi0) * cos( phi - phi0) * sqrt( b*b/4 - r*r ) );
        dlx *= dphi;

	dly = r * (b/2 - r*cos(phi0) ) / ( cos(phi - phi0) * cos(phi - phi0) * sqrt( b*b/4 - r*r ) );
        dly *= dphi;

	dlz = L / ( 2 * M_PI * N);
        dlz *= dphi;

	dl = sqrt( dlx * dlx + dly * dly + dlz * dlz );

      }
      else if( phi - loopno * 2 * M_PI >  2 * M_PI - phi0 )
      {
        circular = false;

	if( verbose ) std::cout << " straight wire2";
        s = 1 - r*tan( phi + phi0 - 2 * M_PI )/sqrt( b * b / 4 - r * r);
        xw = -s * r * sin( phi0 );
	yw = s * (b / 2 - r * cos( phi0 ));
        zw = phi * L / ( 2 * M_PI * N );

	// direction vector
	nxw = r * sin( phi0 );
	nyw = -b/2 + r * cos( phi0 );
        nzw = L / ( 2 * M_PI * N );

	// element length
	dlx = r * r * sin( phi0 ) / ( cos(phi + phi0) * cos( phi + phi0) * sqrt( b*b/4 - r*r ) );
        dlx *= dphi;

	dly = r * (b/2 - r*cos(phi0) ) / ( cos(phi + phi0) * cos(phi + phi0) * sqrt( b*b/4 - r*r ) );
        dly *= dphi;

	dlz = L / ( 2 * M_PI * N);
        dlz *= dphi;

	dl = sqrt( dlx * dlx + dly * dly + dlz * dlz );

      }
      else
      {
        if( verbose ) std::cout << " loop1";

	circular = true;

        xw = r * sin( phi );
        yw = -r * cos( phi ) + b/2;
        zw = phi * L / ( 2 * M_PI * N );

        // direction of current density vector along with wire
        nxw = r * cos( phi );
        nyw = r * sin( phi );
        nzw = L / ( 2 * M_PI * N );

        dl = r * dphi;

      }
    }
    else
    {
      if( phi - loopno * 2 * M_PI < phi0 )
      {
        if( verbose ) std::cout << " straight wire3";

        circular = false;

	s = 1 - r * tan( phi0 - phi )/sqrt( b * b / 4 - r * r);
        xw = s * r * sin( phi0 );
	yw = -(s * (b / 2 - r * cos( phi0 )) - b/2) - b/2;
        zw = phi * L / ( 2 * M_PI * N );

	// direction vector
	nxw = r * sin( phi0 );
	nyw = -b/2 + r * cos( phi0 );
        nzw = L / ( 2 * M_PI * N );

	// element length
	dlx = r * r * sin( phi0 ) / ( cos(phi - phi0) * cos( phi - phi0) * sqrt( b*b/4 - r*r ) );
        dlx *= dphi;

	dly = r * (b/2 - r*cos(phi0) ) / ( cos(phi - phi0) * cos(phi - phi0) * sqrt( b*b/4 - r*r ) );
        dly *= dphi;

	dlz = L / ( 2 * M_PI * N);
        dlz *= dphi;

	dl = sqrt( dlx * dlx + dly * dly + dlz * dlz );

      }
      else if( phi - loopno * 2 * M_PI >  2 * M_PI - phi0 )
      {
        if( verbose ) std::cout << " straight wire4";

        circular = false;

	s = 1 - r*tan( phi + phi0 - 2 * M_PI )/sqrt( b * b / 4 - r * r);
        xw = -s * r * sin( phi0 );
	yw = -(s * (b / 2 - r * cos( phi0 )) - b/2) - b/2;
        zw = phi * L / ( 2 * M_PI * N );

	// direction vector
	nxw = r * sin( phi0 );
	nyw = b/2 - r * cos( phi0 );
        nzw = L / ( 2 * M_PI * N );

	// element length
	dlx = r * r * sin( phi0 ) / ( cos(phi + phi0) * cos( phi + phi0) * sqrt( b*b/4 - r*r ) );
        dlx *= dphi;

	dly = r * (b/2 - r*cos(phi0) ) / ( cos(phi + phi0) * cos(phi + phi0) * sqrt( b*b/4 - r*r ) );
        dly *= dphi;

	dlz = L / ( 2 * M_PI * N);
        dlz *= dphi;

	dl = sqrt( dlx * dlx + dly * dly + dlz * dlz );

      }
      else
      { 
        if( verbose ) std::cout << " loop2";

        circular = true;

	xw = r * sin( phi );
        yw = r * cos( phi ) - b/2;
        zw = phi * L / ( 2 * M_PI * N );

        // direction of current density vector along with wire
        nxw = r * cos( phi );
        nyw = -r * sin( phi );
        nzw = L / ( 2 * M_PI * N );

        dl = r * dphi;

      }
    } 

    if( verbose ) std::cout << " len=" << len << " dl=" << dl << ":";

    lnw = sqrt( nxw*nxw + nyw*nyw + nzw*nzw );
    nxw /= lnw;
    nyw /= lnw;
    nzw /= lnw;

    Jx = J * nxw;
    Jy = J * nyw;
    Jz = J * nzw;

    dv = dl * (d/2) * (d/2) * M_PI;

    JVx += Jx * dv;
    JVy += Jy * dv;
    JVz += Jz * dv;

    len += dl;
    vol += dv;

//    l1 = sqrt( Jx * Jx + Jy * Jy + Jz * Jz );
//    l2 = sqrt( J2x * J2x + J2y * J2y + J2z * J2z );
//    cosangle = (Jx * J2x + Jy * J2y + Jz * J2z )/ ( l1 * l2 );
//    angle = acos( cosangle );

    angle = atan2( Jy, Jx );
    if( angle < 0 ) angle += 2 * M_PI;
    if( verbose ) std::cout << "angle=" << angle << " ";

//    if( !printcircular || ( printcircular && circular ) )
    if( verbose || !test )
    { 
      std::cout << xw << " " << yw << " " << zw;
      std::cout << " " << Jx << " " << Jy << " " << Jz;
      std::cout << " " << dv << std::endl;
    }

    J2x = Jx; J2y = Jy; J2z = Jz;
  }

  if( verbose ) std::cout << "-- total length " << len << std::endl;
  else if( test ) std::cout << len;

  if( verbose ) std::cout << "-- total volume " << vol << std::endl;
  else if( test ) std::cout << " " << vol;
 
  if( verbose ) std::cout << "-- JV = (" << JVx << "," << JVy << "," << JVz << ")" << std::endl;
  else if( test ) std::cout << " " << JVx << " " << JVy << " " << JVz << std::endl;

}
