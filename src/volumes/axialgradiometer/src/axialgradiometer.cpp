// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.

// Create volume elements for axial gradiometer. Bottom ring center is at 
// (0, 0, 0) and top (0, 0, b) with radius R and wire diameter d. w is opening
// in ring for two wires connection top and bottom rings. Current in bottom
// ring is flowing in counter clockwise direction and clockwise direction
// in top ring. 
//
// - R ring radius 
// - b separation between top and bottom rings 
// - w separation of vertical wires between rings 
// - d wire diameter
// - M number of volume elements
// - I current
//
// Sun Aug 25 14:34:17 CDT 2019
// Edit: 
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: axialgradiometer R b w d M I ax ay az theta xt yt zt [v]" << std::endl;
}

void printversion()
{
  std::cout << "axialgradiometer v. 20190825, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create volume elements for axial gradiometer. Bottom ring center is at (0, 0, 0) and top (0, 0, b) with radius R and wire diameter d. w is opening in ring for two wires connection top and bottom rings. Current in bottom ring is flowing in counter clockwise direction and clockwise direction in top ring." << std::endl; 
  std::cout << std::endl;
  std::cout << "- R ring radius" << std::endl;
  std::cout << "- b separation between rings" << std::endl;
  std::cout << "- w opening for wires between top and bottom rings" << std::endl;
  std::cout << "- d wire diameter" << std::endl;

  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
  std::cout << std::endl;
  std::cout << "Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt)." << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  bool test = false;

  // initial values for testing
  double R = 0.01;
  double b = 0.005;
  double w = 0.001;
  double d = 0.0001;
  int M = 2000;
  double I = 1e-3;
  double ax = 0, ay = 0, az = 1;
  double theta = 0;
  double xt = 0, yt = 0, zt = 0;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test", 6 ) == 0 )
  {
    test = true;
    if( argc == 3 ) verbose = true;
  }
  else
  {
    if( argc < 14 )
    {
      printusage();
      return 0;
    }

    if( argc == 15 ) verbose = true;
  
    R = atof( argv[ 1 ] );
    b = atof( argv[ 2 ] );
    w = atof( argv[ 3 ] );
    d = atof( argv[ 4 ] );
    M = atoi( argv[ 5 ] );
    I = atof( argv[ 6 ] );
    ax = atof( argv[ 7 ] );
    ay = atof( argv[ 8 ] );
    az = atof( argv[ 9 ] );
    theta = atof( argv[ 10 ] );
    xt = atof( argv[ 11 ] );
    yt = atof( argv[ 12 ] );
    zt = atof( argv[ 13 ] );
  }

  if( verbose )
  {
    std::cout << "-- ring radius " << R << std::endl;
    std::cout << "-- separation between bottom and top rings " << b << std::endl;
    std::cout << "-- ring opening " << w << std::endl;
    std::cout << "-- wire diameter " << d << std::endl;
    std::cout << "-- volume elements " << M << std::endl;
    std::cout << "-- current " << I << std::endl;
  }

  double len = iiflucks::axialGradiometer(R, b, w, d, M, I, ax, ay, az, theta, xt, yt, zt, verbose, test);

  if( verbose ) std::cout << "-- total length " << len << std::endl;
  else if( test ) std::cout << len << std::endl;

}
