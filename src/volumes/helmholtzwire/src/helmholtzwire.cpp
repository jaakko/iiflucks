// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
// 
// Create volume elements for Helmholtz coil wire. The coil central axis
// is along z-axis with point (0, 0, 0). Wire sections in first turn of 
// first coil are:
//
// 1 straight (+w/2,  -h/2+r, b/2-L/2) to (+w/2,   h/2-r, b/2-L/2) 
// 2 arc      (+w/2,   h/2-r, b/2-L/2) to (+w/2-r, h/2,   b/2-L/2) 
// 3 straight (+w/2-r, h/2,   b/2-L/2) to (-w/2+r, h/2,   b/2-L/2)
// 4 arc      (-w/2+r, h/2,   b/2-L/2) to (-w/2,   h/2-r, b/2-L/2)
// 5 straight (-w/2,   h/2-r, b/2-L/2) to (-w/2,  -h/2+r, b/2-L/2)
// 6 arc      (-w/2,  -h/2+r, b/2-L/2) to (-w/2+r,-h/2,   b/2-L/2)
// 7 straight (-w/2+r,-h/2,   b/2-L/2) to (+w/2-r,-h/2,   b/2-L/2)
// 8 arc      (+w/2-r,-h/2,   b/2-L/2) to (-w/2,  -h/2+r, b/2-L/2+L/N)
//
// Wire sections in first turn of second coil are:
//
// 1 straight (+w/2,  -h/2+r, -b/2-L/2) to (+w/2,   h/2-r, -b/2-L/2) 
// 2 arc      (+w/2,   h/2-r, -b/2-L/2) to (+w/2-r, h/2,   -b/2-L/2) 
// 3 straight (+w/2-r, h/2,   -b/2-L/2) to (-w/2+r, h/2,   -b/2-L/2)
// 4 arc      (-w/2+r, h/2,   -b/2-L/2) to (-w/2,   h/2-r, -b/2-L/2)
// 5 straight (-w/2,   h/2-r, -b/2-L/2) to (-w/2,  -h/2+r, -b/2-L/2)
// 6 arc      (-w/2,  -h/2+r, -b/2-L/2) to (-w/2+r,-h/2,   -b/2-L/2)
// 7 straight (-w/2+r,-h/2,   -b/2-L/2) to (+w/2-r,-h/2,   -b/2-L/2)
// 8 arc      (+w/2-r,-h/2,   -b/2-L/2) to (-w/2,  -h/2+r, -b/2-L/2+L/N)
//
// - h coil height
// - w coil width
// - b separation between two coils
// - r corner radius
// - L coil length
// - d wire diameter
// - N number of turns
// - M number of volume elements
// - I current
//
// Length of wire in one turn
//
// 2 * ( w + h ) + ( 3 pi/2 - 8 ) * r + pi/2 * sqrt( r^2 + L^2/(2*pi*N)^2 )
//
//
// Mon May 20 09:56:41 CDT 2019
// Edit: Sun Aug 18 18:00:21 CDT 2019
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: helmholtzwire h w b r L d N M I [v]" << std::endl;
}

void printversion()
{
  std::cout << "helmholtzwire v. 20190818, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create volume elements for Helmholtz coil wire. The coil central axis is along z-axis with point (0, 0, 0). Wire sections in first turn of first coil are:" << std::endl;
  std::cout << std::endl;
  std::cout << "1 straight (+w/2,  -h/2+r, b/2-L/2) to (+w/2,   h/2-r, b/2-L/2)" << std::endl;
  std::cout << "2 arc      (+w/2,   h/2-r, b/2-L/2) to (+w/2-r, h/2,   b/2-L/2)" << std::endl; 
  std::cout << "3 straight (+w/2-r, h/2,   b/2-L/2) to (-w/2+r, h/2,   b/2-L/2)" << std::endl;
  std::cout << "4 arc      (-w/2+r, h/2,   b/2-L/2) to (-w/2,   h/2-r, b/2-L/2)" << std::endl;
  std::cout << "5 straight (-w/2,   h/2-r, b/2-L/2) to (-w/2,  -h/2+r, b/2-L/2)" << std::endl;
  std::cout << "6 arc      (-w/2,  -h/2+r, b/2-L/2) to (-w/2+r,-h/2,   b/2-L/2)" << std::endl;
  std::cout << "7 straight (-w/2+r,-h/2,   b/2-L/2) to (+w/2-r,-h/2,   b/2-L/2)" << std::endl;
  std::cout << "8 arc      (+w/2-r,-h/2,   b/2-L/2) to (-w/2,  -h/2+r, b/2-L/2+L/N)" << std::endl;
  std::cout << std::endl;
  std::cout << "Wire sections in first turn of second coil are:" << std::endl;
  std::cout << std::endl;
  std::cout << "1 straight (+w/2,  -h/2+r, -b/2-L/2) to (+w/2,   h/2-r, -b/2-L/2)" << std::endl; 
  std::cout << "2 arc      (+w/2,   h/2-r, -b/2-L/2) to (+w/2-r, h/2,   -b/2-L/2)" << std::endl; 
  std::cout << "3 straight (+w/2-r, h/2,   -b/2-L/2) to (-w/2+r, h/2,   -b/2-L/2)" << std::endl;
  std::cout << "4 arc      (-w/2+r, h/2,   -b/2-L/2) to (-w/2,   h/2-r, -b/2-L/2)" << std::endl;
  std::cout << "5 straight (-w/2,   h/2-r, -b/2-L/2) to (-w/2,  -h/2+r, -b/2-L/2)" << std::endl;
  std::cout << "6 arc      (-w/2,  -h/2+r, -b/2-L/2) to (-w/2+r,-h/2,   -b/2-L/2)" << std::endl;
  std::cout << "7 straight (-w/2+r,-h/2,   -b/2-L/2) to (+w/2-r,-h/2,   -b/2-L/2)" << std::endl;
  std::cout << "8 arc      (+w/2-r,-h/2,   -b/2-L/2) to (-w/2,  -h/2+r, -b/2-L/2+L/N)" << std::endl;
  std::cout << std::endl;
  std::cout << "- h coil height" << std::endl;
  std::cout << "- w coil width" << std::endl;
  std::cout << "- b separation between two coils" << std::endl;
  std::cout << "- r corner radius" << std::endl;
  std::cout << "- L coil length" << std::endl;
  std::cout << "- d wire diameter" << std::endl;
  std::cout << "- N number of turns" << std::endl;
  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
  std::cout << std::endl;
  std::cout << "Length of wire in one turn" << std::endl;
  std::cout << std::endl;
  std::cout << "2 * ( w + h ) + ( 3 pi/2 - 8 ) * r + pi/2 * sqrt( r^2 + L^2/(2*pi*N)^2 )" << std::endl;
}


int main(int argc, char **argv)
{
  bool verbose = false;
  bool test = false;

  double h = 3;
  double w = 1.333;
  double b = 1.867;
  double r = 0.1;
  double L = 0.038;
  double d = 0.002;
  int N = 2;
  int M = 1000;
  double I = 1; 

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test", 6 ) == 0 )
  {
    test = true;
    if( argc == 3 ) verbose = true;
  }
  else
  {
    if( argc < 10 )
    {
      printusage();
      return 0;
    }

    if( argc == 11 ) verbose = true;

    h = atof( argv[ 1 ] );
    w = atof( argv[ 2 ] );
    b = atof( argv[ 3 ] );
    r = atof( argv[ 4 ] );
    L = atof( argv[ 5 ] );
    d = atof( argv[ 6 ] );
    N = atoi( argv[ 7 ] );
    M = atoi( argv[ 8 ] );
    I = atof( argv[ 9 ] );

  }

  double wlen = 2*(w+h)+(3*M_PI/2-8)*r+M_PI/2*sqrt(r*r+L*L/(4*M_PI*M_PI*N*N)); 
  wlen *= N;

  if( verbose ) std::cout << "-- one coil wire length " << wlen << std::endl;

  double lc = wlen / (N * M);

  int Mr = ceil( M_PI * r / ( 2 * lc ) ); // corner elements 
  if( verbose ) std::cout << "-- corner elements " << Mr << std::endl;

  int Mh = ceil( ( w - 2 * r) / lc ); // horizontal straight elements
  if( verbose ) std::cout << "-- horizontal elements " << Mh << std::endl;

  int Mv = ceil( ( h - 2 * r) / lc ); // vertical straight elements
  if( verbose ) std::cout << "-- vertical elements " << Mv << std::endl;

  int Mtot = 4 * Mr + 2 * Mh + 2 * Mv;
  if( verbose ) std::cout << "-- total elements " << Mtot << std::endl;

  double Jw = I / (M_PI * (d/2) * (d/2) );
  double Jx = 0, Jy = 0, Jz = 0;
  double A = (d/2) * (d/2) * M_PI;
  double dl = 0;
  double phi = 0, dphi = 0;
  double dz = 0;
  double xw = 0, yw = 0, zw =0;
  double dv = 0;

  double len = 0;
  double slen = 0;
  double vol = 0;

// first coil N turns
// starts from (w/2, -h/2+r, b/2-L/2)
// ends at (w/2, -h/2+r, b/2+L/2)
  for( int i = 0; i < N; i++ )
  {
    // 1 vertical wire
    dl = ( h - 2 * r ) / Mv;
    xw = +w/2;
    yw = -h/2 + r;
    zw = i * L/N + b/2 - L/2;

    slen = iiflucks::verticalWire(Mv, xw, yw, zw, dl, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 2 arc wire
    phi = 0;
    dphi = M_PI / ( 2 * Mr );
    dl = r * dphi;
    xw = w / 2 - r;
    yw = h / 2 - r;
    zw = i * L/N + b/2 - L/2;

    slen = iiflucks::arcWire(Mr, xw, yw, zw, phi, dphi, r, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 3 horizontal wire
    dl = -( w - 2 * r ) / Mh;
    xw = +w/2 - r;
    yw = h/2;
    zw = i * L/N + b/2 - L/2;

    slen = iiflucks::horizontalWire(Mh, xw, yw, zw, dl, A, -Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 4 arc wire
    phi = M_PI_2;
    dphi = M_PI / ( 2 * Mr );
    xw = -w / 2 + r;
    yw = h / 2 - r;
    zw = i * L/N + b/2 - L/2;

    slen = iiflucks::arcWire(Mr, xw, yw, zw, phi, dphi, r, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 5 vertical wire
    dl = -( h - 2 * r ) / Mv;
    xw = -w/2;
    yw = +h/2 - r;
    zw = i * L/N + b/2 - L/2;

    slen = iiflucks::verticalWire(Mv, xw, yw, zw, dl, A, -Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 6 arc wire
    phi = M_PI;
    dphi = M_PI / ( 2 * Mr );
    xw = -w / 2 + r;
    yw = -h / 2 + r;
    zw = i * L/N + b/2 - L/2;

    slen = iiflucks::arcWire(Mr, xw, yw, zw, phi, dphi, r, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 7 horizontal wire
    dl = ( w - 2 * r ) / Mh;
    xw = -w/2 + r;
    yw = -h/2;
    zw = i * L/N + b/2 - L/2;

    slen = iiflucks::horizontalWire(Mh, xw, yw, zw, dl, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 8 arc wire
    phi = 3 * M_PI_2; 
    dphi = M_PI / ( 2 * Mr );
    xw = +w / 2 - r;
    yw = -h / 2 + r;
    zw = i * L/N + b/2 - L/2;

    slen = iiflucks::linkWire(Mr, xw, yw, zw, phi, dphi, r, L, N, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

  }

// second coil N turns
// starts from (w/2, -h/2+r, -b/2-L/2)
// ends at (w/2, -h/2+r, -b/2+L/2)
  for( int i = 0; i < N; i++ )
  {
    // 1 vertical wire
    dl = ( h - 2 * r ) / Mv;
    xw = +w/2;
    yw = -h/2 + r;
    zw = i * L/N - b/2 - L/2;

    slen = iiflucks::verticalWire(Mv, xw, yw, zw, dl, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 2 arc wire
    phi = 0;
    dphi = M_PI / ( 2 * Mr );
    dl = r * dphi;
    xw = w / 2 - r;
    yw = h / 2 - r;
    zw = i * L/N - b/2 - L/2;

    slen = iiflucks::arcWire(Mr, xw, yw, zw, phi, dphi, r, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 3 horizontal wire
    dl = -( w - 2 * r ) / Mh;
    xw = +w/2 - r;
    yw = h/2;
    zw = i * L/N - b/2 - L/2;

    slen = iiflucks::horizontalWire(Mh, xw, yw, zw, dl, A, -Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 4 arc wire
    phi = M_PI_2;
    dphi = M_PI / ( 2 * Mr );
    xw = -w / 2 + r;
    yw = h / 2 - r;
    zw = i * L/N - b/2 - L/2;

    slen = iiflucks::arcWire(Mr, xw, yw, zw, phi, dphi, r, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 5 vertical wire
    dl = -( h - 2 * r ) / Mv;
    xw = -w/2;
    yw = +h/2 - r;
    zw = i * L/N - b/2 - L/2;

    slen = iiflucks::verticalWire(Mv, xw, yw, zw, dl, A, -Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 6 arc wire
    phi = M_PI;
    dphi = M_PI / ( 2 * Mr );
    xw = -w / 2 + r;
    yw = -h / 2 + r;
    zw = i * L/N - b/2 - L/2;

    slen = iiflucks::arcWire(Mr, xw, yw, zw, phi, dphi, r, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 7 horizontal wire
    dl = ( w - 2 * r ) / Mh;
    xw = -w/2 + r;
    yw = -h/2;
    zw = i * L/N - b/2 - L/2;

    slen = iiflucks::horizontalWire(Mh, xw, yw, zw, dl, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

    // 8 arc wire
    phi = 3 * M_PI_2; 
    dphi = M_PI / ( 2 * Mr );
    xw = +w / 2 - r;
    yw = -h / 2 + r;
    zw = i * L/N - b/2 - L/2;

    slen = iiflucks::linkWire(Mr, xw, yw, zw, phi, dphi, r, L, N, A, Jw, verbose, test);
    len += slen;

    dv = slen * A;
    vol += dv;

  }

  if( verbose ) std::cout << "-- total length " << len << std::endl;
  else if( test ) std::cout << len;

  if( verbose ) std::cout << "-- total volume " << vol << std::endl;
  else if( test ) std::cout << " " << vol << std::endl;

}
