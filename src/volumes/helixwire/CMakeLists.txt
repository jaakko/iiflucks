add_executable (helixwire src/helixwire.cpp)
find_package (GSL REQUIRED)

target_link_libraries(helixwire iiflucks GSL::gsl yaml-cpp)

install (TARGETS helixwire DESTINATION bin)
