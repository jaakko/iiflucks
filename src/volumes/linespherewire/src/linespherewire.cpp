// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Create volume elements for wire on spherical surface. 
//
// - R1 line along z-axis distance from x-axis
// - R2 sphere radius
// - theta1 start altitude
// - theta2 end altitude
// - d wire diameter
// - M number of volume elements
// - I current
//
// Wed Jun 26 10:26:30 AM CDT 2024
// Edit: Thu Aug  1 11:06:13 AM CDT 2024
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: linespherewire R1 R2 theta1 theta2 d M I ax ay az rotation xt yt zt [v]" << std::endl;
}

void printversion()
{
  std::cout << "linespherewire v. 20240801, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create volume elements for line on spherical surface. R1 is distance from x-axis for line along z-axis going through point (R2, R1, 0). This line is projected on sphere with radius R2 from altitude theta1 to theta2." << std::endl; 
  std::cout << std::endl;
  std::cout << "- R1 distance from x-axis" << std::endl;
  std::cout << "- R2 sphere radius" << std::endl;
  std::cout << "- theta1 start altitude" << std::endl;
  std::cout << "- theta2 end altitude" << std::endl;
  std::cout << "- d wire diameter" << std::endl;
  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
  std::cout << std::endl;
  std::cout << "Rotate wire elements around axis (ax, ay, az) and translate by vector (xt, yt, zt)." << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  int test = 0;

  // initial values for testing
  double R1 = 0.01;
  double R2 = 0.02;
  double theta1 = -M_PI/4;
  double theta2 = +M_PI/4;
  double d = 0.0001;
  int M = 2000;
  double I = 1e-3;
  double ax = 0, ay = 0, az = 1;
  double rotation = 0;
  double xt = 0, yt = 0, zt = 0;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-parametric-curves5", 25 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    iiflucks::testParametricCurves( 5, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-length", 13 ) == 0 )
  {
    test = 1;
    if( argc == 3 ) verbose = true;
  }

  else if( strncmp( argv[ 1 ], "--test-volume", 13 ) == 0 )
  {
    test = 2;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jxl", 10 ) == 0 )
  {
    test = 3;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jyl", 10 ) == 0 )
  {
    test = 4;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jzl", 10 ) == 0 )
  {
    test = 5;
    if( argc == 3 ) verbose = true;
  }
  else
  {
    if( argc < 15 )
    {
      printusage();
      return 0;
    }

    if( argc == 16 ) verbose = true;
  
    R1 = atof( argv[ 1 ] );
    R2 = atof( argv[ 2 ] );
    theta1 = atof( argv[ 3 ] );
    theta2 = atof( argv[ 4 ] );
    d = atof( argv[ 5 ] );
    M = atoi( argv[ 6 ] );
    I = atof( argv[ 7 ] );
    ax = atof( argv[ 8 ] );
    ay = atof( argv[ 9 ] );
    az = atof( argv[ 10 ] );
    rotation = atof( argv[ 11 ] );
    xt = atof( argv[ 12 ] );
    yt = atof( argv[ 13 ] );
    zt = atof( argv[ 14 ] );
  }

  if( verbose )
  {
    std::cout << "distance_from_x-axis " << R1 << std::endl;
    std::cout << "sphere_radius " << R2 << std::endl;
    std::cout << "start_altitude " << theta1 << std::endl;
    std::cout << "end_altitude " << theta2 << std::endl;
    std::cout << "wire_diameter " << d << std::endl;
    std::cout << "volume_elements " << M << std::endl;
    std::cout << "current " << I << std::endl;
  }

  double A = M_PI * (d/2) * (d/2);
  double J = I / A; 

  // normalize rotation axis vector
  double anorm = sqrt( ax*ax + ay*ay + az*az );
  
  std::vector<double> axis;
  if( anorm > 0 && rotation != 0 )
  {
    axis.push_back( ax / anorm );
    axis.push_back( ay / anorm );
    axis.push_back( az / anorm );
  }
  else
  {
    axis.push_back( 0 );
    axis.push_back( 0 );
    axis.push_back( 0 );
  }

  LineSphere *coil = new LineSphere("line_on_sphere", R1, R2, A, J, rotation, axis, {xt, yt, zt} );

  double len = coil->Elements( theta1, theta2, M, test, verbose );
  if( verbose ) std::cout << "length/volume/Jxl/Jyl/Jzl " << len << std::endl;
  else std::cout << len << std::endl;

}
