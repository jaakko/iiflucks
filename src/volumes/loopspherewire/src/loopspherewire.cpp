// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Create volume elements for wire loop on spherical surface. 
//
// - R2 sphere radius
// - theta1 start altitude
// - phi1 start azimuth
// - dtheta loop altitude angular length
// - dphi loop azimuth angular length
// - d wire diameter
// - M number of volume elements
// - I current
//
// Tue Jul  2 09:40:16 AM CDT 2024
// Edit: Wed Jul  3 09:34:16 AM CDT 2024
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: loopspherewire t1 t2 R2 theta1 phi1 dtheta dphi d M I ax ay az rotation xt yt zt [v]" << std::endl;
}

void printversion()
{
  std::cout << "loopspherewire v. 20240703, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create volume elements for loop on spherical surface." << std::endl; 
  std::cout << std::endl;
  std::cout << "- t = [-1, +1]" << std::endl;
  std::cout << "- R2 sphere radius" << std::endl;
  std::cout << "- theta1 start altitude [rad]" << std::endl;
  std::cout << "- phi1 start azimuth [rad]" << std::endl;
  std::cout << "- dtheta loop angular length [rad]" << std::endl;
  std::cout << "- dphi loop angular widht [rad]" << std::endl;
  std::cout << "- d wire diameter" << std::endl;
  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
  std::cout << std::endl;
  std::cout << "Rotate wire elements around axis (ax, ay, az) and translate by vector (xt, yt, zt)." << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  int test = 0;

  // initial values for testing
  double t1 = -1;
  double t2 = 1;
  double R2 = 0.02;
  double theta1 = M_PI/4;
  double dtheta = M_PI/8;
  double phi1 = M_PI/8;
  double dphi = M_PI/4;
  double d = 0.0001;
  int M = 2000;
  double I = 1e-3;
  double ax = 0, ay = 0, az = 1;
  double rotation = 0;
  double xt = 0, yt = 0, zt = 0;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-parametric-curves6", 25 ) == 0 )
  {
    if( argc == 3 ) verbose = true;
    iiflucks::testParametricCurves( 6, verbose );
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-length", 13 ) == 0 )
  {
    test = 1;
    if( argc == 3 ) verbose = true;
  }

  else if( strncmp( argv[ 1 ], "--test-volume", 13 ) == 0 )
  {
    test = 2;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jxl", 10 ) == 0 )
  {
    test = 3;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jyl", 10 ) == 0 )
  {
    test = 4;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jzl", 10 ) == 0 )
  {
    test = 5;
    if( argc == 3 ) verbose = true;
  }
  else
  {
    if( argc < 16 )
    {
      printusage();
      return 0;
    }

    if( argc == 17 ) verbose = true;
  
    t1 = atof( argv[ 1 ] );
    t2 = atof( argv[ 2 ] );
    R2 = atof( argv[ 3 ] );
    theta1 = atof( argv[ 4 ] );
    dtheta = atof( argv[ 5 ] );
    phi1 = atof( argv[ 6 ] );
    dphi = atof( argv[ 7 ] );
    d = atof( argv[ 8 ] );
    M = atoi( argv[ 9 ] );
    I = atof( argv[ 10 ] );
    ax = atof( argv[ 11 ] );
    ay = atof( argv[ 12 ] );
    az = atof( argv[ 13 ] );
    rotation = atof( argv[ 14 ] );
    xt = atof( argv[ 15 ] );
    yt = atof( argv[ 16 ] );
    zt = atof( argv[ 17 ] );
  }

  if( verbose )
  {
    std::cout << "sphere_radius " << R2 << std::endl;
    std::cout << "start_altitude_angle " << theta1 << std::endl;
    std::cout << "start_azimuth_angle " << phi1 << std::endl;
    std::cout << "loop_angular_length " << dtheta << std::endl;
    std::cout << "loop_angular_width " << dphi << std::endl;
    std::cout << "wire_diameter " << d << std::endl;
    std::cout << "volume_elements " << M << std::endl;
    std::cout << "current " << I << std::endl;
  }

  double A = M_PI * (d/2) * (d/2);
  double J = I / A; 

  // normalize rotation axis vector
  double anorm = sqrt( ax*ax + ay*ay + az*az );
  
  std::vector<double> axis;
  if( anorm > 0 && rotation != 0 )
  {
    axis.push_back( ax / anorm );
    axis.push_back( ay / anorm );
    axis.push_back( az / anorm );
  }
  else
  {
    axis.push_back( 0 );
    axis.push_back( 0 );
    axis.push_back( 0 );
  }

  LoopSphere *coil = new LoopSphere("loop_on_sphere", R2, theta1, phi1, dtheta, dphi, A, J, rotation, axis, {xt, yt, zt} );

  double len = coil->Elements( t1, t2, M, test, verbose );
  if( verbose ) std::cout << "length/volume/Jxl/Jyl/Jzl " << len << std::endl;
  else std::cout << len << std::endl;

}
