// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
// 
// Create volume elements for straight wire. Wire bottom center is at 
// (0, 0, 0) and top end at (0, 0, L). 
//
// - L wire length
// - d wire diameter
// - M number of volume elements
// - I current
//
// Mon May 13 19:04:09 CDT 2019
// Edit: Sun Mar 17 04:47:52 PM CDT 2024
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: straightwire L d M I ax ay az theta xt yt zt [v]" << std::endl;
}

void printversion()
{
  std::cout << "straightwire v. 20240317, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Create volume elements for straight wire. Wire bottom center is at (0, 0, 0) and top end at (0, 0, L). Current flows from bottom to top." << std::endl; 
  std::cout << std::endl;
  std::cout << "- L wire length" << std::endl;
  std::cout << "- d wire diameter" << std::endl;
  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
  std::cout << std::endl;
  std::cout << "Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt)." << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  int test = 0;
  double L = 0.095; 
  double d = 0.0001; 
  int M = 2000;
  double I = 1e-3;
  double ax = 0, ay = 0, az = 1;
  double theta = 0;
  double xt = 0, yt = 0, zt = 0;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-length", 13 ) == 0 )
  {
    test = 1;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-volume", 13 ) == 0 )
  {
    test = 2;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jxl", 10 ) == 0 )
  {
    test = 3;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jyl", 10 ) == 0 )
  {
    test = 4;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jzl", 10 ) == 0 )
  {
    test = 5;
    if( argc == 3 ) verbose = true;
  }
  else if( argc < 12 )
  {
    printusage();
    return 0;
  }
  else
  {
    if( argc == 13 ) verbose = true;

    L = atof( argv[ 1 ] );
    d = atof( argv[ 2 ] );
    M = atoi( argv[ 3 ] );
    I = atof( argv[ 4 ] );
    ax = atof( argv[ 5 ] );
    ay = atof( argv[ 6 ] );
    az = atof( argv[ 7 ] );
    theta = atof( argv[ 8 ] );
    xt = atof( argv[ 9 ] );
    yt = atof( argv[ 10 ] );
    zt = atof( argv[ 11 ] );
  }

  double len =  iiflucks::straightWire(L, d, M, I, ax, ay, az, theta, xt, yt, zt, verbose, test); 

  if( verbose ) std::cout << "-- length/volume/Jxl/Jyl/Jzl " << len << std::endl;
  else std::cout << len << std::endl;

}
