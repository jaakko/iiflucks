// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Create volume elements for planar tracetrack wire. 
//
// - R corner radius 
// - h length of straight section along z-axis
// - e length of straight section along y-axis 
// - w width of coil winding
// - th coil thickness on x-axis 
// - N number of turns
// - O number of layers 
// - d wire diameter
// - M number of volume elements
// - I current
//
// Wed Mar 20 08:10:14 PM CDT 2024
// Edit: Thu Jun  6 03:46:06 PM CDT 2024
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: racetrackplanar R h e w N th O d M I ax ay az theta xt yt zt [v]" << std::endl;
}

void printversion()
{
  std::cout << "racetrackplanar v. 20240606, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Wire sections in first turn on first layer are:" << std::endl;
  std::cout << "0. straight    (0, +R + e/2, -h/2) to (0, +R + e/2, +h/2)" << std::endl;
  std::cout << "1. arc         (0, +R + e/2, +h/2) to (0, +e/2, R + h/2)" << std::endl;
  std::cout << "2. straight    (0, +e/2, R + h/2)   to (0, -e/2, R +h/2)" << std::endl;
  std::cout << "3. arc         (0, -e/2, R + h/2)  to (0, -R - e/2, +h/2)" << std::endl;
  std::cout << "4. straight    (0, -R - e/2, +h/2) to (0, -R - e/2, -h/2)" << std::endl;
  std::cout << "5. arc         (0, -R - e/2, -h/2) to (0, -e/2, -R - h/2)" << std::endl;
  std::cout << "6. straight    (0, -e/2, -R - h/2) to (0, +e/2, -R - h/2}" << std::endl;
  std::cout << "7. arc         (0, +e/2, -R - h/2) to (0, +R + e/2, -h/2)" << std::endl;
  std::cout << "d is wire diameter, M number of volume elements and I current.  Current flows in counter clockwise direction around x-axis." << std::endl;
  std::cout << std::endl;
  std::cout << "- R corner radius" << std::endl;
  std::cout << "- h length of straight section along z-axis" << std::endl;
  std::cout << "- e length of straight section along y-axis" << std::endl;
  std::cout << "- w width of coil winding" << std::endl;
  std::cout << "- th coil thickness along x-axis" << std::endl;
  std::cout << "- N number of turns" << std::endl;
  std::cout << "- O number of layers" << std::endl;
  std::cout << "- d wire diameter" << std::endl;
  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
  std::cout << std::endl;
  std::cout << "Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt)." << std::endl;
  std::cout << std::endl;
  std::cout << "Length of wire on one layer is L = 2*(h+e)*N + 2*pi*R*N + pi*w*N." << std::endl;
}

// Length of wire on one layer is L = 2*(h+e)*N + 2*pi*R*N + pi*w*N.
// The wire elements are rotated around axis ( _ax_ , _ay_ , _az_ ) with 
// angle _theta_ followed by translation with vector ( _xt_ , _yt_ , _zt_ ).
// The flags _verbose_ and _test_ are used for debugging. 
// With _test_=1 wire length is returned, 2 gives wire volume, 3 sum of Jx dv,
// 4 sum of Jy dv and 5 sum on Jz dv. With any other value wire length is
// returned.
int main(int argc, char **argv)
{
  bool verbose = false;
  int test = 0;

  // initial values for testing
  double R = 0.01;
  double h = 0.04;
  double e = 0.005;
  double w = 0.001;
  double th = 0.0005;
  double d = 0.0001;
  int turns = 4;
  int layers = 2;
  int M = 2000;
  double I = 1e-3;
  double ax = 0, ay = 0, az = 1;
  double theta = 0;
  double xt = 0, yt = 0, zt = 0;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-length", 13 ) == 0 )
  {
    test = 1;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-volume", 13 ) == 0 )
  {
    test = 2;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jxl", 10 ) == 0 )
  {
    test = 3;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jyl", 10 ) == 0 )
  {
    test = 4;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jzl", 10 ) == 0 )
  {
    test = 5;
    if( argc == 3 ) verbose = true;
  }
  else
  {
    if( argc < 18 )
    {
      printusage();
      return 0;
    }

    if( argc == 19 ) verbose = true;
  
    R = atof( argv[ 1 ] );
    h = atof( argv[ 2 ] );
    e = atof( argv[ 3 ] );
    w = atof( argv[ 4 ] );
    turns = atoi( argv[ 5 ] );
    th = atof( argv[ 6 ] );
    layers = atoi( argv[ 7 ] );
    d = atof( argv[ 8 ] );
    M = atoi( argv[ 9 ] );
    I = atof( argv[ 10 ] );
    ax = atof( argv[ 11 ] );
    ay = atof( argv[ 12 ] );
    az = atof( argv[ 13 ] );
    theta = atof( argv[ 14 ] );
    xt = atof( argv[ 15 ] );
    yt = atof( argv[ 16 ] );
    zt = atof( argv[ 17 ] );
  }

  if( verbose )
  {
    std::cout << "-- half circle radius " << R << std::endl;
    std::cout << "-- length of straight section " << h << std::endl;
    std::cout << "-- length of end straight section " << e << std::endl;
    std::cout << "-- width of coil winding " << w << std::endl;
    std::cout << "-- number of turns " << turns << std::endl;
    std::cout << "-- coil thickness " << th << std::endl;
    std::cout << "-- number of layer " << layers << std::endl;
    std::cout << "-- wire diameter " << d << std::endl;
    std::cout << "-- volume elements " << M << std::endl;
    std::cout << "-- current " << I << std::endl;
  }

  double A = M_PI * (d/2) * (d/2);
  double J = I / A; 

  // normalize rotation axis vector
  double anorm = sqrt( ax*ax + ay*ay + az*az );
  
  std::vector<double> axis;
  if( anorm > 0 && theta != 0 )
  {
    axis.push_back( ax / anorm );
    axis.push_back( ay / anorm );
    axis.push_back( az / anorm );
  }
  else
  {
    axis.push_back( 0 );
    axis.push_back( 0 );
    axis.push_back( 0 );
  }


//  double len = iiflucks::raceTrackPlanar(R, h, e, w, t, d, N, O, M, I, ax, ay, az, theta, xt, yt, zt, verbose, test);
  RacetrackPlanar *coil = new RacetrackPlanar("racetrack_planar", R, h, e, w, th, turns, layers, A, J, theta, axis, {xt, yt, zt});

  std::vector<double> t1v = {-h/2, 0.0, e/2, 1.57079632679, h/2, 3.14159265359, -e/2, 4.71238898038};
  std::vector<double> t2v = {h/2, 1.57079632679, -e/2, 3.14159265359, -h/2, 4.71238898038, e/2, 6.28318530718};

  // calculate total length and initialize parameters
  double total_length = coil->CalcLength( t1v, t2v, verbose );

  double lc = total_length / M; // length of volume elements
  if( verbose )
  {
    std::cout << "-- length = " << total_length << " m" << std::endl;
    std::cout << "-- lc = " << lc << " m" << std::endl;
  }

  double test_result = 0, section_length;
  int section_elements;
  for(int layer = 0; layer < layers; layer++)
  {
    for(int turn = 0; turn < turns; turn++)
    {
      section_length = coil->Length(0, h, 0, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(-h/2, h/2, 0, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, M_PI/2, 1, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(0, M_PI/2, 1, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, e, 2, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(e/2, -e/2, 2, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, M_PI/2, 3, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(M_PI/2, M_PI, 3, turn, layer, section_elements, test, verbose);
    
      section_length = coil->Length(0, h, 4, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(h/2, -h/2, 4, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, M_PI/2, 5, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(M_PI, 1.5*M_PI, 5, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, e, 6, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(-e/2, e/2, 6, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, M_PI/2, 7, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(1.5*M_PI, 2*M_PI, 7, turn, layer, section_elements, test, verbose);

    }
  }

  if( verbose && test > 0 ) std::cout << "-- length/volume/Jxl/Jyl/Jzl " << test_result << std::endl;
  else if( test > 0 ) std::cout << test_result << std::endl;

}
