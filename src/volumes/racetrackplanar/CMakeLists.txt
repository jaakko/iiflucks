add_executable (racetrackplanar src/racetrackplanar.cpp)
find_package (GSL REQUIRED)

target_link_libraries(racetrackplanar iiflucks GSL::gsl yaml-cpp)

install (TARGETS racetrackplanar DESTINATION bin)
