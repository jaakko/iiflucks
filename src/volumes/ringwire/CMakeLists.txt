add_executable (ringwire src/ringwire.cpp)
find_package (GSL REQUIRED)

target_link_libraries(ringwire iiflucks GSL::gsl yaml-cpp)

install (TARGETS ringwire DESTINATION bin)
