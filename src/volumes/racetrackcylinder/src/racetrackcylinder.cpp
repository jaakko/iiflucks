// This file is part of IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Create volume elements for planar tracetrack wire. 
//
// - R corner radius 
// - R2 cylinder radius 
// - h length of straight section along z-axis
// - w width of coil winding in radial direction
// - t coil thickness on mandrel surface 
// - N number of turns
// - O number of layers 
// - d wire diameter
// - M number of volume elements
// - I current
//
// Fri Mar 22 08:52:01 PM CDT 2024
// Edit: Thu Jun  6 03:47:03 PM CDT 2024
//
// Jaakko Koivuniemi

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string.h>
#include <gsl/gsl_const_mksa.h>
#include "iiflucks.h"

void printusage()
{
  std::cout << "Usage: racetrackcylinder R R2 h w N t O d M I ax ay az theta xt yt zt [v]" << std::endl;
}

void printversion()
{
  std::cout << "racetrackcylinder v. 20240606, Jaakko Koivuniemi" << std::endl;
}

void printhelp()
{
  std::cout << "Straight sections along z-axis have length h, curved corners have radius R. Radius of cylinder is R2. From cylinder central z-axis cross section of winding mandrel has angle alpha = 2 R / R2. Layer of coil is wound radially from R2 to R2 + w. Next new layer is wound on top of the previous layer at distance of t / ( O - 1 ). This is repeated until all layers have been wound." << std::endl;
  std::cout << std::endl;
  std::cout << "Wire sections in first turn on first layer are:" << std::endl;
  std::cout << std::endl;
  std::cout << "1. straight" << std::endl;
  std::cout << "xw = sqrt( Rn^2 -R^2 )" << std::endl;
  std::cout << "yw = R" << std::endl;
  std::cout << "zw = [-h/2, +h/2]" << std::endl;
  std::cout << std::endl;
  std::cout << "2. curved arc t = [0, pi]" << std::endl;
  std::cout << "xw = sqrt( Rn^2 - R^2 cos^2( t ) )" << std::endl;
  std::cout << "yw = R cos( t )" << std::endl;
  std::cout << "zw = R sin( t ) + h / 2" << std::endl;
  std::cout << std::endl;
  std::cout << "3. straight" << std::endl;
  std::cout << "xw = sqrt( Rn^2 - R^2)" << std::endl;
  std::cout << "yw = -R" << std::endl;
  std::cout << "zw = [+h/2, -h/2]" << std::endl;
  std::cout << std::endl;
  std::cout << "4. curved arc t = [pi, 2 pi]" << std::endl;
  std::cout << "xw = sqrt( Rn^2 - R^2 cos^2( t ) )" << std::endl;
  std::cout << "yw = R cos( t )" << std::endl;
  std::cout << "zw = R sin( t ) - h / 2" << std::endl;
  std::cout << std::endl;
  std::cout << "d is wire diameter, M number of volume elements and I current.  Current flows in counter clockwise direction around x-axis." << std::endl;
  std::cout << std::endl;
  std::cout << "- R corner radius" << std::endl;
  std::cout << "- R2 cylinder radius" << std::endl;
  std::cout << "- h length of straight section along z-axis" << std::endl;
  std::cout << "- w radial width of coil winding" << std::endl;
  std::cout << "- t coil thickness" << std::endl;
  std::cout << "- N number of turns" << std::endl;
  std::cout << "- O number of layers" << std::endl;
  std::cout << "- d wire diameter" << std::endl;
  std::cout << "- M number of volume elements" << std::endl;
  std::cout << "- I current" << std::endl;
  std::cout << std::endl;
  std::cout << "Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt)." << std::endl;
  std::cout << std::endl;
  std::cout << "Length of wire on one layer is L = (2*h + integal(sqrt(..),0,2pi) )*N." << std::endl;
}

int main(int argc, char **argv)
{
  bool verbose = false;
  int test = 0;

  // initial values for testing
  double R = 0.01;
  double R2 = 0.08;
  double h = 0.04;
  double w = 0.001;
  double th = 0.0005;
  double d = 0.0001;
  int N = 2;
  int O = 2;
  int M = 500;
  double I = 1e-3;
  double ax = 0, ay = 0, az = 1;
  double theta = 0;
  double xt = 0, yt = 0, zt = 0;

  if( argc < 2 )
  {
    printusage();
    return 0;
  }

  if( strncmp( argv[ 1 ], "v", 1 ) == 0 && argc == 2 )
  {
    printversion();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "h", 1 ) == 0 && argc == 2 )
  {
    printhelp();
    return 0;
  }
  else if( strncmp( argv[ 1 ], "--test-length", 13 ) == 0 )
  {
    test = 1;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-volume", 13 ) == 0 )
  {
    test = 2;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jxl", 10 ) == 0 )
  {
    test = 3;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jyl", 10 ) == 0 )
  {
    test = 4;
    if( argc == 3 ) verbose = true;
  }
  else if( strncmp( argv[ 1 ], "--test-Jzl", 10 ) == 0 )
  {
    test = 5;
    if( argc == 3 ) verbose = true;
  }
  else
  {
    if( argc < 18 )
    {
      printusage();
      return 0;
    }

    if( argc == 19 ) verbose = true;
  
    R = atof( argv[ 1 ] );
    R2 = atof( argv[ 2 ] );
    h = atof( argv[ 3 ] );
    w = atof( argv[ 4 ] );
    N = atoi( argv[ 5 ] );
    th = atof( argv[ 6 ] );
    O = atoi( argv[ 7 ] );
    d = atof( argv[ 8 ] );
    M = atoi( argv[ 9 ] );
    I = atof( argv[ 10 ] );
    ax = atof( argv[ 11 ] );
    ay = atof( argv[ 12 ] );
    az = atof( argv[ 13 ] );
    theta = atof( argv[ 14 ] );
    xt = atof( argv[ 15 ] );
    yt = atof( argv[ 16 ] );
    zt = atof( argv[ 17 ] );
  }

  if( verbose )
  {
    std::cout << "-- corner radius " << R << std::endl;
    std::cout << "-- cylinder radius " << R2 << std::endl;
    std::cout << "-- length of straight section " << h << std::endl;
    std::cout << "-- radial width of coil winding " << w << std::endl;
    std::cout << "-- number of turns " << N << std::endl;
    std::cout << "-- coil thickness " << th << std::endl;
    std::cout << "-- number of layer " << O << std::endl;
    std::cout << "-- wire diameter " << d << std::endl;
    std::cout << "-- volume elements " << M << std::endl;
    std::cout << "-- current " << I << std::endl;
  }

  double A = M_PI * (d/2) * (d/2);
  double J = I / A; 

  // normalize rotation axis vector
  double anorm = sqrt( ax*ax + ay*ay + az*az );
  
  std::vector<double> axis;
  if( anorm > 0 && theta != 0 )
  {
    axis.push_back( ax / anorm );
    axis.push_back( ay / anorm );
    axis.push_back( az / anorm );
  }
  else
  {
    axis.push_back( 0 );
    axis.push_back( 0 );
    axis.push_back( 0 );
  }

  //  double len = iiflucks::raceTrackCylinder(R, R2, h, e, w, t, d, N, O, M, I, ax, ay, az, theta, xt, yt, zt, verbose, test);
  RacetrackCylinder *coil = new RacetrackCylinder("racetrack_cylinder", R, R2, h, w, th, N, O, A, J, theta, axis, {xt, yt, zt});

  std::vector<double> t1v = {-h/2, 0.0, h/2, 3.14159265358979};
  std::vector<double> t2v = {h/2, 3.14159265358979, -h/2, 6.28318530717959};

// calculate total length and initialize parameters
  double total_length = coil->CalcLength( t1v, t2v, verbose );

  double lc = total_length / M; // length of volume elements

  double test_result = 0, section_length;
  int section_elements;
  for(int layer = 0; layer < O; layer++)
  {
    for(int turn = 0; turn < N; turn++)
    {
      section_length = coil->Length(0, h, 0, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(-h/2, h/2, 0, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, M_PI, 1, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(0, M_PI, 1, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, h, 2, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(h/2, -h/2, 2, turn, layer, section_elements, test, verbose);

      section_length = coil->Length(0, M_PI, 3, turn, layer);
      section_elements = ceil( section_length / lc );
      test_result += coil->Elements(M_PI, 2*M_PI, 3, turn, layer, section_elements, test, verbose);
    
    }
  }

  if( verbose && test > 0 ) std::cout << "-- length/volume/Jxl/Jyl/Jxl " << test_result << std::endl;
  else if( test > 0 ) std::cout << test_result << std::endl;

}
