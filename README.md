# IIFlucks

IIFlucks is collection of command line programs for wire mutual inductance 
calculation and inductively coupled thermal noise modeling using simulated
random current distributions. Thin wires can be modeled directly by giving
geometry dimensions to the program generating volume elements. 
More general 2D surface or 3D volume mesh are generated using __gmsh__ API. The mesh programs are 

- __cylindertube__ Create mesh file for cylindrical tube volume.

- __cylindervolume__ Create mesh file for cylinder volume.

- __disk__ Create mesh file for disk volume.

- __injectionvolume__ More complex example for mesh generation.

Depending on the purpose of the model either 2D or 3D mesh can be generated.
The 2D mesh is useful for calculation of induced flux to coil from geometry 
containing weakly magnetized material. The 3D mesh is used for spatial current 
distributions. Electric current in radial, angular or axial (z-direction) 
can be given allowing to get first estimate of inductance or field quickly
since no partial differential equation needs to be solved.
These values are ignored in thermal noise current simulation. 
More accurate results are of course obtained by solving the relevant partial
differential equation with finite element method using the 3D mesh 
including environment and boundary conditions of the problem.

Implementing similar programs for other geometries is quite straightforward 
following examples in _gmsh-4.4.1-source/demos/api_ directory and source code
of one of the programs listed above.

The __gmsh__ can open STEP files if 3D CAD model is already available.
2D or 3D mesh can then be generated and saved as _file.msh_. If current
distribution is needed for inductance calculation an additional file
_file_J.pos_ has to be created. This is done automatically by the
3D mesh generation programs listed above. Note that simulation 
of thermal noise currents does not need this information and mutual 
inductance is not well defined quantity for random currents. 
The mesh file can be transformed to
_position-current_density-volume_ file with command __mesh2dv__. This file
is used by the mutual inductance calculation program __indmtrx__. 

The program __mesh2da__ creates _position-area-surface_current_ file from
2D mesh. This program reads files _holes.txt_, _planes.txt_ and _axis.txt_ 
to help in determining the "outward" direction on volume surface that is 
needed in calculation of
surface current vector from constant uniform magnetization. The _planes_ 
are usually end planes of geometry while the _axis_ gives symmetry around 
given axis. The program __fluxind__ uses the _position-area-surface_current_
file to calculate induced flux to current carrying volume (usually to 
coil wire).

The program __probeB__ calculates magnetic field at given position from
_position-current_density-volume_ file.

__simJNB__ generates new random thermal electric current distribution for given
_position-current_density-volume_ file and calculates magnetic field from this
distribution at given position.

__fluxfield__ calculates induced flux to coil from perfectly homogeneous 
constant field. This is not real physical model since the field energy is
infinite. More realistic model can be generated with program 
__helmholtzwire__.

Example
-------

Print command line options of program __helixwire__.

```shell
> helixwire

Usage: helixwire r L d N M I ax ay az theta xt yt zt [v]
```

Print short help text.

```shell
> helixwire h

Create volume elements for helix wire. The coil bottom center is at (0, 0, 0). Wire starts from (r, 0, 0) and is wound counter clockwise around z-axis with length L along helix axis and N number of turns.

- r helix radius
- L length along helix axis
- d wire diameter
- N number of turns
- M number of volume elements
- I current

Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt).
```

Create 10 mm diameter and 10 mm long helix with 100 um wire and current
1 mA. The coil has 10 turns and is divided into 1000 volume elements.

```shell
> helixwire 5e-3 1e-2 1e-4 10 1000 1e-3 0 0 1 0 0 0 0 > helix.pdv
```

The file _helix.pdv_ has position coordinates ( _x_ , _y_ , _z_ ) 
and current density vector ( _Jx_ , _Jy_ , _Jz_ ) for small volume 
element _dv_ .

```ascii
x1 y1 z1 J1x J1y J1z dv1
x2 y2 z2 J2x J2y J2z dv2
...
```

Calculate stored energy, field flux and self inductance of coil. In this
case the volumes 1 and 2 are the same _helix.pdv_, 
translation vector ( _xt_ , _yt_ , _zt_ ) is (0, 0, 0)
and rotation axis ( _ax_ , _ay_ , _az_ ) can be any (1, 0, 0) 
with rotation angle _theta_ = 0.
The current flowing in
circuit 1 and 2 needs to be known for mutual inductance 
(or self inductance in this case) calculation. 
These values are written into file _file.yaml_.

```yaml
# example yaml for position-current-density-dv file
---
# coil definition with end-to-end current
current:
  refdes: L1
  current_A: 1e-3
  rotation_axis: [0.0,0.0,1.0] # orientation and position in space
  rotation_angle: 0.0
  translation_m: [0.0,0.0,0.0]
  file: helix.pdv
```

```shell
indmtrx file.yaml file.yaml 6 v
-- parse pdv-file1
-- parse pdv-file2
file.yaml file.yaml
-- zero distance 991 times
-- V1 = 2.46865e-09 m^3, V2 = 2.46865e-09 m^3
-- E12 = 3.1789e-13 +- 3.47032e-15 J
-- Flux12 = 6.35779e-10 +- 6.94063e-12 Wb
-- M12 = 6.35779e-07 +- 6.94063e-09
-- F12 = (-2.35284e-13 N, -2.64578e-13 N, -1.85032e-13 N)
-- +-(4.49187e-13 N, 4.53474e-13 N, 2.58094e-12 N)
```

10^6 random samples are taken from the two volumes.
Note that self inductance calculation has points where distance between 
elements is zero and this needs to be taken into account to get correct
results.

To plot histograms run same command again. Now the file _.indmtrx.yaml_ has
the updated maximum and minimum values for histograms. Plot all the histograms
to PNG files with script calling __gnuplot__.

```shell
> allhist.sh
```

With Python __matplotlib__ these histograms can be plotted with command

```shell
> allhist-py.sh
```

but this needs installation of the needed packages __argparse__, __re__,
__numpy__ and __pandas__.

![Helix distance histogram](/doc/pictures/hdst_helix_example.png)

__Element distance histogram__ for helix self inductance calculation.

For new calculation it is better to remove the previous hidden _.indmtrx.yaml_ file.

```shell
> rm .indmtrx.yaml
```

This file is overwritten with minimum and maximum values by the __indmtrx__
program. It allows to adjust the histogram ranges by manual editing before
running __indmtrx__.

More examples are in _test_ directory. See the _test/README.md_ file.
These scripts show how to plot the histograms of Monte Carlo sampling
of current carrying volumes in inductance estimation, how to create field maps 
and how to plot mutual inductance as a function of rotation angle around 
given axis or as a function of translation along given axis.

Coordinates
-----------

Right-handed coordinate system is used with meter units.

```ascii

     |y
     |
     |______x
     /
    /
   /z(towards observer)
```

Rotation around arbitrary axis ( _ax_ , _ay_ , _az_ ) with angle _theta_
is also right-handed. The rotations are done with quaternion calculation.
Positive electric current generates magnetic field following the same
right-hand rule. For the yaml-files current flows in direction of integration
of the parametric curve _t_ = [_t1_, _t2_].
 
Gmsh shared library
-------------------

The Gmsh library needs to be compiled for 2D and 3D mesh functions. 
This can be done for example by downloading the source code from

http://gmsh.info/

and following instructions in the README.txt file. The __cmake__
is invoked with option

```shell
> cmake -DENABLE_BUILD_SHARED=ON ..
```

Compiling the __gmsh__ graphical user interface needs Fltk1.3 and
Basic Linear Algebra Subprograms (BLAS/LAPACK) for meshing.
PETSc is used for linear solvers.

GNU Scientific Library 
----------------------

GNU Scientific Library 

https://www.gnu.org/software/gsl/

is used for the physical constants, random number generators and histograms.

YAML files
----------

The parametric wire models are written in YAML file format.

https://github.com/jbeder/yaml-cpp


Model Size Considerations
-------------------------

The maximum number of elements is defined by line

```C++
const int maxpoints = 70000;
```

in the source code.

Memory usage for __indmtrx__ that reads all the volume element data into 
memory is `2 * 7 * Size( double ) * number of elements`.

The _double_ has size of 8 bytes and this gives 7656.25 kB for array size of
70 000.

In Linux the stack size can be printed with __ulimit -a__.

Install
-------

Compiling is done with __cmake__ and __make__.
If Gmsh library is not available for meshing set 
_USE_GMSH_LIBS_ OFF in _src/CMakeLists.txt_.

```shell
> cd iiflucks

> mkdir build

> cd build

> cmake ../src

> make

> make install

> /sbin/ldconfig (first install only)
```

Optionally tests (if any) can be run in _build_ directory with

```shell
> ctest
```

Documentation can be found from _build/doc_oxygen_ directory. Use __make__ in
_latex_ directory to generate PDF-manual.

Programs can be compiled from command line if needed. For example in Linux 
systems

```shell
> g++ -o mesh2da -Iinclude mesh2da.cpp -Llib -liiflucks -lgmsh
```

Directories
-----------

```ascii
doc                     - Doxygen automatic document generation configuration
gnuplot                 - gnuplot scripts to plot histograms
include                 - API for iiflucks library
LICENSE.txt             - same like for GSL and Gmsh
octave                  - Octave scripts
perl                    - Perl scripts
README.md               - this file
src/fluxfield           - calculate flux from static magnetic field
src/fluxind             - calculate flux from weakly magnetized volume
src/geometries          - gmsh 2D and 3D mesh generation programs 
src/iiflucks            - free functions in iiflucks library
src/indmtrx             - calculate inductance and flux linkage
src/mesh2da             - create x, y, z, jmx, jmy, jmz, da surface file
src/mesh2dv             - create x, y, z, Jx, Jy, Jz, dv volume file
src/simJNB              - simulate random noise Bx, By, Bz from volume
src/surfaces            - programs to create surface files
src/volumes             - programs to create volume files
test                    - test scripts for calculations
WSL.md                  - Windows Subsystem for Linux instructions
```

API
---

The __IIFlucks__ API is documented in _include/iiflucks.h_.

Coils and Wires
---------------

See _doc/samples_ for examples of wire geometries.

![Straight Wire](/doc/pictures/StraightWire.svg)

__Straight Wire__ has diameter _d_, length _L_ and current _I_ flowing
from bottom to top. The (0, 0, 0) is at bottom center of wire. Program
to generate volume elements is __straightwire__.

![Ring Wire](/doc/pictures/Ring.svg)

__Ring Wire__ is defined by radius _R_, start angle _phi0_, end angle _phi1_
and current _I_. The (0, 0, 0) is in center of ring. Program for volume 
elements is __ringwire__.

![Ring Cylinder Wire](/doc/pictures/RingCylinder.svg)

__Ring Cylinder Wire__ is defined by radius _R_, start angle _phi0_, end 
angle _phi1_ and current _I_. The wire is on cylinder surface of radius
_R2_. The ring or arc center is at (_R2_, 0, 0).  Program for volume elements 
is __ringcylinderwire__.


![Planar Gradiometer](/doc/pictures/PlanarGradiometer.svg)

__Planar Gradiometer__ is defined by radius _R_, distance between centers
_b_, winding length outward from xy-plane _L_, and number of turns _N_.
The (0, 0, 0) is center point between two loops with current _I_ flowing in
counter clockwise direction in upper loop. Program for volume elements
is __gradient8wire__.

![Axial Gradiometer](/doc/pictures/AxialGradiometer.svg)

__Axial Gradiometer__ is given by loop radius _R_, baseline _b_, loop opening
width _w_ and current _I_. The (0, 0, 0) is in center of bottom ring.
Current is flowing in counter clockwise direction in bottom ring and
clockwise direction in top ring. Program for volume elements is
__axialgradiometer__.

![Helix Wire](/doc/pictures/Helix.svg)

__Helix Wire__ is defined by helix radius _r_, length _L_, number of turns
_N_ and wire diameter _d_. The current _I_ flows on wire in counter clockwise
direction. The coil bottom center is at (0, 0, 0) and wire starts from 
(r, 0, 0). Program for volume elements is __helixwire__.

![Helmholtz Wire](/doc/pictures/HelmholtzWire.svg)

__Helmholtz Wire__ geometry is given by coil height _h_, coil width _w_,
separation between two windings _b_, corner radius _r_ and winding length
_L_ along z-axis. _d_ is wire diameter. Coil central axis is along z-axis with
(0, 0, 0) in center. Program for volume elements is __helmholtzwire__.

![Racetrack Planar](/doc/pictures/RacetrackPlanar.svg)

__Racetrack Planar__ geometry is given by length of straight section along 
z-axis _h_, radius _R_, straight section along y-axis _e_, width of winding 
_w_ and number of turns _N_. Thickness of the coil on x-axis is _t_ with
number of layers _O_. _d_ is wire diameter. Coil center for the first layer is 
at  (0, 0, 0). Program for volume elements is __racetrackplanar__.

![Racetrack Cylinder](/doc/pictures/RacetrackCylinder.svg)

__Racetrack Cylinder__ geometry is given by length of straight section along 
z-axis _h_, radius _R_, curved section along cylinder surface _e_, radial
width of winding _w_ and number of turns _N_ on one layer. Thickness of the 
coil from mandrel surface is _t_ with number of layers _O_. _d_ is wire 
diameter. The winding starts from cylindrical surface of radius _R2_. Coil 
center for the first layer is at  (_R2_, 0, 0). Program for volume elements 
is __racetrackcylinder__.

Application Programs
--------------------

__axialgradiometer R b w d M I ax ay az theta xt yt zt [v]__

```ascii
Create volume elements for axial gradiometer. Bottom ring center is at (0, 0, 0) and top (0, 0, b) with radius R and wire diameter d. w is opening in ring for two wires connection top and bottom rings. Current in bottom ring is flowing in counter clockwise direction and clockwise direction in top ring.

- R ring radius
- b separation between rings
- w opening for wires between top and bottom rings
- d wire diameter
- M number of volume elements
- I current

Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt).
```

__cylindertube file irad wall height lc Nlayers [Ir Iphi Iz] [v]__

```ascii
Create 3D mesh for cylindrical tube with Gmsh library. The written mesh file has name 'file.msh' and current density file 'file_J.pos'. The current density vector is defined for each mesh node coordinate.

The mesh has physical groups Top, Bottom, Inner, Outer and Solid.

- irad inner radius [m]
- wall thickness [m]
- height [m]
- lc characteristic length for meshing [m]
- nlayers number of radial mesh layers
- Ir current in outward radial direction [A]
- Iphi current in circumference [A]
- Iz current in axial direction [A]
```

__cylindervolume file.msh dim rad height lc Nlayers [v]__

```ascii
Create 1D, 2D or 3D mesh for cylinder with Gmsh library.

The mesh has physical groups Top, Bottom, Outer and Solid.

- dim dimension 1, 2 or 3
- rad radius [m]
- height [m]
- lc characteristic length for mesh generation [m]
- Nlayers number of mesh layers
```

__disk file.msh rad thickness lc Nlayers [v]__

```ascii
Create 3D mesh for disk with Gmsh library.

The mesh has physical groups Top, Bottom, Outer and Solid.

- rad radius [m]
- thickness [m]
- lc characteristic length for mesh generation [m]
- Nlayes number of mesh layers
```

__fluxfield file.pdv alpha beta gamma xt yt zt I1 Bx By Bz [v]__

```ascii
Read position-current_density-volume file (coil) and calculate induced flux from static magnetic field.

The volume is rotated counter clockwise around z-axis by angle alpha followed by counter clockwise rotation around y-axis by angle beta. Finally it is rotated counter clockwise by angle gamma around z-axis.

After rotation the volume is translated by vector (xt, yt, zt).

Model for fractional energy is

dW = 0.5 * (Jx * Ax + Jy * Ay + Jz * Az) * dv

Here J = (Jx, Jy, Jz) is current density vector in volume dv. Vector potential A = (Ax, Ay, Az) is calculated from A = -0.5 r x B. I1 is current flowing in volume.

Flux linkage is calculate from energy

Phi = 2 * W / I1

Translations can be used to test invariance of results with (xt, yt, zt).
```

__fluxind file1.pdv file2.pda xt yt zt ax ay az theta I1 nrand [sigma T f Nsim] [v]__

```ascii
Read position-surface_current-area file and calculate induced flux to coil.

The file1.pdv has list of coordinates, current densities and volume of small element in this position.

x y z Jx Jy Jz dv
...

The file2.pda has list of coordinates, surface current densities jm = M x n and area of element in this position.

x y z jmx jmy jmz da
...

The first volume is rotated around axis (ax, ay, az) by angle theta. After rotation the first volume is translated by vector (xt, yt, zt).

Model for coupled energy and flux is

dFlux12 = 1e-7 * sqrt(ur1*ur2) * dv * da * J1 . jm2 / (r12 * prob *I1)

Here prob = prob1 * prob2 with prob1 = dv/V and prob2 = da/A.

10^nrand random samples are taken from volume V and surface A. Average of dFlux12 gives estimate of total flux coupling. The error is estimated from variance of these samples.

I1 is current flowing in volume V. This was used to generate the file1.pdv from known geometry like helix. Otherwise it needs to be estimated from the data.

The minimum and maximum values, and number of bins for histograms are read from file '.fluxind' if available. The file is written with new values before program exits. The histograms are:

- x, y, z position of randomly taken volume or surface sample
- element volume
- element area
- distance between elements
- probability to select these two elements
- volume current density J1
- surface current jm2

File '.fluxind' can be edited for histogram minimum and maximum values. It has following lines

min_prob max_prob min_vol max_vol min_area max_area min_dist max_dist
mix_x max_x min_y max_y min_z max_z
min_J max_J min_jm max_jm
min_jmx max_jmx min_jmy max_jmy min_jmz max_jmz
min_Jrnd max_Jrnd
number of bins

Random thermal noise currents can be simulated by giving electrical conductivity sigma [S/m], temperature T [K] and number of different random direction surface current density Jrnd distributions in file 2. The jm in file2.pda is ignored in this case. The length of current vector in da is taken from random Gaussian distribution with standard deviation Jrnd = sqrt( 4 * sigma * kB * T / dv ). Here dv = skin_depth * da and skin depth is given by sqrt( 2 / ur * u0 * 2 * pi * f * sigma ).
```

__gradient8wire r b L d N M I [v]__

```ascii
Create volume elements for wire making continuous '8' loops gradually spacing wire along z-axis. Point (0, 0, 0) is between two bobbins with symmetry z-axes on points (0, +b/2, 0) and (0, -b/2, 0). Here +dz is given by coil length L and number of turns N. When winding the gradiometer on two parallel bobbins each with diameter 2*r there are straight wire sections in between that move gradually along z-axis too. The wire starts from (0, 0, 0) and is would counter clockwise around first bobbin and then clockwise around second bobbin.
- r bobbin radius
- b distance between two gradiometer bobbin centers
- L gradiometer coil length
- d wire diameter
- N number of turns
- M number of volume elements
- I current
```

__helixwire r L d N M I ax ay az theta xt yt zt [v]__

```ascii
Create volume elements for helix wire. The coil bottom center is at (0, 0, 0). Wire starts from (r, 0, 0) and is wound counter clockwise around z-axis with length L along helix axis and N number of turns.

- r helix radius
- L length along helix axis
- d wire diameter
- N number of turns
- M number of volume elements
- I current

Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt).
```

__helmholtzwire h w b r L d N M I [v]__

```ascii
Create volume elements for Helmholtz coil wire. The coil central axis is along z-axis with point (0, 0, 0). Wire sections in first turn of first coil are:

1 straight (+w/2,  -h/2+r, b/2-L/2) to (+w/2,   h/2-r, b/2-L/2)
2 arc      (+w/2,   h/2-r, b/2-L/2) to (+w/2-r, h/2,   b/2-L/2)
3 straight (+w/2-r, h/2,   b/2-L/2) to (-w/2+r, h/2,   b/2-L/2)
4 arc      (-w/2+r, h/2,   b/2-L/2) to (-w/2,   h/2-r, b/2-L/2)
5 straight (-w/2,   h/2-r, b/2-L/2) to (-w/2,  -h/2+r, b/2-L/2)
6 arc      (-w/2,  -h/2+r, b/2-L/2) to (-w/2+r,-h/2,   b/2-L/2)
7 straight (-w/2+r,-h/2,   b/2-L/2) to (+w/2-r,-h/2,   b/2-L/2)
8 arc      (+w/2-r,-h/2,   b/2-L/2) to (-w/2,  -h/2+r, b/2-L/2+L/N)

Wire sections in first turn of second coil are:

1 straight (+w/2,  -h/2+r, -b/2-L/2) to (+w/2,   h/2-r, -b/2-L/2)
2 arc      (+w/2,   h/2-r, -b/2-L/2) to (+w/2-r, h/2,   -b/2-L/2)
3 straight (+w/2-r, h/2,   -b/2-L/2) to (-w/2+r, h/2,   -b/2-L/2)
4 arc      (-w/2+r, h/2,   -b/2-L/2) to (-w/2,   h/2-r, -b/2-L/2)
5 straight (-w/2,   h/2-r, -b/2-L/2) to (-w/2,  -h/2+r, -b/2-L/2)
6 arc      (-w/2,  -h/2+r, -b/2-L/2) to (-w/2+r,-h/2,   -b/2-L/2)
7 straight (-w/2+r,-h/2,   -b/2-L/2) to (+w/2-r,-h/2,   -b/2-L/2)
8 arc      (+w/2-r,-h/2,   -b/2-L/2) to (-w/2,  -h/2+r, -b/2-L/2+L/N)

- h coil height
- w coil width
- b separation between two coils
- r corner radius
- L coil length
- d wire diameter
- N number of turns
- M number of volume elements
- I current

Length of wire in one turn

2 * ( w + h ) + ( 3 pi/2 - 8 ) * r + pi/2 * sqrt( r^2 + L^2/(2*pi*N)^2 )
```

__indmtrx file1.yaml file2.yaml Nrand [v]__

```ascii
The file1.yaml and file2.yaml have parametic definition of coil integration or file name for list of coordinates, current densities and volume of small element in this position.

x y z Jx Jy Jz dv
...

The yaml-files define rotation around axis (ax, ay, az) by angle theta. After rotation the volume is translated by vector (xt, yt, zt).

Model for small fractional inductance is

dM12 = 1e-7 * sqrt( ur1 * ur2 ) * dv1 * dv2 * J1 . J2 /( r12 * prob * I1 * I2)

Here prob = prob1 * prob2 with prob1 = dv1/V1 and prob2 = dv2/V2.

Fractional force between two volume elements is

dF12 = 1e-7 * sqrt( ur1 * ur2 ) * dv1 * dv2 * J2 x ( J1 x ( r2 - r1 ) ) / ( r12^3 * prob )

10^nrand random samples are taken from volumes V1 and V2. Average of dM12 gives estimate of total mutual inductance between the volumes. Error is estimated from variance of these samples.

I1 is current flowing in volume V1 and I2 current in V2. These were used to generate file1.pdv and file2.pdv. Otherwise they need to be estimated from the data.

Minimum and maximum values, and number of bins for histograms are read from file '.indmtrx' if available. This file is written with new values before program exits. The histograms are:

- x, y, z position of randomly taken volume sample
- element volume dv1 and dv2
- distance between elements
- probability to select these two elements
- volume current density J1 and J2

File '.indmtrx' can be edited for histogram maximum and minimum values but not for number of bins. It has following lines:

nbin_prob1 mnprob1 mxprob1
nbin_prob2 mnprob2 mxprob2
nbin_vol1 mnvol1 mxvol1
nbin_vol2 mnvol2 mxvol2
nbin_dst mndst mxdst
nbin_J1 minJ1 maxJ1
nbin_J2 minJ2 maxJ2
nbin_Jrnd minJrnd maxJrnd
ignored min_dM12 max_dM12
nbin_x minx maxx
ignored miny maxy
ignored minz maxz
ignored minFx maxFx
ignored minFy maxFy
ignored minFz maxFz
ignored minF maxF
ignored minBx maxBx
ignored minBy maxBy
ignored minBz maxBz
ignored minB maxB
nbin_xy

File '.indmtrx' is overwritten with new histogram ranges and good values need to be kept in a separate file.
```

__injectionvolume file.msh dim rcyl hcyl rtube htube lhorizon lc [v]__

```ascii
Create 1D, 2D or 3D mesh for cylider with 90 degree elbow on bottom connected to horizontal tube.

- dimension 1, 2 or 3
- rcyl top cylinder radius [m]
- hcyl top cylinder height [m]
- rtube bottom tube radius [m]
- htube bottom tube height [m]
- lhorizon horizontal tube length [m]
- lc characteristic length for mesh generation [m]
```

__mapB file.yaml x0/r0 y0/phi0 z0 dx/dr dy/dphi dz steps Nrand [cyl] [v]__

```ascii
Read yaml-file (coil) and calculate magnetic field at grid positions r2 = (x0 + l * dx, y0 + k * dy, z0 + j * dz).
j = [-steps,steps+1], k = [-steps,steps+1], l = [-steps, steps+1].
For one or two dimensional map set dx, dy or dz to zero.

Model for fractional field is

dB(r2) = u0/4*pi * J(r1) x (r2 - r1) * dv/|r2 - r1|^3

For position-current-density-dv files sum of dB(r2) is taken for the total field. With parametric coil models 10^Nrand random samples are calculated to estimate total field B(r2).
Here J = (Jx, Jy, Jz) is current density vector in volume dv at position r1.
```

__mesh2da file.msh scale holes.txt planes.txt axis.txt Mx My Mz [v|view|plane]__

```ascii
Read gmsh surface mesh file and print centroid of each triangle and quadrangle element followed by surface current vector and area.

The file.msh has Gmsh 2D mesh to model surface of weakly magnetized volume with M = (Mx, My, Mz). Verbosed printing of file processing can be seen with option 'v'.

File holes.txt is list of holes that are defined by central axis vector (v1x, v1y, v1z), point on axis (p1x, p1y, p1z) and bounding box radius Rb and height Lb. Here +-Lb/2 is taken along the central axis vector with (p1x, p1y, p1z) at center.

v1x v1y v1z p1x p1y p1z Rb Lb
...

File planes.txt is list of known planes with outward normal vector. The plane is defined by normal vector (n1x, n1y, n1z) and point on plane (p1x, p1y, p1z). After this there is outward normal vector of the plane (nn1x, nn1y, nn1z).

n1x n1y n1z p1x p1y p1z nn1x nn1y nn1z
...

File axis.txt is list of symmetry axes defined by direction vector (v1x, v1y, v1z) and point on axis (p1x, p1y, p1z).

v1x v1y v1z p1x p1y p1z
...

Node tags and coordinates from mesh are read in with function call

gmsh::model::mesh::getNodes()

List (vector) of 2D elements (triangles) are read in with
gmsh::model::mesh::getElements()

List of elements is used to get node coordinates (vertices) of each triangle. Centroid Pc = (xc, yc, zc) and area da of each triangle is calculated from these coordinates. The point Pc is tested to see if it is on one of the planes given in file planes.txt. This is then used to define outward direction for the pseudovector normal calculated from triangle edges nv = (P1 - P0) x (P2 - P0).

If the point Pc is not inside hole bounding box and does not belong to one of the known planes the nv is tested to see if it is on any plane perpendicular to axis in file axis.txt. Distance of Pc is calculated from symmetry axis. New vector is calculated from centroid by adding triangle normal vector scaled by square root of triangle area. Distance of this new point is calculated from symmetry axis. If distance is larger than the first distance the normal is pointing outwards for outer surface.

Outer surface current is jm = M x nv. This data is printed as list

xc yc zc jmx jmy jmz da
...

for each surface element.

With option 'view' Gmsh postprocessing data is printed. This file can be merged in Gmsh with the mesh file to see calculate jm vector field.

The 'scale' is used for linear scaling of dimensions.

With option 'plane' two unit length vectors to define area element are printed instead of surface current

xc yc zc ux uy uz vx vy vz da
...

This is useful for random surface current simulations.
```

__mesh2dv file scale [zeroJ] [v]__

```ascii
Read gmsh mesh file and print centroid of each tetrahedron element followed by tetrahedron volume.

The file.msh has Gmsh 3D mesh to model current carrying volume. In addition file_J.pos is needed to calculate current density vector in each volume element. Verbosed printing of file processing can be seen with option 'v'.

Node tags and coordinates from mesh are read in with function call

gmsh::model::mesh::getNodes()

3D elements are read in with

gmsh::model::mesh::getElements()

The current density J data is read in with

gmsh::view::getListData()

List (vector) of elements is used to get node coordinates (vertices) of each tetrahedron. Centroid (xc, yc, zc) and volume dv of each tetrahedron is calculated from these coordinates. Current density vector (Jx, Jy, Jz) is taken as average from values on each tetrahedron node.  This data is printed as list

xc, yc, zc, Jx, Jy, Jz, dv
...

for each volume element.

The 'scale' is used for linear scaling of dimensions.

With option 'zeroJ' file_J.pos is not used and zero is written to each current density vector (Jx, Jy, Jz).
```

__probeB file.pdv ax ay az theta xt yt zt x2 y2 z2 [v]__

```ascii
Read position-current_density-volume file (coil) and calculate magnetic field at position r2 = (x2, y2, z2).

The volume is rotated around axis (ax, ay, az) by angle theta. After rotation the volume is translated by vector (xt, yt, zt).

Model for fractional field is

dB(r2) = u0/4*pi * J(r1) x (r2 - r1) * dv/|r2 - r1|^3

Here J = (Jx, Jy, Jz) is current density vector in volume dv at position r1.
```

__racetrackplanar R h e w N t O d M I ax ay az theta xt yt zt [v]__

```ascii
Wire sections in first turn on first layer are:
1. straight    (0, +R, -h/2) to (0, +R, +h/2)
2. half circle (0, +R, +h/2) to (0, -R, +h/2)
3. straight    (0, -R, +h/2) to (0, -R, -h/2)
4. half circle (0, -R, -h/2) to (0, +R, -h/2)
d is wire diameter, M number of volume elements and I current.  Current flows in counter clockwise direction around x-axis.

- R corner radius
- h length of straight section along z-axis
- e length of straight section along y-axis
- w width of coil winding
- t coil thickness along x-axis
- N number of turns
- O number of layers
- d wire diameter
- M number of volume elements
- I current

Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt).

Length of wire on one layer is L = 2*(h+e)*N + 2*pi*R*N + pi*w*N.
```

__ringwire R phi0 phi1 d M I ax ay az theta xt yt zt [v]__

```ascii
Create volume elements for wire ring or arc. Ring or arc center is at (0, 0, 0) and current flows in counter clockwise direction. The ring starts at (R cos(phi0), R sin(phi0), 0) and ends at (R cos(phi1), R sin(phi1), 0).
Sweep angle phi1 - phi0 = 2 pi gives complete ring.

- R ring radius
- phi0 start angle
- phi1 end angle
- d wire diameter
- M number of volume elements
- I current

Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt).
```

__simJNB file.pdv ax ay az theta xt yt zt x2 y2 z2 sigma T Nsim [v]__

```ascii
Read position-current_density-volume file, generate new random current distribution and calculate magnetic field at position r2 = (x2, y2, z2).

The volume is rotated around axis (ax, ay, az) by angle theta. After rotation the volume is translated by vector (xt, yt, zt).

Model for fractional field is

dB(r2) = u0/4*pi * J(r1) x (r2 - r1) * dv/|r2 - r1|^3

Here J = (Jx, Jy, Jz) is current density vector in volume dv at position r1.

The simulation is repeated Nsim times using conductivity sigma [S/m] and temperature T [K]. Length of current vector in dv is taken from random Gaussian distribution with standard deviation Jrnd = sqrt( 4 * sigma * kB * T / dv ).
```

__straightwire L d M I ax ay az theta xt yt zt [v]__

```ascii

Create volume elements for straight wire. Wire bottom center is at (0, 0, 0) and top end at (0, 0, L). Current flows from bottom to top.

- L wire length
- d wire diameter
- M number of volume elements
- I current

Rotate wire elements around axis (ax, ay, az) with angle theta and translate by vector (xt, yt, zt).
```

Perl Scripts
------------

__mapfield -1 volume1.pdv -a ax,ay,az,theta -t xt,yt,zt -p x2,y2,z2 -d dx2,dy2,dz2 -s steps [-v] [-V]__

```ascii

Rotate and translate volume 1 to different positions and call 'probeB' to get magnetic field.
```

__mapindmtrx -1 volume1.pdv -2 volume2.pdv -p x0,y0,z0 -t dx,dy,dz -a ax,ay,az -r theta,dtheta -s steps -N Nrand -c I1,I2 [-v] [-V]__

```ascii

Rotate and translate volume 1 to different positions and call 'indmtrx' to get flux linkage and mutual inductance.
```

Python Scripts
--------------

__plotB.py [-h] [-x] [-y] [-z] [--xy] [--xz] [--yz] [--Bx] [--By] [--Bz]
                [--B] [-c] [--colormap COLORMAP] [-s] [-e]
                filename [filename ...]__

```ascii

Plot calculated field map from mapB.
```

__plotJ.py [-h] [-s] [-e] length filename [length2] [filename2]__

```ascii

Plot current density vectors of small differential volumes.
```

__plotpdv.py [-h] [-s] [-e] filename [filename2]__

```ascii

Plot position of small differential volumes.
```


Windows Subsystem for Linux
===========================

Instructions on how to install WSL for Windows 10

https://docs.microsoft.com/en-us/windows/wsl/install-win10

The WSL is minimal system to run programs from command line. It may not have
all the necessary tools to compile the IIFlucks code. These need to be
installed for example

```shell
sudo apt-get install gcc g++ cmake libgsl-dev pkg-config
```

The utility __dos2unix__ or __unix2dos__ can translate line feed and 
carriage return codes in text files when moving these files between 
WSL shell and Windows file system.

To produce histogram plot files __gnuplot__ is needed. Copy the scripts
to local _/bin_ directory.

```shell
mkdir ~/bin
cp iiflucks/gnuplot/*.p ~/bin
```

To build the gmsh library following packages are needed

```shell
sudo apt-get install gfortran libblas-dev liblapack-dev
```
