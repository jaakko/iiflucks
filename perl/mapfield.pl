#!/usr/bin/perl -w 
# 
# Rotate and translate volume 1 to different positions and call 'probeB' to
# get magnetic field.
#
# Sun Jun  9 16:27:57 CDT 2019
# Edit: 
#
# Jaakko Koivuniemi

use strict;
use Getopt::Std;

use subs qw/printusage/;

my $verno = "20190609";
my $dbg = 0;

# options: -V version, -v verbose
our ($opt_1,$opt_a,$opt_t,$opt_p,$opt_d,$opt_s,$opt_v,$opt_V);

$opt_1="";$opt_a="";$opt_t="";$opt_p="";$opt_d="";$opt_s=0;$opt_V=0;$opt_v=0;

getopts('1:a:t:p:d:s:vV');

if($opt_V)
{
    print "mapfield v. $verno, Jaakko Koivuniemi\n";
    exit;
}

printusage() if((!$opt_1)||(!$opt_a)||(!$opt_t)||(!$opt_p)||(!$opt_d)||(!$opt_s));

# initial position coordinates (x, y, z)
my ($x2, $y2, $z2) = split /,/, $opt_p;

# step vector (dx, dy, dz)
my ($dx, $dy, $dz) = split /,/, $opt_d;

# axis of rotation and angle 
my ($ax, $ay, $az, $theta) = split /,/, $opt_a;

# translation vector (xt, yt, zt)
my ($xt, $yt, $zt) = split /,/, $opt_t;

my $cmd = "";
my $res = "";
my ($x, $y, $z) = (0, 0, 0);
my ($Bx, $By, $Bz) = (0, 0, 0);

print "x[m]     y[m]     z[m]     Bx[T]        By[T]        Bz[T]\r\n";

for( my $j = 0; $j < $opt_s; $j++ )
{
  $x = $x2 + $j * $dx;
  $y = $y2 + $j * $dy;
  $z = $z2 + $j * $dz;

  print (sprintf("%-+8.4f %-+8.4f %-+8.4f ", $x, $y, $z) ); 

  $cmd = "probeB $opt_1 $ax $ay $az $theta $xt $yt $zt $x $y $z"; 

  print "\r\n$cmd\r\n" if( $dbg );

  $res = `$cmd`;

  print $res if( $opt_v );

  if( $res =~ /.+\)\R(.+),(.+),(.+)/m )
  {
    $Bx = scalar( $1 );
    $By = scalar( $2 );
    $Bz = scalar( $3 );
    print (sprintf("%-+12.5e %-+12.5e %-+12.5e\r\n", $Bx, $By, $Bz) );
  }
}

# print usage message
sub printusage 
{    
    print "usage: mapfield -1 volume1.pdv -a ax,ay,az,theta -t xt,yt,zt -p x2,y2,z2 -d dx2,dy2,dz2 -s steps [-v] [-V]\n";
    exit;
}


