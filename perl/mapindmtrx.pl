#!/usr/bin/perl -w 
# 
# Rotate and translate volume 1 to different positions and call 'indmtrx' to
# get flux linkage and mutual inductance.
#
# Tue May 28 13:39:32 CDT 2019
# Edit: Mon Jun  3 12:24:19 CDT 2019
#
# Jaakko Koivuniemi

use strict;
use Getopt::Std;

use subs qw/printusage/;

my $verno="20190603";
my $dbg=0;

# options: -V version, -v verbose
our ($opt_1,$opt_2,$opt_p,$opt_t,$opt_a,$opt_r,$opt_s,$opt_N,$opt_c,$opt_v,$opt_V);

$opt_1="";$opt_2="";$opt_p="";$opt_t="";$opt_a="";$opt_r="";$opt_s=0;$opt_N=0;
$opt_c="";$opt_V=0;$opt_v=0;

getopts('1:2:p:t:a:r:s:N:c:vV');

if($opt_V)
{
    print "mapindmtrx v. $verno, Jaakko Koivuniemi\n";
    exit;
}

printusage() if((!$opt_1)||(!$opt_2)||(!$opt_p)||(!$opt_t)||(!$opt_a)||(!$opt_r)||(!$opt_s)||(!$opt_N)||(!$opt_c));

# initial position coordinates (x, y, z)
my ($x0, $y0, $z0) = split /,/, $opt_p;

# translation vector (dx, dy, dz)
my ($dx, $dy, $dz) = split /,/, $opt_t;

# axis of rotation 
my ($ax, $ay, $az) = split /,/, $opt_a;

# rotation from theta with dtheta 
my ($theta, $dtheta) = split /,/, $opt_r;

# currents
my ($I1, $I2) = split /,/, $opt_c;

my $cmd = "";
my $res = "";
my ($x, $y, $z) = (0, 0, 0);
my $t = 0;

print "x[m]     y[m]     z[m]     theta      E21[J]                    Flux21[Wb]                M21[H]\r\n";

for( my $j = 0; $j < $opt_s; $j++ )
{
  $x = $x0 + $j * $dx;
  $y = $y0 + $j * $dy;
  $z = $z0 + $j * $dz;

  $t = $theta + $j * $dtheta;

  print (sprintf("%-+8.4f %-+8.4f %-+8.4f %-+9.5f ", $x, $y, $z, $t) ); 

  $cmd = "indmtrx $opt_1 $opt_2 $x $y $z $ax $ay $az $t $opt_N $I1 $I2"; 

  $res = `$cmd`;

  my $str = "E21\\s(.+)\\s\\+\\-\\s(.+)\\sJ";
  if( $res =~ /$str/m )
  {
    print (sprintf("%-+12.5e %-+12.5e", $1, $2) );
  } 

  $str = "Flux21\\s(.+)\\s\\+\\-\\s(.+)\\sWb";
  if( $res =~ /$str/m )
  {
    print (sprintf(" %-+12.5e %-+12.5e", $1, $2) );
  } 

  $str = "M21\\s(.+)\\s\\+\\-\\s(.+)\\sH";
  if( $res =~ /$str/m )
  {
    print (sprintf(" %-+12.5e %-+12.5e", $1, $2) );
  } 

  print "\r\n";

  print $res if( $opt_v );
}

# print usage message
sub printusage 
{    
    print "usage: mapindmtrx -1 volume1.pdv -2 volume2.pdv -p x0,y0,z0 -t dx,dy,dz -a ax,ay,az -r theta,dtheta -s steps -N Nrand -c I1,I2 [-v] [-V]\n";
    exit;
}


