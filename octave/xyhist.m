#!/usr/bin/octave
# https://stackoverflow.com/questions/33610420/creating-a-color-map-heatmap-in-matlab-octave

data = load('hxy.txt');

x = ( data(:, 1) + data(:, 2) ) / 2;
y = ( data(:, 3) + data(:, 4) ) / 2;
w = data(:, 5);

n = 256;

[X, Y] = meshgrid( linspace( min( x ), max( x ), n), linspace( min( y ), max( y ), n ) );

W = griddata(x, y, w, X, Y);

W( isnan(W) ) = 0;

m = min( W( W~= 0 ) );
M = max( W( W~= 0 ) );

imagesc( (W - m)/(M - m) );

print ( "hxy.png", "-dpng");

