#!/usr/bin/octave
# https://stackoverflow.com/questions/33610420/creating-a-color-map-heatmap-in-matlab-octave

data = load('hyz.txt');

y = ( data(:, 1) + data(:, 2) ) / 2;
z = ( data(:, 3) + data(:, 4) ) / 2;
w = data(:, 5);

n = 256;

[Y, Z] = meshgrid( linspace( min( y ), max( y ), n), linspace( min( z ), max( z ), n ) );

W = griddata(y, z, w, Y, Z);

W( isnan(W) ) = 0;

m = min( W( W~= 0 ) );
M = max( W( W~= 0 ) );

imagesc( (W - m)/(M - m) );

print ( "hyz.png", "-dpng");

