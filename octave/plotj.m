
cd("C:/Users/me/iiflucks/samples");

A = load("ringcylinderwire-sample.pdv");

x = A(:, 1);

y = A(:, 2);

z = A(:, 3);

jx = A(:, 4);

jy = A(:, 5);

jz = A(:, 6);

quiver3(x, y, z, jx, jy, jz, 0.3);

print("ringcylinderwire-sample-j.svg", "-dsvg");
print("ringcylinderwire-sample-j.png", "-dpng");
print("ringcylinderwire-sample-j.pdf", "-dpdf");


