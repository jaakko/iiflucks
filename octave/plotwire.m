
cd("C:/Users/me/iiflucks/doc/samples");

A = load("ringcylinderwire-sample.pdv");

x = A(:, 1);

y = A(:, 2);

z = A(:, 3);

jx = A(:, 4);

jy = A(:, 5);

jz = A(:, 6);

plot3(x, y, z);

print("ringcylinderwire-sample.svg", "-dsvg");
print("ringcylinderwire-sample.png", "-dpng");
print("ringcylinderwire-sample.pdf", "-dpdf");


