file = 'noisedata.txt';

data = load( file );

hist( data(:, 3) );

xlabel( 'Flux[Wb]' );
ylabel( 'Samples' );
title( 'Copper at 1 K to coil' );

m = mean( data(:, 3) );
s = std( data(:, 3) );
n = length( data(:, 3) );

timenow = strftime ("%Y-%m-%d %H:%M:%S", localtime ( time () ) );
fprintf('-- %s\n\r', timenow);
fprintf('-- %s\n\r', file);
fprintf('-- mean %e and standard deviation %e with %d samples\n\r', m, s, n);

