#ifndef HELIX_H
#define HELIX_H
#pragma once

#include "Coil.h"

/// Class for helix inherited from Coil base class. 

/// The constructor _Helix_ sets name tag and parameters.
class Helix : public Coil 
{
  std::string name;       ///< name tag for helix 
  double R;               ///< helix radius[m]
  double L;               ///< helix length[m]
  double N;               ///< number of turns

  public:
  /// Construct Helix object with parameters.
  Helix(std::string name, double R, double L, double N, double A, double J, double theta, std::vector<double> axis, std::vector<double> tr) : Coil(A, J, theta, axis, tr)
  {
    this->name = name;
    this->R = R;
    this->L = L;
    this->N = N;
  };

  Helix(std::string name, double R, double L, double N, double A, double J) : Coil( A, J, 0, {0, 0, 1}, {0, 0, 0} )
  {
    this->name = name;
    this->R = R;
    this->L = L;
    this->N = N;
  };

  virtual ~Helix();

  /// Get helix name tag.
  std::string GetName() { return name; }

  /// Get helix radius [m].
  double GetR() { return R; }

  /// Set helix radius [m].
  void SetR(double R) { this->R = R; }

  /// Get helix length [m].
  double GetL() { return L; }

  /// Set helix length [m].
  void SetL(double L) { this->L = L; }

  /// Get number of turns.
  double GetN() { return N; }

  /// Set number of turns.
  void SetN(double N) { this->N = N; }

  /// Helix curve length [m].
  double Length(const double t1, const double t2);

  /// Helix small section t = [ t - dt/2, t + dt/2 ] length [m].
  double dl(const double dt, const double t);

  /// Helix small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t);

  /// Helix small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const double r1, const double r2);

  /// Helix curve parametric length [m].
  double ds(const double t);

  /// Parametric position vector with parameter t.
  std::vector<double> XYZ(const double t);

  /// Parametric tangent vector with parameter t.
  std::vector<double> Jv(const double t);

  /// Numerical integration of magnetic field at position P. Return 0 in success.
  int B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

  /// Print list of elements with position coordinates and current density vectors.
  double Elements(const int M, const int test, const bool verbose);
};

#endif
