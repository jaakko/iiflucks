#ifndef CONSTANTS_H
#define CONSTANTS_H
#pragma once
#include <cstddef>

namespace constants
{
  inline constexpr int maxpoints { 70000 }; 
  inline constexpr int matrixsize{ 100 }; 
  // logarithmic binning limits
  inline constexpr int histo_log_bins = 15;
  inline constexpr double histo_log_range[ histo_log_bins + 1 ] = {-1e-1, -1e-2, -1e-3, -1e-4, -1e-5, -1e-6, -1e-7, -1e-8, +1e-8, +1e-7, +1e-6, +1e-5, +1e-4, +1e-3, +1e-2, +1e-1};

  // epsilon for float comparison | x - y | < equal
  inline constexpr double equal = 1e-3;

  // stored array size for field maps
  inline constexpr std::size_t field_map_size = 10000;
}

#endif
