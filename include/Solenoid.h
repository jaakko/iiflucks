#ifndef SOLENOID_H
#define SOLENOID_H
#pragma once

#include "Coil.h"

/// Class for multilayer solenoid inherited from Coil base class. 

/// The constructor _Solenoid_ sets name tag and parameters.
class Solenoid : public Coil 
{
  std::string name;       ///< name tag for ring 
  double R;               ///< solenoid inner radius[m]
  double L;               ///< solenoid length [m]
  double th;              ///< total thickness of layers[m]
  int sections = 1;       ///< number of sections in one turn
  int turns;              ///< number of turns on one layer
  int layers;             ///< number of layers

  // thickness step for next layer
  double dth;

  public:
  /// Construct Solenoid object with parameters.
  Solenoid(std::string name, double R, double L, double th, int turns, int layers, double A, double J, double theta, std::vector<double> axis, std::vector<double> tr) : Coil(A, J, theta, axis, tr)
  {
    this->name = name;
    this->R = R;
    this->L = L;
    this->th = th;
    this->turns = turns;
    this->layers  = layers;
  };

  Solenoid(std::string name, double R, double L, double th, int turns, int layers, double A, double J) : Coil( A, J, 0, {0, 0, 1}, {0, 0, 0} )
  {
    this->name = name;
    this->R = R;
    this->L = L;
    this->th = th;
    this->turns = turns;
    this->layers  = layers;
  };

  virtual ~Solenoid();

  /// Get solenoid name tag.
  std::string GetName() { return name; }

  /// Get inner radius [m].
  double GetR() { return R; }

  /// Set inner radius [m].
  void SetR(double R) { this->R = R; }

  /// Get length [m].
  double GetL() { return L; }

  /// Set length [m].
  void SetL(double L) { this->L = L; }

  /// Get layers total thickness [m].
  double Getth() { return th; }

  /// Set layers total thickness [m].
  void Setth(double th) { this->th = th; }

  /// Get number of sections on one turn.
  int GetSections() { return sections; }

  /// Get number of turns on one layer.
  int GetTurns() { return turns; }

  /// Set number of turns on one layer.
  void SetTurns(int turns) { this->turns  = turns; }
   
  /// Get number of layers.
  int GetLayers() { return layers; }

  /// Set number of layers.
  void SetLayers(int layers) { this->layers = layers; }
   
  /// Calculate total lenght of coil [m]. Do this first before other calculations.
  double CalcLength(std::vector<double> t1v, std::vector<double> t2v, bool verbose);

  /// Solenoid curve length | t2 - t1 | * helix length [m]. 
  double Length(const double t1, const double t2, const int section, const int turn, const int layer);

  /// Solenoid small section t = [ t - dt/2, t + dt/2 ] length [m].
  double dl(const double dt, const double t, const int section, const int turn, const int layer);

  /// Solenoid small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const int section, const int turn, const int layer);

  /// Solenoid small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const double r1, const double r2, const int section, const int turn, const int layer);

  /// Solenoid curve parametric length [m].
  double ds(const double t, const int section, const int turn, const int layer);

  /// Parametric position vector with parameter t.
  std::vector<double> XYZ(const double t, const int section, const int turn, const int layer);

  /// Parametric tangent vector with parameter t.
  std::vector<double> Jv(const double t, const int section, const int turn, const int layer);

  /// Number of equally long elements lc for section, turn and layer.
  int NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v, const int section, const int turn, const int layer);

  /// Number of equally long elements lc for coil.
  int NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v);

  /// Print helix elements with position coordinates and current density vectors.
  double Elements(const double t1, const double t2, const int section, const int turn, const int layer, const int M, const int test, const bool verbose);

};

#endif
