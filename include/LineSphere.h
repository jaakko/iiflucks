#ifndef LINESPHERE_H
#define LINESPHERE_H
#pragma once

#include "Coil.h"

/// Class for line on sphere inherited from Coil base class. 

/// The constructor _LineSphere_ sets name tag, distance _R1_ from x-axis for
/// line along z-axis projected on spherical surface and sphere radius _R2_. 
/// Parameter _t = [-Pi/2, +Pi/2]_ is angle between xy-plane and position
/// vector _P(x,y,z)_ with _z = R2 sin( t )_. The azimulthal angle is 
/// _cos( phi ) = R2 / sqrt( R1^2 + R2^2 )_ and 
/// _sin( phi ) = R1 / sqrt( R1^2 + R2^2 ).
///
class LineSphere : public Coil 
{
  std::string name;       ///< name tag for line on sphere 
  double R1;              ///< line along z-axis distance from x-axis[m]
  double R2;              ///< sphere radius[m]

  public:
  /// Construct LineSphere object with parameters.
  LineSphere(std::string name, double R1, double R2, double A, double J, double theta, std::vector<double> axis, std::vector<double> tr) : Coil(A, J, theta, axis, tr)
  {
    this->name = name;
    this->R1 = R1;
    this->R2 = R2;
  };

  LineSphere(std::string name, double R1, double R2, double A, double J) : Coil( A, J, 0, {0, 0, 1}, {0, 0, 0} )
  {
    this->name = name;
    this->R1 = R1;
    this->R2 = R2;
  };

  virtual ~LineSphere();

  /// Get line on sphere name tag.
  std::string GetName() { return name; }

  /// Get projected line along z-axis distance from x-axis [m].
  double GetR1() { return R1; }

  /// Set projected line along z-axis distance from x-axis [m].
  void SetR1(double R1) { this->R1 = R1; }

  /// Get sphere radius [m].
  double GetR2() { return R2; }

  /// Set sphere radius [m].
  void SetR2(double R2) { this->R2 = R2; }

  /// LineSphere curve length [m].
  double Length(const double t1, const double t2);

  /// LineSphere small section t = [ t - dt/2, t + dt/2 ] length [m].
  double dl(const double dt, const double t);

  /// LineSphere small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t);

  /// LineSphere small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const double r1, const double r2);

  /// LineSphere curve parametric length [m].
  double ds(const double t);

  /// Parametric position vector with parameter t.
  std::vector<double> XYZ(const double t);

  /// Parametric tangent vector with parameter t.
  std::vector<double> Jv(const double t);

  /// Calculation of magnetic field at position P. Return 0 in success.
  int B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

  /// Print list of elements with position coordinates and current density vectors.
  double Elements(const double t1, const double t2, const int M, const int test, const bool verbose);
};

#endif
