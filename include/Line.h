#ifndef LINE_H
#define LINE_H
#pragma once

#include "Coil.h"

/// Class for line inherited from Coil base class. 

/// The constructor _Line_ sets name tag and parameters.
class Line : public Coil 
{
  std::string name;       ///< name tag for line 

  public:
  /// Construct Line object with parameters.
  Line(std::string name, double A, double J, double theta, std::vector<double> axis, std::vector<double> tr) : Coil(A, J, theta, axis, tr)
  {
    this->name = name;
  };

  Line(std::string name, double A, double J) : Coil( A, J, 0, {0, 0, 1}, {0, 0, 0} )
  {
    this->name = name;
  };

  virtual ~Line();

  /// Get line name tag.
  std::string GetName() { return name; }

  /// Line curve length [m].
  double Length(const double t1, const double t2);

  /// Line small section t = [ t - dt/2, t + dt/2 ] length [m].
  double dl(const double dt, const double t);

  /// Line small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t);

  /// Line small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const double r1, const double r2);

  /// Line curve parametric length [m].
  double ds(const double t);

  /// Parametric position vector with parameter t.
  std::vector<double> XYZ(const double t);

  /// Parametric tangent vector with parameter t.
  std::vector<double> Jv(const double t);

  /// Calculation of magnetic field at position P. Return 0 in success.
  int B(const double I, const double t1, const double t2, const std::vector<double> P, std::vector<double> &Bv);

  /// Print list of elements with position coordinates and current density vectors.
  double Elements(const double t1, const double t2, const int M, const int test, const bool verbose);
};

#endif
