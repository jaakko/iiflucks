#ifndef HISTOGRAMS_H
#define HISTOGRAMS_H
#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <yaml-cpp/yaml.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_histogram2d.h>
#include "constants.h"

/// Class for Monte Carlo histograms.
class Histograms 
{
  bool verbose = false; ///< verbose printing
  std::string name;     ///< name tag for histogram collection 

  // histogram minimum and maximum values
  const int nbin = 100;
  const int nbin_x = 200;
  const int nbin_xy = 400;
  double minx = 1e20, maxx = -1e20;
  double miny = 1e20, maxy = -1e20;
  double minz = 1e20, maxz = -1e20;
  double mndst = 1e10, mxdst = -1e10;
  double mnvol1 = 1e10, mxvol1 = -1e10;
  double mnvol2 = 1e10, mxvol2 = -1e10;
  double mnprob1 = 1e10, mxprob1 = -1e10;
  double mnprob2 = 1e10, mxprob2 = -1e10;
  double minJ1 = 1e20, maxJ1 = -1e20;
  double minJ1x = 1e20, maxJ1x = -1e20;
  double minJ1y = 1e20, maxJ1y = -1e20;
  double minJ1z = 1e20, maxJ1z = -1e20;
  double minJ2 = 1e20, maxJ2 = -1e20;
  double minJ2x = 1e20, maxJ2x = -1e20;
  double minJ2y = 1e20, maxJ2y = -1e20;
  double minJ2z = 1e20, maxJ2z = -1e20;
  double minJrnd = 1e20, maxJrnd = -1e20;
  double minFx = 1e20, maxFx = -1e20;
  double minFy = 1e20, maxFy = -1e20;
  double minFz = 1e20, maxFz = -1e20;
  double minF = 1e20, maxF = -1e20;
  double minBx = 1e20, maxBx = -1e20;
  double minBy = 1e20, maxBy = -1e20;
  double minBz = 1e20, maxBz = -1e20;
  double minB = 1e20, maxB = -1e20;
  double min_dM12 = 1e20, max_dM12 = -1e20;

  static const int nbin_log = 15; // log bins
  double log_range[ nbin_log + 1 ] = {-1e-1, -1e-2, -1e-3, -1e-4, -1e-5, -1e-6, -1e-7, -1e-8, +1e-8, +1e-7, +1e-6, +1e-5, +1e-4, +1e-3, +1e-2, +1e-1};

  gsl_histogram * hprob1 = gsl_histogram_alloc( nbin );
  gsl_histogram * hprob2 = gsl_histogram_alloc( nbin );
  gsl_histogram * hvol1 = gsl_histogram_alloc( nbin );
  gsl_histogram * hvol2 = gsl_histogram_alloc( nbin );
  gsl_histogram * hdst = gsl_histogram_alloc( nbin );
  gsl_histogram * hx = gsl_histogram_alloc( nbin_x );
  gsl_histogram * hy = gsl_histogram_alloc( nbin_x );
  gsl_histogram * hz = gsl_histogram_alloc( nbin_x );
  gsl_histogram2d * hxy = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hxz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hyz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * scale_xy = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * scale_xz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * scale_yz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram * hJ1x = gsl_histogram_alloc( nbin );
  gsl_histogram * hJ1y = gsl_histogram_alloc( nbin );
  gsl_histogram * hJ1z = gsl_histogram_alloc( nbin );
  gsl_histogram * hJ1 = gsl_histogram_alloc( nbin );
  gsl_histogram2d * hJ1x_xy = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ1y_xy = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ1z_xy = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ1x_xz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ1y_xz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ1z_xz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ1x_yz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ1y_yz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ1z_yz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram * hJ2 = gsl_histogram_alloc( nbin );
  gsl_histogram * hJ2x = gsl_histogram_alloc( nbin );
  gsl_histogram * hJ2y = gsl_histogram_alloc( nbin );
  gsl_histogram * hJ2z = gsl_histogram_alloc( nbin );
  gsl_histogram2d * hJ2x_xy = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ2y_xy = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ2z_xy = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ2x_xz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ2y_xz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ2z_xz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ2x_yz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ2y_yz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hJ2z_yz = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram * hJrnd = gsl_histogram_alloc( nbin );
  gsl_histogram * hFx = gsl_histogram_alloc( nbin_x );
  gsl_histogram * hFy = gsl_histogram_alloc( nbin_x );
  gsl_histogram * hFz = gsl_histogram_alloc( nbin_x );
  gsl_histogram * hF = gsl_histogram_alloc( nbin_x );
  gsl_histogram2d * hFxy1 = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hFxy2 = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hFxz1 = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hFxz2 = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hFyz1 = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram2d * hFyz2 = gsl_histogram2d_alloc( nbin_xy, nbin_xy );
  gsl_histogram * hBx = gsl_histogram_alloc( nbin_x );
  gsl_histogram * hBy = gsl_histogram_alloc( nbin_x );
  gsl_histogram * hBz = gsl_histogram_alloc( nbin_x );
  gsl_histogram * hB = gsl_histogram_alloc( nbin_x );

  gsl_histogram * hdM12 = gsl_histogram_alloc( nbin_log );

  public:

  /// Construct new histogram collection.
  Histograms(std::string name)
  {
    this->name = name;
  };

  virtual ~Histograms();

  /// Quiet mode without printing (default).
  void Quiet() { this->verbose = false; }

  /// Print debugging data. 
  void Debug() { this->verbose = true; }

  /// Get histogram collection name tag.
  std::string GetName() { return name; }

  /// Update histogram minimum and maximum values from yaml node. 
  void ParseRangesYaml(const YAML::Node histo_minmax);

  /// Set histogram ranges.
  void SetRanges(const bool default_ranges);

  /// Scale F and J histogram values with xy-, xz- and yz-position histograms.
  void ScaleValues();

  /// Update position histogram values.
  void UpdateXYZ(const std::vector<double> P1);

  /// Update position histogram values.
  void UpdateXYZ(const std::vector<double> P1, const std::vector<double> P2);

  /// Update distance histogram values.
  void UpdateDistance(const double r12);

  /// Update probability histogram values.
  void UpdateProbability(const double prob1);

  /// Update probability histogram values.
  void UpdateProbability(const double prob1, const double prob2);

  /// Update volume histogram values.
  void UpdateVolume(const double dv1, const double dv2);

  /// Update volume histogram values.
  void UpdateVolume(const double dv1);

  /// Update current density histogram values.
  void UpdateCurrent(const double J1, const std::vector<double> J1v, const std::vector<double> P1);

  /// Update current density histogram values.
  void UpdateCurrent(const double J1, const double J2, const std::vector<double> J1v, const std::vector<double> P1, const std::vector<double> J2v, const std::vector<double> P2);

  /// Update force histogram values.
  void UpdateForce(const double F, const std::vector<double> F12, const std::vector<double> P1, const std::vector<double> P2);

  /// Update inductance histogram values.
  void UpdateInductance(const double dM12);

  /// Update field histogram values.
  void UpdateField(const std::vector<double> dBv, const double dB);

  /// Save histograms.
  void SaveHistograms(const std::string prefix);

  /// Save histograms for field calculations.
  void SaveHistogramsB(const std::string prefix);

  /// Save histogram minimum and maximum ranges.
  void SaveRangesYaml(const std::string file_name, bool test);

  /// Free memory allocation for histograms.
  void Delete();

};

#endif
