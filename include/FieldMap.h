#ifndef FIELD_MAP_H
#define FIELD_MAP_H
#pragma once

#include "constants.h"
#include "iiflucks.h"
#include "MonteCarlo.h"
#include <map>

/// Class for field map calculations. Calculate magnetic field at grid
/// positions r2 = (x0 + l * dx, y0 + k * dy, z0 + j * dz).
///
/// j = [-steps,steps+1], k = [-steps,steps+1], l = [-steps, steps+1].

/// The constructor _FieldMap()_ 
class FieldMap
{
    bool cylindrical = false; ///< use cylindrical coordinate grid
    bool spherical = false; ///< use spherical coordinate grid
    bool verbose = false; ///< verbosed printing
    bool debug = false; ///< debug printing
    MonteCarlo * monte_carlo; ///< Monte Carlo calculation

  public:
    size_t map_size; /// field map size
    /// Grid center position coordinates.
    std::vector<double> grid_center;
    /// Steps to calculate position coordinates.
    std::vector<double> grid_steps;
    /// Number of steps in calculation of position coordinates for l, k and j.
    std::vector<int> steps;
    /// Grid position coordinate indices l, k and j.
    std::vector<int> P[ constants::field_map_size ];
    /// Map from position string to coordinate indices.
    std::map<std::string, size_t> grid;
    /// Field vector at grid points.
    std::vector<double> B[ constants::field_map_size ];
    /// Field error vector at grid points.
    std::vector<double> Berr[ constants::field_map_size ];

    /// Construct field map object.
    FieldMap(){};

    /// Construct field map object with Monte Carlo.
    FieldMap(MonteCarlo *monte_carlo)
    { 
      this->monte_carlo = monte_carlo;
    };

    virtual ~FieldMap();

    /// Cartesian coordinate system calculation grid. Return field map size.
    size_t Cartesian(std::vector<double> grid_center, std::vector<double> grid_steps, std::vector<int> steps);

    /// Cylindrical coordinate system calculation grid.
    size_t Cylindrical(std::vector<double> grid_center, std::vector<double> grid_steps, std::vector<int> steps);

    /// Spherical coordinate system calculation grid.
    size_t Spherical(std::vector<double> grid_center, std::vector<double> grid_steps, std::vector<int> steps);

    /// Verbosed printing.
    void Verbose() { this->verbose = true; }

    /// Verbosed printing.
    void Verbose(bool debug)
    {
      this->verbose = true;
      this->debug = debug;
    }

    /// No verbosed printing.
    void Quiet()
    { 
      this->verbose = false;
      this->debug = false;
    }

    /// Maximum stored calculation grid size.
    int MaxSize() { return constants::field_map_size; }

    /// Print field map.
    void Print();

    /// Print cylindrical coordinate grid field map.
//    void PrintCylindrical();

    /// Transform Cartesian field vectors to cylindrical field vectors.
    void CartesianCylindrical();

    /// Transform Cartesian field vectors to spherical field vectors.
    void CartesianSpherical();

    /// Return coordinate vector on field map grid (l, k, j).
    std::vector<double> Coordinates(std::vector<int> P);

    /// Calculate field map for coil T t=[t1, t2] at grid points.
    template <typename T> int Add(T *coil, const double t1, const double t2, const double I, const double epsabs);

    /// Calculate field map for coil T t=[t1, t2] at grid points.
    template <typename T> int Add(T *coil, const std::vector<double> t1v, const std::vector<double> t2v, const double I, const double epsabs);

    /// Calculate field map for coil T t=[t1, t2] at grid points.
    template <typename T> int Add(T *coil, const double dt, const double t1, const double t2);

    /// Calculate field map for coil T t=[t1, t2] at grid points.
    template <typename T> int Add(T *coil, const double lc, const std::vector<double> t1v, const std::vector<double> t2v);

    /// Add field maps. Return 0 in success.
    int Add(FieldMap *map, const double scale);

    /// Rotate cylindrically symmetric field maps. Return 0 in success.
    int Rotate(FieldMap *map, const int angle, const int full_rotation);

    /// Rotate and add cylindrically symmetric field maps. Return 0 in success.
    int RotateAdd(FieldMap *map, const int count, const int angle_step, const int full_rotation, const std::vector<double> scale);

};

#include "FieldMap.tpp"

#endif
