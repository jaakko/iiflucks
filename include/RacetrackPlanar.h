#ifndef RACETRACKPLANAR_H
#define RACETRACKPLANAR_H
#pragma once

#include "Coil.h"

/// Class for planar racetrack coil inherited from Coil base class. 

/// The constructor _RacetrackPlanar_ sets name tag and parameters.
class RacetrackPlanar : public Coil 
{
  std::string name;       ///< name tag for ring 
  double R;               ///< corner radius[m]
  double h;               ///< coil length[m]
  double e;               ///< coil end length[m]
  double w;               ///< width of winding[m]
  double th;              ///< total thickness of layers[m]
  int sections = 8;       ///< number of sections in one turn
  int turns;              ///< number of turns on one layer
  int layers;             ///< number of layers

  // winding and thickness increments, calculated with CalcLength()  
  double dw;
  double dth;

  public:
  /// Construct RacetrackPlanar object with parameters.
  RacetrackPlanar(std::string name, double R, double h, double e, double w, double th, int turns, int layers, double A, double J, double theta, std::vector<double> axis, std::vector<double> tr) : Coil(A, J, theta, axis, tr)
  {
    this->name = name;
    this->R = R;
    this->h = h;
    this->e = e;
    this->w = w;
    this->th = th;
    this->turns = turns;
    this->layers  = layers;
  };

  RacetrackPlanar(std::string name, double R, double h, double e, double w, double th, int turns, int layers, double A, double J) : Coil( A, J, 0, {0, 0, 1}, {0, 0, 0} )
  {
    this->name = name;
    this->R = R;
    this->h = h;
    this->e = e;
    this->w = w;
    this->th = th;
    this->turns = turns;
    this->layers = layers;
  };

  virtual ~RacetrackPlanar();

  /// Get planar racetrack coil name tag.
  std::string GetName() { return name; }

  /// Get corner radius [m].
  double GetR() { return R; }

  /// Set corner radius [m].
  void SetR(double R) { this->R = R; }

  /// Get coil length [m].
  double Geth() { return h; }

  /// Set coil length [m].
  void Seth(double h) { this->h = h; }

  /// Get coil end length [m].
  double Gete() { return e; }

  /// Set coil end length [m].
  void Sete(double e) { this->e = e; }

  /// Get winding width [m].
  double Getw() { return w; }

  /// Set winding width [m].
  void Setw(double w) { this->w = w; }

  /// Get layers total thickness [m].
  double Getth() { return th; }

  /// Set layers total thickness [m].
  void Setth(double th) { this->th = th; }

  /// Get number of sections on one turn.
  int GetSections() { return sections; }

  /// Get number of turns on one layer.
  int GetTurns() { return turns; }

  /// Set number of turns on one layer.
  void SetTurns(int turns) { this->turns  = turns; }
   
  /// Get number of layers.
  int GetLayers() { return layers; }

  /// Set number of layers.
  void SetLayers(int layers) { this->layers = layers; }
   
  /// Calculate total lenght of coil [m]. Do this first before other calculations.
  double CalcLength(std::vector<double> t1v, std::vector<double> t2v, bool verbose);

  /// Calculate section, turn and layer from length. Return t on parametric curve.
  double CalcTurn(const double length, int &section, int &turn, int &layer);

  /// Planar racetrack coil curve length | t2 - t1 | * one winding [m]. 
  double Length(const double t1, const double t2, const int section, const int turn, const int layer);

  /// Planar racetrack coil small section t = [ t - dt/2, t + dt/2 ] length [m].
  double dl(const double dt, const double t, const int section, const int turn, const int layer);

  /// Planar racetrack coil small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const int section, const int turn, const int layer);

  /// Planar racetrack coil small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const double r1, const double r2, const int section, const int turn, const int layer);

  /// Planar racetrack coil curve parametric length [m].
  double ds(const double t, const int section, const int turn, const int layer);

  /// Parametric position vector with parameter t.
  std::vector<double> XYZ(const double t, const int section, const int turn, const int layer);

  /// Parametric tangent vector with parameter t.
  std::vector<double> Jv(const double t, const int section, const int turn, const int layer);

  /// Calculation of magnetic field at position P for section, turn and layer.
  int B(const double I, const std::vector<double> t1v, const std::vector<double> t2v, const int section, const int turn, const int layer, const double epsabs, const std::vector<double> P, const bool verbose, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

  /// Calculation of magnetic field at position P for coil.
  int B(const double I, const std::vector<double> t1v, const std::vector<double> t2v, const double epsabs, const std::vector<double> P, const bool verbose, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

  /// Number of equally long elements lc for section, turn and layer.
  int NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v, const int section, const int turn, const int layer);

  /// Number of equally long elements lc for coil.
  int NElements(const double lc, const std::vector<double> t1v, const std::vector<double> t2v);

  /// Print section elements with position coordinates and current density vectors.
  double Elements(const double t1, const double t2, const int section, const int turn, const int layer, const int M, const int test, const bool verbose);

};

#endif
