/// Calculate field map for coil T t=[t1, t2] at grid points. 
template <typename T> int FieldMap::Add(T *coil, const double t1, const double t2, const double I, const double epsabs)
{
  std::vector<double> Bv, Berrv, P1, P2;
  double Btotal;
  int intres = 0;
  size_t neval;
  for( int i = 0; i < map_size; i++)
  {
    P1 = Coordinates( P[ i ] );

    if( cylindrical )
    {
      iiflucks::cylindricalCartesian( P1, P2 );
      intres += coil->B(I, t1, t2, P2, epsabs, Bv, Berrv, neval);
    }
    else if( spherical )
    {
      iiflucks::sphericalCartesian( P1, P2 );
      intres += coil->B(I, t1, t2, P2, epsabs, Bv, Berrv, neval);
    }
    else
    {
      intres += coil->B(I, t1, t2, P1, epsabs, Bv, Berrv, neval);
    }

    iiflucks::addVectors(Bv, B[ i ], B[ i ]);
    iiflucks::addVectors(Berrv, Berr[ i ], Berr[ i ]);
    Btotal = iiflucks::vectorLength( Bv );
    B[ i ].push_back( Btotal );

    if( verbose )
    { 
      if( debug ) std::cout << i << ",";
      std::cout << P[ i ][ 0 ] << "," << P[ i ][ 1 ] << "," << P[ i ][ 2 ];
      std::cout << "," << B[ i ][ 0 ] << "," << B[ i ][ 1 ] << "," << B[ i ][ 2 ] << "," << B[ i ][ 3 ];
      std::cout << "," << Berr[ i ][ 0 ] << "," << Berr[ i ][ 1 ] << "," << Berr[ i ][ 2 ];
      if( debug ) std::cout << "," << intres << "," << neval;
      std::cout << std::endl;
    }
  }

  return intres;
}

/// Calculate field map for coil T t=[t1, t2] at grid points.
template <typename T> int FieldMap::Add(T *coil, const std::vector<double> t1v, const std::vector<double> t2v, const double I, const double epsabs)
{
  std::vector<double> Bv, Berrv, P1, P2;

  double Btotal;
  int intres = 0;
  size_t neval;
  for( int i = 0; i < map_size; i++)
  {
    P1 = Coordinates( P[ i ] );

    if( cylindrical )
    {
      iiflucks::cylindricalCartesian( P1, P2 );
      intres += coil->B(I, t1v, t2v, epsabs, P2, false, Bv, Berrv, neval);
    }
    else if( spherical )
    {
      iiflucks::sphericalCartesian( P1, P2 );
      intres += coil->B(I, t1v, t2v, epsabs, P2, false, Bv, Berrv, neval);
    }
    else
    {
      intres += coil->B(I, t1v, t2v, epsabs, P1, false, Bv, Berrv, neval);
    }

    iiflucks::addVectors(Bv, B[ i ], B[ i ]);
    iiflucks::addVectors(Berrv, Berr[ i ], Berr[ i ]);
    Btotal = iiflucks::vectorLength( Bv );
    B[ i ].push_back( Btotal );

    if( verbose )
    { 
      if( debug ) std::cout << i << ",";
      std::cout << P[ i ][ 0 ] << "," << P[ i ][ 1 ] << "," << P[ i ][ 2 ];
      std::cout << "," << B[ i ][ 0 ] << "," << B[ i ][ 1 ] << "," << B[ i ][ 2 ] << "," << B[ i ][ 3 ];
      std::cout << "," << Berr[ i ][ 0 ] << "," << Berr[ i ][ 1 ] << "," << Berr[ i ][ 2 ];
      if( debug ) std::cout << "," << intres << "," << neval;
      std::cout << std::endl;
    }
  }

  return intres;
}

/// Calculate field map for coil T t=[t1, t2] at grid points. 
template <typename T> int FieldMap::Add(T *coil, const double dt, const double t1, const double t2)
{
  std::vector<double> P1, P2;
  double Btotal;
  int nzero = 0;

  for( int i = 0; i < map_size; i++)
  {
    P1 = Coordinates( P[ i ] );

    if( cylindrical )
    {
      iiflucks::cylindricalCartesian( P1, P2 );
      nzero += monte_carlo->Run<T>(coil, dt, t1, t2, P2);
    }
    else if( spherical )
    {
      iiflucks::sphericalCartesian( P1, P2 );
      nzero += monte_carlo->Run<T>(coil, dt, t1, t2, P2);
    }
    else
    {
      nzero += monte_carlo->Run<T>(coil, dt, t1, t2, P1);
    }

    iiflucks::addVectors(monte_carlo->B, B[ i ], B[ i ]);
    iiflucks::addVectors(monte_carlo->Berr, Berr[ i ], Berr[ i ]);
    Btotal = iiflucks::vectorLength( monte_carlo->B );
    B[ i ].push_back( Btotal );

    if( verbose )
    { 
      if( debug ) std::cout << i << ",";
      std::cout << P[ i ][ 0 ] << "," << P[ i ][ 1 ] << "," << P[ i ][ 2 ];
      std::cout << "," << B[ i ][ 0 ] << "," << B[ i ][ 1 ] << "," << B[ i ][ 2 ] << "," << B[ i ][ 3 ];
      std::cout << "," << Berr[ i ][ 0 ] << "," << Berr[ i ][ 1 ] << "," << Berr[ i ][ 2 ];
      std::cout << std::endl;
    }
  }
      
  return nzero;
}

/// Calculate field map for coil T t=[t1, t2] at grid points.
template <typename T> int FieldMap::Add(T *coil, const double lc, const std::vector<double> t1v, const std::vector<double> t2v)
{
  std::vector<double> P1, P2;
  double Btotal;
  int nzero = 0;

  for( int i = 0; i < map_size; i++)
  {
    P1 = Coordinates( P[ i ] );

    if( cylindrical )
    {
      iiflucks::cylindricalCartesian( P1, P2 );
      nzero += monte_carlo->Run<T>(coil, lc, t1v, t2v, P2);
    }
    else if( spherical )
    {
      iiflucks::sphericalCartesian( P1, P2 );
      nzero += monte_carlo->Run<T>(coil, lc, t1v, t2v, P2);
    }
    else
    {
      nzero += monte_carlo->Run<T>(coil, lc, t1v, t2v, P1 );
    }

    iiflucks::addVectors(monte_carlo->B, B[ i ], B[ i ]);
    iiflucks::addVectors(monte_carlo->Berr, Berr[ i ], Berr[ i ]);
    Btotal = iiflucks::vectorLength( monte_carlo->B );
    B[ i ].push_back( Btotal );

    if( verbose )
    {
      if( debug ) std::cout << i << ",";
      std::cout << P[ i ][ 0 ] << "," << P[ i ][ 1 ] << "," << P[ i ][ 2 ];
      std::cout << "," << B[ i ][ 0 ] << "," << B[ i ][ 1 ] << "," << B[ i ][ 2 ] << "," << B[ i ][ 3 ];
      std::cout << "," << Berr[ i ][ 0 ] << "," << Berr[ i ][ 1 ] << "," << Berr[ i ][ 2 ];
      std::cout << std::endl;
    }
  }

  return nzero;
}

