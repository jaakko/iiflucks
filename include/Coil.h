#ifndef COIL_H
#define COIL_H
#pragma once

/// Class for current carrying circuits. 

/// The constructor _Coil()_ takes cross sectional area, current density, 
/// rotation vector and angle followed by translation vector. 
class Coil 
{
  protected:
    double A; ///< wire cross sectional area 
    double J; ///< wire current density 
    double theta; ///< rotation angle around vector a
    std::vector<double> axis; ///< vector for rotation
    std::vector<double> tr; ///< translation vector

  public:
    /// Construct wire object.
    Coil();

    /// Construct wire object with parameters.
    Coil(double A, double J, double theta, std::vector<double> axis, std::vector<double> tr);

    virtual ~Coil();

    /// Get wire cross sectional area [m^2].
    double GetA() { return A; }

    /// Set wire cross sectional area [m^2].
    void SetA(double A) { this->A = A; }

    /// Get wire current density [A/m^2].
    double GetJ() { return J; }

    /// Set wire current density [A/m^2].
    void SetJ(double J) { this->J = J; }

    /// Get wire rotation angle [rad].
    double GetTheta() { return theta; }

    /// Set wire rotation angle [rad].
    void SetTheta(double theta) { this->theta = theta; }

    /// Get wire rotation axis vector.
    std::vector<double> GetRotationAxis() { return axis; }

    /// Set wire rotation axis vector.
    void SetRotationAxis(std::vector<double> axis) { this->axis = axis; }

    /// Get wire translation vector.
    std::vector<double> GetTranslation() { return tr; }

    /// Set wire translation vector.
    void SetTranslation(std::vector<double> tr) { this->tr = tr; }

};

#endif
