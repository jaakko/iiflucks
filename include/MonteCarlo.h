#ifndef MONTECARLO_H
#define MONTECARLO_H
#pragma once

#include <fstream>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_histogram2d.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
//#include "constants.h"
#include "iiflucks.h"
#include "Histograms.h"

/// Class for Monte Carlo integration between circuits.
class MonteCarlo 
{
  bool verbose = false; ///< verbose printing
  bool test = false;    ///< calculate with test data 
  bool confread = false; ///< .indmtrx file has been read
  std::string name;     ///< name tag for Monte Carlo
  int Nrnd = 0;         ///< exponent for number of samples 
  double I1;            ///< current in circuit 1 [A]
  double I2;            ///< current in circuit 2 [A]
  double ur1 = 1;       ///< circuit 1 relative permeability
  double ur2 = 1;       ///< circuit 2 relative permeability
  Histograms * histograms; ///< histogram collection

  public:
  int nrand;            ///< 10^Nrnd number of samples
  double M12 = 0;       ///< mutual inductance between circuits 1 and 2 [H] 
  double M12err = 0;    ///< mutual inductance maximum error [H] 
  double E12 = 0;       ///< field energy between circuits 1 and 2 [J] 
  double E12err = 0;    ///< field energy maximum error [J] 
  double Flux12 = 0;    ///< flux linkage between circuits 1 and 2 [Wb] 
  double Flux12err = 0; ///< flux linkage maximum error [Wb] 
  std::vector<double> F12; ///< force between circuits 1 and 2 [N] 
  std::vector<double> F12err; ///< force maximum error [N] 
  std::vector<double> B;///< magnetic field vector
  std::vector<double> Berr;///< magnetic field error vector

  double dM12;
  double Msqsum;
  double var;
  int nzero; // number of zero distance samples

  double dF;
  std::vector<double> dF12; // random estimated force between volume elements
  std::vector<double> Fsqsum; // square sum of force between volume elements 
  std::vector<double> Fvar; // variance of force between volume elements 

  double dB; // random estimated field magnitude
  std::vector<double> dBv; // random estimated field vector
  std::vector<double> Bsqsum; // square sum of field components
  std::vector<double> Bvar; // variance of field components

  // probability to select dv1 and dv2 segments
  int np1, np2;
  double len1, len2, dl1, dl2;
  double V1, V2, dv1, dv2;
  double prob1, prob2, prob;

  double t, u;
  std::vector<double> P1, P2, P21, J1v, J2v, J1xr;
  double J1, J2;
  double rij;
  int n1, n2;

  // position coordinates for small volume dv1, initialize with test data
  double x1c[ constants::maxpoints ] = {0.01, 0.00951057, 0.00809017, 0.00587785, 0.00309017, 6.12323e-19, -0.00309017, -0.00587785, -0.00809017, -0.00951057, -0.01, -0.00951057, -0.00809017, -0.00587785, -0.00309017, -1.83697e-18, 0.00309017, 0.00587785, 0.00809017, 0.00951057};
  double y1c[ constants::maxpoints ] = {0, 0.00309017, 0.00587785, 0.00809017, 0.00951057,  0.01, 0.00951057, 0.00809017, 0.00587785, 0.00309017, 1.22465e-18, -0.00309017, -0.00587785,  -0.00809017, -0.00951057, -0.01, -0.00951057, -0.00809017, -0.00587785, -0.00309017};
  double z1c[ constants::maxpoints ] = {0, 5e-05, 0.0001, 0.00015, 0.0002, 0.00025, 0.0003, 0.00035, 0.0004, 0.00045, 0.0005, 0.00055, 0.0006, 0.00065, 0.0007, 0.00075, 0.0008, 0.00085, 0.0009, 0.00095};
  double dv1a[ constants::maxpoints ] = {2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11};

  // vector field with same position coordinates, initialize with test data
  double J1x[ constants::maxpoints ] = {0, -39340.3, -74829.7,  -102994,  -121077, -127308, -121077, -102994, -74829.7,  -39340.3,  -1.55907e-11, 39340.3, 74829.7, 102994, 121077, 127308, 121077,  102994, 74829.7, 39340.3};
  double J1y[ constants::maxpoints ] = {127308, 121077, 102994, 74829.7, 39340.3, 7.79536e-12, -39340.3,  -74829.7,  -102994,  -121077,  -127308, -121077, -102994, -74829.7, -39340.3, -2.33861e-11,  39340.3,  74829.7,  102994,  121077};
  double J1z[ constants::maxpoints ] = {2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17};

  // position coordinates for small volume dv2, initialize with test data
  double x2c[ constants::maxpoints ] = {0.01, 0.00951057, 0.00809017, 0.00587785, 0.00309017, 6.12323e-19, -0.00309017, -0.00587785, -0.00809017, -0.00951057, -0.01, -0.00951057, -0.00809017, -0.00587785, -0.00309017, -1.83697e-18, 0.00309017, 0.00587785, 0.00809017, 0.00951057};
  double y2c[ constants::maxpoints ] = {0, 0.00309017, 0.00587785, 0.00809017, 0.00951057,  0.01, 0.00951057, 0.00809017, 0.00587785, 0.00309017, 1.22465e-18, -0.00309017, -0.00587785,  -0.00809017, -0.00951057, -0.01, -0.00951057, -0.00809017, -0.00587785, -0.00309017};
  double z2c[ constants::maxpoints ] = {0.01, 0.01005, 0.0101, 0.01015, 0.0102, 0.01025, 0.0103, 0.01035, 0.0104, 0.01045, 0.0105, 0.01055, 0.0106, 0.01065, 0.0107, 0.01075, 0.0108, 0.01085, 0.0109, 0.01095};
  double dv2a[ constants::maxpoints ] = {2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11, 2.46771e-11};

  // vector field with same position coordinates, initialize with test data
  double J2x[ constants::maxpoints ] = {0, -39340.3, -74829.7,  -102994,  -121077, -127308, -121077, -102994, -74829.7,  -39340.3,  -1.55907e-11, 39340.3, 74829.7, 102994, 121077, 127308, 121077,  102994, 74829.7, 39340.3};
  double J2y[ constants::maxpoints ] = {127308, 121077, 102994, 74829.7, 39340.3,  7.79536e-12, -39340.3,  -74829.7,  -102994,  -121077,  -127308, -121077, -102994, -74829.7, -39340.3, -2.33861e-11,  39340.3,  74829.7,  102994,  121077};
  double J2z[ constants::maxpoints ] = {2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17, 2026.17};

  /// Construct new Monte Carlo integration.
  MonteCarlo(std::string name, int Nrnd, double I1, double I2, Histograms *histograms)
  {
    this->name = name;
    this->Nrnd = Nrnd;
    this->I1 = I1;
    this->I2 = I2;
    this->histograms = histograms;
  };

  virtual ~MonteCarlo();

  /// Quiet mode without printing (default).
  void Quiet() { this->verbose = false; }

  /// Print debugging data. 
  void Debug() { this->verbose = true; }

  /// Calculate with test data. 
  void TestCalc() { this->test = true; }

  /// Turn off test data calculation. 
  void TestDisable() { this->test = false; }

  /// Get Monte Carlo name tag.
  std::string GetName() { return name; }

  /// Get number of random samples exponent.
  int GetNrnd() { return Nrnd; }

  /// Set number of random samples exponent.
  void SetNrnd(const int Nrnd) { this->Nrnd = Nrnd; }

  /// Get circuit 1 current [A].
  int GetI1() { return I1; }

  /// Set circuit 1 current [A].
  void SetI1(const double I1) { this->I1 = I1; }

  /// Get circuit 2 current [A].
  int GetI2() { return I2; }

  /// Set circuit 2 current [A].
  void SetI2(const double I2) { this->I2 = I2; }

  /// Get coil 1 relative permeability.
  int Getur1() { return ur1; }

  /// Set coil 1 relative permeability.
  void Setur1(const double ur1) { this->ur1 = ur1; }

  /// Get coil 2 relative permeability.
  int Getur2() { return ur2; }

  /// Set coil 2 relative permeability.
  void Setur2(const double ur2) { this->ur2 = ur2; }

  /// Initialize Monte Carlo parameters for inductance calculation.
  void Init();

  /// Initialize Monte Carlo parameters for field calculation.
  void InitB();

  /// Inner loop calculations for inductance Monte Carlo.
  void Innerloop();

  /// Inner loop calculations for field Monte Carlo.
  void InnerloopB();

  /// Fill data arrays with thermal noise current.
  void ThermalNoise( const int np, const double dv, double Jx[], double Jy[], double Jz[]);

  /// Run Monte Carlo integration for coils T t=[t1, t2] and U u=[u1, u2].
  template <typename T, typename U> int Run(T *coil1, U *coil2, const double dt, const double t1, const double t2, const double du, const double u1, const double u2);

  /// Run Monte Carlo integration for coils T t=[t1, t2] and U u=[u1, u2].
  template <typename T, typename U> int Run(T *coil1, U *coil2, const double lc1, const std::vector<double> t1v, const std::vector<double> t2v, const double lc2, const std::vector<double> u1v, const std::vector<double> u2v);

  /// Run Monte Carlo integration for coils T t=[t1, t2] and U u=[u1, u2].
  template <typename T, typename U> int Run(T *coil1, U *coil2, const double dt, const double t1, const double t2, const double lc2, const std::vector<double> u1v, const std::vector<double> u2v);

  /// Run Monte Carlo integration for coils T t=[t1, t2] and U u=[u1, u2].
  template <typename T, typename U> int Run(T *coil1, U *coil2, const double lc1, const std::vector<double> t1v, const std::vector<double> t2v, const double du, const double u1, const double u2);

  /// General 'xc yc zc Jx Jy Jz dv' file and curve Monte Carlo. 
  template <class U> int Run_pdv(const std::string file1, U *coil2, const double du, const double u1, const double u2, const double theta, const std::vector<double> axis, const std::vector<double> tr);

  /// General 'xc yc zc Jx Jy Jz dv' file and multi-turn coil Monte Carlo.
  template <class U> int Run_pdv(const std::string file1, U *coil2, const double lc2, const std::vector<double> u1v, const std::vector<double> u2v, const double theta, const std::vector<double> axis, const std::vector<double> tr);

  /// Run Monte Carlo integration for two position-current-density-dv files.
  int Run(const std::string file1, const std::string file2, const double theta, const std::vector<double> axis, const std::vector<double> tr);

  /// Calculate E12, Flux12, M12 and F12 with errors from accumulated values.
  void Calculate();

  /// Calculate B with errors from accumulated values.
  void CalculateB();

  /// Run Monte Carlo integration of field for coil T t=[t1, t2] at P.
  template <typename T> int Run(T *coil, const double dt, const double t1, const double t2, const std::vector<double> P);

  /// Run Monte Carlo integration of field for coil T t=[t1, t2] at P.
  template <typename T> int Run(T *coil, const double lc, const std::vector<double> t1v, const std::vector<double> t2v, const std::vector<double> P);

  /// Run Monte Carlo integration of field for position-current-density-dv file at P.
  int Run(const std::string file, const double theta, const std::vector<double> axis, const std::vector<double> tr, const std::vector<double> P);

};

#include "MonteCarlo.tpp"

#endif
