#ifndef RINGCYLINDER_H
#define RINGCYLINDER_H
#pragma once

#include "Coil.h"

/// Class for ring inherited from Coil base class. 

/// The constructor _RingCylinder_ sets name tag and parameters.
class RingCylinder : public Coil 
{
  std::string name;       ///< name tag for ring 
  double R;               ///< ring radius[m]
  double R2;              ///< cylinder radius[m]

  public:
  /// Construct RingCylinder object with parameters.
  RingCylinder(std::string name, double R, double R2, double A, double J, double theta, std::vector<double> axis, std::vector<double> tr) : Coil(A, J, theta, axis, tr)
  {
    this->name = name;
    this->R = R;
    this->R2 = R2;
  };

  RingCylinder(std::string name, double R, double R2, double A, double J) : Coil( A, J, 0, {0, 0, 1}, {0, 0, 0} )
  {
    this->name = name;
    this->R = R;
    this->R2 = R2;
  };

  virtual ~RingCylinder();

  /// Get ring name tag.
  std::string GetName() { return name; }

  /// Get ring radius [m].
  double GetR() { return R; }

  /// Set ring radius [m].
  void SetR(double R) { this->R = R; }

  /// Get cylinder radius [m].
  double GetR2() { return R2; }

  /// Set cylinder radius [m].
  void SetR2(double R2) { this->R2 = R2; }

  /// RingCylinder curve length [m].
  double Length(const double t1, const double t2);

  /// RingCylinder small section t = [ t - dt/2, t + dt/2 ] length [m].
  double dl(const double dt, const double t);

  /// RingCylinder small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t);

  /// RingCylinder small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const double r1, const double r2);

  /// RingCylinder curve parametric length [m].
  double ds(const double t);

  /// Parametric position vector with parameter t.
  std::vector<double> XYZ(const double t);

  /// Parametric tangent vector with parameter t.
  std::vector<double> Jv(const double t);

  /// Numerical integration of magnetic field at position P. Return 0 in success.
  int B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

  /// Print list of elements with position coordinates and current density vectors.
  double Elements(const double t1, const double t2, const int M, const int test, const bool verbose);
};

#endif
