/// coil1 is sampled with parametric length dt and coil2 with length du. 
template <class T, class U> int MonteCarlo::Run(T *coil1, U *coil2, const double dt, const double t1, const double t2, const double du, const double u1, const double u2)
{
  len1 = coil1->Length(t1, t2);
  len2 = coil2->Length(u1, u2);
  V1 = len1 * coil1->GetA();
  V2 = len2 * coil2->GetA();

  double dt_signed, du_signed;
  if( len1 > 0 && len2 > 0 )
  {
    np1 = ceil( std::abs( ( t2 - t1 ) / dt ) );
    np2 = ceil( std::abs( ( u2 - u1 ) / du ) );
    dt_signed = std::abs( dt );
    if( t1 > t2 ) dt_signed *= -1.0;
    du_signed = std::abs( du );
    if( u1 > u2 ) du_signed *= -1.0;
  }
  else
  {
    std::cerr << "Parametric model does not have non-zero positive length or volume." << std::endl;
    std::cerr << "dt_signed = " << dt_signed << " len1 = " << len1 << " V1 = " << V1 << std::endl;
    std::cerr << "du_signed = " << du_signed << " len2 = " << len2 << " V2 = " << V2 << std::endl;
    return -1;
  }

  Init(); // calculate probabilities and reset cumulative variables 

  if( verbose ) // CSV printing
  {
    std::cout << "dt, " << dt << std::endl;
    std::cout << "du, " << du << std::endl;
    std::cout << "np1, " << np1 << std::endl;
    std::cout << "np2, " << np2 << std::endl;
    std::cout << "prob1, " << prob1 << std::endl;
    std::cout << "prob2, " << prob2 << std::endl;
    std::cout << "len1, " << len1 << std::endl;
    std::cout << "len2, " << len2 << std::endl;
    std::cout << "V1, " << V1 << std::endl;
    std::cout << "V2, " << V2 << std::endl;
    std::cout << "M12, " << M12 << std::endl;
    std::cout << "Msqsum, " << Msqsum << std::endl;
    std::cout << "F12, " << F12[0] << "," << F12[1] << "," << F12[2] << std::endl;
    std::cout << "Fsqsum, " << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
    std::cout << "nrand, " << nrand << std::endl;
    std::cout << "i,n1,dt_signed,t,n2,du_signed,u,dl1,dv1,prob1,dl2,dv2,prob2,P1x,P1y,P1z,P2x,P2y,P2z,J1vx,J1vy,J1vz,J2vx,J2vy,J2vz,rij,dM12,M12,Msqsum,dF12x,dF12y,dF12z,F12x,F12y,F12z,Fsqsumx,Fsqsumy,Fsqsumz" << std::endl;
  }

  const gsl_rng_type * Ur;
  gsl_rng * rnd;

  Ur = gsl_rng_default;
  rnd = gsl_rng_alloc (Ur);

  for(int i=0; i < nrand; i++)
  {
    n1 = (int)( gsl_ran_flat (rnd, 0, np1 ) );
    n2 = (int)( gsl_ran_flat (rnd, 0, np2 ) );

    t = n1 * dt_signed + dt_signed/2 + t1;
    u = n2 * du_signed + du_signed/2 + u1;

    dl1 = coil1->dl( dt, t );
    dl2 = coil2->dl( du, u );
    dv1 = coil1->GetA() * dl1;
    dv2 = coil2->GetA() * dl2;

    prob1 = dv1 / V1;
    prob2 = dv2 / V2;
    prob = prob1 * prob2;

    P1 = coil1->XYZ( t );
    P2 = coil2->XYZ( u );

    J1v = coil1->Jv( t );
    if( dt_signed < 0 ) iiflucks::scaleVector( -1.0, J1v );
    J2v = coil2->Jv( u );
    if( du_signed < 0 ) iiflucks::scaleVector( -1.0, J2v );

    if( verbose )
    {
      std::cout << i << "," << n1 << "," << dt_signed << "," << t;
      std::cout << "," << n2 << "," << du_signed << "," << u;
      std::cout << "," << dl1 << "," << dv1 << "," << prob1;
      std::cout << "," << dl2 << "," << dv2 << "," << prob2;
      std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
      std::cout << "," << P2[0] << "," << P2[1] << "," << P2[2];
      std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
      std::cout << "," << J2v[0] << "," << J2v[1] << "," << J2v[2];
    }

    Innerloop();

    if( verbose )
    {
      std::cout << "," << rij;
      std::cout << "," << dM12 << "," << M12 << "," << Msqsum;
      std::cout << "," << dF12[0] << "," << dF12[1] << "," << dF12[2];
      std::cout << "," << F12[0] << "," << F12[1] << "," << F12[2];
      std::cout << "," << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
    }
  }

  Calculate(); // calculate values and errors from accumulated values

  return nzero;	
}

/// General 'xc yc zc Jx Jy Jz dv' file and helix curve Monte Carlo. 
template <class U> int MonteCarlo::Run_pdv(const std::string file1, U *coil2, const double du, const double u1, const double u2, const double theta, const std::vector<double> axis, const std::vector<double> tr)
{
  len2 = coil2->Length(u1, u2);
  V2 = len2 * coil2->GetA();

  M12 = 0;
  Msqsum = 0;
  nzero = 0;

  iiflucks::zeroVector( F12 );
  iiflucks::zeroVector( Fsqsum );
  nrand = pow(10, Nrnd);

  double du_signed;
  if( len2 > 0 && V2 > 0 )
  {
    np2 = ceil( std::abs( ( u2 - u1 )  / du ) );
    du_signed = std::abs( du );
    if( u1 > u2 ) du_signed *= -1.0;
  }
  else
  {
    std::cerr << "Parametric model does not have non-zero positive length or volume. du = " << du << " len2 = " << len2 << " V2 = " << V2 << std::endl;
    return -1;
  }

  std::vector<double> a = axis;
  if( theta != 0 ) iiflucks::normalizeVector( a ); 

  if( test )
  {
    np1 = 20;
    V1 = 0;
    for( int i = 0; i < np1 ; i++ ) V1 += dv1a[ i ];
  }
  else
  {
    np1 = iiflucks::totalVolume( file1, V1 );
  }

  const gsl_rng_type * Ur;
  gsl_rng * r;

  Ur = gsl_rng_default;
  r = gsl_rng_alloc (Ur);

  if( np1 < 1 || V1 <= 0 )
  {
    std::cerr << "Failed to read file " << file1 << " V1 = " << V1 << std::endl;
    return -1;
  }
  else if( np1 >= constants::maxpoints )
  {
    std::cerr << "Maximum number of points " << constants::maxpoints << std::endl;
    return -2;
  }
  else
  {
    if( !test ) np1 = iiflucks::readFile( file1, x1c, y1c, z1c, J1x, J1y, J1z, dv1a );

    if( verbose ) // CSV printing
    {
      std::cout << "du_signed, " << du_signed << std::endl;
      std::cout << "np1, " << np1 << std::endl;
      std::cout << "np2, " << np2 << std::endl;
      std::cout << "prob2, " << prob2 << std::endl;
      std::cout << "len2, " << len2 << std::endl;
      std::cout << "V1, " << V1 << std::endl;
      std::cout << "V2, " << V2 << std::endl;
      std::cout << "M12, " << M12 << std::endl;
      std::cout << "Msqsum, " << Msqsum << std::endl;
      std::cout << "F12, " << F12[0] << "," << F12[1] << "," << F12[2] << std::endl;
      std::cout << "Fsqsum, " << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
      std::cout << "nrand, " << nrand << std::endl;
      std::cout << "i,n1,n2,du_signed,u,dv1,prob1,dl2,dv2,prob2,P1x,P1y,P1z,P2x,P2y,P2z,J1vx,J1vy,J1vz,J2vx,J2vy,J2vz,rij,dM12,M12,Msqsum,dF12x,dF12y,dF12z,F12x,F12y,F12z,Fsqsumx,Fsqsumy,Fsqsumz" << std::endl;
    }

    for( int i = 0; i < nrand; i++)
    {
      n1 = (int)( gsl_ran_flat (r, 0, np1 ) );
      n2 = (int)( gsl_ran_flat (r, 0, np2 ) );

      u = n2 * du_signed + du_signed/2 + u1;

      dl2 = coil2->dl( du, u );
      dv2 = coil2->GetA() * dl2;

      // probability to select n1 and n2 segments
      dv1 = dv1a[ n1 ];
      prob1 = dv1 / V1;
      prob2 = dv2 / V2;            
      prob = prob1 * prob2;

      iiflucks::makeVector( x1c[ n1 ], y1c[ n1 ], z1c[ n1 ], P1 );
      iiflucks::makeVector( J1x[ n1 ], J1y[ n1 ], J1z[ n1 ], J1v );

      P2 = coil2->XYZ( u );
      J2v = coil2->Jv( u );
      if( du_signed < 0 ) iiflucks::scaleVector( -1.0, J2v );
    
      if( theta != 0 )
      {
        iiflucks::QuaternionRotation( a, theta, P1 );
        iiflucks::QuaternionRotation( a, theta, J1v );
      }
      iiflucks::addVectors( P1, tr, P1);
    
      if( verbose )
      {
        std::cout << i << "," << n1; 
        std::cout << "," << n2 << "," << du_signed << "," << u;
        std::cout << "," << dv1 << "," << prob1;
        std::cout << "," << dl2 << "," << dv2 << "," << prob2;
        std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
        std::cout << "," << P2[0] << "," << P2[1] << "," << P2[2];
        std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
        std::cout << "," << J2v[0] << "," << J2v[1] << "," << J2v[2];
      }

      Innerloop();
    
      if( verbose )
      {
        std::cout << "," << rij;
        std::cout << "," << dM12 << "," << M12 << "," << Msqsum;
        std::cout << "," << dF12[0] << "," << dF12[1] << "," << dF12[2];
        std::cout << "," << F12[0] << "," << F12[1] << "," << F12[2];
        std::cout << "," << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
      }
    }
    
    Calculate(); // calculate values and errors from accumulated values
    
  }

  return nzero;
}

/// General 'xc yc zc Jx Jy Jz dv' file and multi-turn coil Monte Carlo.
template <class U> int MonteCarlo::Run_pdv(const std::string file1, U *coil2, const double lc2, const std::vector<double> u1v, const std::vector<double> u2v, const double theta, const std::vector<double> axis, const std::vector<double> tr)
{
  int section2;
  int turn2;
  int layer2;

  double length2 = coil2->CalcLength( u1v, u2v, verbose );
  V2 = length2 * coil2->GetA();

  int elements2 = coil2->NElements(lc2, u1v, u2v);

  prob2 = 1.0 / (double)elements2;

  M12 = 0;
  Msqsum = 0;
  nzero = 0;

  iiflucks::zeroVector( F12 );
  iiflucks::zeroVector( Fsqsum );
  nrand = pow(10, Nrnd);

  std::vector<double> a = axis;
  if( theta != 0 ) iiflucks::normalizeVector( a );

  if( test )
  {
    np1 = 20;
    V1 = 0;
    for( int i = 0; i < np1 ; i++ ) V1 += dv1a[ i ];
  }
  else
  {
    np1 = iiflucks::totalVolume( file1, V1 );
  }

  const gsl_rng_type * Ur;
  gsl_rng * rnd;

  Ur = gsl_rng_default;
  rnd = gsl_rng_alloc (Ur);

  if( np1 < 1 || V1 <= 0 )
  {
    std::cerr << "Failed to read file " << file1 << " V1 = " << V1 << std::endl;
    return -1;
  }
  else if( np1 >= constants::maxpoints )
  {
    std::cerr << "Maximum number of points " << constants::maxpoints << std::endl;
    return -2;
  }
  else
  {
    if( !test ) np1 = iiflucks::readFile( file1, x1c, y1c, z1c, J1x, J1y, J1z, dv1a );

    if( verbose ) // CSV printing
    {
      std::cout << "np1, " << np1 << std::endl;
      std::cout << "np2, " << np2 << std::endl;
      std::cout << "prob2, " << prob2 << std::endl;
      std::cout << "len2, " << len2 << std::endl;
      std::cout << "V1, " << V1 << std::endl;
      std::cout << "V2, " << V2 << std::endl;
      std::cout << "M12, " << M12 << std::endl;
      std::cout << "Msqsum, " << Msqsum << std::endl;
      std::cout << "F12, " << F12[0] << "," << F12[1] << "," << F12[2] << std::endl;
      std::cout << "Fsqsum, " << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
      std::cout << "nrand, " << nrand << std::endl;
      std::cout << "i,np1,n1,section2,turn2,layer2,len2,np2,n2,du_signed,u,dl1,dv1,prob1,dl2,dv2,prob2,P1x,P1y,P1z,P2x,P2y,P2z,J1vx,J1vy,J1vz,J2vx,J2vy,J2vz,rij,dM12,M12,Msqsum,dF12x,dF12y,dF12z,F12x,F12y,F12z,Fsqsumx,Fsqsumy,Fsqsumz" << std::endl;
    }

    int tries;
    double du_signed;
    for( int i = 0; i < nrand; i++)
    {
      section2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetSections() ) );
      turn2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetTurns() ) );
      layer2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetLayers() ) );

      len2 = coil2->Length(u1v[ section2 ], u2v[ section2 ], section2, turn2, layer2);

//    if( verbose ) std::cout << "section2 = " << section2 << " turn2 = " << turn2<< " layer2 = " << layer2 << " len2 = " << len2 << std::endl;

      tries = 0;
      while( len2 <= 0 && tries < nrand )
      {
        section2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetSections() ) );
        turn2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetTurns() ) );
        layer2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetLayers() ) );

        len2 = coil2->Length(u1v[ section2 ], u2v[ section2 ], section2, turn2, layer2);

//      if( verbose ) std::cout << "section2 = " << section2 << " turn2 = " << turn2<< " layer2 = " << layer2 << " len2 = " << len2 << std::endl;

        tries++;
      }

      if( tries >= nrand )
      {
         std::cerr << "Model does not have non-zero positive lengths" << std::endl;
         return -1;
      }

      np2 = coil2->NElements( lc2, u1v, u2v, section2, turn2, layer2 );

      du_signed = 0;
      if( np2 > 0 ) du_signed = ( u2v[ section2 ] - u1v[ section2 ] ) / np2;

      n1 = (int)( gsl_ran_flat (rnd, 0, np1 ) );
      n2 = (int)( gsl_ran_flat (rnd, 0, np2 ) );

      u = n2 * du_signed + du_signed / 2 + u1v[ section2 ];

      dl2 = coil2->dl( du_signed, u, section2, turn2, layer2 );
      dv2 = coil2->GetA() * dl2;

  // probability to select dv1 and dv2 segments
      dv1 = dv1a[ n1 ];
      prob1 = dv1 / V1;
      prob2 = dv2 / V2;
      prob = prob1 * prob2;

      iiflucks::makeVector( x1c[ n1 ], y1c[ n1 ], z1c[ n1 ], P1 );
      iiflucks::makeVector( J1x[ n1 ], J1y[ n1 ], J1z[ n1 ], J1v );

      P2 = coil2->XYZ( u, section2, turn2, layer2 );
      J2v = coil2->Jv( u, section2, turn2, layer2 );
      if( du_signed < 0 ) iiflucks::scaleVector( -1.0, J2v );

      if( theta != 0 )
      {
        iiflucks::QuaternionRotation( a, theta, P1 );
        iiflucks::QuaternionRotation( a, theta, J1v );
      }
      iiflucks::addVectors( P1, tr, P1);

      if( verbose )
      {
        std::cout << i << "," << np1 << "," << n1;
        std::cout	<< "," << section2 << "," << turn2 << "," << layer2;
        std::cout << "," << len2 << "," << np2 << "," << n2;
        std::cout << "," << du_signed << "," << u;
        std::cout << "," << dl1 << "," << dv1 << "," << prob1;
        std::cout << "," << dl2 << "," << dv2 << "," << prob2;
        std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
        std::cout << "," << P2[0] << "," << P2[1] << "," << P2[2];
        std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
        std::cout << "," << J2v[0] << "," << J2v[1] << "," << J2v[2];
      }

      Innerloop();

      if( verbose )
      {
        std::cout << "," << rij;
        std::cout << "," << dM12 << "," << M12 << "," << Msqsum;
        std::cout << "," << dF12[0] << "," << dF12[1] << "," << dF12[2];
        std::cout << "," << F12[0] << "," << F12[1] << "," << F12[2];
        std::cout << "," << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
      }
    }

    Calculate(); // calculate values and errors from accumulated values

  }

  return nzero;
}

template <typename T, typename U> int MonteCarlo::Run(T *coil1, U *coil2, const double lc1, const std::vector<double> t1v, const std::vector<double> t2v, const double lc2, const std::vector<double> u1v, const std::vector<double> u2v)
{
  int section1, section2;
  int turn1, turn2;
  int layer1, layer2;

  double length1 = coil1->CalcLength( t1v, t2v, verbose );
  double length2 = coil2->CalcLength( u1v, u2v, verbose );
  V1 = length1 * coil1->GetA();
  V2 = length2 * coil2->GetA();

  int elements1 = coil1->NElements(lc1, t1v, t2v);
  int elements2 = coil2->NElements(lc2, u1v, u2v);

  prob1 = 1.0 / (double)elements1;
  prob2 = 1.0 / (double)elements2;

  M12 = 0;
  Msqsum = 0;
  nzero = 0;

  iiflucks::zeroVector( F12 );
  iiflucks::zeroVector( Fsqsum );
  nrand = pow(10, Nrnd);

  if( verbose ) // CSV printing 
  {
    std::cout << "lc1, " << lc1 << std::endl;
    std::cout << "lc2, " << lc2 << std::endl;
    std::cout << "elements1, " << elements1 << std::endl;
    std::cout << "elements2, " << elements2 << std::endl;
    std::cout << "prob1, " << prob1 << std::endl;
    std::cout << "prob2, " << prob2 << std::endl;
    std::cout << "length1, " << length1 << std::endl;
    std::cout << "length2, " << length2 << std::endl;
    std::cout << "V1, " << V1 << std::endl;
    std::cout << "V2, " << V2 << std::endl;
    std::cout << "M12, " << M12 << std::endl;
    std::cout << "Msqsum, " << Msqsum << std::endl;
    std::cout << "F12, " << F12[0] << "," << F12[1] << "," << F12[2] << std::endl;
    std::cout << "Fsqsum, " << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
    std::cout << "nrand, " << nrand << std::endl;
    std::cout << "i,section1,turn1,layer1,len1,np1,n1,dt_signed,t,section2,turn2,layer2,len2,np2,n2,du_signed,u,dl1,dv1,prob1,dl2,dv2,prob2,P1x,P1y,P1z,P2x,P2y,P2z,J1vx,J1vy,J1vz,J2vx,J2vy,J2vz,rij,dM12,M12,Msqsum,dF12x,dF12y,dF12z,F12x,F12y,F12z,Fsqsumx,Fsqsumy,Fsqsumz" << std::endl;
  }

  const gsl_rng_type * Ur;
  gsl_rng * rnd;

  Ur = gsl_rng_default;
  rnd = gsl_rng_alloc (Ur);

  double len1, len2;
  double dt_signed, du_signed;
  int tries;
  for(int i=0; i < nrand; i++)
  {
    section1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetSections() ) );
    section2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetSections() ) );
    turn1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetTurns() ) );
    turn2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetTurns() ) );
    layer1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetLayers() ) );
    layer2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetLayers() ) );

    len1 = coil1->Length(t1v[ section1 ], t2v[ section1 ], section1, turn1, layer1);
    len2 = coil2->Length(u1v[ section2 ], u2v[ section2 ], section2, turn2, layer2);

//    if( verbose ) std::cout << "section1 = " << section1 << " turn1 = " << turn1<< " layer1 = " << layer1 << " len1 = " << len1 << std::endl;
//    if( verbose ) std::cout << "section2 = " << section2 << " turn2 = " << turn2<< " layer2 = " << layer2 << " len2 = " << len2 << std::endl;

    tries = 0;
    while( ( len1 <= 0 || len2 <= 0 ) && tries < nrand )
    {
      section1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetSections() ) );
      section2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetSections() ) );
      turn1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetTurns() ) );
      turn2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetTurns() ) );
      layer1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetLayers() ) );
      layer2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetLayers() ) );

      len1 = coil1->Length(t1v[ section1 ], t2v[ section1 ], section1, turn1, layer1);
      len2 = coil2->Length(u1v[ section2 ], u2v[ section2 ], section2, turn2, layer2);

//      if( verbose ) std::cout << "section1 = " << section1 << " turn1 = " << turn1<< " layer1 = " << layer1 << " len1 = " << len1 << std::endl;
//      if( verbose ) std::cout << "section2 = " << section2 << " turn2 = " << turn2<< " layer2 = " << layer2 << " len2 = " << len2 << std::endl;

      tries++;
    }

    if( tries >= nrand )
    {
       std::cerr << "Model does not have non-zero positive lengths" << std::endl;
       return -1;
    }

    np1 = coil1->NElements( lc1, t1v, t2v, section1, turn1, layer1 );
    np2 = coil2->NElements( lc2, u1v, u2v, section2, turn2, layer2 );

    dt_signed = 0;
    if( np1 > 0 ) dt_signed = ( t2v[ section1 ] - t1v[ section1 ] ) / np1;
    du_signed = 0;
    if( np2 > 0 ) du_signed = ( u2v[ section2 ] - u1v[ section2 ] ) / np2;

    n1 = (int)( gsl_ran_flat (rnd, 0, np1 ) );
    n2 = (int)( gsl_ran_flat (rnd, 0, np2 ) );

    t = n1 * dt_signed + dt_signed / 2 + t1v[ section1 ];
    u = n2 * du_signed + du_signed / 2 + u1v[ section2 ];

    dl1 = coil1->dl( dt_signed, t, section1, turn1, layer1 );
    dl2 = coil2->dl( du_signed, u, section2, turn2, layer2 );
    dv1 = coil1->GetA() * dl1;
    dv2 = coil2->GetA() * dl2;

  // probability to select dv1 and dv2 segments
    prob1 = dv1 / V1;
    prob2 = dv2 / V2;
    prob = prob1 * prob2;

    P1 = coil1->XYZ( t, section1, turn1, layer1 );
    P2 = coil2->XYZ( u, section2, turn2, layer2 );

    J1v = coil1->Jv( t, section1, turn1, layer1 );
    if( dt_signed < 0 ) iiflucks::scaleVector( -1.0, J1v );
    J2v = coil2->Jv( u, section2, turn2, layer2 );
    if( du_signed < 0 ) iiflucks::scaleVector( -1.0, J2v );

    if( verbose )
    {
      std::cout << i; 
      std::cout	<< "," << section1 << "," << turn1 << "," << layer1;
      std::cout << "," << len1 << "," << np1 << "," << n1;
      std::cout << "," << dt_signed << "," << t;
      std::cout	<< "," << section2 << "," << turn2 << "," << layer2;
      std::cout << "," << len2 << "," << np2 << "," << n2;
      std::cout << "," << du_signed << "," << u;
      std::cout << "," << dl1 << "," << dv1 << "," << prob1;
      std::cout << "," << dl2 << "," << dv2 << "," << prob2;
      std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
      std::cout << "," << P2[0] << "," << P2[1] << "," << P2[2];
      std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
      std::cout << "," << J2v[0] << "," << J2v[1] << "," << J2v[2];
    }

    Innerloop();

    if( verbose )
    {
      std::cout << "," << rij;
      std::cout << "," << dM12 << "," << M12 << "," << Msqsum;
      std::cout << "," << dF12[0] << "," << dF12[1] << "," << dF12[2];
      std::cout << "," << F12[0] << "," << F12[1] << "," << F12[2];
      std::cout << "," << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
    }
  }

  Calculate(); // calculate values and errors from accumulated values

  return nzero;
}

template <typename T, typename U> int MonteCarlo::Run(T *coil1, U *coil2, const double dt, const double t1, const double t2, const double lc2, const std::vector<double> u1v, const std::vector<double> u2v)
{
  double length1 = coil1->Length(t1, t2);
  double length2 = coil2->CalcLength( u1v, u2v, verbose );
  V1 = length1 * coil1->GetA();
  V2 = length2 * coil2->GetA();

  double dt_signed;
  if( std::abs( dt ) > 0 && length1 > 0 && length2 > 0 )
  {
    np1 = ceil( std::abs( ( t2 - t1 ) / dt ) );
    dt_signed = std::abs( dt );
    if( t1 > t2 ) dt_signed *= -1.0;
  }
  else
  {
    std::cerr << "Parametric model does not have non-zero positive length or volume." << std::endl;
    std::cerr << "dt = " << dt << " length1 = " << length1 << " V1 = " << V1 << std::endl;
    std::cerr << "lc2 = " << lc2 << " length2 = " << length2 << " V2 = " << V2 << std::endl;
    return -1;
  }

  int section2;
  int turn2;
  int layer2;

  int elements2 = coil2->NElements(lc2, u1v, u2v);

  dv1 = V1 / np1;
  prob1 = dv1 / V1;

  prob2 = 1.0 / (double)elements2;

  M12 = 0;
  Msqsum = 0;
  nzero = 0;

  iiflucks::zeroVector( F12 );
  iiflucks::zeroVector( Fsqsum );
  nrand = pow(10, Nrnd);

  if( verbose ) // CSV printing 
  {
    std::cout << "lc2, " << lc2 << std::endl;
    std::cout << "elements2, " << elements2 << std::endl;
    std::cout << "prob1, " << prob1 << std::endl;
    std::cout << "prob2, " << prob2 << std::endl;
    std::cout << "length1, " << length1 << std::endl;
    std::cout << "length2, " << length2 << std::endl;
    std::cout << "V1, " << V1 << std::endl;
    std::cout << "V2, " << V2 << std::endl;
    std::cout << "M12, " << M12 << std::endl;
    std::cout << "Msqsum, " << Msqsum << std::endl;
    std::cout << "F12, " << F12[0] << "," << F12[1] << "," << F12[2] << std::endl;
    std::cout << "Fsqsum, " << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
    std::cout << "nrand, " << nrand << std::endl;
    std::cout << "i,len1,np1,n1,dt_signed,t,section2,turn2,layer2,len2,np2,n2,du_signed,u,dl1,dv1,prob1,dl2,dv2,prob2,P1x,P1y,P1z,P2x,P2y,P2z,J1vx,J1vy,J1vz,J2vx,J2vy,J2vz,rij,dM12,M12,Msqsum,dF12x,dF12y,dF12z,F12x,F12y,F12z,Fsqsumx,Fsqsumy,Fsqsumz" << std::endl;
  }

  const gsl_rng_type * Ur;
  gsl_rng * rnd;

  Ur = gsl_rng_default;
  rnd = gsl_rng_alloc (Ur);

  double len2;
  double du_signed;
  int tries;
  for(int i=0; i < nrand; i++)
  {
    section2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetSections() ) );
    turn2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetTurns() ) );
    layer2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetLayers() ) );

    len2 = coil2->Length(u1v[ section2 ], u2v[ section2 ], section2, turn2, layer2);

//    if( verbose ) std::cout << "section2 = " << section2 << " turn2 = " << turn2 << " layer2 = " << layer2 << " len2 = " << len2 << std::endl;

    tries = 0;
    while( len2 <= 0 && tries < nrand )
    {
      section2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetSections() ) );
      turn2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetTurns() ) );
      layer2 = (int)( gsl_ran_flat (rnd, 0, coil2->GetLayers() ) );

      len2 = coil2->Length(u1v[ section2 ], u2v[ section2 ], section2, turn2, layer2);

//      if( verbose ) std::cout << "section2 = " << section2 << " turn2 = " << turn2 << " layer2 = " << layer2 << " len2 = " << len2 << std::endl;

      tries++;
    }

    if( tries >= nrand )
    {
       std::cerr << "Model does not have non-zero positive lengths" << std::endl;
       return -1;
    }

    np2 = coil2->NElements( lc2, u1v, u2v, section2, turn2, layer2 );

    du_signed = 0;
    if( np2 > 0 ) du_signed = ( u2v[ section2 ] - u1v[ section2 ] ) / np2;

    n1 = (int)( gsl_ran_flat (rnd, 0, np1 ) );
    t = n1 * dt_signed + dt_signed/2 + t1;

    n2 = (int)( gsl_ran_flat (rnd, 0, np2 ) );
    u = n2 * du_signed + du_signed / 2 + u1v[ section2 ];

    dl1 = coil1->dl( dt, t );
    dl2 = coil2->dl( du_signed, u, section2, turn2, layer2 );
    dv1 = coil1->GetA() * dl1;
    dv2 = coil2->GetA() * dl2;

  // probability to select dv1 and dv2 segments
    prob1 = dv1 / V1;
    prob2 = dv2 / V2;
    prob = prob1 * prob2;

    P1 = coil1->XYZ( t );
    P2 = coil2->XYZ( u, section2, turn2, layer2 );

    J1v = coil1->Jv( t );
    if( dt_signed < 0 ) iiflucks::scaleVector( -1.0, J1v );
    J2v = coil2->Jv( u, section2, turn2, layer2 );
    if( du_signed < 0 ) iiflucks::scaleVector( -1.0, J2v );

    if( verbose )
    {
      std::cout << i << "," << len1 << "," << np1 << "," << n1;
      std::cout << "," << dt_signed << "," << t;
      std::cout	<< section2 << "," << turn2 << "," << layer2;
      std::cout << "," << len2 << "," << np2 << "," << n2;
      std::cout << "," << du_signed << "," << u;
      std::cout << "," << dl1 << "," << dv1 << "," << prob1;
      std::cout << "," << dl2 << "," << dv2 << "," << prob2;
      std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
      std::cout << "," << P2[0] << "," << P2[1] << "," << P2[2];
      std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
      std::cout << "," << J2v[0] << "," << J2v[1] << "," << J2v[2];
    }

    Innerloop();

    if( verbose )
    {
      std::cout << "," << rij;
      std::cout << "," << dM12 << "," << M12 << "," << Msqsum;
      std::cout << "," << dF12[0] << "," << dF12[1] << "," << dF12[2];
      std::cout << "," << F12[0] << "," << F12[1] << "," << F12[2];
      std::cout << "," << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
    }
  }

  Calculate(); // calculate values and errors from accumulated values

  return nzero;
}

template <typename T, typename U> int MonteCarlo::Run(T *coil1, U *coil2, const double lc1, const std::vector<double> t1v, const std::vector<double> t2v, const double du, const double u1, const double u2)
{
  double length1 = coil1->CalcLength( t1v, t2v, verbose );
  double length2 = coil2->Length(u1, u2);
  V1 = length1 * coil1->GetA();
  V2 = length2 * coil2->GetA();

  double du_signed;
  if( std::abs( du ) > 0 && length1 > 0 && length2 > 0 )
  {
    np2 = ceil( std::abs( ( u2 - u1 ) / du ) );
    du_signed = std::abs( du );
    if( u1 > u2 ) du_signed *= -1.0;
  }
  else
  {
    std::cerr << "Parametric model does not have non-zero positive length or volume." << std::endl;
    std::cerr << "lc1 = " << lc1 << " length1 = " << length1 << " V1 = " << V1 << std::endl;
    std::cerr << "du = " << du << " length2 = " << length2 << " V2 = " << V2 << std::endl;
    return -1;
  }

  int section1;
  int turn1;
  int layer1;

  int elements1 = coil1->NElements(lc1, t1v, t2v);
  prob1 = 1.0 / (double)elements1;

  dv2 = V2 / np2;
  prob2 = dv2 / V2;

  M12 = 0;
  Msqsum = 0;
  nzero = 0;

  iiflucks::zeroVector( F12 );
  iiflucks::zeroVector( Fsqsum );
  nrand = pow(10, Nrnd);

  if( verbose ) // CSV printing
  {
    std::cout << "lc1, " << lc1 << std::endl;
    std::cout << "elements1, " << elements1 << std::endl;
    std::cout << "prob1, " << prob1 << std::endl;
    std::cout << "prob2, " << prob2 << std::endl;
    std::cout << "length1, " << length1 << std::endl;
    std::cout << "length2, " << length2 << std::endl;
    std::cout << "V1, " << V1 << std::endl;
    std::cout << "V2, " << V2 << std::endl;
    std::cout << "M12, " << M12 << std::endl;
    std::cout << "Msqsum, " << Msqsum << std::endl;
    std::cout << "F12, " << F12[0] << "," << F12[1] << "," << F12[2] << std::endl;
    std::cout << "Fsqsum, " << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
    std::cout << "nrand, " << nrand << std::endl;
    std::cout << "i,section1,turn1,layer1,len1,np1,n1,dt_signed,t,len2,np2,n2,du_signed,u,dl1,dv1,prob1,dl2,dv2,prob2,P1x,P1y,P1z,P2x,P2y,P2z,J1vx,J1vy,J1vz,J2vx,J2vy,J2vz,rij,dM12,M12,Msqsum,dF12x,dF12y,dF12z,F12x,F12y,F12z,Fsqsumx,Fsqsumy,Fsqsumz" << std::endl;
  }

  const gsl_rng_type * Ur;
  gsl_rng * rnd;

  Ur = gsl_rng_default;
  rnd = gsl_rng_alloc (Ur);

  double len1;
  double dt_signed;
  int tries;
  for(int i=0; i < nrand; i++)
  {
    section1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetSections() ) );
    turn1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetTurns() ) );
    layer1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetLayers() ) );

    len1 = coil1->Length(t1v[ section1 ], t2v[ section1 ], section1, turn1, layer1);

    tries = 0;
    while( len1 <= 0 && tries < nrand )
    {
      section1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetSections() ) );
      turn1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetTurns() ) );
      layer1 = (int)( gsl_ran_flat (rnd, 0, coil1->GetLayers() ) );

      len1 = coil1->Length(t1v[ section1 ], t2v[ section1 ], section1, turn1, layer1);

//      if( verbose ) std::cout << "section1 = " << section1 << " turn1 = " << turn1 << " layer1 = " << layer1 << " len1 = " << len1 << std::endl;

      tries++;
    }

    if( tries >= nrand )
    {
       std::cerr << "Model does not have non-zero positive lengths" << std::endl;
       return -1;
    }

    np1 = coil1->NElements( lc1, t1v, t2v, section1, turn1, layer1 );

    dt_signed = 0;
    if( np1 > 0 ) dt_signed = ( t2v[ section1 ] - t1v[ section1 ] ) / np1;

    n1 = (int)( gsl_ran_flat (rnd, 0, np1 ) );
    t = n1 * dt_signed + dt_signed / 2 + t1v[ section1 ];

    n2 = (int)( gsl_ran_flat (rnd, 0, np2 ) );
    u = n2 * du_signed + du_signed/2 + u1;

    dl1 = coil1->dl( dt_signed, t, section1, turn1, layer1 );
    dl2 = coil2->dl( du, u );
    dv1 = coil1->GetA() * dl1;
    dv2 = coil2->GetA() * dl2;

  // probability to select dv1 and dv2 segments
    prob1 = dv1 / V1;
    prob = prob1 * prob2;

    P1 = coil1->XYZ( t, section1, turn1, layer1 );
    P2 = coil2->XYZ( u );

    J1v = coil1->Jv( t, section1, turn1, layer1 );
    if( dt_signed < 0 ) iiflucks::scaleVector( -1.0, J1v );
    J2v = coil2->Jv( t );
    if( du_signed < 0 ) iiflucks::scaleVector( -1.0, J2v );

    if( verbose )
    {
      std::cout << i << "," << section1 << "," << turn1 << "," << layer1;
      std::cout << "," << len1 << "," << np1 << "," << n1;
      std::cout << "," << dt_signed << "," << t;
      std::cout << "," << len2 << "," << np2 << "," << n2;
      std::cout << "," << du_signed << "," << u;
      std::cout << "," << dl1 << "," << dv1 << "," << prob1;
      std::cout << "," << dl2 << "," << dv2 << "," << prob2;
      std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
      std::cout << "," << P2[0] << "," << P2[1] << "," << P2[2];
      std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
      std::cout << "," << J2v[0] << "," << J2v[1] << "," << J2v[2];
    }

    Innerloop();

    if( verbose )
    {
      std::cout << "," << rij;
      std::cout << "," << dM12 << "," << M12 << "," << Msqsum;
      std::cout << "," << dF12[0] << "," << dF12[1] << "," << dF12[2];
      std::cout << "," << F12[0] << "," << F12[1] << "," << F12[2];
      std::cout << "," << Fsqsum[0] << "," << Fsqsum[1] << "," << Fsqsum[2] << std::endl;
    }
  }

  Calculate(); // calculate values and errors from accumulated values

  return nzero;
}

/// Run Monte Carlo integration of field for coil T t=[t1, t2] at P.
template <typename T> int MonteCarlo::Run(T *coil, const double dt, const double t1, const double t2, const std::vector<double> P)
{
  len1 = coil->Length(t1, t2);
  V1 = len1 * coil->GetA();

  double dt_signed;
  if( std::abs( dt ) > 0 && len1 > 0 && V1 > 0 )
  {
    np1 = ceil( std::abs( ( t2 - t1 ) / dt ) );
    dt_signed = std::abs( dt );
    if( t1 > t2 ) dt_signed *= -1.0;
  }
  else
  {
    std::cerr << "Parametric model does not have non-zero positive length or volume. dt = " << dt << " len1 = " << len1 << " V1 = " << V1 << std::endl;
    return -1;
  }

  InitB(); // calculate probabilities and reset cumulative variables

  const gsl_rng_type * Ur;
  gsl_rng * rnd;

  Ur = gsl_rng_default;
  rnd = gsl_rng_alloc (Ur);

  P2 = P;

  double len2 = 0;
  for(int n1 = 0; n1 < np1; n1++)
  {
    t = n1 * dt_signed + dt_signed/2 + t1;
    dl1 = coil->dl( dt_signed, t );
    len2 += dl1;
  }

  if( verbose ) // CSV printing 
  {
    std::cout << "dt_signed, " << dt_signed << std::endl;
    std::cout << "np1, " << np1 << std::endl;
    std::cout << "dv1, " << dv1 << std::endl;
    std::cout << "prob1, " << prob1 << std::endl;
    std::cout << "len1, " << len1 << std::endl;
    std::cout << "len2, " << len2 << std::endl;
    std::cout << "V1, " << V1 << std::endl;
    std::cout << "P2, " << P2[0] << "," << P2[1] << "," << P2[2] << std::endl;
    std::cout << "B, " << B[0] << "," << B[1] << "," << B[2] << std::endl;
    std::cout << "Bsqsum, " << Bsqsum[0] << "," << Bsqsum[1] << "," << Bsqsum[2] << std::endl;
    std::cout << "nrand, " << nrand << std::endl;
    std::cout << "i,n1,t,dl1,dv1,prob1,P1x,P1y,P1z,J1vx,J1vy,J1vz,rij,dBx,dBy,dBz,Bx,By,Bz" << std::endl;
  }

  nzero = 0;
  for(int i=0; i < nrand; i++)
  {
    n1 = (int)( gsl_ran_flat (rnd, 0, np1 ) );

    t = n1 * dt_signed + dt_signed/2 + t1;

    dl1 = coil->dl( dt_signed, t );
    dv1 = coil->GetA() * dl1;
    prob1 = dl1 / len1;

    P1 = coil->XYZ( t );

    J1v = coil->Jv( t );
    if( dt_signed < 0 ) iiflucks::scaleVector( -1.0, J1v );

    if( verbose )
    {
      std::cout << i << "," << n1 << "," << t;
      std::cout << "," << dl1 << "," << dv1 << "," << prob1;
      std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
      std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
    }

    InnerloopB();

    if( verbose )
    {
      std::cout << "," << rij;
      std::cout << "," << dBv[0] << "," << dBv[1] << ","<< dBv[2];
      std::cout << "," << B[0] << "," << B[1] << "," << B[2] << std::endl;
    }
  }

  CalculateB(); // calculate values and errors from accumulated values

  return nzero;
}

/// Run Monte Carlo integration of field for coil T t=[t1, t2] at P.
template <typename T> int MonteCarlo::Run(T *coil, const double lc, const std::vector<double> t1v, const std::vector<double> t2v, const std::vector<double> P)
{
  int section1;
  int turn1;
  int layer1;

  double length1 = coil->CalcLength( t1v, t2v, verbose );
  V1 = length1 * coil->GetA();
  P2 = P;

  int elements = coil->NElements(lc, t1v, t2v);

  // probability to select element 
  prob1 = 1.0 / (double)elements;

  iiflucks::zeroVector( B );
  iiflucks::zeroVector( Bsqsum );
  nrand = pow(10, Nrnd);

  if( verbose ) // CSV printing 
  {
    std::cout << "lc, " << lc << std::endl;
    std::cout << "elements, " << elements << std::endl;
    std::cout << "prob1, " << prob1 << std::endl;
    std::cout << "length1, " << length1 << std::endl;
    std::cout << "V1, " << V1 << std::endl;
    std::cout << "P2, " << P2[0] << "," << P2[1] << "," << P2[2] << std::endl;
    std::cout << "B, " << B[0] << "," << B[1] << "," << B[2] << std::endl;
    std::cout << "Bsqsum, " << Bsqsum[0] << "," << Bsqsum[1] << "," << Bsqsum[2] << std::endl;
    std::cout << "nrand, " << nrand << std::endl;
    std::cout << "i,section1,turn1,layer1,len1,np1,n1,dt_signed,t,dl1,dv1,prob1,P1x,P1y,P1z,J1vx,J1vy,J1vz,rij,dBx,dBy,dBz,Bx,By,Bz" << std::endl;
  }

  const gsl_rng_type * Ur;
  gsl_rng * rnd;

  Ur = gsl_rng_default;
  rnd = gsl_rng_alloc (Ur);

  nzero = 0;
  int tries;
  double dt_signed;
  for(int i=0; i < nrand; i++)
  {
    section1 = (int)( gsl_ran_flat (rnd, 0, coil->GetSections() ) );
    turn1 = (int)( gsl_ran_flat (rnd, 0, coil->GetTurns() ) );
    layer1 = (int)( gsl_ran_flat (rnd, 0, coil->GetLayers() ) );

    len1 = coil->Length(t1v[ section1 ], t2v[ section1 ], section1, turn1, layer1);
//    if( verbose ) std::cout << "section1 = " << section1 << " turn1 = " << turn1<< " layer1 = " << layer1 << " len1 = " << len1 << std::endl;

    tries = 0;
    while( len1 <= 0 && tries < nrand )
    {
      section1 = (int)( gsl_ran_flat (rnd, 0, coil->GetSections() ) );
      turn1 = (int)( gsl_ran_flat (rnd, 0, coil->GetTurns() ) );
      layer1 = (int)( gsl_ran_flat (rnd, 0, coil->GetLayers() ) );

      len1 = coil->Length(t1v[ section1 ], t2v[ section1 ], section1, turn1, layer1);

//      if( verbose ) std::cout << "section1 = " << section1 << " turn1 = " << turn1<< " layer1 = " << layer1 << " len1 = " << len1 << std::endl;

      tries++;
    }

    if( tries >= nrand )
    {
       std::cerr << "Model does not have non-zero positive lengths" << std::endl;
       return -1;
    }

    np1 = coil->NElements( lc, t1v, t2v, section1, turn1, layer1 );

    dt_signed = 0;
    if( np1 > 0 ) dt_signed = ( t2v[ section1 ] - t1v[ section1 ] ) / np1;

    n1 = (int)( gsl_ran_flat (rnd, 0, np1 ) );

    t = n1 * dt_signed + dt_signed / 2 + t1v[ section1 ];

    dl1 = coil->dl( dt_signed, t, section1, turn1, layer1 );
    dv1 = coil->GetA() * dl1;

    P1 = coil->XYZ( t, section1, turn1, layer1 );

    J1v = coil->Jv( t, section1, turn1, layer1 );
    if( dt_signed < 0 ) iiflucks::scaleVector( -1.0, J1v );

    if( verbose )
    {
      std::cout << i << "," << section1 << "," << turn1 << "," << layer1;
      std::cout << "," << len1 << "," << np1 << "," << n1;
      std::cout << "," << dt_signed << "," << t;
      std::cout << "," << dl1 << "," << dv1 << "," << prob1;
      std::cout << "," << P1[0] << "," << P1[1] << "," << P1[2];
      std::cout << "," << J1v[0] << "," << J1v[1] << "," << J1v[2];
    }

    InnerloopB();

    if( verbose )
    {
      std::cout << "," << rij;
      std::cout << "," << dBv[0] << "," << dBv[1] << ","<< dBv[2];
      std::cout << "," << B[0] << "," << B[1] << "," << B[2] << std::endl;
    }
  }

  CalculateB(); // calculate values and errors from accumulated values

  return nzero;
}

