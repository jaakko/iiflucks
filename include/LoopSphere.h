#ifndef LOOPSPHERE_H
#define LOOPSPHERE_H
#pragma once

#include "Coil.h"

/// Class for loop on sphere inherited from Coil base class. 

/// The constructor _LoopSphere_ sets name tag, altitude theta1 and azimuth
/// phi1 for start point with dtheta and dphi for size of loop. Sphere radius
/// is _R2_ and _t = [-1, +1]_ gives half loop. Use negative dtheta for the
/// second half. In polar coordinates with _theta_ between xy-plane and
/// position vector P(x,y,z):
/// theta = theta1 + ( 1 - t^4 ) * dtheta
/// phi = phi1 + dphi * ( t + 1 ) / 2  
///
class LoopSphere : public Coil 
{
  std::string name;       ///< name tag for loop on sphere 
  double theta1;          ///< start point altitude[rad]
  double dtheta;          ///< loop altitude angular length[rad]
  double phi1;            ///< start point azimuth[rad]
  double dphi;            ///< loop azimuthal angular length[rad]
  double R2;              ///< sphere radius[m]

  public:
  /// Construct LoopSphere object with parameters.
  LoopSphere(std::string name, double R2, double theta1, double phi1, double dtheta, double dphi, double A, double J, double theta, std::vector<double> axis, std::vector<double> tr) : Coil(A, J, theta, axis, tr)
  {
    this->name = name;
    this->theta1 = theta1;
    this->dtheta = dtheta;
    this->phi1 = phi1;
    this->dphi = dphi;
    this->R2 = R2;
  };

  LoopSphere(std::string name, double R2, double theta1, double phi1, double dtheta, double dphi, double A, double J) : Coil( A, J, 0, {0, 0, 1}, {0, 0, 0} )
  {
    this->name = name;
    this->theta1 = theta1;
    this->dtheta = dtheta;
    this->phi1 = phi1;
    this->dphi = dphi;
    this->R2 = R2;
  };

  virtual ~LoopSphere();

  /// Get loop on sphere name tag.
  std::string GetName() { return name; }

  /// Get start point altitude [rad].
  double Get_theta1() { return theta1; }

  /// Set start point altitude [rad].
  void Set_theta1(double theta1) { this->theta1 = theta1; }

  /// Get loop altitude angular length [rad]. 
  double Get_dtheta() { return dtheta; }

  /// Set loop altitude angular length [rad]. 
  void Set_dtheta(double dtheta) { this->dtheta = dtheta; }

  /// Get start point azimuth [rad].
  double Get_phi1() { return phi1; }

  /// Set start point azimuth [rad].
  void Set_phi1(double phi1) { this->phi1 = phi1; }

  /// Get loop azimuthal angular length [rad]. 
  double Get_dphi() { return dphi; }

  /// Set loop azimuthal angular length [rad]. 
  void Set_dphi(double dphi) { this->dphi = dphi; }

  /// Get sphere radius [m].
  double GetR2() { return R2; }

  /// Set sphere radius [m].
  void SetR2(double R2) { this->R2 = R2; }

  /// LoopSphere curve length [m].
  double Length(const double t1, const double t2);

  /// LoopSphere small section t = [ t - dt/2, t + dt/2 ] length [m].
  double dl(const double dt, const double t);

  /// LoopSphere small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t);

  /// LoopSphere small section t = [ t - dt/2, t + dt/2 ] volume [m^3].
  double dv(const double dt, const double t, const double r1, const double r2);

  /// LoopSphere curve parametric length [m].
  double ds(const double t);

  /// Parametric position vector with parameter t.
  std::vector<double> XYZ(const double t);

  /// Parametric tangent vector with parameter t.
  std::vector<double> Jv(const double t);

  /// Calculation of magnetic field at position P. Return 0 in success.
  int B(const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

  /// Print list of elements with position coordinates and current density vectors.
  double Elements(const double t1, const double t2, const int M, const int test, const bool verbose);
};

#endif
