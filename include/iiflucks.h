//
// Function definitions (API) for IIFlucks.
//
// IIFlucks is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// IIFlucks is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with IIFlucks.  If not, see <https://www.gnu.org/licenses/>.
//
// Copyright (C) 2019 - 2024 Jaakko Koivuniemi. All rights reserved.

#include <yaml-cpp/yaml.h>
#include "Helix.h"
#include "Line.h"
#include "LineSphere.h"
#include "LoopSphere.h"
#include "RacetrackCylinder.h"
#include "RacetrackPlanar.h"
#include "Ring.h"
#include "RingCylinder.h"
#include "Solenoid.h"

namespace iiflucks {

///
/// Free functions defined in namespace `iiflucks` .
///

/// Hamilton product of two quaternions.
void HamiltonProduct(const std::vector<double> &q1, const std::vector<double> &q2, std::vector<double> &q);

/// Conjugate quaternion.
void ConjugateQuaternion(const std::vector<double> &q1, std::vector<double> &q);

/// Rotate vector __r__ with angle _theta_ around axis vector __a__.
void QuaternionRotation(const std::vector<double> &a, const double theta, std::vector<double> &r);

/// Calculate cross product of two vectors.
void crossProduct(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c);

/// Add two vectors.
void addVectors(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c);

/// Subtract two vectors.
void subtractVectors(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c);

/// Add square of vector element to an other vector element.
void squareAddVector(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c);

/// Return zero vector.
void zeroVector(std::vector<double> &a);

/// Return vector (x, y, z).
void makeVector(const int x, const int y, const int z, std::vector<int> &a);

/// Return vector (x, y, z).
void makeVector(const double x, const double y, const double z, std::vector<double> &a);

/// Normalize vector length to 1.
void normalizeVector(std::vector<double> &a);

/// Scale vector with factor scale.
void scaleVector(const double scale, std::vector<double> &a);

/// Calculate Cartesian coordinates from cylindrical coordinates.
void cylindricalCartesian(const std::vector<double> &a, std::vector<double> &b);

/// Calculate cylindrical coordinates from Cartesian coordinates.
void cartesianCylindrical(const std::vector<double> &a, std::vector<double> &b);

/// Calculate Cartesian coordinates from spherical coordinates.
void sphericalCartesian(const std::vector<double> &a, std::vector<double> &b);

/// Calculate spherical coordinates from Cartesian coordinates.
void cartesianSpherical(const std::vector<double> &a, std::vector<double> &b);

/// Calculate vector length.
double vectorLength(const std::vector<double> &A);

/// Calculate vector dot product.
double dotProduct(const std::vector<double> &a, const std::vector<double> &b);

/// Calculate angle between two vectors.
double angleVectors(const std::vector<double> &a, const std::vector<double> &b);

/// Calculate distance between two points.
double distancePoints(const std::vector<double> &P1, const std::vector<double> &P2);

/// Calculate distance between two points.
int distancePoints(const std::vector<int> &P1, const std::vector<int> &P2);

/// Rotate vector on XY-plane in counter clockiwise direction around z-axis.
void rotateXY(const double alpha, std::vector<double> &a);

/// Weak test if vector is on xy-plane.
bool onXYPlane(const std::vector<double> &a);

/// Weak test if vector is on xz-plane.
bool onXZPlane(const std::vector<double> &a);

/// Weak test if vector is on yz-plane.
bool onYZPlane(const std::vector<double> &a);

/// Test if point is on plane defined by normal vector and point.
bool pointOnPlane(const std::vector<double> &n, const std::vector<double> &P0, const std::vector<double> &P);

/// Weak test if vector __v__ is on any plane defined by normal vector __n__ .
bool vectorAnyPlane(const std::vector<double> &n, const std::vector<double> &v);

/// Distance between point _P1_ and line defined by direction vector __v__ and point _P0_.
double distancePointLine(const std::vector<double> &v, const std::vector<double> &P0, const std::vector<double> &P1);

/// Calculate surface area of triangle and normal.
double triangleAreaNormal(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, std::vector<double> &nv);

/// Calculate centroid of triangle with vertices _P0_, _P1_ and _P2_.
void triangleCentroid(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, std::vector<double> &Pc);

/// Calculate quadrangle area.
double quadrangleArea(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3);

/// Calculate quadrangle centroid.
void quadrangleCentroid(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3, std::vector<double> &Pc);

/// Calculate volume of tetrahedron with vertices _P0_, _P1_, _P2_ and _P3_.
double tetrahedronVolume(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3);

/// Calculate centroid of tetrahedron with vertices _P0_, _P1_, _P2_ and _P3_.
void tetrahedronCentroid(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3, std::vector<double> &Pc);

/// Calculate quadrangle area.
double quadrangleArea(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3);

/// Distance of point _P1_ from plane spanned by two vectors __a__ and __b__ with _P0_ on it.
double distancePointPlane(const std::vector<double> P0, const std::vector<double> P1, const std::vector<double> &a, const std::vector<double> &b);

/// Calculate scalar triple product of three vectors.
double tripleProduct(const std::vector<double> &a, const std::vector<double> &b, const std::vector<double> &c);

/// Approximate volume of prism with vertices _P0_, _P1_, _P2_, _P3_, _P4_ and _P5_.
double prismVolume(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3, const std::vector<double> &P4, const std::vector<double> &P5);

/// Calculate centroid of prism with vertices _P0_, _P1_, _P2_, _P3_, _P4_ and _P5_.
void prismCentroid(std::vector<double> &P0, std::vector<double> &P1, std::vector<double> &P2, std::vector<double> &P3, std::vector<double> &P4, std::vector<double> &P5, std::vector<double> &Pc);

/// Calculate pyramid volume with vertices _P0_, _P1_, _P2_, _P3_ and _P4_.
double pyramidVolume(const std::vector<double> P0, const std::vector<double> P1, const std::vector<double> P2, const std::vector<double> P3, const std::vector<double> P4);

/// Calculate centroid of pyramid with vertices _P0_, _P1_, _P2_, _P3_ and _P4_.
void pyramidCentroid(const std::vector<double> &P0, const std::vector<double> &P1, const std::vector<double> &P2, const std::vector<double> &P3, const std::vector<double> &P4, std::vector<double> &Pc);

/// Print elements for vertical wire and return wire length.
double verticalWire(const int Mv, const double xw, const double yw0, const double zw, const double dl, const double A, const double Jw, const bool verbose, const bool test);

/// Print elements for horizontal wire and return wire length.
double horizontalWire(const int Mh, const double xw0, const double yw, const double zw, const double dl, const double A, const double Jw, const bool verbose, const bool test);

/// Print elements for arc wire and return wire length.
double arcWire(const int Mr, const double xw0, const double yw0, const double zw0, const double phi0, const double dphi, const double r, const double A, const double Jw, const bool verbose, const bool test);

/// Parametric curve for helix. Return length of segment t=[t1, t2].
double helix(const double t, const double t1, const double t2, const double r, const double L, const double N,  std::vector<double> &P, std::vector<double> &T, double &ds);

/// Print elements for helix wire and return length.
double helixWire(const double r, const double L, const double N, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test);

/// 1/4 turn section of helix in rectangular coil corner, return wire length.
double linkWire(const int Mr, const double xw0, const double yw0, const double zw0, const double phi0, const double dphi, const double r, const double L, const int N, const double A, const double Jw, const bool verbose, const bool test);

/// Parametric curve for ring. Return length of segment t=[t1, t2].
double ring(const double t, const double t1, const double t2, const double r, std::vector<double> &P, std::vector<double> &T, double &ds);

/// Create volume elements for ring/arc wire and return length.
double ringWire(const double R, const double phi0, const double phi1, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test);

/// Parametric curve for ring on cylinder. Return length of segment t=[t1, t2].
double ringcylinder(const double t, const double t1, const double t2, const double r, const double r2, std::vector<double> &P, std::vector<double> &T, double &ds);

/// Create volume elements for ring/arc on cylinder wire and return length.
double ringCylinderWire(const double R, const double R2, const double phi0, const double phi1, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test);

/// Parametric curve for straight line. Return length of segment t=[t1, t2].
double straightline(const double t, const double t1, const double t2, std::vector<double> &P, std::vector<double> &T, double &ds);

/// Create volume elements for straight wire and return length.
double straightWire(const double L, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test);

/// Create volume elements for axial gradiometer and return length.
double axialGradiometer(const double R, const double b, const double w, const double d, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const bool test);

/// Create volume elements for planar racetrack coil and return length.
double raceTrackPlanar(const double R, const double h, const double e, const double w, const double t, const double d, const int N, const int O, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test);

/// Create volume elements for racetrack coil on cylinder and return length.
double raceTrackCylinder(const double R, const double R2, const double h, const double e, const double w, const double th, const double d, const int N, const int O, const int M, const double I, const double ax, const double ay, const double az, const double theta, const double xt, const double yt, const double zt, const bool verbose, const int test);

/// Open position-current-density-dv file and calculate total volume.
int totalVolume(const std::string fname, double &V);

/// Read position-current-density-dv file.
int readFile(const std::string fname, double xc[], double yc[], double zc[], double Jx[], double Jy[], double Jz[], double dv[]);

/// Rotate position-current-density-dv data around axis by angle theta and translate by tr.
void rotateTranslate(const double theta, const std::vector<double> axis, const std::vector<double> tr, const int np, double xc[], double yc[], double zc[], double Jx[], double Jy[], double Jz[]); 

/// Calculate field from position-current-density-dv data.
std::vector<double> calculateB(const int np, double xc[], double yc[], double zc[], double Jx[], double Jy[], double Jz[], double dv[], const std::vector<double> P);

/// Numerical integration of magnetic field at position P. Return 0 in success.
int fieldHelix(const double R, const double L, const double N, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

/// Parse yaml node and create Helix object if data has definition for it.
Helix * parseHelix(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for Helix data.
bool isHelixYaml(const YAML::Node coil);

/// Numerical integration of magnetic field at position P. Return 0 in success.
int fieldRing(const double R, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

/// Parse yaml node and create Ring object if data has definition for it.
Ring * parseRing(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for Ring data.
bool isRingYaml(const YAML::Node coil);

/// Numerical integration of magnetic field at position P. Return 0 in success.
int fieldRingCylinder(const double R, const double R2, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

/// Parse yaml node and create RingCylinder object if data has definition for it.
RingCylinder * parseRingCylinder(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for RingCylinder data.
bool isRingCylinderYaml(const YAML::Node coil);

/// Parse yaml node and create RacetrackCylinder object if data has definition for it.
RacetrackCylinder * parseRacetrackCylinder(const YAML::Node coil, double &lc, std::vector<double> &t1v, std::vector<double> &t2v, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for RacetrackCylinder data.
bool isRacetrackCylinderYaml(const YAML::Node coil);

/// Parse yaml node and create RacetrackPlanar object if data has definition for it.
RacetrackPlanar * parseRacetrackPlanar(const YAML::Node coil, double &lc, std::vector<double> &t1v, std::vector<double> &t2v, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for RacetrackPlanar data.
bool isRacetrackPlanarYaml(const YAML::Node coil);

/// Parse yaml node and create Solenoid object if data has definition for it.
Solenoid * parseSolenoid(const YAML::Node coil, double &lc, std::vector<double> &t1v, std::vector<double> &t2v, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for Solenoid data.
bool isSolenoidYaml(const YAML::Node coil);

/// Calculation of magnetic field at position P. Return 0 in success.
int fieldLine(const double I, const double t1, const double t2, const std::vector<double> P, std::vector<double> &Bv);

/// Parse yaml node and create Line object if data has definition for it.
Line * parseLine(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for Line data.
bool isLineYaml(const YAML::Node coil);

/// Calculation of magnetic field at position P. Return 0 in success.
int fieldLineSphere(const double R1, const double R2, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

/// Parse yaml node and create LineSphere object if data has definition for it.
LineSphere * parseLineSphere(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for LineSphere data.
bool isLineSphereYaml(const YAML::Node coil);

/// Calculation of magnetic field at position P. Return 0 in success.
int fieldLoopSphere(const double R2, const double theta1, const double phi1, const double dtheta, const double dphi, const double I, const double t1, const double t2, const std::vector<double> P, const double epsabs, std::vector<double> &B, std::vector<double> &Berr, size_t &neval);

/// Parse yaml node and create LoopSphere object if data has definition for it.
LoopSphere * parseLoopSphere(const YAML::Node coil, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, double &I, bool verbose);

/// Test yaml node for LoopSphere data.
bool isLoopSphereYaml(const YAML::Node coil);

/// Parse yaml node and return true in success.
bool parseYaml(const YAML::Node coil, const bool verbose, double &dt, double &t1, double &t2, double &dr, double &r1, double &r2, std::string &refdes, double &I, double &d, double &theta, std::vector<double> &axis, std::vector<double> &tr);

/// Parse yaml node for multi-turn and multilayer coil with sections.
bool parseCoilYaml(const YAML::Node coil, const bool verbose, double &lc, std::vector<double> &t1v, std::vector<double> &t2v, double &dr, double &r1, double &r2, std::string &refdes, double &I, double &d, double &theta, std::vector<double> &axis, std::vector<double> &tr);

/// Test yaml node for current distribution data.
bool isCurrentYaml(const YAML::Node current);

/// Parse yaml node for current distribution and return true in success.
bool parseCurrent(const YAML::Node current, const bool verbose, double &I, double &theta, std::vector<double> &axis, std::vector<double> &tr, std::string &file);

/// Test geometry calculation functions.
void testGeometry(const bool verbose);

/// Test Monte Carlo functions.
void testMonteCarlo(const int test, const bool verbose);

/// Test parametric curve functions.
void testParametricCurves(const int test, const bool verbose);

/// Test quaternion calculation functions.
void testQuaternion(const bool verbose);

/// Test vector calculation functions.
void testVector(const bool verbose);

/// Test creation of Helix object from yaml file.
void testYamlHelix(const bool verbose);

/// Test creation of Ring object from yaml file.
void testYamlRing(const bool verbose);

/// Test creation of RingCylinder object from yaml file.
void testYamlRingCylinder(const bool verbose);

/// Test creation of RacetrackCylinder object from yaml node.
void testYamlRacetrackCylinder(const bool verbose);

/// Test creation of RacetrackPlanar object from yaml node.
void testYamlRacetrackPlanar(const bool verbose);

/// Test creation of Solenoid object from yaml node.
void testYamlSolenoid(const bool verbose);

/// Test creation of Line object from yaml node.
void testYamlLine(const bool verbose);

/// Test creating LineSphere object from yaml file.
void testYamlLineSphere(const bool verbose);

/// Test creating LoopSphere object from yaml file.
void testYamlLoopSphere(const bool verbose);

/// Test histogram ranges from yaml node.
void testYamlHistogramRanges(const bool verbose);

}

