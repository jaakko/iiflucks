# IIFlucks Sample Wire Geometries

The position coordinate (_x_, _y_, _z_) and current density vector 
(_Jx_, _Jy_, _Jz_) file with element volume _dv_ was generated with one of the
commands below.

The center coordinate of each volume element is plotted in Octave. 
The script __plotwire.m__ can be used for this.

```shell
A=load("helmholzwire-sample.pdv")

x = A(:,1)

y = A(:,2)

z = A(:,3)

plot3(x, y, z)
```  

The current density vector of each volume element is plotted in Octave.
The script __plotj.m__ can be used for this.

```shell
jx = A(:,4)

jy = A(:,5)

jz = A(:,6)

quiver3(x, y, z, jx, jy, jz)
```  

1 cm long 100 um diameter wire carrying 1 mA is divided into 10 volume 
elements. No rotation or translation. 

```shell
straightwire 1e-2 1e-4 10 1e-3 0 0 1 0 0 0 0 > straightwire-sample.pdv
```

![Straight Wire](straightwire-sample.svg)

2 cm diameter ring from 0 to 6 rad with wire diameter 100 um and current 1 mA.
20 volume elements.

```shell
ringwire 1e-2 0 6 1e-4 20 1e-3 0 0 1 0 0 0 0 > ringwire-sample.pdv
```

![Ring Wire](ringwire-sample.svg)
![Ring Wire](ringwire-sample-j.svg)

0 to pi arc with radius 1 cm on cylinder with diameter 8 cm. Wire diameter 
100 um carrying 1 mA is divided into 20 volume elements.

```shell
ringcylinderwire 1e-2 4e-2 0 3.141592 1e-4 20 1e-3 0 0 1 0 0 0 0 > ringcylinderwire-sample.pdv
```

![Ring Cylinder Wire](ringcylinderwire-sample.svg)
![Ring Cylinder Wire](ringcylinderwire-sample-j.svg)

Planar gradiometer from 100 um wire wound on 2 cm diameter bobbins.
Distance from one bobbin central axis to other is 2 cm. Current on wire
1 mA. 100 volume elements.
 
```shell
gradient8wire 1e-2 2e-2 1e-3 1e-4 2 100 1e-3 > gradient8wire-sample.pdv
```

![Gradient 8 Wire](gradient8wire-sample.svg)
![Gradient 8 Wire](gradient8wire-sample-j.svg)

Top and bottom ring diameter 2 cm and distance between rings 1 cm. Two
vertical wires separated by 1 mm. Wire diameter 100 um and current 1 mA.
200 volume elements.

```shell
axialgradiometer 1e-2 1e-2 1e-3 1e-4 200 1e-3 0 0 1 0 0 0 0 > axialgradiometer-sample.pdv
```

![Axial Gradiometer Wire](axialgradiometer-sample.svg)
![Axial Gradiometer Wire](axialgradiometer-sample-j.svg)

1 cm diameter and 1 cm long helix from 100 um wire with 4 turns. Current
1 mA. 100 volume elements.

```shell
helixwire 5e-3 1e-2 1e-4 4 100 1e-3 0 0 1 0 0 0 0 > helix-sample.pdv
```

![Helix Wire](helix-sample.svg)
![Helix Wire](helix-sample-j.svg)

Rectangular Helmholtz coil 1 cm height and 1 cm width. The two coils 
separated by 2 cm distance. Corner radius 1 mm and coil length 2 mm.
Wire diameter 100 um carrying 1 mA current. 200 volume elements.
 
```shell
helmholtzwire 1e-2 1e-2 2e-2 1e-3 2e-3 1e-4 2 200 1e-3 > helmholzwire-sample.pdv
```

![Helmholtz Wire](helmholtzwire-sample.svg)
![Helmholtz Wire](helmholtzwire-sample-j.svg)

Racetrack coil with corner radius 1 cm on 8 cm radius cylinder. Length of 
straight section 4 cm, width of winding 1 mm and 2 turns on 2 layers 
separated by 1 mm. Wire diameter 100 um carrying 1 mA current. 500 volume 
elements.

```shell
racetrackcylinder 0.01 0.08 0.04 1e-3 2 1e-3 2 1e-4 500 1e-3 0 0 1 0 0 0 0 > racetrackcylinder-sample.pdv
```

![Racetrack Cylinder](racetrackcylinder-sample.svg)
![Racetrack Cylinder](racetrackcylinder-sample-j.svg)

Racetrack coil with radius 1 cm, length of straight section 4 cm with 5 mm
at end, width of winding 1 mm and 4 turns on 2 layers separated by 1 mm.  
Wire diameter 100 um carrying 1 mA current. 400 volume elements.

```shell
racetrackplanar 1e-2 4e-2 5e-3 1e-3 4 1e-3 2 1e-4 400 1e-3 0 0 1 0 0 0 0 > racetrackplanar-sample.pdv
```

![Racetrack Wire](racetrackplanar-sample.svg)
![Racetrack Wire](racetrackplanar-sample-j.svg)


