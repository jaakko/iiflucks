# IIFlucks Wires

Straight Wire
=============

With $`t = [0, L]`$ position coordinates are

```math
\begin{align*}
x &=& 0 \\
y &=& 0 \\
z &=& t.
\end{align*}
```


Tangential vector

```math
\begin{align*}
\lambda_x &=& 0 \\
\lambda_y &=& 0 \\
\lambda_z &=& 1. 
\end{align*}
```

Wire length with _L_ = 9.5 cm is

```math
\int_0^{L} dt = 0.095\ {\rm m}.
```

Volume element

```math
dv = dt \cdot A.
```

Current density

```math
J = \frac{I}{\pi (d/2)^2}. 
```

Current density vector

```math
\begin{align*}
J_x &=& 0 \\
J_y &=& 0 \\
J_z &=& J. 
\end{align*}
```

Ring Wire
=========

With $`t = [0, 2 \pi]`$ position coordinates are

```math
\begin{align*}
x &=& R \cos(t) \\
y &=& R \sin(t) \\
z &=& 0.
\end{align*}
```

Tangential vector

```math
\begin{align*}
\lambda_x &=& -\sin(t) \\
\lambda_y &=& \cos(t) \\
\lambda_z &=& 0. 
\end{align*}
```

Wire length with _R_ = 1 cm is

```math
\int_0^{2 \pi} R \cdot dt = 0.06283185\ {\rm m}.
```

Volume element

```math
dv = dt \cdot R \cdot A.
```

Current density

```math
J = \frac{I}{\pi (d/2)^2}.
```

Current density vector

```math
\begin{align*}
\lambda_x &=& -J \cdot \sin(t) \\
\lambda_y &=& J \cdot \cos(t) \\
\lambda_z &=& 0.
\end{align*}
```

Ring on Cylinder Wire
=====================

With $`t = [0, 2 \pi]`$ position coordinates are


```math
\begin{align*}
x &=& \sqrt{{R_2}^2 - R^2 \cos^2(t)} \\
y &=& R \cos(t) \\
z &=& R \sin(t).
\end{align*}
```

Tangential vector

```math
\begin{align*}
\lambda_x &=& \frac{\frac{R^2 \cos(t) \sin(t)}{\sqrt{{R_2}^2 - R^2 \cos^2(t)}}}{\sqrt{R^2 + \frac{R^4 \cos^2(t) \sin^2(t)}{{R_2}^2 - R^2 \cos^2(t)}}} \\
\lambda_y &=& \frac{- R \sin(t)}{\sqrt{R^2 + \frac{R^4 \cos^2(t) \sin^2(t)}{{R_2}^2 - R^2 \cos^2(t)}}} \\
\lambda_z &=& \frac{R \cos(t)}{\sqrt{R^2 + \frac{R^4 \cos^2(t) \sin^2(t)}{{R_2}^2 - R^2 \cos^2(t)}}}.
\end{align*}
```

Wire length with _R_ = 1 cm, _R2_ = 4 cm is

```math
\int_0^{2 \pi} \sqrt{R^2 + \frac{R^4 \sin^2(t) \cos^2(t)}{{R_2}^2 - R^2 \cos^2(t)}} dt = 0.0630845\ {\rm m}.
```

Volume element

```math
dv = dt \cdot A \cdot \sqrt{R^2 + \frac{R^4 \sin^2(t) \cos^2(t)}{{R_2}^2 - R^2 \cos^2(t)}}.
```

Current density

```math
J = \frac{I}{\pi (d/2)^2}. 
```

Current density vector

```math
\begin{align*}
J_x &=& \lambda_x \cdot J \\
J_y &=& \lambda_y \cdot J \\
J_z &=& \lambda_z \cdot J. 
\end{align*}
```

Planar Gradiometer
==================


Axial Gradiometer
=================

The coil is made from top and bottom rings and two vertical straight wires.

With _R_ = 1 cm, _b_ = 5 mm and _w_ = 1 mm wire length is x m.

Helix Wire
==========

With _t_ in rad position coordinates are

```math
\begin{align*}
x &=& R \cos(t) \\
y &=& R \sin(t) \\
z &=& \frac{t}{2 \pi} \frac{L}{N}.
\end{align*}
```

Tangential vector

```math
\begin{align*}
\lambda_x &=& \frac{-R \sin(t)}{\sqrt{R^2 + \frac{L^2}{ (2 \pi N)^2 }}} \\
\lambda_y &=& \frac{R \cos(t)}{\sqrt{R^2 + \frac{L^2}{ (2 \pi N)^2 }}} \\
\lambda_z &=& \frac{\frac{L}{2 \pi N}}{\sqrt{R^2 + \frac{L^2}{ (2 \pi N)^2 }}}.
\end{align*}
```

Wire length with _R_ = 1 cm, _L_ = 2 cm and _N_ = 10 is

```math
\int_0^{2 \pi N} \sqrt{R^2 + \frac{L^2}{(2 \pi N)^2}} dt =\  \textrm{0.628637 m}.
```

Volume element

```math
dv = dt \cdot A \cdot \sqrt{R^2 + \frac{L^2}{(2 \pi N)^2}}.
```

Current density

```math
J = \frac{I}{\pi (d/2)^2}. 
```

Current density vector

```math
\begin{align*}
J_x &=& \lambda_x \cdot J \\
J_y &=& \lambda_y \cdot J \\
J_z &=& \lambda_z \cdot J. 
\end{align*}
```

Line on Sphere Wire
===================

With _t_ in rad position coordinates are

```math
\begin{align*}
x &=& \frac{ R_2^2 }{ \sqrt{R_1^2 + R_2^2} } \cos(t) \\
y &=& \frac{ R_1 \cdot R_2 }{ \sqrt{R_1^2 + R_2^2} } \cos(t) \\
z &=& R_2 \sin(t). \\
\end{align*}
```

Tangential vector

```math
\begin{align*}
\lambda_x &=& -\frac{ R_2 }{ \sqrt{R_1^2 + R_2^2 } } \sin(t) \\
\lambda_y &=& -\frac{ R_1 }{ \sqrt{R_1^2 + R_2^2 } } \sin(t) \\
\lambda_z &=& \cos(t). \\
\end{align*}
```

Wire length with _R1_ = 1 cm, _R2_ = 2 cm and $`t = [-\pi/4, +\pi/4]`$ is

```math
\int_{-\pi/4}^{+\pi/4} R_2 \cdot dt = 0.0314159  \ {\rm m}.
```

Volume element

```math
dv = dt \cdot R_2 \cdot A.
```

Current density

```math
J = \frac{I}{\pi (d/2)^2}.
```

Current density vector

```math
\begin{align*}
J_x &=& \lambda_x \cdot J \\
J_y &=& \lambda_y \cdot J \\
J_z &=& \lambda_z \cdot J.
\end{align*}
```

Loop on Sphere Wire
===================

With $`t = [-1, +1]`$ position coordinates are

```math
\begin{align*}
x &=& R_2 \cos( \theta(t) ) \cos( \phi(t) )\\
y &=& R_2 \cos( \theta(t) ) \sin( \phi(t) )\\
z &=& R_2 \sin( \theta(t) ).\\
\end{align*}
```

Here $`\theta(t) = \theta_1 + ( 1 - t^4 ) \cdot \Delta\theta`$ and
$`\phi(t) = \phi_1 + ( t + 1 ) \cdot \Delta\phi / 2`$.

Tangential vector

```math
\begin{align*}
ds \cdot \lambda_x &=& -1/2 \Delta\phi R_2 \cos( \theta(t) ) \sin( \phi(t) ) + 4 \Delta\theta \cdot R_2 \cdot t^3 \cos( \phi(t) ) \sin( \theta(t) ) \\
ds \cdot \lambda_y &=& 1/2 \Delta\phi R_2 \cos( \theta(t) ) \cos( \phi(t) ) + 4 \Delta\theta \cdot R_2 \cdot t^3 \sin( \phi(t) ) \sin( \theta(t) ) \\
ds \cdot \lambda_z &=& -4 \Delta\theta \cdot R_2 \cdot t^3 \cdot \cos( \theta(t) ).\\
\end{align*}
```

Wire length $`t = [-1, +1]`$ is

```math
\int_{-1}^{+1} dt \cdot \sqrt{1/4 (\Delta\phi)^2 \cdot R_2^2 \cos^2( \theta(t) ) +16 (\Delta\theta)^2 \cdot R_2^2 \cdot t^6 }.
```

Volume element

```math
dv = dt \cdot A \cdot \sqrt{1/4 (\Delta\phi)^2 \cdot R_2^2 \cos^2( \theta(t) ) +16 (\Delta\theta)^2 \cdot R_2^2 \cdot t^6 }.
```

Current density

```math
J = \frac{I}{\pi (d/2)^2}.
```

Current density vector

```math
\begin{align*}
J_x &=& \lambda_x \cdot J \\
J_y &=& \lambda_y \cdot J \\
J_z &=& \lambda_z \cdot J.
\end{align*}
```

Helmholtz Wire
==============

The two coils are made from straight wires and arcs (ring wire).

Length of wire in one turn is

```math
2 \cdot ( w + h) + (3 \pi / 2 - 8) \cdot R + \pi / 2 \sqrt{R^2 + L^2/( 2 \pi N )^2}.
```

With _h_ = 3 m, _w_ = 1.333 m, _b_ = 1.867 m, _R_ = 0.1 m, _L_ = 3.8 cm,
_d_ = 2mm and _N_ = 2 this gives 33.9776 m for length and 0.000106744 m$`^3`$ 
for wire volume.

Calculations
============

Mutual inductance from line integral is

```math
M = \frac{\mu_0}{4 \pi} \int \int \frac{ d\vec{l}_1 \cdot d\vec{l}_2 }{| \vec{r}_2 - \vec{r}_1 |}.
```

Force between two circuits is

```math
F = \frac{\mu_0}{4 \pi} \int \int ( d\vec{l}_1 \cdot d\vec{l}_2 )\frac{(\vec{r}_2 - \vec{r}_1)}{| \vec{r}_2 - \vec{r}_1 |^3}.
```

Magnetic field at position $`\vec{r}_2`$ is

```math
\vec{B} = \frac{\mu_0}{4 \pi} I_1 \int \frac{ d\vec{l}_1 \times (\vec{r}_2 - \vec{r}_1)}{| \vec{r}_2 - \vec{r}_1 |^3}.
```

![Coaxial Rings](../pictures/CoaxialRings.svg)

Mutual inductance calculation for two coaxial rings with diameter 
$`2 R`$ = 2 cm, distace $`h`$ = 1 cm and current in each loop $`I`$ = 1 mA.
See F. W. Grover, Bulletin of the Bureau of Standards, Vol. 6 No. 4 p. 489 (1910).

```math
M = 10^{-7} \cdot 2 \pi R^2 \int_0^{2 \pi} \frac{\cos( \phi ) d \phi}{\sqrt{h^2 + 2 R^2 - 2 R^2 \cos( \phi ) }} =\  4.94078\ \textrm{nH}.
```

Force between the rings is

```math
F = 10^{-7} \cdot I_1 \cdot I_2 \cdot R^2 \int_0^{2 \pi} \int_0^{2 \pi} \frac{\cos( \phi_2 - \phi_1 ) d\phi_1 d\phi_2}{r_{12}^3} \big( R (\cos(\phi_2) - \cos(\phi_1)) \vec{i} + R (\sin(\phi_2) - \sin(\phi_1)) \vec{j} + h \vec{k} \big) =\ 7.18366 \cdot 10^{-13}\vec{k}\ \textrm{N}.
```

Field on z-axis for ring at $`z`$ = 0 is

```math
\vec{B}(z) = \frac{\mu_0 I}{2} \frac{R^2}{(z^2+R^2)^{3/2}} \vec{k}.
```

| z[cm]    | B[T]        |
| :---     | :---        |
| -1       | 2.22144E-08 |
|  0       | 6.28319E-08 |
| +1       | 2.22144E-08 |

Mutual inductance between two parallel 10 cm long wires separated by 1 cm
distance. Current in each wire $`I`$ = 1 mA.

```math
M = 10^{-7} \cdot \int_0^{0.1} \int_0^{0.1} \frac{dz_1 dz_2}{\sqrt{0.01^2 + (z_2 - z_1)^2} } =\  41.8647\ \textrm{nH}.
```

Force between the wires is

```math
F = 10^{-7} \cdot I_1 \cdot I_2 \cdot 0.01 \int_0^{0.1} \int_0^{0.1} \frac{dz_1 dz_2}{(0.01^2 + (z_2 - z_1)^2)^{3/2} }\ \vec{j} =\ 1.80998 \cdot 10^{-12} \vec{j}\ \textrm{N}.
```

